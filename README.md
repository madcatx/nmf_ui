NMF
===

NMF is a graphical frontend for the [libNMF](https://gitlab.com/madcatx/nmf) 1D capillary electrophoresis simulator. NMF is derived from [PeakMaster NG](https://github.com/echmet/PeakMasterNG) and reuses many of its user interface components. NMF makes it easy to set up systems for simulation by libNMF and observe progress of the simulation in real time.

Building
---
The following tools and libraries are required to build NMF

- C++17-aware compiler (with the exception of MSVC, see below)
- [libNMF](https://gitlab.com/madcatx/nmf) 
- [Qt 5 toolkit](https://www.qt.io/) (Qt 5.6 or higher is required)
- [Qwt toolkit](http://qwt.sourceforge.net/)
- [SQLite 3](https://www.sqlite.org/index.html)
- [ECHMETCoreLibs](https://github.com/echmet/ECHMETCoreLibs)

Refer to official documentation of the respective projects for details how to set them up.

### Linux/UNIX
In the terminal `cd` to the directory with NMF source code and issue the following commands

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release -DECHMET_CORE_LIBS_DIR=<path_to_your_ECHMETCoreLibs_installation> -DSQLITE3_DIR=<path_to_your_SQLite3_installation> -DQWTPLOT_DIR=<path_to_your_qwtplot_installation> -DLIBNMF_DIR=<path_to_your_libNMF_installation>
    make
    make install

Note that paths to ECHMETCoreLibs, SQLite3, qwtplot and libNMF must be specified explicitly only if you do not have a system-wide installation of the respective libraries.

### Windows
On Windows, both [MinGW](https://sourceforge.net/projects/mingw-w64/) and Microsoft Visual C++ Compiler (MSVC) can be used to build libNMF. Use CMake to generate project files for your compiler of choice. Make sure that `ECHMET_CORE_LIBS_DIR`, `SQLITE3_DIR`, `QWTPLOT_DIR` and `LIBNMF_DIR` variables are set to point to paths with your installations of the respective libraries. Additionaly, you may need to set `CMAKE_PREFIX_PATH` variable to point to your Qt5 installation.

### MSVC and C++17 support.
NMF can be built with MSVC 14 (part of Microsoft Visual Studio 2015) or greater even though MSVC 14 does not fully support the C++17 standard. If a compiler other than MSVC is used to build NMF, full support of the C++17 standard is required.

Licensing
---
NMF project is distributed under the terms of **The GNU General Public License v3** (GNU GPLv3). See the enclosed `LICENSE` file for details.

