#include "types.h"

namespace recording {

Error::Error(const char *what, const ErrorType _type) :
  std::runtime_error{what},
  type{_type}
{
}

FileInfo::FileInfo(const uint_fast64_t creationTime, const uint_fast64_t numFrames, std::string description) noexcept :
  creationTime{creationTime},
  numFrames{numFrames},
  description{std::move(description)}
{
}

FileInfo::FileInfo(FileInfo &&other) noexcept :
  creationTime{other.creationTime},
  numFrames{other.numFrames},
  description{std::move(other.description)}
{
}

auto FileInfo::operator=(FileInfo &&other) noexcept -> FileInfo &
{
  const_cast<uint_fast64_t&>(creationTime) = other.creationTime;
  const_cast<uint_fast64_t&>(numFrames) = other.numFrames;
  const_cast<std::string&>(description) = std::move(const_cast<std::string&>(other.description));

  return *this;
}

} // namespace recording

