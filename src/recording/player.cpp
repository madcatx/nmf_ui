#include "player.h"

#include <recording/blocks.h>
#include <recording/datafile.h>
#include <util/util.h>

/* Sorry :( */
#include <boost/fusion/algorithm/iteration.hpp>
#include <boost/fusion/include/iteration.hpp>

namespace recording {

extern template DataFile<Mode::READ>::DataFile(const std::string &);
extern template auto DataFile<Mode::READ>::readAt(const OffsetAbs &, const std::size_t, std::string &) -> void;
extern template auto DataFile<Mode::READ>::readBlockAt(const OffsetAbs &off, const std::size_t) -> std::vector<uint8_t>;
extern template auto DataFile<Mode::READ>::size() const -> OffsetAbs;

class PlayerInternal {
public:
  PlayerInternal() :
    sysDescOffset{MKOF(0)},
    framesOffset{MKOF(0)},
    framesDictOffset{MKOF(0)}
  {}

  OffsetAbs sysDescOffset;
  OffsetAbs framesOffset;
  OffsetAbs framesDictOffset;
  std::vector<FrameDictEntry> framesDict;
  std::vector<size_t> fullFrameRefs;
};

inline
auto checkFrameHeaderSanity(const FrameHeader &hdr)
{
  if (hdr.t < 0)
    throw Error{"Invalid time", ErrorType::BAD_FILE};
  if (hdr.dt < 0)
    throw Error{"Invalid dt", ErrorType::BAD_FILE};
  if (hdr.t > hdr.tStop)
    throw Error{"Invalid stop time", ErrorType::BAD_FILE};
  if (hdr.tolerance < 1)
    throw Error{"Invalid tolerance", ErrorType::BAD_FILE};
  if (hdr.firstCell >= hdr.lastCell)
    throw Error{"Invalid cells", ErrorType::BAD_FILE};

  if (!(OFV(hdr.conductivityArray) < OFV(hdr.pHArray) && hdr.OFV(pHArray) < OFV(hdr.concentrationsArray)))
    throw Error{"Invalid frame arrays offsets", ErrorType::BAD_FILE};
}

inline
auto makeFullProfile(const std::vector<double> &full, const std::vector<double> &partial, const size_t offset)
{
  assert(full.size() >= partial.size());
  assert(partial.size() + offset <= full.size());

  // TODO: This is horrendously slow and pathetic
  std::vector<double> v = full;
  for (size_t idx{0}; idx < partial.size(); idx++)
    v[idx + offset] = partial[idx];

  return v;
}

inline
auto parseNamesArray(const std::vector<uint8_t> &bytes)
{
  std::vector<std::string> names{};

  std::string s{};
  for (const auto b : bytes) {
    if (b == 0) {
      names.emplace_back(std::move(s));
      s = std::string{};
    } else
      s += static_cast<char>(b);
  }

  return names;
}

inline
auto readDetectors(DataFile<Mode::READ> *fh, const FrameHeader &hdr, const OffsetAbs &base, const size_t numConstituents, const size_t numActiveDetectors)
{
  std::vector<std::vector<std::pair<double, std::vector<double>>>> detectorTraces{};

  if (numActiveDetectors < 1)
    return detectorTraces;

  if (OFV(hdr.detectorTraces) == 0)
    throw Error{"Null pointer to detectorTraces block with active detectors", ErrorType::BAD_FILE};

  OffsetRel off{hdr.detectorTraces};
  for (size_t idx{0}; idx < numActiveDetectors; idx++) {
    std::vector<std::pair<double, std::vector<double>>> trace{};

    for (size_t jdx{0}; jdx < hdr.numDetectorSamples; jdx++) {
      double time{};
      std::vector<double> concentrations{};

      fh->readAt(off.toAbs(base), time);
      OFV(off.value) += sizeof(double);

      fh->readAt(off.toAbs(base), numConstituents, concentrations);
      OFV(off.value) += sizeof(double) * numConstituents;

      trace.emplace_back(time, std::move(concentrations));
    }

    detectorTraces.emplace_back(std::move(trace));
  }

  return detectorTraces;
}


template <typename T>
inline
auto readStruct(const std::vector<uint8_t> &bytes, const std::size_t offset, T &out)
{
  std::size_t pos{offset};

  boost::fusion::for_each(out, [&bytes, &pos](auto &t) {
    using V = std::decay_t<decltype(t)>;
    if (pos >= bytes.size())
      throw Error{"Cannot read frame", ErrorType::BAD_FILE};

    std::memcpy(&t, &bytes[pos], sizeof(V));
    pos += sizeof(V);
  });
}

Player::Player(const std::string &path) :
  m_fh{nullptr},
  m_internal{new PlayerInternal{}},
  m_creationTime{0},
  m_numFrames{0},
  m_haveLastFrame{false},
  m_numCells{0},
  m_capillaryLength{0},
  m_constituentNames{},
  m_dx{0},
  m_numConstituents{0},
  m_numActiveDetectors{0}
{
  m_fh = new DataFile<Mode::READ>{path};

  readHeader();
  readFramesDictionary();
}

Player::~Player()
{
  delete m_fh;
  delete m_internal;
}

auto Player::applyDescription(const size_t numCells, const double capillaryLength, const size_t numConstituents, const size_t numActiveDetectors) noexcept -> void
{
  /* This can be called only once when the system description is processed */
  assert(m_numCells == 0); assert(m_dx == 0); assert(m_numConstituents == 0); assert(m_numActiveDetectors == 0);

  m_numCells = numCells;
  m_capillaryLength = capillaryLength;
  m_dx = util::dx<double>(capillaryLength, numCells);
  m_numConstituents = numConstituents;
  m_numActiveDetectors = numActiveDetectors;
}

auto Player::lastReadFrame() const -> const RecordedFrame &
{
  if (!m_haveLastFrame)
    throw Error{"No frames have been read so far", ErrorType::INVALID_ARGUMENT};

  return m_lastReadFrame;
}

auto Player::numFrames() -> std::size_t
{
  return m_numFrames;
}

auto Player::readFramesDictionary() -> void
{
  OffsetAbs off{m_internal->framesDictOffset};
  size_t currentFullFrame = 0;
  OffsetRel rel{MKOF(0)};
  for (size_t idx{0}; idx < m_numFrames; idx++) {
    auto bytes = m_fh->readBlockAt(rel.toAbs(off), FRAME_DICT_ENTRY_SIZE);

    FrameDictEntry e{};
    readStruct(bytes, 0, e);
    if (e.isFullFrame)
      currentFullFrame = idx;
    m_internal->framesDict.emplace_back(e);
    m_internal->fullFrameRefs.emplace_back(currentFullFrame);
    OFV(rel.value) += FRAME_DICT_ENTRY_SIZE;
  }

  if (m_numFrames != m_internal->framesDict.size())
    throw Error{"Mismathing frames dictionary data", ErrorType::BAD_FILE};

  if (m_numFrames > 0) {
    auto prev = m_internal->framesDict[0].offset;
    if (OFV(prev) != OFV(m_internal->framesOffset.value))
      throw Error{"Invalid frames dictionary data", ErrorType::BAD_FILE};

    for (auto idx{1}; idx < m_internal->framesDict.size(); idx++) {
      auto curr = m_internal->framesDict[idx].offset;
      if (OFV(prev) >= OFV(curr))
        throw Error{"Invalid frames dictionary data", ErrorType::BAD_FILE};
      prev = curr;
    }
  }
}

auto Player::readHeader() -> void
{
  FilePtr sysDescOff{};
  FilePtr framesOff{};
  FilePtr framesDictOff{};

  m_fh->readAt(GLOB_HDR_SYSTEM_DESCRIPTION_OFFSET, sysDescOff);
  m_fh->readAt(GLOB_HDR_FRAMES_OFFSET, framesOff);
  m_fh->readAt(GLOB_HDR_FRAMES_DICTIONARY_OFFSET, framesDictOff);
  m_fh->readAt(GLOB_HDR_CREATION_TIME, m_creationTime);
  m_fh->readAt(GLOB_HDR_NUM_FRAMES, m_numFrames);

  m_internal->sysDescOffset = OffsetAbs{sysDescOff};
  m_internal->framesOffset = OffsetAbs{framesOff};
  m_internal->framesDictOffset = OffsetAbs{framesDictOff};
}

auto Player::readDescription() -> std::string
{
  uint64_t len{};
  std::string json{};

  m_fh->readAt(SYS_DESC_LENGTH.toAbs(m_internal->sysDescOffset), len);
  m_fh->readAt(SYS_DESC_DATA.toAbs(m_internal->sysDescOffset), len, json);

  return json;
}

auto Player::readProfileSlice(const size_t idx, const calculators::ProfileType type, const std::string &name, const double x) -> calculators::ProfileSlice
{
  const auto frame = readFrame(idx);

  auto vdx = size_t(x / 1000.0 / m_capillaryLength * m_numCells);
  if (vdx >= m_numCells)
    vdx = m_numCells - 1;

  calculators::ProfileSlice slice{};

  slice.name = name;
  slice.x = x;
  slice.cell = vdx;
  slice.type = type;
  slice.effectiveMobility = 0.0;

  switch (type) {
  case calculators::ProfileType::CONDUCTIVITY:
    slice.value = frame.state.conductivity[vdx];
    break;
  case calculators::ProfileType::PH:
    slice.value = frame.state.pH[vdx];
    break;
  case calculators::ProfileType::CONCENTRATION:
    for (size_t cdx{0}; cdx < m_constituentNames.size(); cdx++) {
      if (name == m_constituentNames[cdx]) {
        slice.value = frame.state.concentrations[cdx][vdx];
        break;
      }
    }
    break;
  }

  return slice;
}

auto Player::readFrame(const size_t idx) -> RecordedFrame
{
  assert(m_numCells > 0);

  const auto thisFullFrame = m_internal->fullFrameRefs.at(idx);
  if (m_currentFullFrame != thisFullFrame && !m_internal->framesDict.at(idx).isFullFrame)
    readFrame(thisFullFrame);

  if (idx >= m_numFrames)
    throw Error{"No such frame", ErrorType::OUT_OF_RANGE};

  RecordedFrame rec{};

  const auto pos = m_internal->framesDict.at(idx).offset;
  const auto len = idx < m_numFrames - 1 ?
                     OFV(m_internal->framesDict.at(idx + 1).offset) - OFV(pos) :
                     OFV(m_fh->size().value) - OFV(pos);
  OffsetAbs base{pos};

  auto bytes = m_fh->readBlockAt(base, len);

  Frame frm{};
  readStruct(bytes, 0, frm);

  if (OFV(frm.headerOffset) < 1)
    throw Error{"Invalid frame header offset", ErrorType::BAD_FILE};

  FrameHeader hdr{};
  readStruct(bytes, OFV(frm.headerOffset), hdr);
  checkFrameHeaderSanity(hdr);

  rec.state.t = hdr.t;
  rec.state.dt = hdr.dt;
  rec.state.dx = m_dx;
  rec.state.current = hdr.current;
  rec.state.voltage = hdr.voltage;
  rec.state.lastCell = hdr.lastCell;
  rec.state.firstCell = hdr.firstCell;
  rec.state.constantForce = static_cast<calculators::ConstantForce>(hdr.constantForce);
  rec.state.avgTimePerIter = 0;
  rec.tStop = hdr.tStop;
  rec.tolerance = hdr.tolerance;
  rec.autoDt = hdr.autoDt == 1;

  // TODO: Read arrays directly from the prefetched block!!!
  const size_t profSize = frm.isFullFrame ? m_numCells : hdr.lastCell - hdr.firstCell + 1;
  if (frm.isFullFrame) {
    OffsetRel rel{MKOF(OFV(hdr.conductivityArray) + OFV(frm.headerOffset))};
    m_fh->readAt(rel.toAbs(base), profSize, rec.state.conductivity);

    OFV(rel.value) = OFV(hdr.pHArray) + OFV(frm.headerOffset);
    m_fh->readAt(rel.toAbs(base), profSize, rec.state.pH);

    OFV(rel.value) = OFV(hdr.concentrationsArray) + OFV(frm.headerOffset);
     for (size_t vdx{0}; vdx < m_numConstituents; vdx++) {
      std::vector<double> v{};
      m_fh->readAt(rel.toAbs(base), profSize, v);
      rec.state.concentrations.emplace_back(std::move(v));
      OFV(rel.value) += profSize * sizeof(double);
    }

    OFV(rel.value) = OFV(hdr.constituentNames) + OFV(frm.headerOffset);
    std::vector<uint8_t> namesArray{};
    m_fh->readAt(rel.toAbs(base), hdr.bytesConstituentNames, namesArray);
    auto constituentNames = parseNamesArray(namesArray);

    if (constituentNames.size() != m_numConstituents)
      throw Error{"Mismatching number of constituent names", ErrorType::BAD_FILE};

    m_conductivityFull = rec.state.conductivity;
    m_pHFull = rec.state.pH;
    m_concentrationsFull = rec.state.concentrations;
    m_currentFullFrame = idx;
    m_constituentNames = std::move(constituentNames);
    rec.state.constituentNames = m_constituentNames;

    OffsetAbs hdrBase{MKOF(OFV(base.value) + OFV(frm.headerOffset))};
    m_detectorTraces = readDetectors(m_fh, hdr, hdrBase, m_numConstituents, m_numActiveDetectors);
  } else {
    std::vector<double> conductivityPartial{};
    OffsetRel rel{MKOF(OFV(hdr.conductivityArray) + OFV(frm.headerOffset))};
    m_fh->readAt(rel.toAbs(base), profSize, conductivityPartial);

    std::vector<double> pHPartial{};
    OFV(rel.value) = OFV(hdr.pHArray) + OFV(frm.headerOffset);
    m_fh->readAt(rel.toAbs(base), profSize, pHPartial);

    std::vector<std::vector<double>> concsPartial{};
    OFV(rel.value) = OFV(hdr.concentrationsArray) + OFV(frm.headerOffset);
    for (size_t vdx{0}; vdx < m_numConstituents; vdx++) {
      std::vector<double> v{};
      m_fh->readAt(rel.toAbs(base), profSize, v);
      concsPartial.emplace_back(std::move(v));
      OFV(rel.value) += profSize * sizeof(double);
    }

    rec.state.conductivity = makeFullProfile(m_conductivityFull, conductivityPartial, hdr.firstCell);
    rec.state.pH = makeFullProfile(m_pHFull, pHPartial, hdr.firstCell);
    for (size_t vdx{0}; vdx < m_numConstituents; vdx++) {
      auto v = makeFullProfile(m_concentrationsFull[vdx], concsPartial[vdx], hdr.firstCell);
      rec.state.concentrations.emplace_back(std::move(v));
    }
    rec.state.constituentNames = m_constituentNames;

    OffsetAbs hdrBase{MKOF(OFV(base.value) + OFV(frm.headerOffset))};
    m_detectorTraces = readDetectors(m_fh, hdr, hdrBase, m_numConstituents, m_numActiveDetectors);
  }

  rec.state.detectorTraces = &m_detectorTraces;

  m_haveLastFrame = true;
  m_lastReadFrame = rec;

  return rec;
}

auto Player::readInitial() -> FileInfo
{
  auto json = readDescription();

  return FileInfo{m_creationTime, m_numFrames, std::move(json)};
}

} // namespace recording
