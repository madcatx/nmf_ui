#ifndef RECORDING_PLAYER_H
#define RECORDING_PLAYER_H

#include <recording/common.h>
#include <recording/types.h>

#include <string>
#include <utility>
#include <vector>

namespace recording {

template <Mode>
class DataFile;

struct FrameHeader;
class PlayerInternal;

class Player {
public:
  explicit Player(const std::string &path);
  Player(const Player &) = delete;
  ~Player();

  Player & operator=(const Player &) = delete;

  auto applyDescription(const size_t numCells, const double capillaryLength, const size_t numConstituents, const size_t numActiveDetectors) noexcept -> void;
  auto lastReadFrame() const -> const RecordedFrame &;
  auto numFrames() -> size_t;
  auto readFrame(const size_t idx) -> RecordedFrame;
  auto readProfileSlice(const size_t idx, const calculators::ProfileType type, const std::string &name, const double x) -> calculators::ProfileSlice;
  auto readInitial() -> FileInfo;

private:
  auto readDescription() -> std::string;
  auto readFramesDictionary() -> void;
  auto readHeader() -> void;

  DataFile<Mode::READ> *m_fh;
  /* Sort of a workaroud to avoid propagation of platform-specific bits outside */
  PlayerInternal *m_internal;

  uint64_t m_creationTime;
  uint64_t m_numFrames;
  std::vector<double> m_conductivityFull;
  std::vector<double> m_pHFull;
  std::vector<std::vector<double>> m_concentrationsFull;
  std::vector<std::vector<std::pair<double, std::vector<double>>>> m_detectorTraces;

  bool m_haveLastFrame;
  RecordedFrame m_lastReadFrame;

  /* These are set by applyDescription() */
  size_t m_numCells;
  double m_capillaryLength;
  std::vector<std::string> m_constituentNames;
  double m_dx;
  size_t m_numConstituents;
  size_t m_currentFullFrame;
  size_t m_numActiveDetectors;
};

} // namespace recording

#endif // RECORDING_PLAYER_H
