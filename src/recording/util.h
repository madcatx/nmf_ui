#ifndef RECORDING_UTIL_H
#define RECORDING_UTIL_H

#include <recording/types.h>

#include <cstdint>

namespace recording {

auto currentEpoch() -> int64_t;
auto errToType(const int error) -> ErrorType;
#ifdef _WIN32
auto UTF8ToWString(const char *str) -> std::wstring;
#endif // _WIN32

} // namespace recording

#endif // RECORDING_UTIL_H
