#include "datafile.h"

#include <recording/util.h>

#include <array>
#include <cassert>
#ifdef ECHMET_COMPILER_MSVC
#include <memory>
#endif // ECHMET_COMPILER_MSVC

namespace recording {

static const std::array<uint8_t, 18> SIGNATURE = { { 'C', 'R', 'C', 'C', 'N', 'C', 'V', 'T', 'A', 'N', 'G', 'I', 'H', 'W', 'Q', 'V', 'P', 'X' } };

template <>
auto DataFile<Mode::READ>::openFile(const std::string &path) -> void
{
#ifdef NMFUI_WIN32_IO
  const auto wStr = UTF8ToWString(path.c_str());
    m_fh = CreateFileW(wStr.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (m_fh == INVALID_HANDLE_VALUE)
      throw Error{"Cannot open " NMFUI_RECORDING_FILE_SUFFIX " file for reading", errToType(GetLastError())};
#else
  m_fh = open(path.c_str(), O_RDONLY, 0);
  if (m_fh < 0)
    throw Error{"Cannot open " NMFUI_RECORDING_FILE_SUFFIX " file for reading", errToType(errno)};
#endif // NMFUI_WIN32_IO
}

template <>
auto DataFile<Mode::WRITE>::openFile(const std::string &path) -> void
{
#ifdef NMFUI_WIN32_IO
  const auto wStr = UTF8ToWString(path.c_str());
    m_fh = CreateFileW(wStr.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (m_fh == INVALID_HANDLE_VALUE)
      throw Error{"Cannot open " NMFUI_RECORDING_FILE_SUFFIX " file for writing", errToType(GetLastError())};
#else
  m_fh = open(path.c_str(), O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);
  if (m_fh < 0)
    throw Error{"Cannot open " NMFUI_RECORDING_FILE_SUFFIX " file for writing", errToType(errno)};
#endif // NMFUI_WIN32_IO
}

template <>
DataFile<Mode::READ>::DataFile(const std::string &path) :
  m_lastOffset{0},
  m_path{path}
{
  openFile(path);

  std::vector<uint8_t> sig{};
  readAt(OffsetAbs{MKOF(0)}, SIGNATURE.size(), sig);
  if (std::memcmp(sig.data(), SIGNATURE.data(), SIGNATURE.size()) != 0)
    throw Error{"File does not appear to be a " NMFUI_RECORDING_FILE_SUFFIX " file", ErrorType::BAD_FILE};

#ifdef NMFUI_WIN32_IO
  SetFilePointerEx(m_fh, { 0, 0 }, &m_lastOffset, FILE_END);
#else
  m_lastOffset = lseek64(m_fh, 0, SEEK_END);
#endif // NMFUI_WIN32_IO
}

template <>
DataFile<Mode::WRITE>::DataFile(const std::string &path) :
  m_lastOffset{0},
  m_path{path}
{
  openFile(path);

  appendRaw(SIGNATURE.data(), SIGNATURE.size());
}

template <>
auto DataFile<Mode::WRITE>::flush() -> void
{
#ifdef NMFUI_WIN32_IO
  FlushFileBuffers(m_fh);
#else
  fsync(m_fh);
#endif // NMFUI_WIN32_IO
}

template <>
auto DataFile<Mode::READ>::readAt(const OffsetAbs &off, const std::size_t size, std::string &out) -> void
{
  if (size < 1)
    return;

#ifdef ECHMET_COMPILER_MSVC
  static_assert(sizeof(char) == sizeof(uint8_t), "Unexpected char size");
  auto buf = std::unique_ptr<uint8_t[]>(new uint8_t[size]);
  this->readRawAt(off, buf.get(), size);
  out = std::string{reinterpret_cast<char *>(buf.get())};
#else
  out.resize(size, '\0');
  this->readRawAt(off, reinterpret_cast<uint8_t *>(out.data()), size);
#endif // ECHMET_COMPILER_MSVC
}

template <>
auto DataFile<Mode::READ>::readBlockAt(const OffsetAbs &off, const std::size_t size) -> std::vector<uint8_t>
{
  std::vector<uint8_t> buf{};
  buf.resize(size);

  this->readRawAt(off, reinterpret_cast<uint8_t *>(&buf[0]), size);
  return buf;
}

template <>
auto DataFile<Mode::READ>::size() const -> OffsetAbs
{
  return OffsetAbs{m_lastOffset};
}

} // namespace recording
