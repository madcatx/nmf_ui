#ifndef RECORDING_TYPES_H
#define RECORDING_TYPES_H

#include <calculators/nmfinterfacetypes.h>

#include <stdexcept>

namespace recording {

enum class ErrorType {
  UNKNOWN,
  NO_FILE,
  ACCESS_DENIED,
  FILE_EXISTS,
  INVALID_ARGUMENT,
  READ_ONLY,
  INSUFFICIENT_MEMORY,
  OUT_OF_SPACE,
  PATH_IS_DIRECTORY,
  DATA_TOO_LARGE,
  BAD_FILE,
  IO_LOWLEVEL,
  END_OF_FILE,
  OUT_OF_RANGE
};

class Error : public std::runtime_error {
public:
  Error(const char *what, const ErrorType type);

  const ErrorType type;
};

class FileInfo {
public:
  FileInfo(const uint_fast64_t creationTime, const uint_fast64_t numFrames, std::string description) noexcept;
  FileInfo(FileInfo &&other) noexcept;

  auto operator=(FileInfo &&other) noexcept -> FileInfo &;

  const uint_fast64_t creationTime;
  const uint_fast64_t numFrames;
  const std::string description;
};

class RecordedFrame {
public:
  calculators::SimStateData state;
  double tStop;
  int tolerance;
  bool autoDt;
};

} // namespace recording

#endif // RECORDING_TYPES_H
