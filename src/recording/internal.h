#ifndef RECORDING_INTERNAL_H
#define RECORDING_INTERNAL_H


#ifdef _WIN32
  #ifndef WIN32_MEAN_AND_LEAN
    #define WIN32_MEAN_AND_LEAN
  #endif // WIN32_MEAN_AND_LEAN
  #ifndef NOMINMAX
    #define NOMINMAX 1
  #endif // NOMINMAX
  #include <windows.h>
  #define NMFUI_WIN32_IO
#else
  #include <sys/types.h>
#endif // _WIN32

namespace recording {
#ifdef _WIN32
  using FilePtr = LARGE_INTEGER;
  using Written = DWORD;

  #define OFV(v) v.QuadPart
  inline auto MKOF(int_fast64_t value)
  {
    FilePtr ptr;
    ptr.QuadPart = value;
    return ptr;
  }
#else
  using FilePtr = off64_t;
  using Written = ssize_t;

  #define OFV(v) v
  #define MKOF(v) v
#endif // _WIN32
  static_assert(sizeof(FilePtr) >= 8, "Size of FilePtr must be at least 8 bytes");

  enum class OffsetType {
    Absolute,
    Relative
  };

  template <OffsetType>
  class Offset {};

  template <>
  class Offset<OffsetType::Absolute> {
  public:
  #ifdef _WIN32
    explicit Offset(const FilePtr ptr) :
      value{ptr.LowPart, ptr.HighPart}
    {}
  #else
    explicit Offset(const FilePtr ptr) : value{ ptr }
    {}
  #endif // _WIN32
    Offset(const Offset &other) = default;
    const FilePtr value;

    Offset<OffsetType::Absolute> & operator=(const Offset<OffsetType::Absolute> &other)
    {
      const_cast<FilePtr&>(value) = other.value;

      return *this;
    }
  };

  template <>
  class Offset<OffsetType::Relative> {
  public:
  #ifdef _WIN32
    explicit Offset(const FilePtr ptr) :
        value{ptr.LowPart, ptr.HighPart}
    {}
  #else
    explicit Offset(const FilePtr ptr) : value{ptr}
    {}
  #endif // _WIN32


    FilePtr value;

    auto toAbs(const Offset<OffsetType::Absolute> &base) const
    {
      FilePtr newValue;
      OFV(newValue) = OFV(base.value) + OFV(value);
      return Offset<OffsetType::Absolute>{newValue};
    }
  };

  using OffsetAbs = Offset<OffsetType::Absolute>;
  using OffsetRel = Offset<OffsetType::Relative>;
} // namespace recording


#endif // RECORDING_INTERNAL_H
