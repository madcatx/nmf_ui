#include "util.h"

#include <recording/internal.h>

#ifdef _WIN32
#else
  #include <time.h>
#endif // _WIN32


namespace recording {

auto currentEpoch() -> int_fast64_t
{
#ifdef _WIN32
  //Get the number of seconds since January 1, 1970 12:00am UTC
  //Code released into public domain; no attribution required.

  static const int_fast64_t UNIX_TIME_START = 0x019DB1DED53E8000; //January 1, 1970 (start of Unix epoch) in "ticks"
  static const int_fast64_t TICKS_PER_SECOND = 10000000; //a tick is 100ns

  FILETIME ft;
  GetSystemTimeAsFileTime(&ft); //returns ticks in UTC

  //Copy the low and high parts of FILETIME into a LARGE_INTEGER
  //This is so we can access the full 64-bits as an Int64 without causing an alignment fault
  LARGE_INTEGER li;
  li.LowPart = ft.dwLowDateTime;
  li.HighPart = ft.dwHighDateTime;

  //Convert ticks since 1/1/1970 into seconds
  return (li.QuadPart - UNIX_TIME_START) / TICKS_PER_SECOND;
#else
  struct timespec ts;
  if (clock_gettime(CLOCK_REALTIME, &ts) < 0)
    return 0;
  return ts.tv_sec;
#endif // _WIN32
}

#ifdef _WIN32
auto UTF8ToWString(const char *str) -> std::wstring
{
    const int wSize = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS, str, -1,
        NULL, 0);
    if (wSize == 0)
        throw Error{"Cannot determine widechar array size", ErrorType::INVALID_ARGUMENT};

    wchar_t *wArray = new wchar_t[wSize];
    if (wArray == nullptr)
        throw Error{"Insufficient memory", ErrorType::INSUFFICIENT_MEMORY};

    if (MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS, str, -1,
        wArray, wSize) == 0) {
        delete[] wArray;
        throw Error{"Cannot convert UTF-8 sequence to wide string", ErrorType::INVALID_ARGUMENT};
    }

    std::wstring wStr(wArray);

    delete[] wArray;

    return wStr;
}
#endif // _WIN32

#ifdef NMFUI_WIN32_IO
auto errToType(const int error) -> ErrorType
{
  switch (error) {
  case ERROR_ACCESS_DENIED:
    return ErrorType::ACCESS_DENIED;
  case ERROR_OUTOFMEMORY:
    return ErrorType::OUT_OF_SPACE;
  case ERROR_WRITE_PROTECT:
    return ErrorType::READ_ONLY;
  default:
    return ErrorType::UNKNOWN;
  }
}
#else
auto errToType(const int error) -> ErrorType
{
  switch (error) {
  case EACCES:
    return ErrorType::ACCESS_DENIED;
  case EEXIST:
    return ErrorType::FILE_EXISTS;
  case ENOENT:
    return ErrorType::NO_FILE;
  case EINVAL:
    return ErrorType::INVALID_ARGUMENT;
  case EPERM:
    return ErrorType::ACCESS_DENIED;
  case EROFS:
    return ErrorType::READ_ONLY;
  case ENOMEM:
    return ErrorType::INSUFFICIENT_MEMORY;
  case ENOSPC:
    return ErrorType::OUT_OF_SPACE;
  case EISDIR:
    return ErrorType::PATH_IS_DIRECTORY;
  case EOVERFLOW:
    return ErrorType::DATA_TOO_LARGE;
  case EBADFD:
    return ErrorType::BAD_FILE;
  default:
    return ErrorType::UNKNOWN;
  }
}
#endif // NMFUI_WIN32_IO

} // namespace recording
