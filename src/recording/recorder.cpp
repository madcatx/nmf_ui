#include "recorder.h"

#include "blocks.h"
#include "datafile.h"
#include "util.h"

#include <gdm/core/gdm.h>
#include <gearbox/singletons.h>
#include <persistence/persistence.h>
#include <persistence/types.h>

/* Sorry :( */
#include <boost/fusion/algorithm/iteration.hpp>
#include <boost/fusion/include/iteration.hpp>

#include <type_traits>

static_assert(std::is_same<std::underlying_type_t<calculators::ConstantForce>, uint8_t>::value, "Unexpected size of ConstantForce");

namespace recording {

extern template DataFile<Mode::WRITE>::DataFile(const std::string &);

static const OffsetRel FSD_OFFSET{MKOF(16)};

inline
auto namesToBytes(const std::vector<std::string> &names)
{
  std::vector<uint8_t> bytes{};

  for (const auto &n : names) {
    std::copy(n.cbegin(), n.cend(), std::back_inserter(bytes));
    bytes.emplace_back(0);
  }

  return bytes;
}

inline
auto makeFrameHeader(const calculators::SimState *state, const double tStop, const int32_t tolerance, const bool autoDt, const FilePtr arraySize,
                     const std::vector<uint8_t> &constituentNames, const int32_t numDetectors)
{
  static const size_t HEADER_PAD_VALUE{192};
  static_assert(HEADER_PAD_VALUE > sizeof(FrameHeader), "Insufficient HEADER_PAD size");

  static const FilePtr HEADER_PAD{HEADER_PAD_VALUE};

  FrameHeader hdr{};

  hdr.autoDt = autoDt;
  hdr.dt = state->dt;
  hdr.t = state->t;
  hdr.firstCell = state->firstCell;
  hdr.lastCell = state->lastCell;
  hdr.current = state->current;
  hdr.voltage = state->voltage;
  hdr.constantForce = static_cast<uint8_t>(state->constantForce);
  hdr.tStop = tStop;
  hdr.tolerance = tolerance;
  hdr.bytesConstituentNames = constituentNames.size();
  hdr.conductivityArray = HEADER_PAD;
  if (numDetectors > 0)
    hdr.numDetectorSamples = state->detectorTraces->front().size();
  else
    hdr.numDetectorSamples = 0;

  /* Data arrays order:
   * Conductivity[NUM_CELLS]
   * pH[NUM_CELLS]
   * Concentrations[NUM_CELLS * NUM_CONSTITUENTS]
   * Optional: Names[variable * NUM_CONSTITUENTS], zero terminator as separator
   * Optional: Detectors[NUM_CELLS * NUM_DETECTORS]
   */
  OFV(hdr.pHArray) = OFV(hdr.conductivityArray) + OFV(arraySize);
  OFV(hdr.concentrationsArray) = OFV(hdr.pHArray) + OFV(arraySize);

  OFV(hdr.nextFrame) = OFV(hdr.concentrationsArray) + OFV(arraySize) * state->concentrations.size();

  if (hdr.bytesConstituentNames == 0)
    OFV(hdr.constituentNames) = 0;
  else {
    OFV(hdr.constituentNames) = OFV(hdr.nextFrame);
    OFV(hdr.nextFrame) = OFV(hdr.constituentNames) + constituentNames.size();
  }

  if (numDetectors == 0)
    OFV(hdr.detectorTraces) = 0;
  else {
    const auto N = state->constituentNames.size() + 1; // Number of constituents plus timestamp
    const auto totSize = sizeof(double) * N * hdr.numDetectorSamples * numDetectors;

    OFV(hdr.detectorTraces) = OFV(hdr.nextFrame);
    OFV(hdr.nextFrame) = OFV(hdr.detectorTraces) + totSize;
  }

  OFV(hdr.nextFrame) += OFV(FSD_OFFSET.value);

  return hdr;
}

template <typename I, typename E, bool>
class StructMemberWriter {
};

template <typename I, typename E>
class StructMemberWriter<I, E, true> {
public:
  static auto call(const I &it, const E &end, DataFile<Mode::WRITE> *fh)
  {
    fh->append(boost::fusion::deref(it));
    appendStructMembers(boost::fusion::next(it), end, fh);
  }
};

template <typename I, typename E>
class StructMemberWriter<I, E, false> {
public:
  static auto call(const I &it, const E &, DataFile<Mode::WRITE> *fh)
  {
    fh->append(boost::fusion::deref(it));
  }
};

template <typename I, typename E>
inline
auto appendStructMembers(const I &it, const E &end, DataFile<Mode::WRITE> *fh)
{
  StructMemberWriter<I, E, std::is_same<I, E>::value>::call(it, end, fh);
}

template <typename T>
inline
auto appendStruct(const T &payload, DataFile<Mode::WRITE> *fh)
{
  assert(boost::fusion::size(payload) > 0);

  const auto beg = boost::fusion::begin(payload);
  const auto end = boost::fusion::end(payload);
  const auto off = fh->append(boost::fusion::deref(beg));

  appendStructMembers(boost::fusion::next(beg), end, fh);

  return off;
}

inline
auto writeConcentrationsAt(const std::vector<std::vector<double>> &concentrations, DataFile<Mode::WRITE> *fh, const OffsetAbs &base, const std::size_t first, const std::size_t last)
{
  const auto size = (last - first + 1) * sizeof(double);
  OffsetRel off{MKOF(0)};

  for (const auto &v : concentrations) {
    fh->writeAt(off.toAbs(base), v, first, last);
    OFV(off.value) += size;
  }
}

inline
auto writeDetectorsAt(const std::vector<std::vector<std::pair<double, std::vector<double>>>> *detectors, DataFile<Mode::WRITE> *fh, const OffsetAbs &base)
{
  OffsetRel off{MKOF(0)};

  for (const auto &det : *detectors) {
    for (const auto &p : det) {
      fh->writeAt(off.toAbs(base), p.first);
      OFV(off.value) += sizeof(std::decay_t<decltype(p.first)>);

      fh->writeAt(off.toAbs(base), p.second);
      OFV(off.value) += sizeof(std::decay_t<std::remove_pointer_t<decltype(p.second.data())>>) * p.second.size();
    }
  }
}

template <typename T>
inline
auto writeStructAt(const T &payload, DataFile<Mode::WRITE> *fh, const OffsetAbs &base)
{
  OffsetRel off{MKOF(0)};

  boost::fusion::for_each(payload, [&fh, &off, &base](const auto &t) {
    fh->writeAt(off.toAbs(base), t);
    OFV(off.value) += sizeof(std::decay_t<decltype(t)>);
  });
}

Recorder::Recorder(const std::string &path, const int rate, const persistence::System &sys, const size_t numActiveDetectors) :
  m_rate{double(rate) / 1000.0},
  m_fh{nullptr},
  m_tLastFrame{0.0},
  m_numCells{sys.cells},
  m_numConstituents{gearbox::Singletons::gdm().size()},
  m_numActiveDetectors{numActiveDetectors}
{
  if (rate <= 0)
    throw Error{"Invalid recording rate", ErrorType::INVALID_ARGUMENT};

  m_fh = new DataFile<Mode::WRITE>(path);
  writeGlobalHeader();
  writeSystem(sys);
}

Recorder::~Recorder()
{
  if (m_fh) {
    finalize();
    delete m_fh;
  }

  for (auto e : m_framesDict)
    delete e;
}

auto Recorder::currentPath() -> std::string
{
  return m_fh->path();
}

auto Recorder::finalize() -> void
{
  if (!m_framesDict.empty()) {
    const auto frameDictOffset = appendStruct(*m_framesDict[0], m_fh);
    for (size_t idx{1}; idx < m_framesDict.size(); idx++)
      appendStruct(*m_framesDict[idx], m_fh);

    m_fh->writeAt(GLOB_HDR_FRAMES_DICTIONARY_OFFSET, frameDictOffset.value);
    m_fh->writeAt(GLOB_HDR_FRAMES_OFFSET, m_framesDict.front()->offset);
    m_fh->writeAt<uint64_t>(GLOB_HDR_NUM_FRAMES, m_framesDict.size());
  }
}

auto Recorder::writeFrame(const calculators::SimState *state, const double tStop, const int32_t tolerance, const bool autoDt, const bool force) -> void
{
  writeFrameInternal(
    state,
    tStop,
    tolerance,
    autoDt,
    force,
    false
  );
}

auto Recorder::writeFullFrame(const calculators::SimState *state, const double tStop, const int32_t tolerance, const bool autoDt, const bool force) -> void
{
  writeFrameInternal(
    state,
    tStop,
    tolerance,
    autoDt,
    force,
    true
  );
}

auto Recorder::writeFrameInternal(const calculators::SimState *state, const double tStop, const int32_t tolerance, const bool autoDt, const bool force, const bool fullFrame) -> void
{
  if (m_tLastFrame + m_rate >= state->t && !m_framesDict.empty() && !force)
    return;

  FilePtr arraySize;
  const auto frameFirstCell = fullFrame ? 0 : state->firstCell;
  const auto frameLastCell = fullFrame ? m_numCells - 1 : state->lastCell;
  const auto namesArray = fullFrame ? namesToBytes(state->constituentNames) : std::vector<uint8_t>{};

  OFV(arraySize) = (frameLastCell - frameFirstCell + 1) * sizeof(double);
  const auto hdr = makeFrameHeader(state, tStop, tolerance, autoDt, arraySize, namesArray, m_numActiveDetectors);

  Frame frm{};
  frm.headerOffset = FSD_OFFSET.value;
  frm.isFullFrame = fullFrame;

  const auto base = appendStruct(frm, m_fh);
  writeStructAt(hdr, m_fh, FSD_OFFSET.toAbs(base));

  OffsetRel vecOff{MKOF(OFV(frm.headerOffset) + OFV(hdr.conductivityArray))};
  m_fh->writeAt(vecOff.toAbs(base), state->conductivity, frameFirstCell, frameLastCell);

  OFV(vecOff.value) = OFV(frm.headerOffset) + OFV(hdr.pHArray);
  m_fh->writeAt(vecOff.toAbs(base), state->pH, frameFirstCell, frameLastCell);

  OFV(vecOff.value) = OFV(frm.headerOffset) + OFV(hdr.concentrationsArray);
  writeConcentrationsAt(state->concentrations, m_fh, vecOff.toAbs(base), frameFirstCell, frameLastCell);

  if (fullFrame) {
    OFV(vecOff.value) = OFV(frm.headerOffset) + OFV(hdr.constituentNames);
    m_fh->writeAt(vecOff.toAbs(base), namesArray);
  }

  if (state->detectorTraces != nullptr && state->detectorTraces->size() > 0) {
    OFV(vecOff.value) = OFV(frm.headerOffset) + OFV(hdr.detectorTraces);
    writeDetectorsAt(state->detectorTraces, m_fh, vecOff.toAbs(base));
  }

  const auto fo = OFV(base.value);
  m_framesDict.emplace_back(new FrameDictEntry{MKOF(fo), int32_t(fullFrame)});

  m_tLastFrame = state->t;
}

auto Recorder::writeGlobalHeader() -> void
{
  m_fh->writeAt(GLOB_HDR_SYSTEM_DESCRIPTION_OFFSET, SYS_DESC_DESC_START.value);
  m_fh->writeAt<FilePtr>(GLOB_HDR_FRAMES_OFFSET, MKOF(0)); /* This must be filled out later */
  m_fh->writeAt<FilePtr>(GLOB_HDR_FRAMES_DICTIONARY_OFFSET, MKOF(0)); /* This must be filled out later */
  m_fh->writeAt(GLOB_HDR_CREATION_TIME, currentEpoch());
  m_fh->writeAt<uint64_t>(GLOB_HDR_NUM_FRAMES, 0); /* This must be filled out later */
}

auto Recorder::writeSystem(const persistence::System &sys) -> void
{
  const auto comp = persistence::toJson(sys);
  m_fh->writeAt(SYS_DESC_LENGTH.toAbs(SYS_DESC_DESC_START), uint64_t(comp.size()));
  m_fh->writeRawAt(SYS_DESC_DATA.toAbs(SYS_DESC_DESC_START), reinterpret_cast<const uint8_t *>(comp.data()), comp.size());
}

} // namespace recording
