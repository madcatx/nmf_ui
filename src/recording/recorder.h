#ifndef RECODRDING_RECORDER_H
#define RECODRDING_RECORDER_H

#include <recording/common.h>
#include <recording/types.h>

#include <calculators/nmfinterfacetypes.h>

#include <QObject>

#include <string>

namespace persistence {
  class System;
}

namespace recording {

template <Mode>
class DataFile;

struct FrameDictEntry;

class Recorder {
public:
  explicit Recorder(const std::string &path, const int rate, const persistence::System &sys, const size_t m_numActiveDetectors);
  Recorder(const Recorder &) = delete;
  ~Recorder();

  Recorder & operator=(const Recorder &) = delete;

  auto currentPath() -> std::string;
  auto finalize() -> void;
  auto writeFrame(const calculators::SimState *state, const double tStop, const int32_t tolerance, const bool autoDt, const bool force) -> void;
  auto writeFullFrame(const calculators::SimState *state, const double tStop, const int32_t tolerance, const bool autoDt, const bool force) -> void;

private:
  auto writeFrameInternal(const calculators::SimState *state, const double tStop, const int32_t tolerance, const bool autoDt, const bool force, const bool fullFrame) -> void;
  auto writeGlobalHeader() -> void;
  auto writeSystem(const persistence::System &sys) -> void;

  const double m_rate;
  DataFile<Mode::WRITE> *m_fh;
  double m_tLastFrame;

  std::vector<FrameDictEntry *> m_framesDict;

  int m_numCells;
  size_t m_numConstituents;
  const size_t m_numActiveDetectors;
};

} // namespace recording

#endif // RECODRDING_RECORDER_H
