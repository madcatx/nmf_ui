#ifndef RECORDING_BLOCKS_H
#define RECORDING_BLOCKS_H

#include <recording/internal.h>

#include <hacks.h>

/* Sorry :( */
#include <boost/fusion/adapted.hpp>
#include <boost/fusion/include/adapted.hpp>

#include <array>
#include <cstdint>

static_assert(sizeof(double) == 8, "Unexpected double size");
static_assert(sizeof(int32_t) == 4, "Unexpected int32_t size");

namespace recording {

/* --- Global header ---
 * All entries are of type uint64
 */
NMFUI_INLINE const OffsetAbs GLOB_HDR_SYSTEM_DESCRIPTION_OFFSET{MKOF(32)};    /*!< Where to look for system description data */
NMFUI_INLINE const OffsetAbs GLOB_HDR_FRAMES_OFFSET{MKOF(40)};                /*!< Where to look for the first recorded frame */
NMFUI_INLINE const OffsetAbs GLOB_HDR_FRAMES_DICTIONARY_OFFSET{MKOF(48)};     /*!< Where to look for the frames dictionary */
NMFUI_INLINE const OffsetAbs GLOB_HDR_CREATION_TIME{MKOF(56)};                /*!< File creation time in UNIX epoch */
NMFUI_INLINE const OffsetAbs GLOB_HDR_NUM_FRAMES{MKOF(64)};                   /*!< Total number of recorded frames */

/* --- System description ---
 * All entries are of type uint64
 */
NMFUI_INLINE const OffsetAbs SYS_DESC_DESC_START{MKOF(256)};                  /*!< System description starts here */
NMFUI_INLINE const OffsetRel SYS_DESC_LENGTH{MKOF(0)};                        /*!< Length in bytes of the system desctiption block */
NMFUI_INLINE const OffsetRel SYS_DESC_DATA{MKOF(8)};                          /*!< System description data */

struct Frame {
  FilePtr headerOffset;  /*!< Relative offset of frame-specific data (FSD) */
  int32_t isFullFrame;   /*!< Frame contains full arrays */
};

struct FrameHeader {
  FilePtr nextFrame;              /*!< Relative offset pointing to the start of the next frame */
  int32_t autoDt;
  double dt;
  double t;
  int32_t firstCell;
  int32_t lastCell;
  double current;
  double voltage;
  int32_t tolerance;
  double tStop;
  int32_t constantForce;
  FilePtr conductivityArray;      /*!< Relative offset of the conductivity array */
  FilePtr pHArray;                /*!< Relative offset of the pH array */
  FilePtr concentrationsArray;    /*!< Relative offset of the concentrations array */
  int32_t bytesConstituentNames;  /*!< Size of constituent names array. This is non-zero only in full frames */
  FilePtr constituentNames;       /*!< Relative offset of the constituent names array */
  int32_t numDetectorSamples;     /*!< Number of samples in detector trace */
  FilePtr detectorTraces;         /*!< Relative offset of the detector traces array */
};

struct FrameDictEntry {
  FilePtr offset;
  int32_t isFullFrame;
};
NMFUI_INLINE const size_t FRAME_DICT_ENTRY_SIZE{sizeof(FilePtr) + sizeof(int32_t)};

} // namespace recording

BOOST_FUSION_ADAPT_STRUCT(
  recording::Frame,
  (recording::FilePtr, headerOffset),
  (int32_t, isFullFrame)
);

BOOST_FUSION_ADAPT_STRUCT(
  recording::FrameHeader,
  (recording::FilePtr, nextFrame)
  (int32_t, autoDt)
  (double, dt)
  (double, t)
  (int32_t, firstCell)
  (int32_t, lastCell)
  (double, current)
  (double, voltage)
  (int32_t, tolerance)
  (double, tStop)
  (int32_t, constantForce)
  (int32_t, bytesConstituentNames),
  (recording::FilePtr, conductivityArray)
  (recording::FilePtr, pHArray)
  (recording::FilePtr, concentrationsArray),
  (recording::FilePtr, constituentNames),
  (int32_t, numDetectorSamples),
  (recording::FilePtr, detectorTraces)
);

BOOST_FUSION_ADAPT_STRUCT(
  recording::FrameDictEntry,
  (recording::FilePtr, offset)
  (int32_t, isFullFrame)
);

#endif // RECORDING_BLOCKS_H
