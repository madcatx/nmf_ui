#ifndef RECORDING_COMMON_H
#define RECORDING_COMMON_H

#define NMFUI_RECORDING_FILE_SUFFIX "ppg"

#include <hacks.h>

#include <string>

namespace recording {

NMFUI_INLINE const std::string SUFFIX{NMFUI_RECORDING_FILE_SUFFIX};

enum class Mode {
  READ,
  WRITE
};

}

#endif // RECORDING_COMMON_H
