#ifndef RECORDING_DATAFILE_H
#define RECORDING_DATAFILE_H

#include <recording/common.h>
#include <recording/internal.h>
#include <recording/util.h>

#ifndef NMFUI_WIN32_IO
  #include <sys/stat.h>
  #include <fcntl.h>
  #include <errno.h>
  #include <unistd.h>
#endif // NMFUI_WIN32_IO

#include <array>
#include <cassert>
#include <cstddef>
#include <cstring>
#include <memory>
#include <string>
#include <vector>

namespace recording {

template <typename T, bool Flippable>
class EndianFlipper {
};

template <typename T>
class EndianFlipper<T, true> {
public:
#ifdef NMFUI_ARCH_IS_BIG_ENDIAN
  using TRet = T;
#else
  using TRet = const T &;
#endif // NMFUI_ARCH_IS_BIG_ENDIAN

  static
  auto call(const T &payload) -> TRet;
};

template <typename T>
class EndianFlipper<T, false> {
public:
  using TRet = const T &;

  static
  auto call(const T &payload) -> TRet;
};

template <typename T>
auto EndianFlipper<T, true>::call(const T &payload) -> TRet
{
#ifdef NMFUI_ARCH_IS_BIG_ENDIAN
  const auto N = sizeof(payload);
  T flipped{};

  const auto rawSrc = reinterpret_cast<const uint8_t *>(&payload);
  auto rawDst = reinterpret_cast<uint8_t *>(&flipped);
  for (std::size_t idx{0}; idx < N/2; idx++)
    rawDst[idx] = rawSrc[N - 1 - idx];

  return flipped;
#else
  return EndianFlipper<T, false>::call(payload);
#endif // NMFUI_ARCH_IS_BIG_ENDIAN
}

template <typename T>
auto EndianFlipper<T, false>::call(const T &payload) -> TRet
{
  return payload;
}

template <Mode M>
class DataFile {
public:
  DataFile(const std::string &path);
  DataFile(const DataFile &) = delete;

  ~DataFile()
  {
  #ifdef NMFUI_WIN32_IO
    CloseHandle(m_fh);
  #else
    close(m_fh);
  #endif // NMFUI_WIN32_IO
  }

  auto operator=(const DataFile &) -> DataFile & = delete;

  template <typename T>
  auto append(const T &payload) -> OffsetAbs;
  template <typename T>
  auto append(const std::vector<T> &payload) -> OffsetAbs;
  template <typename T>
  auto append(const std::vector<T> &payload, const std::size_t first, const std::size_t last) -> OffsetAbs;

  auto appendRaw(const uint8_t *payload, const std::size_t size) -> OffsetAbs;
  auto flush() -> void;

  auto path() const -> std::string
  {
    return m_path;
  }

  auto position() const
  {
    return m_lastOffset;
  }

  template <typename T>
  auto readAt(const OffsetAbs &off, T &out) -> void;
  template <typename T>
  auto readAt(const OffsetAbs &off, const std::size_t size, std::vector<T> &out) -> void;
  auto readAt(const OffsetAbs &off, const std::size_t size, std::string &out) -> void;
  auto readBlockAt(const OffsetAbs &off, const std::size_t) -> std::vector<uint8_t>;

  auto readRawAt(const OffsetAbs &off, uint8_t *buf, const std::size_t size) -> void;

  auto size() const -> OffsetAbs;

  template <typename T>
  auto writeAt(const OffsetAbs &off, const T &payload) -> void;
  template <typename T>
  auto writeAt(const OffsetAbs &off, const std::vector<T> &payload) -> void;
  template <typename T>
  auto writeAt(const OffsetAbs &off, const std::vector<T> &payload, const std::size_t first, const std::size_t last) -> void;

  auto writeRawAt(const OffsetAbs &off, const uint8_t *payload, const std::size_t size) -> void;

private:
  template <typename T>
  static
  auto flipEndianess(const T &payload) -> typename EndianFlipper<T, std::is_fundamental<T>::value>::TRet
  {
    return EndianFlipper<T, std::is_fundamental<T>::value>::call(payload);
  }

#ifdef NMFUI_WIN32_IO
  using FileHandle = HANDLE;
#else
  using FileHandle = int;
#endif // NMFUI_WIN32_IO

  auto extend(const std::size_t size) -> void
  {
    std::vector<uint8_t> zeroes{};
    zeroes.resize(size);

    auto ptr = zeroes.data();
    std::memset(ptr, 0, size);

#ifdef NMFUI_WIN32_IO
    FilePtr dummy{};
    if (SetFilePointerEx(m_fh, {0, 0}, &dummy, FILE_END) == FALSE)
      throw Error{"Cannot seek to the end of " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(GetLastError())};
  #else
    if (lseek64(m_fh, 0, SEEK_END) < 0)
      throw Error{"Cannot seek to the end of " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(errno)};
  #endif // NMFUI_WIN32_IO

    writeRawAt(OffsetAbs{m_lastOffset}, ptr, size);
  }

  auto openFile(const std::string &path) -> void;

  std::vector<uint8_t> m_buffer;
  FilePtr m_lastOffset;

  FileHandle m_fh;
  std::string m_path;
};

template <>
inline
auto DataFile<Mode::WRITE>::writeRawAt(const OffsetAbs &off, const uint8_t *payload, const std::size_t size) -> void
{
  if (OFV(off.value) > OFV(m_lastOffset))
    extend(OFV(off.value) - OFV(m_lastOffset));

  if (OFV(off.value) != OFV(m_lastOffset)) {
#ifdef NMFUI_WIN32_IO
    FilePtr dummy{};
    if (SetFilePointerEx(m_fh, off.value, &dummy, FILE_BEGIN) == FALSE)
      throw Error{ "Cannot write data to " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(GetLastError())};
#else
    if (lseek64(m_fh, off.value, SEEK_SET) < 0)
      throw Error{"Cannot write data to " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(errno)};
#endif // NMFUI_WIN32_IO
  }

  std::size_t total{0};
  while (total < size) {
  #ifdef NMFUI_WIN32_IO
    DWORD last{};
    auto ret = WriteFile(m_fh, payload, size, &last, nullptr);
    if (ret == FALSE || size != last)
      throw Error{"Cannot write data to " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(GetLastError())};
  #else
    auto last = write(m_fh, payload, size);
    if (last < 0)
      throw Error{"Cannot write data to " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(errno)};
  #endif // NMFUI_WIN32_IO

    total += size;
    payload += last;
  }

#ifdef NMFUI_WIN32_IO
  if (SetFilePointerEx(m_fh, {0, 0}, &m_lastOffset, FILE_END) == FALSE)
    throw Error{"Cannot seek the end of the " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(GetLastError())};
#else
  m_lastOffset = lseek64(m_fh, 0, SEEK_END);
  if (m_lastOffset < 0)
    throw Error{"Cannot seek the end of the " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(errno)};
#endif // NMFUI_WIN32_IO
}

template <>
inline
auto DataFile<Mode::WRITE>::appendRaw(const uint8_t *payload, const std::size_t size) -> OffsetAbs
{
#ifdef NMFUI_WIN32_IO
  FilePtr end{};
  SetFilePointerEx(m_fh, {0, 0}, &end, FILE_END);
  OffsetAbs off{end};
#else
  const auto end = lseek64(m_fh, 0, SEEK_END);
  if (end < 0)
    throw Error{"Cannot seek to the end of " NMFUI_RECORDING_FILE_SUFFIX " file", errToType(errno)};

  OffsetAbs off{end};
#endif // NMFUI_WIN32_IO
  writeRawAt(off, payload, size);

  return off;
}

template <> template <typename T>
auto DataFile<Mode::WRITE>::append(const T &payload) -> OffsetAbs
{
  const auto flipped = this->flipEndianess<T>(payload);
  return appendRaw(reinterpret_cast<const uint8_t *>(&flipped), sizeof(T));
}

template <> template <typename T>
auto DataFile<Mode::WRITE>::append(const std::vector<T> &payload) -> OffsetAbs
{
#ifdef NMFUI_ARCH_IS_BIG_ENDIAN
  auto flipped = payload;
  for (auto &v : flipped)
    flipEndianess(v);

    append(off, flipped.data(), sizeof(T) * payload.size());
#else
  return appendRaw(reinterpret_cast<const uint8_t *>(payload.data()), sizeof(T) * payload.size());
#endif // NMFUI_ARCH_IS_BIG_ENDIAN
}

template <> template <typename T>
auto DataFile<Mode::WRITE>::append(const std::vector<T> &payload, const std::size_t first, const std::size_t last) -> OffsetAbs
{
  assert(last >= first);

  const auto size = (last - first + 1) * sizeof(T);
#ifdef NMFUI_ARCH_IS_BIG_ENDIAN
  auto flipped = payload;
  for (size_t idx{first}; idx <= last; idx++)
    flipEndianess(flipped[idx]);

    append(reinterpret_cast<const uint8_t *>(flipped.data() + first), size);
#else
   return appendRaw(reinterpret_cast<const uint8_t *>(payload.data() + first), size);
#endif // NMFUI_ARCH_IS_BIG_ENDIAN
}

template <> template <typename T>
auto DataFile<Mode::WRITE>::writeAt(const OffsetAbs &off, const T &payload) -> void
{
  const auto flipped = flipEndianess<T>(payload);
  writeRawAt(off, reinterpret_cast<const uint8_t *>(&flipped), sizeof(payload));
}

template <>
inline
auto DataFile<Mode::READ>::readRawAt(const OffsetAbs &off, uint8_t *buf, const size_t size) -> void
{
#ifdef NMFUI_WIN32_IO
  FilePtr pos{};
  auto ret = SetFilePointerEx(m_fh, off.value, &pos, FILE_BEGIN);
  if (ret == FALSE)
    throw Error{"Reading failure", errToType(GetLastError())};

  DWORD read{};
  ret = ReadFile(m_fh, buf, size, &read, nullptr);
  if (ret == FALSE || size != read)
    throw Error{"Reading failure", errToType(GetLastError())};
#else
  (void)off;

  ssize_t rd{0};
  while (rd < size) {
    auto ret = read(m_fh, static_cast<void *>(buf), size);
    if (ret == 0)
      throw Error{"Reading failure", ErrorType::END_OF_FILE};
    else if (ret < 0)
      throw Error{"Reading failure", errToType(errno)};
    rd += ret;
  }
#endif // NMFUI_WIN32_IO
}

template <> template <typename T>
auto DataFile<Mode::READ>::readAt(const OffsetAbs &off, T &out) -> void
{
  static_assert(std::is_fundamental<T>::value, "Only fundamental types are allowed");

  this->readRawAt(off, reinterpret_cast<uint8_t *>(&out), sizeof(T));
  out = this->flipEndianess(out);
}

#ifdef NMFUI_WIN32_IO
template <> template <>
inline
auto DataFile<Mode::READ>::readAt(const OffsetAbs &off, LARGE_INTEGER &out) -> void
{
    this->readRawAt(off, reinterpret_cast<uint8_t *>(&out.QuadPart), sizeof(out.QuadPart));
    out = this->flipEndianess(out);
}
#endif // NMFUI_WIN32_IO

template <> template <typename T>
auto DataFile<Mode::READ>::readAt(const OffsetAbs &off, const std::size_t size, std::vector<T> &out) -> void
{
  if (size < 1)
    return;

  out.resize(size);
  this->readRawAt(off, reinterpret_cast<uint8_t *>(&out[0]), size * sizeof(T));

#ifdef NMFUI_ARCH_IS_BIG_ENDIAN
  for (auto &v : out)
    this->flipEndianess(out);
#endif // NMFUI_ARCH_IS_BIG_ENDIAN
}

template <> template <typename T>
auto DataFile<Mode::WRITE>::writeAt(const OffsetAbs &off, const std::vector<T> &payload) -> void
{
#ifdef NMFUI_ARCH_IS_BIG_ENDIAN
  auto flipped = payload;
  for (auto &v : flipped)
    flipEndianess(v);

    writeAt(off, flipped.data(), sizeof(T) * payload.size());
#else
  writeRawAt(off, reinterpret_cast<const uint8_t *>(payload.data()), sizeof(T) * payload.size());
#endif // NMFUI_ARCH_IS_BIG_ENDIAN
}

template <> template <typename T>
auto DataFile<Mode::WRITE>::writeAt(const OffsetAbs &off, const std::vector<T> &payload, const std::size_t first, const std::size_t last) -> void
{
  assert(last >= first);

  const auto size = (last - first + 1) * sizeof(T);
#ifdef NMFUI_ARCH_IS_BIG_ENDIAN
  auto flipped = payload;
  for (size_t idx{first}; idx <= last; idx++)
    flipEndianess(flipped[idx]);

    writeAt(off, reinterpret_cast<const uint8_t *>(flipped.data() + first), size);
#else
  writeRawAt(off, reinterpret_cast<const uint8_t *>(payload.data() + first), size);
#endif // NMFUI_ARCH_IS_BIG_ENDIAN
}

} // namespace recording

#endif // RECORDING_DATAFILE_H
