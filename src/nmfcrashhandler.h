#ifndef NMFCRASHHANDLER_H
#define NMFCRASHHANDLER_H

#include "crasheventcatcher.h"
#include <type_traits>

class CrashHandlerBase;
class UICrashFinalizer;
class CrashEventCatcher;

class NMFCrashHandler {
public:
  friend class UICrashFinalizer;
  NMFCrashHandler() = delete;

  template <typename Receiver, void (Receiver::*EmergencySlot)()>
  static auto connectToEmergency(const Receiver *receiver) -> bool
  {
    static_assert(std::is_base_of<QObject, Receiver>::value, "Receiver must be a QObject");

    if (s_catcher == nullptr)
      return false;

    QObject::connect(s_catcher, &CrashEventCatcher::emergency, receiver, EmergencySlot, Qt::DirectConnection);

    return true;
  }

  static auto checkForCrash() -> void;
  static auto installCrashHandler() -> bool;
  static auto uninstallCrashHandler() -> void;
  static auto handler() -> CrashHandlerBase *;

private:
  static UICrashFinalizer *s_uiFinalizer;
  static CrashEventCatcher *s_catcher;

  static const std::string s_textCrashDumpFile;

};

#endif // NMFCRASHHANDLER_H
