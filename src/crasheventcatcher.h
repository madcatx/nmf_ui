#ifndef CRASHEVENTCATCHER_H
#define CRASHEVENTCATCHER_H

#include <QEvent>
#include <QObject>

class CrashEvent;

class CrashEventCatcher : public QObject {
  Q_OBJECT
public:
  CrashEventCatcher();
  auto executeEmergency() -> void;

  static auto displayCrashMessage(const char *dump, const bool postCrash = false) -> void;

protected:
  auto eventFilter(QObject *watched, QEvent *event) -> bool override;

private:
  auto handleCrash(const CrashEvent *event) -> void;

  const QEvent::Type m_crashEventId;

signals:
  void emergency();
};

#endif // CRASHEVENTCATCHER_H
