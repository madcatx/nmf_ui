#ifndef ADDITIONALVALIDATOR_H
#define ADDITIONALVALIDATOR_H

#include <functional>
#include <memory>
#include <QMetaType>
#include <QString>
#include <QVariant>
#include <QVector>

namespace util {

template <typename T>
class AdditionalValidator {
public:
  class InvalidValidatorException : public std::runtime_error {
  public:
    using std::runtime_error::runtime_error;
  };

  using ValidFunc = std::function<bool (const T &)>;

  AdditionalValidator() :
    m_valid{false}
  {}

  AdditionalValidator(const AdditionalValidator &other) = default;
  AdditionalValidator(AdditionalValidator &&other) noexcept :
    m_valid{other.m_valid},
    m_func{std::move(m_func)}
  {
  }

  AdditionalValidator & operator=(AdditionalValidator &&other) noexcept
  {
    const_cast<bool&>(m_valid) = other.m_valid;
    m_func = std::move(other.m_func);

    return *this;
  }

  AdditionalValidator(ValidFunc &&func) noexcept :
    m_valid{true},
    m_func{std::move(func)}
  {
  }

  virtual ~AdditionalValidator() = default;

  static auto additionalValidatorsOk(const QObject *obj, const T &value) -> bool;

  virtual auto validate(const T &value) const -> bool
  {
    if (!m_valid)
      throw InvalidValidatorException{"Validator is invalid"};

    return m_func(value);
  }

  constexpr static const char * const PROPERTY_NAME{"ADDITIONAL_VALIDATORS"};

private:
  const bool m_valid;
  ValidFunc m_func;
};

template <typename T>
using AdditionalValidatorSPtr = std::shared_ptr<AdditionalValidator<T>>;

template <typename T>
using AdditionalValidatorVec = QVector<AdditionalValidatorSPtr<T>>;

template <typename T>
auto AdditionalValidator<T>::additionalValidatorsOk(const QObject *obj, const T &value) -> bool
{
  auto prop = obj->property(PROPERTY_NAME);
  if (prop.isValid()) {
    if (prop.canConvert<AdditionalValidatorVec<T>>()) {
      auto validators = prop.value<AdditionalValidatorVec<T>>();

      for (auto &&v : validators) {
        if (!v->validate(value))
          return false;
      }
    }
  }

  return true;
}

} // namespace util

// Register more specializations here as needed
Q_DECLARE_METATYPE(util::AdditionalValidator<double>)
Q_DECLARE_METATYPE(util::AdditionalValidatorVec<double>)
Q_DECLARE_METATYPE(util::AdditionalValidator<int>)
Q_DECLARE_METATYPE(util::AdditionalValidatorVec<int>)

#endif // ADDITIONALVALIDATOR_H
