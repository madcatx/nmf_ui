#ifndef PREMADEVALIDATORS_H
#define PREMADEVALIDATORS_H

#include <util/additionalvalidator.h>

#include <mutex>

namespace util {

class PremadeValidators {
public:
  template <typename T>
  using Validator = std::shared_ptr<AdditionalValidator<T>>;

  template <typename T>
  static auto mustBePositive() {
    static Validator<T> validator{nullptr};

    lk.lock();
    if (validator == nullptr)
      validator = std::make_shared<AdditionalValidator<T>>([](const T &v) { return v > T{0}; });
    lk.unlock();

    return validator;
  }

  template <typename T>
  static auto mustBeNonNegative() {
    static Validator<T> validator{nullptr};

    lk.lock();
    if (validator == nullptr)
      validator = std::make_shared<AdditionalValidator<T>>([](const T &v) { return v >= T{0}; });
    lk.unlock();

    return validator;
  }

private:
  static std::mutex lk;
};

template <typename T>
class IsWithinValidator : public AdditionalValidator<T> {
public:
  IsWithinValidator(T lo, T hi) :
    AdditionalValidator<T>{[this](const T &value) {
      return m_hi >= value && m_lo <= value;
    }},
    m_hi{std::move(hi)},
    m_lo{std::move(lo)}
  {
  }

  IsWithinValidator(IsWithinValidator &&other) noexcept :
    AdditionalValidator<T>{std::move(other)},
    m_hi{std::move(other.m_hi)},
    m_lo{std::move(other.m_lo)}
  {
  }

  IsWithinValidator & operator=(IsWithinValidator &&other) noexcept
  {
    AdditionalValidator<T>::operator=(other);

    m_hi = other.m_hi;
    m_lo = other.m_lo;

    return *this;
  }

  auto changeRange(T lo, T hi) -> void
  {
    m_hi = std::move(hi);
    m_lo = std::move(lo);
  }

private:
  T m_hi;
  T m_lo;
};

} // namespace util

#endif // PREMADEVALIDATORS_H
