#ifndef UTIL_LOWLEVEL_H
#define UTIL_LOWLEVEL_H

#include <cassert>
#include <cstdio>
#include <cstdlib>

namespace util {

[[noreturn]] inline
void impossible_path() {
#ifdef NDEBUG
    std::fputs("Impossible execution path", stderr);
    std::abort();
#else
    assert(false);
#endif // NDEBUG
}

} // namespace util

#endif // UTIL_LOWLEVEL_H
