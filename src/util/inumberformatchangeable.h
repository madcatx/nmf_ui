#ifndef INUMBERFORMATCHANGEABLE_H
#define INUMBERFORMATCHANGEABLE_H

#include <globals.h>

#include <QtPlugin>

class QLocale;

namespace util {

class INumberFormatChangeable {
public:
  virtual ~INumberFormatChangeable();
  virtual void onNumberFormatChanged(const QLocale *oldLocale) = 0;
};

} // namespace util

Q_DECLARE_INTERFACE(util::INumberFormatChangeable, "ch.protonmail." SOFTWARE_NAME_INTERNAL_S ".INumberFormatChangeable/1.0")

#endif // INUMBERFORMATCHANGEABLE_H
