#ifndef UTIL_UTIL_H
#define UTIL_UTIL_H

#include <hacks.h>

#include <cmath>

namespace util {

template <typename T>
constexpr auto dx(const T length, const T points) -> T
{
  return length / points;
}

template <typename T>
constexpr auto logz(const T &value, const T &base)
{
  return std::log(value) / std::log(base);
}

template <typename T>
inline
NMFUI_CONSTEXPR auto index(const T pos, const T dx, const size_t pts) -> size_t
{
  auto p = pos / dx;
  return p >= pts ? pts - 1 : p;
}

template <>
inline
NMFUI_CONSTEXPR auto index(const float pos, const float dx, const size_t pts) -> size_t
{
  auto p = size_t(pos / dx + 0.5f);
  return p >= pts ? pts - 1 : p;
}

template <>
inline
NMFUI_CONSTEXPR auto index(const double pos, const double dx, const size_t pts) -> size_t
{
  auto p = size_t(pos / dx + 0.5);
  return p >= pts ? pts - 1 : p;
}

template <typename T>
NMFUI_CONSTEXPR auto sigmoid(const T &x, const T &position, const T &width)
{
  /* Steepness parameter of the sigmoid function */
  NMFUI_CONSTEXPR T CUTOFF{1.0 / (1.0 - 0.995)};
  NMFUI_CONSTEXPR T FLATLINE{1.0 / (1.0 - 0.9999)};
  const T EXP = std::pow(CUTOFF, 1.0 / (width / 2.0));
  const T LIMIT = logz(FLATLINE, EXP);

  const T sx = x - position;
  if (sx < -LIMIT)
    return T{0.0};
  else if (sx > LIMIT)
    return T{1.0};
  else
    return T{1.0} / (T{1.0} + std::pow(EXP, -sx));
}

template <typename T>
NMFUI_CONSTEXPR auto sgn(const T &v) -> T
{
  return (v > 0) - (v < 0);
}

template <typename T>
NMFUI_CONSTEXPR auto within(const T &lo, const T &hi, const T &val) -> bool
{
  return (hi >= val) && (lo <= val);
}

class HMS {
public:
  int h;
  int m;
  double s;
};

HMS msecToHms(const double msec);

}

#endif // UTIL_UTIL_H
