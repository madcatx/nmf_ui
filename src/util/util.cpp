#include "util.h"
#include <utility>


namespace util {

HMS msecToHms(const double msec)
{
  auto chop = [](int t, double v) -> std::pair<int, double> {
    int f = std::floor(double(t) / v);
    auto r = t - f * v;
    return {f, r};
  };

  HMS hms{};

  double rem;
  auto hPart = chop(msec, 3600000.0);
  hms.h = hPart.first; rem = hPart.second;

  auto mPart = chop(rem, 60000.0);
  hms.m = mPart.first; rem = mPart.second;

  hms.s = rem / 1000.0;

  return hms;
}

}
