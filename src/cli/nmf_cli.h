/* vim: set sw=2 ts=2 sts=2 expandtab: */

#ifndef NMF_CLI_H
#define NMF_CLI_H

#include <calculators/nmfinterface.h>

#include <QObject>

#include <fstream>

class NMFCli : public QObject {
  Q_OBJECT

public:
  explicit NMFCli();
  auto start(const char *setupFile, const char *outputFile) -> void;

private:
  auto connectCalculatorSignals() -> void;

  calculators::NMFInterface *m_calculator;
  std::fstream m_output;

private slots:
  void onSimInitialized(const calculators::SimState *state);
  void onSimPaused(const calculators::SimState *state);
  void onSimStarted();
  void onSimUpdated(const calculators::SimState *state);
  void onSimTerminated(QString reason);

signals:
  void runCompleted(bool success);
};

#endif // NMF_CLI_H
