/* vim: set sw=2 ts=2 sts=2 expandtab: */

#include "calculators/nmfinterfacetypes.h"
#include <cli/nmf_cli.h>
#include <nmfcrashhandler.h>
#include <gearbox/singletons.h>
#include <persistence/persistence.h>
#include <persistence/types.h>
#include <util/doubletostringconvertor.h>

#include <QCoreApplication>
#include <QTimer>

#include <chrono>
#include <cstdlib>
#include <iostream>

NMFCli::NMFCli() : QObject{nullptr}
{
  m_calculator = new calculators::NMFInterface{this};
  connectCalculatorSignals();
}

auto NMFCli::connectCalculatorSignals() -> void
{
  connect(
    m_calculator,
    &calculators::NMFInterface::simInitialized,
    this,
    &NMFCli::onSimInitialized,
    Qt::QueuedConnection
  );
  connect(
    m_calculator,
    &calculators::NMFInterface::simPaused,
    this,
    &NMFCli::onSimPaused,
    Qt::QueuedConnection
  );
  connect(
    m_calculator,
    &calculators::NMFInterface::simStarted,
    this,
    &NMFCli::onSimStarted,
    Qt::QueuedConnection
  );
  connect(
    m_calculator,
    &calculators::NMFInterface::simUpdated,
    this,
    &NMFCli::onSimUpdated,
    Qt::QueuedConnection
  );
  connect(
    m_calculator,
    &calculators::NMFInterface::simTerminated,
    this,
    &NMFCli::onSimTerminated,
    Qt::QueuedConnection
  );
}

auto NMFCli::start(const char *setupFile, const char *outputFile) -> void
{
  m_output.open(outputFile, std::ios::out);
  if (!m_output.good()) {
    std::cout << "Cannot open output file for writing\n";
    emit runCompleted(false);
  }

  persistence::System sys{};
  calculators::InitParams initParams{};
  calculators::StartParams startParams{};

  persistence::loadSetup(setupFile, sys);

  initParams.adaptCalcSpace = sys.autoCells;
  initParams.autoDt = sys.autoDt;
  initParams.cells = sys.cells;
  initParams.capillaryDiameter = 50.0; /* 50 um - hardcoded for now */
  initParams.capillaryLength = sys.capillaryLength * 1000.0;
  initParams.constantForce = sys.constantForce == "V" ? calculators::ConstantForce::VOLTAGE : calculators::ConstantForce::CURRENT;
  initParams.constantForceValue = initParams.constantForce == calculators::ConstantForce::VOLTAGE ? sys.voltage : sys.current;
  initParams.debyeHuckel = false;
  initParams.onsagerFuoss = false;
  initParams.firstCell = sys.firstCell;
  initParams.lastCell = sys.lastCell;

  startParams.adaptCalcSpace = initParams.adaptCalcSpace;
  startParams.autoDt = initParams.autoDt;
  startParams.constantForce = initParams.constantForce;
  startParams.constantForceValue = initParams.constantForceValue;
  startParams.dt = sys.dt;
  startParams.eofMode = calculators::EOFMode::MOBILITY;
  startParams.eofValue = 0.0; /* Hardcoded for now */
  startParams.firstCell = initParams.firstCell;
  startParams.lastCell = initParams.lastCell;
  startParams.tStop = sys.stopTime;
  startParams.tolerance = sys.tolerance;
  startParams.updateInterval = sys.updateInterval;

  if (!m_calculator->initialize(initParams, gearbox::Singletons::gdm(), gearbox::Singletons::concentrationProfilesModel())) {
    std::cout << "Intialization failure: " << m_calculator->initErrorToString() << "\n";
    emit runCompleted(false);
  }

  if (!m_calculator->start(startParams)) {
    std::cout << "Cannot start simulation: " << m_calculator->startErrorToString() << "\n";
    emit runCompleted(false);
  }
}

void NMFCli::onSimInitialized(const calculators::SimState *state)
{
  std::cout << "Simulation initialized\n";
}

void NMFCli::onSimPaused(const calculators::SimState *state)
{
  state->lock.lock();

  const char DELIM{';'};
  m_output << "cell" << DELIM;
  for (const auto &name : state->constituentNames) {
    m_output << name << " (mM)" << DELIM;
  }
  m_output << "\xCE\xBA (S/m)" << DELIM;
  m_output << "\n";

  const size_t numCells = state->conductivity.size();
  for (size_t cell{0}; cell < numCells; cell++) {
    m_output << cell << DELIM;

    for (const auto &concs : state->concentrations)
      m_output << concs.at(cell) << DELIM;

    m_output << state->conductivity.at(cell) << DELIM;
    m_output << "\n";
  }

  state->lock.unlock();

  emit runCompleted(true);
}

void NMFCli::onSimStarted()
{
  std::cout << "Simulation started\n";
}

void NMFCli::onSimUpdated(const calculators::SimState *state)
{
  state->lock.lock();

  std::cout << "t = " << state->t << ", dt = " << state->dt << ", avg iter time = " << state->avgTimePerIter << " (msec)\n";
  state->lock.unlock();
}

void NMFCli::onSimTerminated(QString reason)
{
  std::cout << "Simulation terminated prematurely: " << reason.toStdString() << "\n";
}

int main(int argc, char *argv[])
{
  QCoreApplication app{argc, argv};

  NMFCrashHandler::installCrashHandler();

  if (argc < 3) {
	  std::cout << "Usage: " << argv[0] << " SETUP_FILE.json output_file\n";
	  return EXIT_FAILURE;
  }

  auto setupFile = argv[1];
  auto outputFile = argv[2];

  util::DoubleToStringConvertor::initialize();
  gearbox::Singletons::initialize();


  int ret = EXIT_FAILURE;
  try {
    NMFCli cli{};

    const auto tStart = std::chrono::steady_clock::now();

    QObject::connect(&cli, &NMFCli::runCompleted, [&](bool success) {
      if (!success) {
        std::cout << "Simulation did not finish successfully\n";
        app.exit(EXIT_FAILURE);
      } else {
        const auto tEnd = std::chrono::steady_clock::now();
        std::cout << "Simulation complete\n";
        std::cout << "Wall clock runtime: "
                  << double(std::chrono::duration_cast<std::chrono::milliseconds>(tEnd - tStart).count()) / 1000.0
                  << " (sec)\n";
        app.exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
      }
    });


    QTimer::singleShot(0, [&]{ cli.start(setupFile, outputFile); });
    app.exec();
  } catch (const persistence::Error &error) {
	  std::cout << "Cannot load setup: " << error.what() << "\n";
	  ret = EXIT_FAILURE;
  }

  NMFCrashHandler::uninstallCrashHandler();

  return ret;
}
