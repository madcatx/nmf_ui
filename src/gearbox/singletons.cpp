#include "singletons.h"

#include <gdm/core/gdm.h>
#include <gearbox/complexationmanager.h>
#include <gearbox/concentrationprofilesmodel.h>
#include <gearbox/databaseproxy.h>
#include <gearbox/defaults.h>
#include <gearbox/gdmproxyimpl.h>
#ifndef NMF_NO_GUI
#include <gearbox/profilescolorizermodel.h>
#endif // NMF_NO_GUI

#include <cassert>
#include <memory>
#include <stdexcept>

namespace gearbox {

static std::unique_ptr<ComplexationManager> COMPLEXATION_MANAGER{nullptr};
static std::unique_ptr<ConcentrationProfilesModel> CONCENTRATION_PROFILES_MODEL{nullptr};
static std::unique_ptr<DatabaseProxy> CONSTITUENT_DATABASE{nullptr};
static std::unique_ptr<gdm::GDM> GDM{nullptr};
static std::unique_ptr<GDMProxy> GDMPROXY{nullptr};
static bool IS_INITIALIZED{false};
static const double MINIMUM_COMPLEXING_CONCENTRATION{1.0e-12};
#ifndef NMF_NO_GUI
static std::unique_ptr<ProfilesColorizerModel> PROFILES_COLORIZER_MODEL{nullptr};
#endif // NMF_NO_GUI

auto Singletons::complexationManager() -> ComplexationManager &
{
  return *COMPLEXATION_MANAGER.get();
}

auto Singletons::concentrationProfilesModel() -> ConcentrationProfilesModel &
{
  return *CONCENTRATION_PROFILES_MODEL.get();
}

auto Singletons::databaseProxy() -> DatabaseProxy &
{
  return *CONSTITUENT_DATABASE.get();
}

auto Singletons::gdm() -> gdm::GDM &
{
  return *GDM.get();
}

auto Singletons::gdmProxy() -> GDMProxy &
{
  return *GDMPROXY.get();
}

auto Singletons::initialize() -> void
{
  assert(!IS_INITIALIZED);

  CONSTITUENT_DATABASE = std::make_unique<DatabaseProxy>();
  GDM = std::make_unique<gdm::GDM>(Defaults::MAXIMUM_BLOCKS);
  GDMPROXY = std::unique_ptr<GDMProxy>(new GDMProxyImpl{*GDM.get(), MINIMUM_COMPLEXING_CONCENTRATION});
  CONCENTRATION_PROFILES_MODEL = std::make_unique<ConcentrationProfilesModel>(*GDMPROXY.get());
#ifndef NMF_NO_GUI
  PROFILES_COLORIZER_MODEL = std::make_unique<ProfilesColorizerModel>();
#endif // NMF_NO_GUI
  COMPLEXATION_MANAGER = std::make_unique<ComplexationManager>(*GDM.get());

  IS_INITIALIZED = true;
}

auto Singletons::minimumComplexingConcentration() -> double
{
  return MINIMUM_COMPLEXING_CONCENTRATION;
}

auto Singletons::profilesColorizerModel() -> ProfilesColorizerModel &
{
#ifndef NMF_NO_GUI
  return *PROFILES_COLORIZER_MODEL.get();
#else
  assert(false);
  throw std::logic_error{"Attempted to get ProfilesColorizerModel in a non-GUI context"};
#endif // NMF_NO_GUI
}

} // namespace gearbox
