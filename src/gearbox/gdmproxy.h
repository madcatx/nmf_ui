#ifndef GDMPROXY_H
#define GDMPROXY_H

#include <gdm/core/basiccomposition.h>
#include <string>
#include <stdexcept>
#include <vector>

namespace gdm {
  class Constituent;
}

class GDMProxyException : public std::runtime_error {
public:
  using std::runtime_error::runtime_error;
};

namespace gearbox {

class GDMProxy {
public:
  using const_iterator = gdm::BasicComposition::const_iterator;

  GDMProxy(const double minimumComplexingConcentration);
  virtual ~GDMProxy() = 0;
  virtual auto cbegin() const noexcept -> const_iterator = 0;
  virtual auto clear() noexcept -> void= 0;
  virtual auto concentrations(const std::string &name) const noexcept -> std::vector<double> = 0;
  virtual auto complexes(const std::string &name) const noexcept -> bool = 0;
  virtual auto contains(const std::string &name) const noexcept -> bool = 0;
  virtual auto cend() const noexcept -> const_iterator = 0;
  virtual auto erase(const std::string &name) noexcept -> void = 0;
  virtual auto get(const std::string &name) -> gdm::Constituent = 0;
  virtual auto get(const std::string &name) const -> const gdm::Constituent = 0;
  virtual auto insert(const gdm::Constituent &ctuent) noexcept -> bool = 0;
  virtual auto isNucleus(const std::string &name) const noexcept -> bool = 0;
  virtual auto setConcentrations(const std::string &name, std::vector<double> concentrations) noexcept -> void = 0;
  virtual auto update(const std::string &name, const gdm::Constituent &ctuent) -> bool = 0;

protected:
  const double m_minimumComplexingConcentration;
};

} // namespace gearbox

#endif // GDMPROXY_H
