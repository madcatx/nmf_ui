#ifndef GDMPROXYIMPL_H
#define GDMPROXYIMPL_H

#include "gdmproxy.h"

namespace gdm {
  class GDM;
}

namespace gearbox {

class GDMProxyImpl : public GDMProxy {
public:
  GDMProxyImpl(gdm::GDM &model, const double minimumComplexingConcentration);
  ~GDMProxyImpl() override;
  auto cbegin() const noexcept -> const_iterator override;
  auto clear() noexcept -> void override;
  auto concentrations(const std::string &name) const noexcept -> std::vector<double> override;
  auto complexes(const std::string &name) const noexcept -> bool override;
  auto contains(const std::string &name) const noexcept -> bool override;
  auto cend() const noexcept -> const_iterator override;
  auto erase(const std::string &name) noexcept -> void override;
  auto get(const std::string &name) -> gdm::Constituent override;
  auto get(const std::string &name) const -> const gdm::Constituent override;
  auto insert(const gdm::Constituent &ctuent) noexcept -> bool override;
  auto isNucleus(const std::string &name) const noexcept -> bool override;
  auto setConcentrations(const std::string &name, std::vector<double> concentrations) noexcept -> void override;
  auto update(const std::string &name, const gdm::Constituent &ctuent) -> bool override;

private:
  auto fixupConcentrations() -> void;

  gdm::GDM &h_model;
};

} // namespace gearbox

#endif // GDMPROXYIMPL_H
