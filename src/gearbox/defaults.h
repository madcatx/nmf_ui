#ifndef DEFAULTS_H
#define DEFAULTS_H

#include <gearbox/concentrationprofilesmodel.h>

#include <cstddef>

namespace gearbox {

class Defaults {
public:
  Defaults() = delete;

  static const double CAPILLARY_LENGTH;
  static const int CELLS;
  static const double DT;
  static const double TIME;
  static const int TOLERANCE;
  static const double STOP_TIME;
  static const double VOLTAGE;

  static const ConcentrationProfilesModel::ProfileShape CONCENTRATION_PROFILE_SHAPE;
  static const double INJECTION_POSITION;
  static const double INJECTION_WIDTH;
  static const double INJECTION_STEEPNESS;

  static const size_t MAXIMUM_BLOCKS;
};

} // namespace gearbox

#endif // DEFAULTS_H
