#include "profilescolorizermodel.h"

#include <cassert>

namespace gearbox {

ProfilesColorizerModel::ProfilesColorizerModel(QObject *parent) :
  QObject{parent},
  m_hue{0},
  m_saturation{255},
  m_value{255}
{
}

auto ProfilesColorizerModel::add(const std::string &name, QColor color) -> void
{
  assert(m_colorMap.find(name) == m_colorMap.end());

  if (!color.isValid())
    color.setHsv(m_hue, m_saturation, 255);

  m_colorMap.emplace(name, std::move(color));

  advanceColor();
}

auto ProfilesColorizerModel::advanceColor() -> void
{
  const auto currentHue = m_hue;
  m_hue += 46;
  if (m_hue <= currentHue) {
    m_saturation -= 46;
    m_value -= 46;
  }
}

auto ProfilesColorizerModel::changeColor(const std::string &name, QColor newColor) -> void
{
  assert(m_colorMap.find(name) != m_colorMap.end());

  m_colorMap[name] = newColor;

  emit colorChanged(QString::fromStdString(name));
}

auto ProfilesColorizerModel::clear() noexcept -> void
{
  m_colorMap.clear();
}

auto ProfilesColorizerModel::get(const std::string &name) const -> QColor
{
  auto it = m_colorMap.find(name);
  assert(it != m_colorMap.cend());

  return it->second;
}

auto ProfilesColorizerModel::remove(const std::string &name) -> void
{
  auto it = m_colorMap.find(name);
  assert(it != m_colorMap.end());

  m_colorMap.erase(it);
}

auto ProfilesColorizerModel::reset() -> void
{
  m_hue = 0;
  m_saturation = 255;
  m_value = 255;
}

auto ProfilesColorizerModel::updateName(const std::string &oldName, const std::string &newName) -> void
{
  auto it = m_colorMap.find(oldName);
  assert(it != m_colorMap.end());

  auto clr = it->second;
  m_colorMap.erase(it);
  m_colorMap.emplace(newName, std::move(clr));
}

} // namespace gearbox
