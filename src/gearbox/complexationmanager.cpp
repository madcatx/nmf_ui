#include "complexationmanager.h"

#include <gdm/core/gdm.h>
#include <gdm/core/common/gdmexcept.h>
#include <ui/complexation/complexationrelationshipsmodel.h>

#ifndef NMF_NO_GUI
#include <ui/complexation/editcomplexationdialog.h>
#include <QMessageBox>
#else
#include <iostream>
#endif // NMF_NO_GUI

#include <cassert>
#include <algorithm>

namespace gearbox {

enum class Conflict {
  NONE,
  RESOLVED,
  UNRESOLVED
};

static
uint8_t calculateHue(const uint8_t step, const uint8_t lastHue)
{
  static const uint8_t ROT_BASE{85};
  static const uint8_t ROT_STEPS{255 / ROT_BASE};
  static_assert(255 % ROT_BASE == 0, "Invalid color rotation parameters");
  static_assert(ROT_STEPS > 1, "There have to be at least two rotation steps");

  uint8_t newHue;
  if (step % ROT_STEPS == 0 && step > 0)
    newHue = lastHue + ROT_BASE / (2 * step / ROT_STEPS);
  else
    newHue = lastHue + ROT_BASE;

  return newHue;
}

static
gdm::ConstituentType otherType(const gdm::ConstituentType t)
{
  if (t == gdm::ConstituentType::Ligand)
    return gdm::ConstituentType::Nucleus;
  return gdm::ConstituentType::Ligand;
}

static
QString makeOptionText(const std::string &name, const gdm::ConstituentType newType)
{
  const auto typeName = [newType]() -> QString {
    if (newType == gdm::ConstituentType::Ligand)
      return "ligand";
    return "nucleus";
  }();

  return QString(QObject::tr("Convert \"%1\" to %2")).arg(name.c_str(), typeName);
}

static
void switchRole(gdm::GDM::iterator ctuent, gdm::GDM &gdm)
{
  const auto c = gdm.concentrations(ctuent);
  auto changed = gdm::Constituent(otherType(ctuent->type()), ctuent->name(), ctuent->physicalProperties());

  gdm.erase(ctuent);
  auto inserted = gdm.insert(changed);
  gdm.setConcentrations(inserted.first, { c });

  /* TODO: Do we need this?
  auto bgIt = gdm.find(changed.name());
  if (bgIt != backgroundGDM.end()) {
    c = backgroundGDM.concentrations(bgIt).at(0);
    backgroundGDM.erase(bgIt);
    inserted = backgroundGDM.insert(changed);
    backgroundGDM.setConcentrations(inserted.first, { c });
  }
  */
}

#ifndef NMF_NO_GUI
static
Conflict resolveTypeConflict(gdm::GDM::iterator first, gdm::GDM::iterator second, gdm::GDM &gdm)
{
  if (first->type() != second->type())
    return Conflict::NONE;

  QMessageBox mbox{QMessageBox::Question, QObject::tr("Constituent types conflict"), QObject::tr("Constituents have incompatible types to form a complexation relationship. What do you want to do?")};

  QPushButton *fixFirst = nullptr;
  QPushButton *fixSecond = nullptr;

  auto cpxs = gdm::findComplexations(gdm.composition(), first);
  if (cpxs.empty())
    fixFirst = mbox.addButton(makeOptionText(first->name(), otherType(first->type())), QMessageBox::AcceptRole);

  cpxs = gdm::findComplexations(gdm.composition(), second);
  if (cpxs.empty())
    fixSecond = mbox.addButton(makeOptionText(second->name(), otherType(second->type())), QMessageBox::AcceptRole);

  mbox.addButton(QMessageBox::Cancel);

  mbox.exec();

  if (static_cast<void *>(mbox.clickedButton()) == fixFirst) {
    assert(fixFirst != nullptr);
    switchRole(first, gdm);
    return Conflict::RESOLVED;
  } else if (static_cast<void *>(mbox.clickedButton()) == fixSecond) {
    assert(fixSecond != nullptr);
    switchRole(second, gdm);
    return Conflict::RESOLVED;
  }

  return Conflict::UNRESOLVED;
}

static
EditComplexationDialog * makeComplexationDialog(const gdm::GDM::const_iterator nucleusIt, const gdm::GDM::const_iterator ligandIt, ComplexationRelationshipsModel *model, const gdm::GDM &gdm)
{
  auto dlg = new EditComplexationDialog{QString::fromStdString(nucleusIt->name())};
  dlg->setComplexationModel(model);

  const auto nucleusCharges = nucleusIt->physicalProperties().charges();

  auto nucleus = std::make_shared<::Constituent>(nucleusCharges.low(), nucleusCharges.high(), QString::fromStdString(nucleusIt->name()), ::Constituent::Type::NUCLEUS);

  const auto complexations = gdm::findComplexations(gdm.composition(), nucleusIt);

  for (const auto &cpxn : complexations) {
    const auto &_ligandIt = cpxn.first;
    const auto &_cpxn = cpxn.second;
    const auto _ligandCharges = _ligandIt->physicalProperties().charges();

    auto ligand = std::make_shared<::Constituent>(_ligandCharges.low(), _ligandCharges.high(), QString::fromStdString(_ligandIt->name()), ::Constituent::Type::LIGAND);
    ComplexationRelationship rel{nucleus, ligand};

    for (auto nucleusCharge = nucleusCharges.low(); nucleusCharge <= nucleusCharges.high(); nucleusCharge++) {
      for (auto _ligandCharge = _ligandCharges.low(); _ligandCharge <= _ligandCharges.high(); _ligandCharge++) {
        gdm::ChargeCombination chargesCombo{nucleusCharge, _ligandCharge};

        auto complexFormIt = _cpxn.find(chargesCombo);
        if (complexFormIt != _cpxn.cend())
            rel.addComplexForm(nucleusCharge, _ligandCharge,
                QVector<double>(complexFormIt->mobilities().cbegin(), complexFormIt->mobilities().cend()),
                QVector<double>(complexFormIt->pBs().cbegin(), complexFormIt->pBs().cend())
	    );
      }
    }

    dlg->addRelationship(rel);
  }

  if (ligandIt != gdm.cend()) {
    const auto ligandCharges = ligandIt->physicalProperties().charges();
    auto newLigand = std::make_shared<::Constituent>(ligandCharges.low(), ligandCharges.high(), QString::fromStdString(ligandIt->name()), ::Constituent::Type::LIGAND);
    dlg->addRelationship({nucleus, newLigand});
  }

  return dlg;
}

static
bool processComplexationsForGDM(const std::string &nucleus, gdm::GDM &GDM,
                                const EditComplexationDialog::AllComplexationRelationships &allRels,
                                const EditComplexationDialog::AllComplexationRelationships &oldRels)
{
  std::vector<std::pair<gdm::GDM::const_iterator, gdm::Complexation>> allComplexations{};

  const auto nucleusIt = GDM.find(nucleus);

  if (nucleusIt == GDM.cend())
    return true;

  /* Clear all old complexations */
  for (auto mapIt = oldRels.cbegin(); mapIt != oldRels.cend(); mapIt++) {
    const auto ligandIt = GDM.find(mapIt.key().toStdString());
    if (ligandIt == GDM.cend())
      continue;

    GDM.eraseComplexation(nucleusIt, ligandIt);
  }

  const auto ncCharges = nucleusIt->physicalProperties().charges();

  for (auto mapIt = allRels.cbegin(); mapIt != allRels.cend(); mapIt++) {
    gdm::Complexation complexations{};

    auto ligandIt = GDM.find(mapIt.key().toStdString());

    if (ligandIt == GDM.cend())
      continue;

    const auto &ligandMap = *mapIt;

    const auto lgCharges = ligandIt->physicalProperties().charges();
    for (auto nucleusCharge = ncCharges.low(); nucleusCharge <= ncCharges.high(); nucleusCharge++) {
      if (!ligandMap.contains(nucleusCharge))
        continue;

      const auto &ncBlock = ligandMap[nucleusCharge];
      for (auto ligandCharge = lgCharges.low(); ligandCharge <= lgCharges.high(); ligandCharge++) {
        if (!ncBlock.contains(ligandCharge))
          continue;

        const auto &cpxnProps = ncBlock[ligandCharge];
        const auto mobilities = std::vector<double>{std::get<0>(cpxnProps).cbegin(), std::get<0>(cpxnProps).cend()};
        const auto pBs = std::vector<double>{std::get<1>(cpxnProps).cbegin(), std::get<1>(cpxnProps).cend()};

        if (mobilities.size() != pBs.size()) {
          QMessageBox mbox{QMessageBox::Warning, QObject::tr("Invalid complexation parameters"), QObject::tr("Mismatching number of mobilities and pBs")};
          mbox.exec();
          return false;
        }

        if (mobilities.empty())
          continue;

        try {
          gdm::ChargeCombination cc = {nucleusCharge, ligandCharge};
          gdm::ComplexForm cForm{cc, mobilities.size(), pBs, mobilities};
          complexations.insert(std::move(cForm));
        } catch (gdm::InvalidArgument &ex) {
          QMessageBox mbox{QMessageBox::Warning, QObject::tr("Invalid complexation parameters"), ex.what()};
          mbox.exec();
          return false;
        }
      }
    }

    allComplexations.emplace_back(ligandIt, std::move(complexations));
  }

  for (auto it = allComplexations.begin(); it != allComplexations.end(); it++) {
    const auto ligandIt = it->first;
    auto &&complexations = it->second;

    if (!complexations.empty()) {
      try {
        GDM.setComplexation(nucleusIt, ligandIt, std::move(complexations));
      } catch (gdm::InvalidArgument &ex) {
        QMessageBox mbox{QMessageBox::Warning, QObject::tr("Failed to set complexations"), ex.what()};
        mbox.exec();
        return false;
      }
    }
  }

  return true;
}
#endif // NMF_NO_GUI

void ComplexationManager::addNucleus(const std::string &nucleusName)
{
  if (m_complexingNuclei.find(nucleusName) != m_complexingNuclei.cend())
    return;

  uint8_t maxHue{0};
  uint8_t maxCtr{0};
  for (auto it = m_complexingNuclei.cbegin(); it != m_complexingNuclei.cend(); it++) {
    if (it->second.hue > maxHue) {
      maxHue = it->second.hue;
      maxCtr = it->second.ctr;
    }
  }

  m_complexingNuclei.emplace(nucleusName, Hue{++maxHue, ++maxCtr});
}

ComplexationManager::ComplexationManager(gdm::GDM &gdm, QObject *parent) :
  QObject{parent},
  h_gdm{gdm}
{
}

std::vector<int> ComplexationManager::complexationStatus(const std::string &name) const
{
  const auto it = m_complexationStatus.find(name);
  if (it == m_complexationStatus.cend())
    return {};

  return it->second;
}

bool ComplexationManager::complexes(const std::string &name)
{
  const auto it = h_gdm.find(name);

  if (it == h_gdm.cend())
    return false;

  const auto found = gdm::findComplexations(h_gdm.composition(), it);
  return !found.empty();
}

void ComplexationManager::editComplexation(const std::string &name)
{
#ifndef NMF_NO_GUI
  auto it = h_gdm.find(name);

  if (it == h_gdm.cend())
    return;

  if (it->type() == gdm::ConstituentType::Ligand) {
    QMessageBox mbox{QMessageBox::Information, QObject::tr("Invalid input"), QObject::tr("Please use nuclei to edit complexations")};
    mbox.exec();
    return;
  }

  auto model = new ComplexationRelationshipsModel{};
  auto dlg = makeComplexationDialog(it, h_gdm.cend(), model, h_gdm);
  handleUserInput(dlg, name, "");

  delete model;
  delete dlg;
#else
  std::cout << "Warning: Complexations can be edited only in GUI frontend\n";
#endif // NMF_NO_GUI
}

#ifndef NMF_NO_GUI
void ComplexationManager::handleUserInput(EditComplexationDialog *dlg, const std::string &nucleusName, const std::string &ligandName)
{
  if (ligandName.length() > 0)
    dlg->showLigand(QString::fromStdString(ligandName));

  const auto oldRelationships = dlg->allRelationships();

  while (true) {
    const int answer = dlg->exec();
    if (answer != QDialog::Accepted)
      return;

    const auto relationships = dlg->allRelationships();

    if (!processComplexationsForGDM(nucleusName, h_gdm, relationships, oldRelationships))
      continue;

    break;
  }

  updateComplexingNuclei();
  updateComplexationStatus();
}
#endif // NMF_NO_GUI

void ComplexationManager::makeComplexation(const std::string &first, const std::string &second)
{
#ifndef NMF_NO_GUI
  auto nucleusIt = h_gdm.find(first);
  auto ligandIt = h_gdm.find(second);

  if (nucleusIt == h_gdm.end() || ligandIt == h_gdm.end())
    return;

  if (nucleusIt == ligandIt) {
    QMessageBox mbox{QMessageBox::Information,
                     tr("Invalid input"),
                     tr("Constituent cannot form a complex with itself")};
    mbox.exec();

    return;
  }

  auto conflict = resolveTypeConflict(nucleusIt, ligandIt, h_gdm);
  switch (conflict) {
  case Conflict::NONE:
    break;
  case Conflict::UNRESOLVED:
    return;
  case Conflict::RESOLVED:
    /* Iterators may have been invalidated, get them again */
    nucleusIt = h_gdm.find(first);
    ligandIt = h_gdm.find(second);
    break;
  }

  assert(nucleusIt != h_gdm.end() && ligandIt != h_gdm.end());
  assert(nucleusIt->type() != ligandIt->type());
  assert(nucleusIt != ligandIt);

  if (nucleusIt->type() != gdm::ConstituentType::Nucleus)
    std::swap(nucleusIt, ligandIt);

  assert(nucleusIt->type() == gdm::ConstituentType::Nucleus && ligandIt->type() == gdm::ConstituentType::Ligand);

  auto model = new ComplexationRelationshipsModel{};
  auto dlg = makeComplexationDialog(nucleusIt, ligandIt, model, h_gdm);
  handleUserInput(dlg, nucleusIt->name(), ligandIt->name());

  delete model;
  delete dlg;
#else
  std::cout << "Warning: Complexations can be made only in GUI frontend\n";
#endif // NMF_NO_GUI
}

void ComplexationManager::notifyConstituentRemoved()
{
  updateComplexingNuclei();
  updateComplexationStatus();
}

void ComplexationManager::updateComplexingNuclei()
{
  uint8_t ctr = 0;
  uint8_t hue = 105;
  ComplexingNucleiMap map{};
  std::vector<int> excludeHues{};

  for (auto it = h_gdm.cbegin(); it != h_gdm.cend(); it++) {
    if (it->type() != gdm::ConstituentType::Nucleus)
      continue;

    const auto found = gdm::findComplexations(h_gdm.composition(), it);
    if (!found.empty()) {
      const auto mit = m_complexingNuclei.find(it->name());
      if (mit != m_complexingNuclei.cend()) {
        map[it->name()] =  mit->second;
        excludeHues.emplace_back(mit->second.ctr);
      }
    }
  }

  /* This needs a double pass */
  for (auto it = h_gdm.cbegin(); it != h_gdm.cend(); it++) {
    if (it->type() != gdm::ConstituentType::Nucleus)
      continue;

    const auto found = gdm::findComplexations(h_gdm.composition(), it);
    if (!found.empty()) {
    const auto mit = m_complexingNuclei.find(it->name());
      if (mit == m_complexingNuclei.cend()) {
        while (std::any_of(excludeHues.cbegin(), excludeHues.cend(), [ctr](const int v){ return v == ctr;}))
          hue = calculateHue(ctr++, hue);

        hue = calculateHue(ctr, hue);
        map[it->name()] = {hue, ctr};
        ctr++;
      }
    }
  }

  m_complexingNuclei = std::move(map);
}

void ComplexationManager::updateComplexationStatus()
{
  ComplexationStatusMap map{};

  for (auto it = h_gdm.cbegin(); it != h_gdm.cend(); it++) {
    if (it->type() == gdm::ConstituentType::Nucleus) {
      const auto _it = m_complexingNuclei.find(it->name());
      if (_it != m_complexingNuclei.cend())
        map[it->name()] = { _it->second.hue };
    } else {
      std::vector<int> nucleiIDs{};

      const auto found = gdm::findComplexations(h_gdm.composition(), it);
      for (auto _it = found.cbegin(); _it != found.cend(); _it++)
        nucleiIDs.emplace_back(m_complexingNuclei.at(_it->first->name()).hue);

      map[it->name()] = std::move(nucleiIDs);
    }
  }

  m_complexationStatus = std::move(map);
  emit complexationStatusChanged();
}

void ComplexationManager::refreshAll()
{
  m_complexationStatus.clear();
  m_complexingNuclei.clear();

  updateComplexingNuclei();
  updateComplexationStatus();
}

} // namespace gearbox
