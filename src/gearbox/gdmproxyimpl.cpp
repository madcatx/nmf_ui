#include "gdmproxyimpl.h"

#include <gdm/core/gdm.h>
#include "gdmproxy_p.h"
#include <gearbox/defaults.h>

#include <algorithm>
#include <cassert>

namespace gearbox {

GDMProxyImpl::GDMProxyImpl(gdm::GDM &model,
  const double minimumComplexingConcentration) :
  GDMProxy{minimumComplexingConcentration},
  h_model{model}
{
}

GDMProxyImpl::~GDMProxyImpl()
{
}

auto GDMProxyImpl::cbegin() const noexcept -> const_iterator
{
  return h_model.cbegin();
}

auto GDMProxyImpl::clear() noexcept -> void
{
  h_model.clear();
}

auto GDMProxyImpl::concentrations(const std::string &name) const noexcept -> std::vector<double>
{
  assert(h_model.find(name) != h_model.cend());

  const auto it = h_model.find(name);
  auto concs = h_model.concentrations(it);

  /* Fixup concentrations for complexing systems
   * Beware of the dragon here - concentrations in the underlying GDM
   * will remain unfixed. If someone accesses concentrations
   * directly through GDM, they will get the raw concentrations
   */
  if (h_model.isComplexing()) {
    std::transform(
      concs.begin(),
      concs.end(),
      concs.begin(),
      [this](const auto v) {
        if (v < m_minimumComplexingConcentration)
          return m_minimumComplexingConcentration;
        else if (v < 0.0)
          return 0.0;
        return v;
    });
  }

  return concs;
}

auto GDMProxyImpl::complexes(const std::string &name) const noexcept -> bool
{
  const auto it = h_model.find(name);

  assert(it != h_model.cend());

  const auto found = gdm::findComplexations(h_model.composition(), it);
  return !found.empty();
}

auto GDMProxyImpl::contains(const std::string &name) const noexcept -> bool
{
  const auto sampleIt = h_model.find(name);

  return sampleIt != h_model.cend();
}

auto GDMProxyImpl::cend() const noexcept -> const_iterator
{
  return h_model.cend();
}

auto GDMProxyImpl::erase(const std::string &name) noexcept -> void
{
  const auto it = h_model.find(name);

  GDMProxy_p::eraseComplexations(it, h_model);

  if (it != h_model.cend())
    h_model.erase(it);
}

auto GDMProxyImpl::fixupConcentrations() -> void
{
  for (auto it = h_model.begin(); it != h_model.end(); it++) {
    auto concs = h_model.concentrations(it);

    if (complexes(it->name())) {
      std::transform(
        concs.begin(),
        concs.end(),
        concs.begin(),
        [this](const double v) {
          if (v <= m_minimumComplexingConcentration)
            return m_minimumComplexingConcentration;
          return m_minimumComplexingConcentration;
        });

      h_model.setConcentrations(it, concs);
    }
  }
}

auto GDMProxyImpl::get(const std::string &name) -> gdm::Constituent
{
  const auto it = h_model.find(name);
  if (it == h_model.cend())
    throw GDMProxyException{"Constituent not found"};

  return *it;
}

auto GDMProxyImpl::get(const std::string &name) const -> const gdm::Constituent
{
  auto it = h_model.find(name);
  if (it == h_model.cend())
    throw GDMProxyException{"Constituent not found"};

  return *it;
}

auto GDMProxyImpl::insert(const gdm::Constituent &ctuent) noexcept -> bool
{
  try {
    h_model.insert(ctuent);
  } catch (std::bad_alloc &) {
    erase(ctuent.name());
    return false;
  }

  setConcentrations(ctuent.name(), { 0 } );

  return true;
}

auto GDMProxyImpl::isNucleus(const std::string &name) const noexcept -> bool
{
  const auto it = h_model.find(name);
  assert(it != h_model.cend());

  return it->type() == gdm::ConstituentType::Nucleus;
}

auto GDMProxyImpl::setConcentrations(
  const std::string &name,
  std::vector<double> concentrations
) noexcept -> void
{
  assert(h_model.find(name) != h_model.cend());
  assert(concentrations.size() <= Defaults::MAXIMUM_BLOCKS);

  auto it = h_model.find(name);
  std::transform(
    concentrations.begin(),
    concentrations.end(),
    concentrations.begin(),
    [this](const auto v) {
      if (v < m_minimumComplexingConcentration && h_model.isComplexing())
        return m_minimumComplexingConcentration;
      else if (v < 0.0)
        return 0.0;
      return v;
  });

  h_model.setConcentrations(it, std::move(concentrations));
}

auto GDMProxyImpl::update(
  const std::string &name,
  const gdm::Constituent &ctuent
) -> bool
{
  const auto it = h_model.find(name);
  assert(it != h_model.cend());

  if (!GDMProxy_p::typeChangeOk(ctuent, *it, h_model))
    throw GDMProxyException{"Type changes are not allowed for complexing constituents"};

  auto ret = h_model.update(it, ctuent);
  fixupConcentrations();

  return std::get<1>(ret);
}

} // namespace gearbox
