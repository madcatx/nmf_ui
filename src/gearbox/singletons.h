#ifndef SINGLETONS_H
#define SINGLETONS_H

namespace gdm {
  class GDM;
}

namespace gearbox {

class ComplexationManager;
class ConcentrationProfilesModel;
class DatabaseProxy;
class GDMProxy;
class ProfilesColorizerModel;

class Singletons {
public:
  Singletons() = delete;

  static auto complexationManager() -> ComplexationManager &;
  static auto concentrationProfilesModel() -> ConcentrationProfilesModel &;
  static auto databaseProxy() -> DatabaseProxy &;
  static auto gdm() -> gdm::GDM &;
  static auto gdmProxy() -> GDMProxy &;
  static auto initialize() -> void;
  static auto minimumComplexingConcentration() -> double;
  static auto profilesColorizerModel() -> ProfilesColorizerModel &;
};

} // namespace gearbox

#endif // SINGLETONS_H
