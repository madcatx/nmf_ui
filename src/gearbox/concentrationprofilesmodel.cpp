#include "concentrationprofilesmodel.h"

#include <gearbox/gdmproxy.h>
#include <util/util.h>

#include <cassert>

namespace gearbox {

ConcentrationProfilesModel::Edge::Edge(const double _position, const double _width, const double _steepness) :
  position{_position},
  width{_width},
  steepness{_steepness}
{
}

ConcentrationProfilesModel::ConcentrationProfilesModel(GDMProxy &gdmProxy, QObject *parent) :
  QObject{parent},
  h_gdmProxy{gdmProxy}
{
}

auto ConcentrationProfilesModel::add(
  const std::string &name,
  ProfileShape shape,
  std::vector<double> concentrations,
  std::vector<Edge> edges
) -> void
{
  assert(m_data.find(name) == m_data.cend());
  assert([&]()
    { if (shape != ZONES)
        return edges.size() == 1;
      else
        return edges.size() + 1 == concentrations.size();
    }()
  );

  m_data.try_emplace(name, shape, std::move(edges));
  h_gdmProxy.setConcentrations(name, std::move(concentrations));
}

auto ConcentrationProfilesModel::announceUpdate() -> void
{
  emit compositionChanged();
}

auto ConcentrationProfilesModel::cbegin() const noexcept -> Map::const_iterator
{
  return m_data.cbegin();
}

auto ConcentrationProfilesModel::cend() const noexcept -> Map::const_iterator
{
  return m_data.cend();
}

auto ConcentrationProfilesModel::clear() noexcept -> void
{
  m_data.clear();
}

auto ConcentrationProfilesModel::find(const std::string &name) const -> Map::const_iterator
{
  return m_data.find(name);
}

auto ConcentrationProfilesModel::get(const std::string &name) const -> Info
{
  const auto it = m_data.find(name);

  assert(it != m_data.cend());

  return { it->second.first, h_gdmProxy.concentrations(name), it->second.second };
}

auto ConcentrationProfilesModel::remove(const std::string &name) -> void
{
  const auto it = m_data.find(name);

  if (it != m_data.cend())
    m_data.erase(it);
}

auto ConcentrationProfilesModel::set(
  const std::string &name,
  ProfileShape shape,
  std::vector<double> concentrations,
  std::vector<Edge> edges
) -> void
{
  assert([&]()
    { if (shape != ZONES)
        return edges.size() == 1;
      else
        return edges.size() + 1 == concentrations.size();
    }()
  );

  assert(m_data.find(name) != m_data.end());

  m_data.at(name) = { std::move(shape), std::move(edges) };
  h_gdmProxy.setConcentrations(name, std::move(concentrations));
}

auto ConcentrationProfilesModel::boxProfile(
  const std::vector<double> &concentrations,
  const std::vector<Edge> &edges,
  const size_t points,
  const double dx
) -> std::vector<std::pair<double, double>>
{
  assert(concentrations.size() > 1);
  assert(edges.size() == 1);

  std::vector<std::pair<double, double>> profile{};
  profile.resize(points);

  const double cBGE = concentrations[0];
  const double cDelta = concentrations[1] - cBGE;
  const double width = edges[0].width;
  const double position = edges[0].position;
  const double steepness = edges[0].steepness;

  double x = 0.0;
  for (size_t col{0}; col < points; col++) {
    const double centerLeft = position - 0.5 * width;
    const double centerRight = position + 0.5 * width;

    const double vs1 = cDelta * util::sigmoid(x, centerLeft, steepness);
    const double vs2 = cDelta * util::sigmoid(x, centerRight, steepness);

    auto &pt = profile[col];
    pt.first = x;
    pt.second = cBGE + (vs1 - vs2);
    x += dx;
  }

  return profile;
}

auto ConcentrationProfilesModel::updateName(const std::string &oldName, const std::string &newName) -> void
{
  assert(m_data.find(oldName) != m_data.end());

  if (oldName == newName)
    return;

  const auto data = m_data[oldName];
  m_data[newName] = data;
  m_data.erase(oldName);
}

auto ConcentrationProfilesModel::gaussianProfile(
  const std::vector<double> &concentrations,
  const std::vector<Edge> &edges,
  const size_t points,
  const double dx
) -> std::vector<std::pair<double, double>>
{
  assert(concentrations.size() > 1);
  assert(edges.size() == 1);

  std::vector<std::pair<double, double>> profile{};
  profile.resize(points);

  const double cBGE = concentrations[0];
  const double cDelta = concentrations[1] - cBGE;
  const double width = edges[0].width;
  const double position = edges[0].position;

  /* Gaussian sigma to set peak width at 2 % of its height */
  const double sigma = std::sqrt(-std::pow(0.5 *width, 2) / -7.824046010856292);

  double x = 0.0;
  for (size_t col{0}; col < points; col++) {
    const double E = (x - position) / sigma;
    const double vg = cDelta * std::exp(-0.5 * std::pow(E, 2));

    auto &pt = profile[col];
    pt.first = x;
    pt.second = cBGE + vg;

    x += dx;
  }

  return profile;
}

auto ConcentrationProfilesModel::zonedProfile(
  const std::vector<double> &concentrations,
  const std::vector<Edge> &edges,
  const size_t points,
  const double dx
) -> std::vector<std::pair<double, double>>
{
  std::vector<std::pair<double, double>> profile{};

  assert(edges.size() + 1 <= concentrations.size());
  profile.resize(points);

  double x{0.0};
  for (size_t pt{0}; pt < points; pt++) {
    profile[pt] = { x, concentrations[0] };
    x += dx;
  }

  for (size_t idx{0}; idx < edges.size(); idx++) {
    const auto position = edges[idx].position;
    const auto width = edges[idx].width;
    const double cDelta = concentrations[idx+1] - concentrations[idx];

    x = 0.0;
    for (size_t pt{0}; pt < points; pt++) {
      auto &item = profile[pt];
      item.second += cDelta * util::sigmoid(x, position, width);
      x += dx;
    }
  }

  return profile;
}

} // namespace gearbox
