#include "gdmproxy_p.h"

#include <cassert>

namespace gearbox {

auto GDMProxy_p::eraseComplexations(gdm::GDM::const_iterator ctuentIt, gdm::GDM &gdm) -> void
{
  for (auto it = gdm.cbegin(); it != gdm.cend(); it++) {
    if (it->type() == ctuentIt->type())
        continue;
    if (it == ctuentIt)
      continue;
    if (gdm.haveComplexation(ctuentIt, it))
      gdm.eraseComplexation(ctuentIt, it);
  }
}

auto GDMProxy_p::typeChangeOk(
  const gdm::Constituent &newCtuent,
  const gdm::Constituent &oldCtuent, gdm::GDM &_gdm
) -> bool
{
  const auto it = _gdm.find(oldCtuent);

  assert(it != _gdm.cend());

  const auto found = gdm::findComplexations(_gdm.composition(), it);

  return !(!found.empty() && (oldCtuent.type() != newCtuent.type()));
}

} // namespace gearbox
