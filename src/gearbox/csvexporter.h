#ifndef CSVEXPORTER_H
#define CSVEXPORTER_H

#include <QMetaType>
#include <QString>

#include <stdexcept>

namespace calculators {
  class SimStateData;
} // namespace calculators

namespace gearbox {

class CSVExporter {
public:
  class Error : public std::runtime_error {
  public:
    using std::runtime_error::runtime_error;
  };

  enum class Separator {
    PERIOD,
    COMMA
  };

  enum class Target {
    CLIPBOARD,
    FILE
  };

  CSVExporter() = delete;

  static auto write(
    const Target target,
    const Separator separator,
    const QString &delimiter,
    const QString &path,
    const calculators::SimStateData &data,
    const std::vector<std::string> &selected,
    const int trace
  ) -> void;
};

} // namespace gearbox

Q_DECLARE_METATYPE(gearbox::CSVExporter::Separator)

#endif // CSVEXPORTER_H
