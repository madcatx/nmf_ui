#ifndef COMPLEXATIONMANAGER_H
#define COMPLEXATIONMANAGER_H

#include <ui/complexation/complexationrelationship.h>

#include <QObject>
#include <map>
#include <vector>

namespace gdm {
  class Constituent;
  class GDM;
}

class EditComplexationDialog;

namespace gearbox {
  class GDMProxy;

class ComplexationManager : public QObject {
  Q_OBJECT
public:
  explicit ComplexationManager(gdm::GDM &gdm, QObject *parent = nullptr);
  std::vector<int> complexationStatus(const std::string &name) const;
  bool complexes(const std::string &name);
  void editComplexation(const std::string &name);
#ifndef NMF_NO_GUI
  void handleUserInput(EditComplexationDialog *dlg, const std::string &nucleusName, const std::string &ligandName);
#endif // NMF_NO_GUI
  void makeComplexation(const std::string &first, const std::string &second);
  void notifyConstituentRemoved();
  void refreshAll();

private:
  class Hue {
  public:
    Hue() = default;
    Hue(uint8_t _hue, uint8_t _ctr) :
      hue{_hue}, ctr{_ctr}
    {}
    Hue(const Hue &other) = default;

    uint8_t hue;
    uint8_t ctr;
  };

  using ComplexationStatusMap = std::map<std::string, std::vector<int>>;
  using ComplexingNucleiMap = std::map<std::string, Hue>;

  void addNucleus(const std::string &nucleusName);
  void updateComplexingNuclei();
  void updateComplexationStatus();

  gdm::GDM &h_gdm;
  ComplexingNucleiMap m_complexingNuclei;
  ComplexationStatusMap m_complexationStatus;

signals:
  void complexationStatusChanged();
};

} // namespace gearbox

#endif // COMPLEXATIONMANAGER_H
