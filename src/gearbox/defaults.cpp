#include "defaults.h"

namespace gearbox {

const double Defaults::CAPILLARY_LENGTH{25.0};
const int Defaults::CELLS{1000};
const double Defaults::DT{1.0e-4};
const double Defaults::TIME{0.0};
const int Defaults::TOLERANCE{6};
const double Defaults::STOP_TIME{3600};
const double Defaults::VOLTAGE{250};

const ConcentrationProfilesModel::ProfileShape Defaults::CONCENTRATION_PROFILE_SHAPE{ConcentrationProfilesModel::INJECTION_BOX};
const double Defaults::INJECTION_POSITION{3.0};
const double Defaults::INJECTION_WIDTH{1.0};
const double Defaults::INJECTION_STEEPNESS{0.5};

const size_t Defaults::MAXIMUM_BLOCKS{100};

} // namespace gearbox
