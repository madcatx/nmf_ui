#include "databaseproxy.h"

#include <database/db_constituentsdatabase.h>
#include <globals.h>

#include <algorithm>
#include <cassert>
#include <QDir>
#include <QString>

namespace gearbox {

#ifdef Q_OS_LINUX
#ifdef AF_FLATPAK_BUILD
  const char *DatabaseProxy::DATABASE_PATH = "/app/share/" SOFTWARE_NAME_INTERNAL_S "/pmng_db.sql";
#else
  const char *DatabaseProxy::DATABASE_PATH = "~/.local/share/ECHMET/" SOFTWARE_NAME_INTERNAL_S "/pmng_db.sql";
#endif // AF_FLATPAK_BUILD
#else
const char *DatabaseProxy::DATABASE_PATH = "pmng_db.sql";
#endif // Q_OS_LINUX

static
auto makeDatabaseConstituent(const database::Constituent &c) -> DatabaseConstituent
{
  assert(c.chargeLow() <= c.chargeHigh());

  auto pKas = c.pKas();

  const int bChg = database::ConstituentsDatabase::baseCharge(c.chargeLow(), c.chargeHigh());
  assert(pKas.find(bChg) == pKas.cend());
  pKas.emplace(bChg, 0);

  return DatabaseConstituent{c.id(), QString::fromStdString(c.name()), pKas, c.mobilities(), c.chargeLow(), c.chargeHigh()};
}

static
auto makeProperties(
  const std::vector<double> &pKas, const std::vector<double> &mobilities,
  const int chargeLow, const int chargeHigh
) -> std::vector<std::tuple<int, double, double>>
{
  std::vector<std::tuple<int, double, double>> properties{};

  const int bChg = database::ConstituentsDatabase::baseCharge(chargeLow, chargeHigh);
  for (int charge = chargeLow; charge <= chargeHigh; charge++) {
    if (charge == bChg)
      properties.emplace_back(charge, mobilities.at(charge - chargeLow), 0);
    else
      properties.emplace_back(charge, mobilities.at(charge - chargeLow), pKas.at(charge - chargeLow - (charge > bChg)));
  }

  return properties;
}

static
auto doSearch(
  database::ConstituentsDatabase *dbh,
  const database::ConstituentsDatabase::MatchType match, const std::string &name
) -> std::vector<DatabaseConstituent>
{
  database::SearchResults _results{};
  std::vector<DatabaseConstituent> results{};

  database::ConstituentsDatabase::RetCode tRet;
  tRet = dbh->searchByName(name.c_str(), match, _results);
  switch (tRet) {
  case database::ConstituentsDatabase::RetCode::OK:
    break;
  case database::ConstituentsDatabase::RetCode::E_DB_NO_RECORD:
    return {};
  default:
    {
    std::string err{"Database lookup failed: " + dbh->lastDBErrorMessage() + " " + dbh->retCodeToString(tRet)};
    throw DatabaseException{err};
    }
  }

  results.reserve(_results.size());

  for (const auto &c : _results)
    results.emplace_back(makeDatabaseConstituent(c));

  return results;
}

DatabaseProxy::DatabaseProxy()
{
  QString usableDbPath;

  const QString userPath{};

#ifdef Q_OS_LINUX
  if (userPath.isEmpty()) {
    static const QString locPath(".local/share/ECHMET/" SOFTWARE_NAME_INTERNAL_S "/");

    QDir homeDir = QDir::home();
    const QString editableDbPath = homeDir.absolutePath() + "/" + locPath + "pmng_db.sql";
    if (QFileInfo::exists(editableDbPath))
      usableDbPath = editableDbPath;
    else {
      if (!QFileInfo::exists(DATABASE_PATH)) {
        m_db = nullptr;
        return;
      }

      if (!homeDir.mkpath(locPath)) {
        m_db = nullptr;
        return;
      }

      if (!QFile::copy(DATABASE_PATH, editableDbPath)) {
        m_db = nullptr;
        return;
      }

      usableDbPath = editableDbPath;
    }
  } else
    usableDbPath = userPath;
#else
  if (userPath.isEmpty())
    usableDbPath = DATABASE_PATH;
  else
    usableDbPath = userPath;
#endif // Q_OS_LINUX

  try {
    m_db = new database::ConstituentsDatabase{usableDbPath.toUtf8()};
  } catch (const std::runtime_error &) {
    m_db = nullptr;
  }
}

DatabaseProxy::~DatabaseProxy() noexcept
{
  delete m_db;
}

auto DatabaseProxy::addConstituent(
  const std::string &name, const std::vector<double> &pKas,
  const std::vector<double> &mobilities,
  const int chargeLow, const int chargeHigh
) -> bool
{
  const auto properties = makeProperties(pKas, mobilities, chargeLow, chargeHigh);

  const auto tRet = m_db->addConstituent(name.c_str(), chargeLow, chargeHigh, properties);

  return tRet == database::ConstituentsDatabase::RetCode::OK;
}

auto DatabaseProxy::deleteById(const int64_t id) -> bool
{
  return m_db->deleteConstituent(id) == database::ConstituentsDatabase::RetCode::OK;
}

auto DatabaseProxy::editConstituent(
  const int64_t id, const std::string &name, const std::vector<double> &pKas,
  const std::vector<double> &mobilities,
  const int chargeLow, const int chargeHigh
) -> bool
{
  const auto properties = makeProperties(pKas, mobilities, chargeLow, chargeHigh);

  const auto tRet = m_db->editConstituent(id, name.c_str(), chargeLow, chargeHigh, properties);

  return tRet == database::ConstituentsDatabase::RetCode::OK;
}

auto DatabaseProxy::fetchAll() -> std::vector<DatabaseConstituent>
{
  return doSearch(m_db, database::ConstituentsDatabase::MatchType::ENTIRE_DB, std::string{});
}

auto DatabaseProxy::isAvailable() const -> bool
{
  return m_db != nullptr;
}

auto DatabaseProxy::openDatabase(const QString &path) -> bool
{
  delete m_db;

  try {
    m_db = new database::ConstituentsDatabase{path.toLocal8Bit()};
  } catch (const std::runtime_error &) {
    m_db = nullptr;
    return false;
  }

  return true;
}

auto DatabaseProxy::search(const std::string &name, const MatchType matchType) -> std::vector<DatabaseConstituent>
{
  const auto match = [matchType]() {
    switch (matchType) {
      case MatchType::CONTAINS:
        return database::ConstituentsDatabase::MatchType::CONTAINS;
      default:
      return database::ConstituentsDatabase::MatchType::BEGINS_WITH;
    }
  }();

  return doSearch(m_db, match, name);
}

} // namespace gearbox
