#ifndef GDMPROXY_P_H
#define GDMPROXY_P_H

#include <gdm/core/gdm.h>

namespace gearbox {

class GDMProxy_p {
public:
  static auto eraseComplexations(gdm::GDM::const_iterator ctuentIt, gdm::GDM &gdm) -> void;
  static auto typeChangeOk(const gdm::Constituent &newCtuent, const gdm::Constituent &oldCtuent, gdm::GDM &gdm) -> bool;
};

} // namespace gearbox

#endif // GDMPROXY_P_H
