#include "constituentmanipulator.h"

#include <gdm/core/common/gdmexcept.h>
#include <gdm/core/constituent/physicalproperties.h>
#include <gearbox/gdmproxy.h>
#include <gearbox/iconstituenteditor.h>
#include <util/lowlevel.h>

#ifndef NMF_NO_GUI
#include <QMessageBox>
#else
#include <iostream>
#endif // NMF_NO_GUI

namespace gearbox {

ConstituentManipulator::ConstituentManipulator(const GDMProxy &proxy) :
  h_proxy{proxy}
{
}

auto ConstituentManipulator::makeConstituent(const IConstituentEditor *editor) -> gdm::Constituent
{
  const auto type = [](IConstituentEditor::ConstituentType _type) {
    switch (_type) {
      case IConstituentEditor::ConstituentType::NUCLEUS:
        return gdm::ConstituentType::Nucleus;
      case IConstituentEditor::ConstituentType::LIGAND:
        return gdm::ConstituentType::Ligand;
    }
    util::impossible_path();
  }(editor->type());
  const auto name = editor->name().toStdString();
  auto mobilities = editor->mobilities();
  auto pKas = editor->pKas();
  const int chargeLow = editor->chargeLow();
  const int chargeHigh = editor->chargeHigh();
  const double viscosityCoefficient = editor->viscosityCoefficient();
  gdm::PhysicalProperties physProps{
    gdm::ChargeInterval{chargeLow, chargeHigh},
    pKas,
    mobilities,
    viscosityCoefficient
  };

  return gdm::Constituent{type, name, std::move(physProps)};
}

auto ConstituentManipulator::isTypeChangeAllowed(const std::string &name) const -> bool
{
  assert(h_proxy.contains(name));

  return h_proxy.complexes(name);
}

auto ConstituentManipulator::onValidateConstituentInput(const IConstituentEditor *dlg, bool *ok) -> void
{
  *ok = validateConstituentProperties(dlg);

  if (h_proxy.contains(dlg->name().toStdString())) {
#ifndef  NMF_NO_GUI
    QMessageBox mbox{QMessageBox::Warning, tr("Invalid constituent"), QString(tr("Constituent with name \"%1\" is already present")).arg(dlg->name())};
    mbox.exec();
#else
    std::cout << "Invalid constituent: Constituent with name \"" << dlg->name().toStdString() << "\" is already present";
#endif // NMF_NO_GUI
    *ok = false;
  }
}

auto ConstituentManipulator::onValidateConstituentInputUpdate(const IConstituentEditor *dlg, bool *ok) -> void
{
  *ok = validateConstituentProperties(dlg);
}

auto ConstituentManipulator::validateConstituentProperties(const IConstituentEditor *dlg) -> bool
{
  const QString name = dlg->name();
  const auto pKas = dlg->pKas();
  const auto mobilities = dlg->mobilities();
  const int chargeLow = dlg->chargeLow();
  const int chargeHigh = dlg->chargeHigh();
  const double viscosityCoefficient = dlg->viscosityCoefficient();

  if (name.isEmpty()) {
#ifndef NMF_NO_GUI
    QMessageBox mbox{QMessageBox::Warning, QObject::tr("Invalid constituent properties"), QObject::tr("Constituent must have a name")};
    mbox.exec();
#else
    std::cout << "Invalid constituent properties: Constituent must have a name\n";
#endif // NMF_NO_GUI
    return false;
  }

  /* Try to create physical properties of constituent */
  try {
    gdm::PhysicalProperties props{gdm::ChargeInterval{chargeLow, chargeHigh}, pKas, mobilities, viscosityCoefficient};
  } catch (gdm::InvalidArgument &ex) {
#ifndef NMF_NO_GUI
    QMessageBox mbox{QMessageBox::Warning, QObject::tr("Invalid constituent properties"), ex.what()};
    mbox.exec();
#else
    std::cout << "Invalid constituent properties: " << ex.what() << "\n";
#endif // NMF_NO_GUI
    return false;
  }

  return true;
}

} // namespace gearbox
