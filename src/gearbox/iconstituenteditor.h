#ifndef ICONSTITUENTEDITOR_H
#define ICONSTITUENTEDITOR_H

#include <vector>
#include <QString>
#include <QObject>
#include <QMetaObject>

class IConstituentEditor {
public:
  enum class ConstituentType {
    NUCLEUS,
    LIGAND
  };

  virtual ~IConstituentEditor();

  virtual auto chargeLow() const -> int = 0;
  virtual auto chargeHigh() const -> int = 0;
  virtual auto mobilities() const -> std::vector<double> = 0;
  virtual auto name() const -> QString = 0;
  virtual auto pKas() const -> std::vector<double> = 0;
  virtual auto type() const -> ConstituentType = 0;
  virtual auto viscosityCoefficient() const -> double = 0;
};

Q_DECLARE_METATYPE(IConstituentEditor::ConstituentType)
#endif // ICONSTITUENTEDITOR_H
