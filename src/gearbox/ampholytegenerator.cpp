#include "ampholytegenerator.h"

#include <gdm/core/gdm.h>
#include <gearbox/gdmproxy.h>
#include <gearbox/defaults.h>
#include <gearbox/concentrationprofilesmodel.h>
#include <gearbox/profilescolorizermodel.h>
#include <gearbox/singletons.h>

#include <cassert>

namespace gearbox {

auto AmpholyteGenerator::generate(
  const int numAmpholytes,
  const std::string &prefix,
  const std::vector<double> &mobilities,
  const std::vector<double> &basepKas,
  const std::vector<double> &pKaSteps,
  const int chargeLow,
  const int chargeHigh
) -> RetCode
{
  assert(mobilities.size() == basepKas.size() + 1);
  assert(basepKas.size() == pKaSteps.size());
  assert(chargeHigh > chargeLow);

  if (numAmpholytes < 1)
    return RetCode::E_NUM_AMPHOLYTES;
  if (prefix.empty())
    return RetCode::E_PREFIX;

  /* Check that final pKas do not cross each other */
  for (size_t idx{1}; idx < basepKas.size(); idx++) {
    const auto baseFirst = basepKas[idx - 1];
    const auto baseSecond = basepKas[idx];
    const auto stepFirst = pKaSteps[idx - 1];
    const auto stepSecond = pKaSteps[idx];

    if (baseFirst <= baseSecond)
      return RetCode::E_INVERTED_PKAS;
    const auto finalFirst = baseFirst + stepFirst * numAmpholytes;
    const auto finalSecond = baseSecond + stepSecond * numAmpholytes;

    if (finalFirst <= finalSecond)
        return RetCode::E_CROSSING_PKAS;
  }

  auto &gdmProxy = Singletons::gdmProxy();
  auto &concProfsModel = Singletons::concentrationProfilesModel();
  auto &profClrModel = Singletons::profilesColorizerModel();

  auto pKas = basepKas;
  for (int ctr{0}; ctr < numAmpholytes; ctr++) {
    gdm::PhysicalProperties props{
      gdm::ChargeInterval(chargeLow, chargeHigh),
      pKas,
      mobilities,
      0.0
    };
    gdm::Constituent ctuent{gdm::ConstituentType::Nucleus, prefix + "_" + std::to_string(ctr + 1), std::move(props)};

    for (size_t idx{0}; idx < pKaSteps.size(); idx++)
      pKas[idx] += pKaSteps[idx];

    if (!gdmProxy.insert(std::move(ctuent))) {
      concProfsModel.announceUpdate();
      return RetCode::E_CONSTITUENT;
    }

    concProfsModel.add(
      ctuent.name(),
      gearbox::Defaults::CONCENTRATION_PROFILE_SHAPE,
      { 0.0, 0.0 },
      {{ gearbox::Defaults::INJECTION_POSITION, gearbox::Defaults::INJECTION_WIDTH }}
    );
    profClrModel.add(ctuent.name());
  }

  concProfsModel.announceUpdate();

  return RetCode::OK;
}

} // namespace gearbox
