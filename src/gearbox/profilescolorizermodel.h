#ifndef PROFILESCOLORIZERMODEL_H
#define PROFILESCOLORIZERMODEL_H

#include <QColor>
#include <QObject>

#include <map>

namespace gearbox {

class ProfilesColorizerModel : public QObject {
  Q_OBJECT
public:
  explicit ProfilesColorizerModel(QObject *parent = nullptr);

  auto add(const std::string &name, QColor color = QColor{}) -> void;
  auto changeColor(const std::string &name, QColor newColor) -> void;
  auto clear() noexcept -> void;
  auto get(const std::string &name) const -> QColor;
  auto remove(const std::string &name) -> void;
  auto reset() -> void;
  auto updateName(const std::string &oldName, const std::string &newName) -> void;

private:
  auto advanceColor() -> void;

  uint8_t m_hue;
  uint8_t m_saturation;
  uint8_t m_value;

  std::map<std::string, QColor> m_colorMap;

signals:
  void colorChanged(const QString &name);
};

} // namespace gearbox

#endif // PROFILESCOLORIZERMODEL_H
