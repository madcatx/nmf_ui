#ifndef CONCENTRATIONPROFILESMODEL_H
#define CONCENTRATIONPROFILESMODEL_H

#include <QObject>

#include <map>
#include <string>
#include <utility>
#include <vector>

namespace gearbox {

class GDMProxy;

class ConcentrationProfilesModel : public QObject {
  Q_OBJECT
public:
  enum ProfileShape {
    INJECTION_BOX,
    INJECTION_GAUSS,
    ZONES
  };

  class Edge {
  public:
    Edge(const double position, const double width, const double steepness = 1);

    double position;
    double width;
    double steepness; /* Used only by box profile */
  };

  class Info {
  public:
    ProfileShape shape;
    std::vector<double> concentrations;
    std::vector<Edge> edges;
  };

  using Map = std::map<std::string, std::pair<ProfileShape, std::vector<Edge>>>;

  explicit ConcentrationProfilesModel(GDMProxy &gdmProxy, QObject *parent = nullptr);
  auto announceUpdate() -> void;
  auto add(
    const std::string &name,
    ProfileShape shape,
    std::vector<double> concentrations,
    std::vector<Edge> edges
  ) -> void;
  auto cbegin() const noexcept -> Map::const_iterator;
  auto cend() const noexcept -> Map::const_iterator;
  auto clear() noexcept -> void;
  auto find(const std::string &name) const -> Map::const_iterator;
  auto get(const std::string &name) const -> Info;
  auto remove(const std::string &name) -> void;
  auto set(
    const std::string &name,
    ProfileShape shape,
    std::vector<double> concentrations,
    std::vector<Edge> edges
  ) -> void;
  auto updateName(const std::string &oldName, const std::string &newName) -> void;

  static
  auto boxProfile(
    const std::vector<double> &concentrations,
    const std::vector<Edge> &edges,
    const size_t points,
    const double dx
  ) -> std::vector<std::pair<double, double>>;

  static
  auto gaussianProfile(
    const std::vector<double> &concentrations,
    const std::vector<Edge> &edges,
    const size_t points,
    const double dx
  ) -> std::vector<std::pair<double, double>>;

  static
  auto zonedProfile(
    const std::vector<double> &concentrations,
    const std::vector<Edge> &edges,
    const size_t points,
    const double dx
  ) -> std::vector<std::pair<double, double>>;

private:
  GDMProxy &h_gdmProxy;

  Map m_data;

signals:
  void compositionChanged();
};

} // namespace gearbox

#endif // CONCENTRATIONPROFILESMODEL_H
