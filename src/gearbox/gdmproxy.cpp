#include "gdmproxy.h"

namespace gearbox {

GDMProxy::GDMProxy(const double minimumComplexingConcentration) :
  m_minimumComplexingConcentration{minimumComplexingConcentration}
{
}

GDMProxy::~GDMProxy()
{
}

} // namespace gearbox
