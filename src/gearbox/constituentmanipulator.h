#ifndef CONSTITUENTMANIPULATOR_H
#define CONSTITUENTMANIPULATOR_H

#include <gdm/core/constituent/constituent.h>

#include <QObject>

namespace gearbox {
  class DatabaseProxy;
  class GDMProxy;
} // namespace gearbox

class IConstituentEditor;

namespace gearbox {

class ConstituentManipulator : public QObject {
  Q_OBJECT
public:
  explicit ConstituentManipulator(const GDMProxy &proxy);
  //EditConstituentDialog * makeEditDialog(const std::string &name, GDMProxy &proxy, DatabaseProxy &dbProxy);
  auto isTypeChangeAllowed(const std::string &name) const -> bool;

  static auto makeConstituent(const IConstituentEditor *editor) -> gdm::Constituent;
  static auto validateConstituentProperties(const IConstituentEditor *dlg) -> bool;

public slots:
  void onValidateConstituentInput(const IConstituentEditor *dlg, bool *ok);
  void onValidateConstituentInputUpdate(const IConstituentEditor *dlg, bool *ok);

private:
  const GDMProxy &h_proxy;
};

} // namespace gearbox

#endif // CONSTITUENTMANIPULATOR_H
