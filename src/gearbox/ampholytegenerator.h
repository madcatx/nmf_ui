#ifndef AMPHOLYTEGENERATOR_H
#define AMPHOLYTEGENERATOR_H

#include <string>
#include <vector>

namespace gearbox {

class AmpholyteGenerator {
public:
  enum class RetCode {
    OK,
    E_NUM_AMPHOLYTES,
    E_PREFIX,
    E_INVERTED_PKAS,
    E_CROSSING_PKAS,
    E_CONSTITUENT
  };

  AmpholyteGenerator() = delete;

  static auto generate(
    const int numAmpholytes,
    const std::string &prefix,
    const std::vector<double> &mobilities,
    const std::vector<double> &basepKas,
    const std::vector<double> &pKaSteps,
    const int chargeLow,
    const int chargeHigh
  ) -> RetCode;
};

} // namespace gearbox

#endif // AMPHOLYTEGENERATOR_H
