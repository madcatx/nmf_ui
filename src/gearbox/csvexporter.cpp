#include "csvexporter.h"

#include <calculators/nmfinterfacetypes.h>

#ifndef NMF_NO_GUI
#include <QClipboard>
#include <QGuiApplication>
#else
#include <iostream>
#endif // NMF_NO_GUI
#include <QFile>
#include <QLocale>
#include <QTextCodec>
#include <QTextStream>

#include <algorithm>
#include <cassert>

namespace gearbox {

using ProfToWrite = std::vector<const std::vector<double> *>;
using IdxToWrite = std::vector<size_t>;

static
auto writeDetectorHeader(const std::vector<std::string> &selected, QTextStream &stm, const QChar &delimiter)
{
  stm << "Time (s)";

  for (const auto &item : selected)
    stm << delimiter << item.c_str();
  stm << "\n";
}

static
auto writeProfilesHeader(const std::vector<std::string> &selected, QTextStream &stm, const QChar &delimiter)
{
  stm << "Cell" << delimiter << "x (mm)";

  for (const auto &item : selected)
    stm << delimiter << item.c_str();
  stm << "\n";
}

static
auto writeProfiles(const ProfToWrite &profiles, QTextStream &stm, const QChar &delimiter, const double dx)
{
  auto len = profiles.at(0)->size();

  double x{0};
  for (size_t cell{0}; cell < len; cell++) {
    stm << cell << delimiter << x * 1000.0;

    for (const auto &v : profiles)
      stm << delimiter << v->at(cell);
    stm << "\n";
    x += dx;
  }
}

static
auto writeDetector(
  const IdxToWrite &indices,
  QTextStream &stm,
  const QChar &delimiter,
  const std::vector<std::pair<double, std::vector<double>>> &det)
{
  for (const auto &item : det) {
    stm << item.first << delimiter;

    for (const auto &idx : indices)
      stm << item.second[idx] << delimiter;
    stm << "\n";
  }
}

static
auto processConcentrations(QTextStream &stm, const std::vector<std::string> &selected, const QChar &delimiter, const calculators::SimStateData &data)
{
  ProfToWrite profilesToWrite{};

  auto it = std::find_if(selected.cbegin(), selected.cend(), [](const auto &v){return v == std::string{"Conductivity"};});
  if (it != selected.cend())
    profilesToWrite.emplace_back(&data.conductivity);

  it = std::find_if(selected.cbegin(), selected.cend(), [](const auto &v){return v == std::string{"pH"};});
  if (it != selected.cend())
    profilesToWrite.emplace_back(&data.pH);

  for (size_t idx{0}; idx < data.constituentNames.size(); idx++) {
    const auto &name = data.constituentNames[idx];
    it = std::find_if(selected.cbegin(), selected.cend(), [&name](const auto &v){return v == name;});
    if (it != selected.cend())
      profilesToWrite.emplace_back(&data.concentrations[idx]);
  }

  if (profilesToWrite.empty())
    throw CSVExporter::Error{"No profiles to export"};

  writeProfilesHeader(selected, stm, delimiter);
  writeProfiles(profilesToWrite, stm, delimiter, data.dx);
}

static
auto processDetector(
  QTextStream &stm,
  const std::vector<std::string> &selected,
  const QChar &delimiter,
  const int trace,
  const calculators::SimStateData &data
) {
  assert(trace < data.detectorTraces->size());

  auto det = data.detectorTraces->at(trace);

  IdxToWrite toWrite{};
  for (size_t idx{0}; idx < data.constituentNames.size(); idx++) {
    const auto &name = data.constituentNames[idx];
    auto it = std::find_if(selected.cbegin(), selected.cend(), [&name](const auto &v){return v == name;});
    if (it != selected.cend())
      toWrite.emplace_back(idx);
  }

  if (toWrite.empty())
    throw CSVExporter::Error{"No profiles to export"};

  writeDetectorHeader(selected, stm, delimiter);
  writeDetector(toWrite, stm, delimiter, det);
}

auto CSVExporter::write(
  const Target target,
  const Separator separator,
  const QString &delimiter,
  const QString &path,
  const calculators::SimStateData &data,
  const std::vector<std::string> &selected,
  const int trace
) -> void
{
  QChar delim{};
  if (delimiter == "\t")
    delim = QChar::Tabulation;
  else if (delimiter.length() > 1 || delimiter.isEmpty())
    throw Error{ "Invalid field delimiter" };
  else
    delim = delimiter.at(0);

  QString output{};

  auto loc = [separator]() {
    switch (separator) {
    case Separator::COMMA:
      return QLocale{QLocale::Czech};
    case Separator::PERIOD:
      return QLocale{QLocale::English};
    }

    assert(false);
#ifdef NDEBUG
    std::abort();
#endif // NDEBUG
  }();
  loc.setNumberOptions(QLocale::OmitGroupSeparator);

  QTextStream stm{&output, QIODevice::WriteOnly};
  stm.setLocale(loc);
  stm.setCodec(QTextCodec::codecForName("UTF-8"));
  stm.setRealNumberNotation(QTextStream::ScientificNotation);
  stm.setRealNumberPrecision(10);

  if (trace == -1)
    processConcentrations(stm, selected, delim, data);
  else
    processDetector(stm, selected, delim, trace, data);

  switch (target) {
  case Target::FILE:
  {
    QFile fh{path};

    if (!fh.open(QIODevice::WriteOnly))
      throw Error{"Failed to open output file"};

    fh.write(output.toUtf8());
    break;
  }
  case Target::CLIPBOARD:
  {
#ifndef NMF_NO_GUI
    auto clipboard = QGuiApplication::clipboard();
    clipboard->setText(output);
#else
    std::cout << "Clipboard is supported only in GUI frontend\n";
#endif // NMF_NO_GUI
    break;
  }
  }
}

} // namespace gearbox
