#ifndef DATABASEPROXY_H
#define DATABASEPROXY_H

#include <map>
#include <stdexcept>
#include <QObject>
#include <QString>
#include <vector>

namespace database {
  class ConstituentsDatabase;
}

namespace gearbox {

using DBChargePropsMap = std::map<int, double>;

class DatabaseConstituent {
public:
  const int64_t id;
  const QString name;
  const DBChargePropsMap pKas;
  const DBChargePropsMap mobilities;
  const int chargeLow;
  const int chargeHigh;
};

class DatabaseException : public std::runtime_error
{
  using std::runtime_error::runtime_error;
};

class DatabaseProxy {
  Q_GADGET
public:
  enum class MatchType {
    BEGINS_WITH,
    CONTAINS
  };
  Q_ENUM(MatchType)

  DatabaseProxy();
  ~DatabaseProxy() noexcept;
  auto addConstituent(const std::string &name, const std::vector<double> &pKas, const std::vector<double> &mobilities, const int chargeLow, const int chargeHigh) -> bool;
  auto deleteById(const int64_t id) -> bool;
  auto editConstituent(const int64_t id, const std::string &name, const std::vector<double> &pKas, const std::vector<double> &mobilities, const int chargeLow, const int chargeHigh) -> bool;
  auto fetchAll() -> std::vector<DatabaseConstituent>;
  auto isAvailable() const -> bool;
  auto openDatabase(const QString &path) -> bool;
  auto search(const std::string &name, const MatchType matchType) -> std::vector<DatabaseConstituent>;

private:
  database::ConstituentsDatabase *m_db;

  static const char *DATABASE_PATH;
};

} // namespace gearbox

Q_DECLARE_METATYPE(gearbox::DatabaseProxy::MatchType)

#endif // DATABASEPROXY_H
