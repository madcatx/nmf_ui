#include "crashhandlerbase.h"

CrashHandlerBase::CrashHandlerBase(const std::string &miniDumpPath) :
  m_miniDumpPath(miniDumpPath),
  m_finalizer(nullptr)
{
}

auto CrashHandlerBase::finalize() -> void
{
  if (m_finalizer)
    (*m_finalizer)();
}

auto CrashHandlerBase::proceedToKill() const -> void
{
  return;
}

auto CrashHandlerBase::setFinalizer(CrashHandlerFinalizerRoot *finalizer) -> void
{
  m_finalizer = finalizer;
}

auto CrashHandlerBase::waitForKill() -> void
{
  return;
}
