#ifndef CRASHHANDLERLINUX_H
#define CRASHHANDLERLINUX_H

#ifdef CRASHHANDLING_LINUX

#include <crashhandling/crashhandlerbase.h>
#include <array>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include <ucontext.h>

class CrashHandlerLinux : public CrashHandlerBase {
public:
  explicit CrashHandlerLinux(const std::string &miniDumpPath);
  virtual ~CrashHandlerLinux() override;
  virtual auto crashInfo() const -> const std::string & override;
  virtual auto install() -> bool override;
  virtual auto proceedToKill() const -> void override;
  virtual auto mainThreadCrashed() const -> bool override;
  virtual auto uninstall() -> void override;
  virtual auto waitForKill() -> void override;

private:
  class ChildParam {
  public:
    pid_t pid;

  };

  class CrashContext {
  public:
    siginfo_t siginfo;                  /* Currently unused */
    pid_t exceptionThreadId;
    ucontext_t uctx;                    /* Currently unused */
    struct _libc_fpstate fpuState;

  };

  auto generateMiniDump(const int signum) -> bool;
  auto handleSignal(siginfo_t *siginfo, void *vuctx) -> bool;
  auto restoreHandler(const int signum) -> void;
  auto restoreOriginalStack() -> bool;
  static auto alternateStackSize() -> size_t;
  static auto clonedProcessFunc(void *param) -> int;
  static auto crashHandlerFunc(int signum, siginfo_t *siginfo, void *vuctx) -> void;
  static auto retrigger(const int signum) -> bool;

  CrashContext m_crashCtx;
  std::string m_crashInfo;

  pthread_mutex_t m_handlerMutex;
  stack_t m_originalStack;
  stack_t m_alternateStack;
  std::array<struct sigaction, 6> m_originalHandlers;
  std::array<struct sigaction, 6> m_crashHandlers;
  int m_pipeDes[2];
  sem_t m_waitForKillSemaphore;
  bool m_installed;

  const pid_t m_mainThreadId;

  static const std::array<int, 6> m_handledSignals;
};

#endif // CRASHHANDLING_LINUX

#endif // CRASHHANDLERLINUX_H
