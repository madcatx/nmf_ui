#include <crashhandling/nullcrashhandler.h>

NullCrashHandler::NullCrashHandler(const std::string &) :
  CrashHandlerBase(""),
  m_nullCrashInfo("")
{
}

auto NullCrashHandler::crashInfo() const -> const std::string &
{
  return m_nullCrashInfo;
}

auto NullCrashHandler::install() -> bool
{
  return true;
}

auto NullCrashHandler::mainThreadCrashed() const -> bool
{
  return true;
}

auto NullCrashHandler::uninstall() -> void
{
  return;
}
