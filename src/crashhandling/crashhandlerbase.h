#ifndef CRASHHANDLERBASE_H
#define CRASHHANDLERBASE_H

#include <crashhandling/crashhandlerfinalizer.h>
#include <string>

class CrashHandlerBase {
public:
  explicit CrashHandlerBase(const std::string &miniDumpPath);
  virtual ~CrashHandlerBase() {}
  virtual auto crashInfo() const -> const std::string & = 0;
  virtual auto install() -> bool = 0;
  virtual auto mainThreadCrashed() const -> bool = 0;
  virtual auto proceedToKill() const -> void;
  virtual auto uninstall() -> void = 0;
  auto setFinalizer(CrashHandlerFinalizerRoot *finalizer) -> void;
  virtual auto waitForKill() -> void;

protected:
  auto finalize() -> void;

  const std::string m_miniDumpPath;

private:
  CrashHandlerFinalizerRoot *m_finalizer;

};

#endif // CRASHHANDLERBASE_H
