#ifndef NULLCRASHHANDLER_H
#define NULLCRASHHANDLER_H

#include "crashhandlerbase.h"

class NullCrashHandler : public CrashHandlerBase {
public:
  explicit NullCrashHandler(const std::string &);

  virtual auto crashInfo() const -> const std::string & override;
  virtual auto mainThreadCrashed() const -> bool override;
  virtual auto install() -> bool override;
  virtual auto uninstall() -> void override;

private:
  const std::string m_nullCrashInfo;
};

#endif // NULLCRASHHANDLER_H
