#include "setup.h"

#include <gdm/core/gdm.h>
#include <gearbox/concentrationprofilesmodel.h>
#ifndef NMF_NO_GUI
#include <gearbox/profilescolorizermodel.h>
#endif // NMF_NO_GUI
#include <gearbox/singletons.h>
#include <persistence/types.h>
#include <persistence/util.h>

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include <cassert>

namespace persistence::setup {

static
auto generateFromNucleusComplexForm(const std::string &ligandName, gdm::ChargeNumber ligandCharge, const gdm::ComplexForm &complexForm)
{
  QJsonObject obj{};

  obj[CPX_NAME] = QString::fromStdString(ligandName);
  obj[CPX_CHARGE] = ligandCharge;
  obj[CPX_MAX_COUNT] = static_cast<int>(complexForm.pBs().size());
  obj[CPX_PBS] = vectorToArray(complexForm.pBs());
  obj[CPX_MOBILITIES] = vectorToArray(complexForm.mobilities());

  return obj;
}

static
auto shapeToString(const gearbox::ConcentrationProfilesModel::ProfileShape shape)
{
  switch (shape) {
  case gearbox::ConcentrationProfilesModel::INJECTION_BOX:
    return CONC_SHAPE_BOX;
  case gearbox::ConcentrationProfilesModel::INJECTION_GAUSS:
    return CONC_SHAPE_GAUSS;
  case gearbox::ConcentrationProfilesModel::ZONES:
    return CONC_SHAPE_ZONE;
  }

  assert(false);
#ifdef NDEBUG
  std::abort();
#endif // NDEBUG
}

static
auto typeToString(const gdm::ConstituentType type)
{
  switch (type) {
  case gdm::ConstituentType::Nucleus:
    return CTUENT_TYPE_NUCLEUS;
  case gdm::ConstituentType::Ligand:
    return CTUENT_TYPE_LIGAND;
  }

  assert(false);
#ifdef NDEBUG
  std::abort();
#endif // NDEBUG
}

static
auto serializeConcentrations(QJsonObject &obj, const gdm::Constituent &c)
{
  const auto &concProfsModel = gearbox::Singletons::concentrationProfilesModel();

  assert(concProfsModel.find(c.name()) != concProfsModel.cend());

  const auto &info = concProfsModel.get(c.name());

  QJsonArray edges{};
  for (const auto &e : info.edges) {
    QJsonObject eo{};
    eo[EDGE_POSITION] = e.position;
    eo[EDGE_WIDTH] = e.width;

    edges.append(eo);
  }

  assert(info.concentrations.size() > info.edges.size());
  QJsonArray concs{};
  for (size_t idx{0}; idx < info.edges.size() + 1; idx++)
    concs.append(info.concentrations[idx]);

  obj[CONC_SHAPE] = shapeToString(info.shape);
  obj[CONC_EDGES] = std::move(edges);
  obj[CONC_VALUES] = std::move(concs);
}

static
auto serializeComposition()
{
  QJsonArray array{};

  const auto &gdm = gearbox::Singletons::gdm();
#ifndef NMF_NO_GUI
  const auto &colorizer = gearbox::Singletons::profilesColorizerModel();
#endif // NMF_NO_GUI

  for (auto it = gdm.cbegin(); it != gdm.cend(); it++) {
    QJsonObject obj{};

    const auto &c = *it;
    const auto &pp = c.physicalProperties();
#ifndef NMF_NO_GUI
    const auto color = colorizer.get(c.name());
#endif // NMF_NO_GUI

    /* Common properties */
    obj[CTUENT_TYPE] = typeToString(c.type());
    obj[CTUENT_NAME] = QString::fromStdString(c.name());
    obj[CTUENT_CHARGE_LOW] = pp.charges().low();
    obj[CTUENT_CHARGE_HIGH] = pp.charges().high();
    obj[CTUENT_PKAS] = vectorToArray(pp.pKas());
    obj[CTUENT_MOBILITIES] = vectorToArray(pp.mobilities());
    obj[CTUENT_VISCOSITY_COEFFICIENT] = pp.viscosityCoefficient();
#ifndef NMF_NO_GUI
    obj[CTUENT_PROFILE_COLOR] = color.name();
#else
    obj[CTUENT_PROFILE_COLOR] = "#000000";
#endif // NMF_NO_GUI

    /* Concentration profiles */
    serializeConcentrations(obj, c);

    /* Complexations */
    if (c.type() == gdm::ConstituentType::Nucleus) {
      QJsonArray complexForms{};

      auto nucleusCharges = pp.charges();
      auto foundComplexations = findComplexations(gdm.composition(), it);

      for (auto nucleusCharge = nucleusCharges.low(); nucleusCharge <= nucleusCharges.high(); ++nucleusCharge) {
        QJsonObject complexForm{};

        complexForm[CPX_NUCLEUS_CHARGE] = nucleusCharge;

        QJsonArray ligandGroups{};

        for (const auto &foundComplexation : foundComplexations) {
          assert(foundComplexation.first->type() == gdm::ConstituentType::Ligand);

          const auto& ligandIt = foundComplexation.first;
          const auto& complexation = foundComplexation.second;

          auto ligandCharges = ligandIt->physicalProperties().charges();
          for (auto ligandCharge = ligandCharges.low(); ligandCharge <= ligandCharges.high(); ++ligandCharge) {
            gdm::ChargeCombination charges{nucleusCharge, ligandCharge};

            auto complexFormIt = complexation.find(charges);

            if (complexFormIt != complexation.end()) {
              auto ligand = generateFromNucleusComplexForm(ligandIt->name(), ligandCharge, *complexFormIt);

              QJsonObject ligandGroup{{CPX_LIGANDS, QJsonArray{ligand}}};

              ligandGroups.push_back(ligandGroup);
            }
          }
        }
        complexForm[CPX_LIGAND_GROUPS] = ligandGroups;

        complexForms.push_back(std::move(complexForm));
      }
      obj[CPX_COMPLEX_FORMS] = complexForms;
    }
    array.append(std::move(obj));
  }

  return array;
}

static
auto serializeDetectors(const std::vector<std::pair<bool, double>> &detectors)
{
  QJsonArray array{};

  for (const auto &det : detectors) {
    QJsonObject obj{};
    obj[SYS_DETECTOR_ENABLED] = det.first;
    obj[SYS_DETECTOR_POSITION] = det.second;

    array.append(obj);
  }

  return array;
}

static
auto serializeSystem(const System &sys)
{
  QJsonObject obj{};

  obj[SYS_AUTO_DT] = sys.autoDt;
  obj[SYS_DT] = sys.dt;
  obj[SYS_STOP_TIME] = sys.stopTime;
  obj[SYS_CAPILLARY_LENGTH] = sys.capillaryLength;
  obj[SYS_VOLTAGE] = sys.voltage;
  obj[SYS_CURRENT] = sys.current;
  obj[SYS_CONSTANT_FORCE] = sys.constantForce;
  obj[SYS_CELLS] = sys.cells;
  obj[SYS_EOF_VALUE] = sys.eofValue;
  obj[SYS_EOF_MODE] = sys.eofMode;
  obj[SYS_DIAMETER_SEGMENTS] = vectorToArray(sys.diameterSegments);
  obj[SYS_DIAMETER_VALUES] = vectorToArray(sys.diameterValues);
  obj[SYS_TOLERANCE] = sys.tolerance;
  obj[SYS_FISRT_CELL] = sys.firstCell;
  obj[SYS_LAST_CELL] = sys.lastCell;
  obj[SYS_AUTO_CELLS] = sys.autoCells;
  obj[SYS_UPDATE_INTERVAL] = sys.updateInterval;
  obj[SYS_DETECTORS] = serializeDetectors(sys.detectors);

  return obj;
}

auto save(QString path, const System &sys) -> void
{
  if (!path.endsWith(".json", Qt::CaseInsensitive))
    path.append(".json");

  QFile fh{path};

  if (!fh.open(QIODevice::WriteOnly | QIODevice::Text))
    throw Error{"Cannot open output file"};

  auto str = toJson(sys);

  fh.write(str);
}

auto toJson(const System &sys) -> QByteArray
{
  const auto serSystem = serializeSystem(sys);
  const auto serComposition = serializeComposition();

  QJsonDocument doc{
    {
      {ROOT_SYSTEM, serSystem},
      {ROOT_COMPOSITION, serComposition}
    }
  };

  return doc.toJson();
}

} // persistence::setup
