#ifndef PERSISTENCE_H
#define PERSISTENCE_H

#include <QString>

#include <string>

namespace persistence {

class System;

auto loadSetup(const QString &path, System &sys) -> void;
auto loadSetupFromRaw(const std::string &json, System &sys) -> void;
auto saveSetup(const QString &path, const System &sys) -> void;
auto toJson(const System &sys) -> QByteArray;

} // namespace persistence

#endif // PERSISTENCE_H
