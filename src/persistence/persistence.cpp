#include "persistence.h"

#include <gdm/core/gdm.h>
#include <gearbox/complexationmanager.h>
#include <gearbox/concentrationprofilesmodel.h>
#ifndef NMF_NO_GUI
#include <gearbox/profilescolorizermodel.h>
#endif // NMF_NO_GUI
#include <gearbox/singletons.h>
#include <persistence/setup.h>
#include <persistence/types.h>

namespace persistence {

static
auto clearAll()
{
  gearbox::Singletons::gdm().clear();
  gearbox::Singletons::concentrationProfilesModel().clear();
  gearbox::Singletons::concentrationProfilesModel().announceUpdate();
  gearbox::Singletons::complexationManager().refreshAll();
#ifndef NMF_NO_GUI
  gearbox::Singletons::profilesColorizerModel().clear();
  gearbox::Singletons::profilesColorizerModel().reset();
#endif // NMF_NO_GUI
}

auto loadSetup(const QString &path, System &sys) -> void
{
  clearAll();

  try {
    setup::load(path, sys);
  } catch (const Error &) {
    clearAll();
    throw;
  }

  gearbox::Singletons::concentrationProfilesModel().announceUpdate();
  gearbox::Singletons::complexationManager().refreshAll();
}

auto loadSetupFromRaw(const std::string &json, System &sys) -> void
{
  clearAll();

  try {
    const QByteArray raw{json.c_str()};
    setup::loadRaw(raw, sys);
  } catch (const Error &) {
    clearAll();
    throw;
  }

  gearbox::Singletons::concentrationProfilesModel().announceUpdate();
  gearbox::Singletons::complexationManager().refreshAll();
}

auto saveSetup(const QString &path, const System &sys) -> void
{
  setup::save(path, sys);
}

auto toJson(const System &sys) -> QByteArray
{
  return setup::toJson(sys);
}

} // namespace persistence
