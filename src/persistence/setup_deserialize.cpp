﻿#include "setup.h"

#include <gdm/core/gdm.h>
#include <gdm/core/common/gdmexcept.h>
#include <gearbox/defaults.h>
#include <gearbox/concentrationprofilesmodel.h>
#ifndef NMF_NO_GUI
#include <gearbox/profilescolorizermodel.h>
#endif // NMF_NO_GUI
#include <gearbox/singletons.h>
#include <persistence/types.h>

#ifndef NMF_NO_GUI
#include <QColor>
#endif // NMF_NO_GUI
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QTextStream>

#include <cassert>

namespace persistence::setup {

static
auto checkIfContains(const QString &key, const QJsonObject &obj, const QJsonValue::Type type) -> void
{
  if (!obj.contains(key))
    throw Error{std::string{"Missing "} + key.toStdString()};

  if (obj[key].type() != type)
    throw Error{std::string{"Unexpected type of field "} + key.toStdString()};
}

template <typename T> struct VT {};

template <> struct VT<int> {
  static const QJsonValue::Type type = QJsonValue::Double;
  static int get(const QJsonValue &v) { return int(v.toDouble()); }
};

template <> struct VT<double> {
  static const QJsonValue::Type type = QJsonValue::Double;
  static double get(const QJsonValue &v) { return v.toDouble(); }
};

template <> struct VT<bool> {
  static const QJsonValue::Type type = QJsonValue::Bool;
  static bool get(const QJsonValue &v) { return v.toBool(); }
};

template <> struct VT<QString> {
  static const QJsonValue::Type type = QJsonValue::String;
  static QString get(const QJsonValue &v) { return v.toString(); }
};

template <> struct VT<std::string> {
  static const QJsonValue::Type type = QJsonValue::String;
  static std::string get(const QJsonValue &v) { return v.toString().toStdString(); }
};

template <> struct VT<gdm::ConstituentType> {
  static const QJsonValue::Type type = QJsonValue::String;
  static gdm::ConstituentType get(const QJsonValue &v) {
    auto str = v.toString();
    if (str == CTUENT_TYPE_NUCLEUS)
      return gdm::ConstituentType::Nucleus;
    else if (str == CTUENT_TYPE_LIGAND)
      return gdm::ConstituentType::Ligand;
    throw Error{"Invalid constituent type"};
  }
};

template <> struct VT<gearbox::ConcentrationProfilesModel::ProfileShape> {
  static const QJsonValue::Type type = QJsonValue::String;
  static gearbox::ConcentrationProfilesModel::ProfileShape get(const QJsonValue &v) {
    auto str = v.toString();
    if (str == CONC_SHAPE_BOX)
      return gearbox::ConcentrationProfilesModel::INJECTION_BOX;
    else if (str == CONC_SHAPE_GAUSS)
      return gearbox::ConcentrationProfilesModel::INJECTION_GAUSS;
    else if (str == CONC_SHAPE_ZONE)
      return gearbox::ConcentrationProfilesModel::ZONES;
    throw Error{"Invalid profile shape"};
  }
};

template <typename I> struct VT<std::vector<I>> {
  static const QJsonValue::Type type = QJsonValue::Array;
  static const std::vector<I> get(const QJsonValue &v) {
    auto array = v.toArray();

    std::vector<I> data{};
    data.reserve(array.size());

    for (auto it = array.begin(); it != array.end(); it++) {
      auto i = VT<I>::get(*it);
      data.emplace_back(std::move(i));
    }
    return data;
  }
};

#ifndef NMF_NO_GUI
template <> struct VT<QColor> {
  static const QJsonValue::Type type = QJsonValue::String;
  static QColor get(const QJsonValue &v) {
    auto str = v.toString();
    QColor clr{};
    clr.setNamedColor(str);
    if (!clr.isValid())
      throw Error{"Invalid constituent color"};
    return clr;
  }
};
#endif // NMF_NO_GUI

template <typename T>
static
auto get(T &data, const QString &key, const QJsonObject &obj)
{
  checkIfContains(key, obj, VT<T>::type);
  data = VT<T>::get(obj[key]);
}

template <typename T>
static
auto get(const QString &key, const QJsonObject &obj) -> T
{
  T data;
  get(data, key, obj);
  return data;
}

template <> struct VT<gearbox::ConcentrationProfilesModel::Edge> {
  static const QJsonValue::Type type = QJsonValue::Object;
  static gearbox::ConcentrationProfilesModel::Edge get(const QJsonValue &v) {
    const auto obj = v.toObject();
    double position = persistence::setup::get<double>(EDGE_POSITION, obj);
    double width = persistence::setup::get<double>(EDGE_WIDTH, obj);

    return { position, width };
  }
};

static
auto deserializeNucleusComplexForms(const QJsonObject &obj, const int nucleusChargeLow, const int nucleusChargeHigh)
{
  std::map<std::string, gdm::Complexation> ret{};

  checkIfContains(CPX_COMPLEX_FORMS, obj, QJsonValue::Array);
  const auto cforms = obj[CPX_COMPLEX_FORMS].toArray();

  for (const auto &item : cforms) {
    const auto cf = item.toObject();
    if (cf.isEmpty())
      throw Error{"Invalid complex form entry"};

    /* Read nucleus charge */
    auto charge = get<int>(CPX_NUCLEUS_CHARGE, cf);

    if (charge < nucleusChargeLow || charge > nucleusChargeHigh)
      throw Error{"Nucleus charge in complexForm definition is outside nucleus' charge range"};

    /* Read ligand groups */
    checkIfContains(CPX_LIGAND_GROUPS, cf, QJsonValue::Array);
    const auto ligandGroups = cf[CPX_LIGAND_GROUPS].toArray();
    for (const auto &lg : ligandGroups) {
      const auto lgObj = lg.toObject();
      checkIfContains(CPX_LIGANDS, lgObj, QJsonValue::Array);
      const auto ligands = lgObj[CPX_LIGANDS].toArray();
      if (ligands.size() != 1)
        throw Error{"Invalid number of ligands in \"ligands\" array"};

      const auto lig = ligands.at(0).toObject();
      if (lig.isEmpty())
        throw Error{"Invalid ligand entry"};

      /* Read ligand name */
      auto name = get<std::string>(CPX_NAME, lig);
      if (name.length() < 1)
        throw Error{"Invalid ligand name"};

      /* Read ligand charge */
      auto ligandCharge = get<int>(CPX_CHARGE, lig);

      /* Read maxCount */
      size_t maxCount = get<int>(CPX_MAX_COUNT, lig);

      auto pBs = get<std::vector<double>>(CPX_PBS, lig);
      if (pBs.size() != maxCount)
        throw Error{"Invalid size of complex \"pBs\" array"};

      auto mobilities = get<std::vector<double>>(CPX_MOBILITIES, lig);
      if (mobilities.size() != maxCount)
        throw Error{"Invalid size of complex \"mobilities\" array"};

      gdm::ChargeCombination charges{charge, ligandCharge};

      bool inserted;
      std::tie(std::ignore, inserted) = ret[name].insert({charges, maxCount, std::move(pBs), std::move(mobilities)});
      if (!inserted)
        throw Error{"Duplicit charge combinations"};
    }
  }

  return ret;
}

static
auto deserializeConstituents(const QJsonArray &array)
{
#ifndef NMF_NO_GUI
  std::vector<std::tuple<gdm::Constituent, std::map<std::string, gdm::Complexation>, QColor>> ret{};
#else
  std::vector<std::tuple<gdm::Constituent, std::map<std::string, gdm::Complexation>>> ret{};
#endif // NMF_NO_GUI
  ret.reserve(array.size());

  for (auto it = array.begin(); it != array.end(); it++) {
    if (it->type() != QJsonValue::Object)
      throw Error{"Constituent array element is not an object"};

    const auto obj = it->toObject();

    auto type = get<gdm::ConstituentType>(CTUENT_TYPE, obj);
    auto name = get<std::string>(CTUENT_NAME, obj);
    auto chargeLow = get<int>(CTUENT_CHARGE_LOW, obj);
    auto chargeHigh = get<int>(CTUENT_CHARGE_HIGH, obj);
    auto pKas = get<std::vector<double>>(CTUENT_PKAS, obj);
    auto mobilities = get<std::vector<double>>(CTUENT_MOBILITIES, obj);
    auto viscos = get<double>(CTUENT_VISCOSITY_COEFFICIENT, obj);
#ifndef NMF_NO_GUI
    auto color = get<QColor>(CTUENT_PROFILE_COLOR, obj);
#endif // NMF_NO_GUI

    gdm::PhysicalProperties physProps{
      gdm::ChargeInterval(chargeLow, chargeHigh),
      std::move(pKas),
      std::move(mobilities),
      viscos
    };
    gdm::Constituent ctuent{
      type,
      std::move(name),
      std::move(physProps)
    };

    if (type == gdm::ConstituentType::Nucleus) {
      auto complexForms = deserializeNucleusComplexForms(obj, chargeLow, chargeHigh);
// Yes, these ifdefs below are insane...
      ret.emplace_back(
#ifndef NMF_NO_GUI
        std::make_tuple<gdm::Constituent,
                        std::map<std::string, gdm::Complexation>,
                        QColor
                       >(std::move(ctuent), std::move(complexForms), std::move(color))
#else
        std::make_tuple<gdm::Constituent,
                        std::map<std::string, gdm::Complexation>
                       >(std::move(ctuent), std::move(complexForms))
#endif // NMF_NO_GUI
      );
    } else {
      if (obj.contains(CPX_COMPLEX_FORMS))
        throw Error{"Ligands must not have \"complexForms\""};
      ret.emplace_back(
#ifndef NMF_NO_GUI
        std::make_tuple<gdm::Constituent,
                        std::map<std::string, gdm::Complexation>,
                        QColor
                       >(std::move(ctuent), {}, std::move(color))
#else
        std::make_tuple<gdm::Constituent,
                        std::map<std::string, gdm::Complexation>
                       >(std::move(ctuent), {})
#endif // NMF_NO_GUI
        );
    }
  }

  return ret;
}

static
auto deserializeProfiles(const QJsonArray &composition)
{
#ifndef NDEBUG
  auto &gdm = gearbox::Singletons::gdm();
#endif // NDEBUG
  auto &concProfsModel = gearbox::Singletons::concentrationProfilesModel();

  for (auto it = composition.begin(); it != composition.end(); it++) {
    if (it->type() != QJsonValue::Object)
      throw Error{"Constituent array element is not an object"};

    const auto obj = it->toObject();
    auto name = get<std::string>(CTUENT_NAME, obj);

    auto shape = get<gearbox::ConcentrationProfilesModel::ProfileShape>(CONC_SHAPE, obj);
    auto edges = get<std::vector<gearbox::ConcentrationProfilesModel::Edge>>(CONC_EDGES, obj);
    auto concs = get<std::vector<double>>(CONC_VALUES, obj);

    if (edges.size() + 1 != concs.size())
      throw Error{"Mismatching edge/sizes combination"};
    if (concs.size() > gearbox::Defaults::MAXIMUM_BLOCKS)
      throw Error{"Number of concentrations is too high"};

    assert(gdm.find(name) != gdm.end());

    concProfsModel.add(std::move(name), shape, std::move(concs), std::move(edges));
  }
}

static
auto deserializeComposition(const QJsonArray &composition)
{
  auto &gdm = gearbox::Singletons::gdm();
#ifndef NMF_NO_GUI
  auto &colorizer = gearbox::Singletons::profilesColorizerModel();
#endif // NMF_NO_GUI
  try {
    auto constituents  = deserializeConstituents(composition);

    /* Insert all constituents first */
    for (const auto &ctuent : constituents) {
      bool inserted;
      std::tie(std::ignore, inserted) = gdm.insert(std::get<0>(ctuent));
      if (!inserted)
        throw Error{"Duplicit constituent"};
#ifndef NMF_NO_GUI
      colorizer.add(std::get<0>(ctuent).name(), std::get<2>(ctuent));
#endif // NMF_NO_GUI
    }

    /* Insert all complexations */
    for (const auto & ctuent : constituents) {
      auto nucleusIt = gdm.find(std::get<0>(ctuent).name());
      if (nucleusIt == gdm.end())
        throw Error{"Internal deserialization error"};

      for (const auto &cf : std::get<1>(ctuent)) {
        auto ligandIt = gdm.find(cf.first);
        if (ligandIt == gdm.end())
          throw Error{"ComplexForm refers to missing ligand"};

        if (gdm.haveComplexation(nucleusIt, ligandIt))
          throw Error{"Internal deserialization error"};

        gdm.setComplexation(nucleusIt, ligandIt, cf.second);
      }
    }
  } catch (const gdm::LogicError &ex) {
    throw Error{std::string{"Failed to parse constituents: "} + std::string{ex.what()}};
  }
}

static
auto deserializeDetectors(const QJsonArray &array, System &sys) -> void
{
  std::vector<std::pair<bool, double>> detectors{};

  for (auto it = array.begin(); it != array.end(); it++) {
    if (!it->isObject())
      throw Error{SYS_DETECTORS.toStdString() + " item is not an object"};

    bool enabled;
    double position;
    get(enabled, SYS_DETECTOR_ENABLED, it->toObject());
    get(position, SYS_DETECTOR_POSITION, it->toObject());

    detectors.emplace_back(enabled, position);
  }

  sys.detectors = std::move(detectors);
}

static
auto deserializeSystem(const QJsonObject &obj, System &sys) -> void
{
  get(sys.dt, SYS_DT, obj);
  get(sys.cells, SYS_CELLS, obj);
  get(sys.autoDt, SYS_AUTO_DT, obj);
  get(sys.current, SYS_CURRENT, obj);
  get(sys.eofMode, SYS_EOF_MODE, obj);
  if (!(sys.eofMode == EM_MOBILITY || sys.eofMode == EM_VELOCITY))
    throw Error{"Invalid eofMode"};

  get(sys.voltage, SYS_VOLTAGE, obj);
  get(sys.eofValue, SYS_EOF_VALUE, obj);

  get(sys.stopTime, SYS_STOP_TIME, obj);
  if (sys.stopTime < 0)
    throw Error{"Invalid stop time"};

  get(sys.constantForce, SYS_CONSTANT_FORCE, obj);
  if (!(sys.constantForce == CF_CURRENT || sys.constantForce == CF_VOLTAGE))
    throw Error{"Invalid constant force"};

  get(sys.diameterValues, SYS_DIAMETER_VALUES, obj);
  get(sys.capillaryLength, SYS_CAPILLARY_LENGTH, obj);
  get(sys.diameterSegments, SYS_DIAMETER_SEGMENTS, obj);
  get(sys.tolerance, SYS_TOLERANCE, obj);
  if (sys.tolerance < 0)
    throw Error{"Invalid tolerance"};

  if (sys.diameterSegments.size() + 1 != sys.diameterValues.size())
    throw Error{"Invalid capillary shape definition"};

  get(sys.firstCell, SYS_FISRT_CELL, obj);
  if (sys.firstCell < 0)
    throw Error{"Invalid first cell value"};
  get(sys.lastCell, SYS_LAST_CELL, obj);
  if (sys.lastCell <= sys.firstCell || sys.lastCell >= sys.cells)
    throw Error{"Invalid last cell value"};
  get(sys.autoCells, SYS_AUTO_CELLS, obj);
  get(sys.updateInterval, SYS_UPDATE_INTERVAL, obj);
  if (sys.updateInterval < 1)
    throw Error{"Invalid update interval"};

  checkIfContains(SYS_DETECTORS, obj, QJsonValue::Array);
  deserializeDetectors(obj[SYS_DETECTORS].toArray(), sys);
}

auto load(const QString &name, System &sys) -> void
{
  QFile fh{name};

  if (!fh.open(QIODevice::ReadOnly | QIODevice::Text))
    throw Error{"Failed to open input file"};

  return loadRaw(fh.readAll(), sys);
}

auto loadRaw(const QByteArray &json, System &sys) -> void
{
  QJsonParseError parseError{};
  auto doc = QJsonDocument::fromJson(json, &parseError);
  if (doc.isNull())
    throw Error{parseError.errorString().toStdString()};

  auto root = doc.object();
  if (root.empty())
    throw Error{"Bad root object"};

  checkIfContains(ROOT_SYSTEM, root, QJsonValue::Object);
  deserializeSystem(root[ROOT_SYSTEM].toObject(), sys);

  checkIfContains(ROOT_COMPOSITION, root, QJsonValue::Array);
  deserializeComposition(root[ROOT_COMPOSITION].toArray());
  deserializeProfiles(root[ROOT_COMPOSITION].toArray());
}

} // namespace persistence::setup
