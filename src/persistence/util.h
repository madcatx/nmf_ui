#ifndef PERSISTENCE_UTIL_H
#define PERSISTENCE_UTIL_H

#include <QJsonArray>
#include <QJsonObject>

#include <vector>

namespace persistence {

template <typename T>
inline
auto vectorToArray(const std::vector<T> &vec)
{
  QJsonArray array{};

  for (const auto &item : vec)
    array.append(item);

  return array;
}

} // namespace persistence

#endif // PERSISTENCE_UTIL_H
