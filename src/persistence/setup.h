#ifndef PERSISTENCE_SETUP_H
#define PERSISTENCE_SETUP_H

#include <hacks.h>

#include <QString>

namespace persistence {

class System;

namespace setup {

NMFUI_INLINE const QString ROOT_SYSTEM{"system"};
NMFUI_INLINE const QString ROOT_COMPOSITION{"composition"};

NMFUI_INLINE const QString SYS_AUTO_DT{"autoDt"};
NMFUI_INLINE const QString SYS_CAPILLARY_LENGTH{"length"};
NMFUI_INLINE const QString SYS_DT{"dt"};
NMFUI_INLINE const QString SYS_VOLTAGE{"voltage"};
NMFUI_INLINE const QString SYS_CURRENT{"current"};
NMFUI_INLINE const QString SYS_CONSTANT_FORCE{"constantForce"};
NMFUI_INLINE const QString SYS_CELLS{"cells"};
NMFUI_INLINE const QString SYS_EOF_VALUE{"eofValue"};
NMFUI_INLINE const QString SYS_EOF_MODE{"eofMode"};
NMFUI_INLINE const QString SYS_DIAMETER_SEGMENTS{"diameterSegments"};
NMFUI_INLINE const QString SYS_DIAMETER_VALUES{"diameterValues"};
NMFUI_INLINE const QString SYS_STOP_TIME{"stopTime"};
NMFUI_INLINE const QString SYS_TOLERANCE{"tolerance"};
NMFUI_INLINE const QString SYS_FISRT_CELL{"firstCell"};
NMFUI_INLINE const QString SYS_LAST_CELL{"lastCell"};
NMFUI_INLINE const QString SYS_AUTO_CELLS{"autoCells"};
NMFUI_INLINE const QString SYS_UPDATE_INTERVAL{"updateInterval"};
NMFUI_INLINE const QString SYS_DETECTORS{"detectors"};
NMFUI_INLINE const QString SYS_DETECTOR_ENABLED{"enabled"};
NMFUI_INLINE const QString SYS_DETECTOR_POSITION{"position"};

NMFUI_INLINE const QString CTUENT_TYPE{"type"};
NMFUI_INLINE const QString CTUENT_NAME{"name"};
NMFUI_INLINE const QString CTUENT_CHARGE_LOW{"chargeLow"};
NMFUI_INLINE const QString CTUENT_CHARGE_HIGH{"chargeHigh"};
NMFUI_INLINE const QString CTUENT_PKAS{"pKas"};
NMFUI_INLINE const QString CTUENT_MOBILITIES{"mobilities"};
NMFUI_INLINE const QString CTUENT_VISCOSITY_COEFFICIENT{"viscosityCoefficient"};
NMFUI_INLINE const QString CTUENT_PROFILE_COLOR{"profileColor"};

NMFUI_INLINE const QString CPX_NAME{"name"};
NMFUI_INLINE const QString CPX_CHARGE{"charge"};
NMFUI_INLINE const QString CPX_MAX_COUNT{"maxCount"};
NMFUI_INLINE const QString CPX_PBS{"pBs"};
NMFUI_INLINE const QString CPX_MOBILITIES{"mobilities"};

NMFUI_INLINE const QString CPX_COMPLEX_FORMS{"complexForms"};
NMFUI_INLINE const QString CPX_NUCLEUS_CHARGE{"nucleusCharge"};
NMFUI_INLINE const QString CPX_LIGAND_GROUPS{"ligandGroups"};
NMFUI_INLINE const QString CPX_LIGANDS{"ligands"};

NMFUI_INLINE const QString CONC_VALUES{"concentrations"};
NMFUI_INLINE const QString CONC_EDGES{"edges"};
NMFUI_INLINE const QString CONC_SHAPE{"shape"};

NMFUI_INLINE const QString EDGE_POSITION{"position"};
NMFUI_INLINE const QString EDGE_WIDTH{"width"};

auto load(const QString &path, System &sys) -> void;
auto loadRaw(const QByteArray &json, System &sys) -> void;
auto save(QString path, const System &sys) -> void;
auto toJson(const System &sys) -> QByteArray;

} // namespace setup
} // namespace persistence

#endif // PERSISTENCE_SETUP_H
