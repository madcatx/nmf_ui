#ifndef PERSISTENCE_TYPES_H
#define PERSISTENCE_TYPES_H

#include <hacks.h>

#include <QString>

#include <vector>

namespace persistence {

class Error : public std::runtime_error {
public:
  using std::runtime_error::runtime_error;
};

class System {
public:
  bool autoDt;
  double capillaryLength;
  double dt;
  double voltage;
  double current;
  QString constantForce;
  int cells;
  double eofValue;
  QString eofMode;
  std::vector<double> diameterSegments;
  std::vector<double> diameterValues;
  double stopTime;
  int tolerance;
  int firstCell;
  int lastCell;
  bool autoCells;
  int updateInterval;
  std::vector<std::pair<bool, double>> detectors;
};

NMFUI_INLINE const QString CF_VOLTAGE{"V"};
NMFUI_INLINE const QString CF_CURRENT{"C"};

NMFUI_INLINE const QString EM_MOBILITY{"M"};
NMFUI_INLINE const QString EM_VELOCITY{"V"};

NMFUI_INLINE const QString CTUENT_TYPE_LIGAND{"L"};
NMFUI_INLINE const QString CTUENT_TYPE_NUCLEUS{"N"};

NMFUI_INLINE const QString CONC_SHAPE_BOX{"B"};
NMFUI_INLINE const QString CONC_SHAPE_GAUSS{"G"};
NMFUI_INLINE const QString CONC_SHAPE_ZONE{"Z"};

} // namespace persistence

#endif // PERSISTENCE_TYPES_H
