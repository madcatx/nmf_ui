#include "crasheventcatcher.h"
#include "crashevent.h"

#ifndef CRASHEVENTCATCHER_NO_GUI
#include <ui/crashhandlerdialog.h>
#else
#include <iostream>
#endif // CRASHEVENTCATCHER_NO_GUI

CrashEventCatcher::CrashEventCatcher() :
  m_crashEventId(CrashEvent::registerMe())
{
}

auto CrashEventCatcher::eventFilter(QObject *watched, QEvent *event) -> bool
{
  if (event->type() == m_crashEventId) {
    CrashEvent *crashEvt = static_cast<CrashEvent *>(event);
    handleCrash(crashEvt);

    /* We should never get here */
    std::abort();
    return true;
  } else {
    return QObject::eventFilter(watched, event);
  }
}

auto CrashEventCatcher::displayCrashMessage(const char *dump, const bool postCrash) -> void
{
#ifndef CRASHEVENTCATCHER_NO_GUI
  CrashHandlerDialog dlg(postCrash);

  dlg.setBacktrace(dump);
  dlg.exec();
#else
  std::cerr << dump << std::endl;
#endif // CRASHEVENTCATCHER_NO_GUI
}

auto CrashEventCatcher::executeEmergency() -> void
{
  emit emergency();
}

auto CrashEventCatcher::handleCrash(const CrashEvent *event) -> void
{
  emit emergency();

  displayCrashMessage(event->crashHandler->crashInfo().c_str());

  event->crashHandler->proceedToKill();
}
