#include "globals.h"

#ifndef NMF_NO_GUI
#include <QPixmap>
#endif // NMF_NO_GUI
#include <QSysInfo>

const QString Globals::SOFTWARE_NAME{"NMF"};
const QString Globals::SOFTWARE_NAME_INTERNAL{SOFTWARE_NAME_INTERNAL_S};
const int Globals::VERSION_MAJ{0};
const int Globals::VERSION_MIN{1};
const QString Globals::VERSION_REV{"a"};

const QVector<Globals::DeveloperID> Globals::DEVELOPERS = {};

#ifndef NMF_NO_GUI
QIcon Globals::icon()
{
#ifdef Q_OS_WIN
  static const QIcon PROGRAM_ICON{":/images/res/nmf_small.ico"};
#else
  static const QIcon PROGRAM_ICON{":/images/res/nmf_small_64.png"};
#endif // Q_OS_WIN

  return PROGRAM_ICON;
}

QIcon Globals::iconHiDpi()
{
#ifdef Q_OS_WIN
  static const QIcon PROGRAM_ICON{":/images/res/nmf_small.ico"};
#else
  static const QIcon PROGRAM_ICON{":/images/res/nmf_small_128.png"};
#endif // Q_OS_WIN

  return PROGRAM_ICON;
}
#endif // NMF_NO_GUI

QString Globals::DeveloperID::linkString() const
{
  return QString{"%1 (<a href=\"mailto:%2\">%2</a>)"}.arg(name).arg(mail.toHtmlEscaped());
}

QString Globals::DeveloperID::prettyString() const
{
  return QString{"%1 (%2)"}.arg(name).arg(mail);
}

QString Globals::VERSION_STRING()
{
  auto s = QString("%1 %2.%3%4").arg(SOFTWARE_NAME).arg(VERSION_MAJ).arg(VERSION_MIN).arg(VERSION_REV);
#ifdef UNSTABLE_VERSION
  s.append(" Build date: [" + QString(__DATE__) + " - " + QString(__TIME__)  + "]");
#endif // UNSTABLE_VERSION

  return s;
}
