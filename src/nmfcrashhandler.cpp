#include "crashevent.h"
#include "crasheventcatcher.h"
#include "nmfcrashhandler.h"
#include <crashhandling/crashhandlingprovider.h>

#include <globals.h>
#include <fstream>
#include <sstream>
#include <cstdio>

#ifndef NMF_NO_GUI
#include <QApplication>
#else
#include <QCoreApplication>
#endif // NMF_NO_GUI

#ifdef Q_OS_WIN
  #include <crashhandling/crashhandlerwindows.h>
  #define CrashHandlerPlatform CrashHandlerWindows
#elif defined (Q_OS_LINUX)
  #include <crashhandling/crashhandlerlinux.h>
  #define CrashHandlerPlatform CrashHandlerLinux
#else
  #include <crashhandling/nullcrashhandler.h>
  #define CrashHandlerPlatform NullCrashHandler
#endif // Q_OS_

inline
auto mkSWStr(const std::string &tail) -> std::string
{
  static const std::string swname = Globals::SOFTWARE_NAME.toStdString();

  return swname + "_" + tail;
}

class UICrashFinalizer : public CrashHandlerFinalizer<CrashHandlerPlatform>
{
public:
  explicit UICrashFinalizer(CrashHandlerPlatform *handler) :
    CrashHandlerFinalizer(handler)
  {}

  virtual auto operator()() -> void override
  {
    CrashEvent *evt = new CrashEvent(m_handler);

    if (m_handler->mainThreadCrashed()) {
      /* Let them know that I have been stiff
         and smiling 'till the crash */
      NMFCrashHandler::s_catcher->executeEmergency();

      std::ofstream textDump(NMFCrashHandler::s_textCrashDumpFile.c_str(), std::ios_base::out);
      if (textDump.is_open()) {
        textDump << m_handler->crashInfo();
        textDump.close();
      }

      /* And now make the final splash... */
      return;
    }

    qApp->postEvent(qApp, evt);

    m_handler->waitForKill();
  }
};

UICrashFinalizer * NMFCrashHandler::s_uiFinalizer{nullptr};
CrashEventCatcher * NMFCrashHandler::s_catcher{nullptr};
const std::string NMFCrashHandler::s_textCrashDumpFile{mkSWStr("crashDump.txt")};

auto NMFCrashHandler::checkForCrash() -> void
{
  std::ifstream textDump(s_textCrashDumpFile.c_str(), std::ios_base::in);
  if (!textDump.good())
    return;

  std::ostringstream textDumpSt;

  textDumpSt << textDump.rdbuf();

  CrashEventCatcher::displayCrashMessage(textDumpSt.str().c_str(), true);
  textDump.close();

  ::remove(s_textCrashDumpFile.c_str());
}

auto NMFCrashHandler::handler() -> CrashHandlerBase *
{
  return CrashHandlingProvider<CrashHandlerPlatform>::handler();
}

auto NMFCrashHandler::installCrashHandler() -> bool
{
#ifndef USE_CRASHHANDLER
  return true;
#else

  if (!CrashHandlingProvider<CrashHandlerPlatform>::initialize(mkSWStr("minidump.mdmp")))
    return false;

  try {
    s_uiFinalizer = new UICrashFinalizer(CrashHandlingProvider<CrashHandlerPlatform>::handler());
  } catch (std::bad_alloc &) {
    CrashHandlingProvider<CrashHandlerPlatform>::deinitialize();
    return false;
  }

  try {
    s_catcher = new CrashEventCatcher();
  } catch (std::bad_alloc &) {
    CrashHandlingProvider<CrashHandlerPlatform>::deinitialize();
    delete s_uiFinalizer;
    return false;
  }

  qApp->installEventFilter(s_catcher);

  return true;
#endif // USE_CRASHHANDLER
}

auto NMFCrashHandler::uninstallCrashHandler() -> void
{
#ifndef USE_CRASHHANDLER
  return;
#else

  if (s_catcher != nullptr) {
    qApp->removeEventFilter(s_catcher);
    delete s_catcher;
  }

  CrashHandlingProvider<CrashHandlerPlatform>::deinitialize();

  delete s_uiFinalizer;
  s_uiFinalizer = nullptr;
  s_catcher = nullptr;
#endif // USE_CRASHHANDLER
}
