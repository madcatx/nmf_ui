#include <nmfcrashhandler.h>
#include <gearbox/singletons.h>
#include <ui/mainwindow.h>
#include <util/doubletostringconvertor.h>

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a{argc, argv};

  NMFCrashHandler::installCrashHandler();

  util::DoubleToStringConvertor::initialize();
  gearbox::Singletons::initialize();

  NMFCrashHandler::checkForCrash();

  MainWindow mWin{};

  mWin.show();
  const auto ret = a.exec();

  NMFCrashHandler::uninstallCrashHandler();

  return ret;
}
