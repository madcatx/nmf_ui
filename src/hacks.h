#ifndef HACKS_H
#define HACKS_H

#ifdef ECHMET_COMPILER_MSVC
#define NMFUI_CONSTEXPR
#else
#define NMFUI_CONSTEXPR constexpr
#endif // ECHMET_COMPILER_MSVC


#ifdef ECHMET_COMPILER_MSVC
#define NMFUI_INLINE static
#else
#define NMFUI_INLINE inline
#endif // ECHMET_COMPILER_MSVC

#endif // HACKS_H
