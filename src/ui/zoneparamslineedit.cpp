#include "zoneparamslineedit.h"

#include <cassert>

ZoneParamsLineEdit::ZoneParamsLineEdit(QWidget *parent) :
  FloatingValueLineEdit{parent},
  m_zoneNumber{0}
{
}

auto ZoneParamsLineEdit::isInputValid() const -> bool
{
  if (m_zoneNumber == 0)
    return true;

  return FloatingValueLineEdit::isInputValid();
}

auto ZoneParamsLineEdit::isInputValid(QString text) const -> bool
{
  if (m_zoneNumber == 0)
    return true;

  return FloatingValueLineEdit::isInputValid(text);
}

auto ZoneParamsLineEdit::setZoneNumber(const int num) -> void
{
  assert(num >= 0);

  m_zoneNumber = num;
}
