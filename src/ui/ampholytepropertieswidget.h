#ifndef AMPHOLYTEPROPERTIESWIDGET_H
#define AMPHOLYTEPROPERTIESWIDGET_H

#include <QWidget>

#include <map>

namespace Ui {
class AmpholytePropertiesWidget;
}

class FloatingValueLineEdit;
class QLabel;
class QLineEdit;

class AmpholytePropertiesWidget : public QWidget
{
  Q_OBJECT

public:
  class Properties {
  public:
    Properties();
    Properties(
      std::vector<double> _mobilities,
      std::vector<double> _basepKas,
      std::vector<double> _pKaSteps,
      int _chargeLow,
      int _chargeHigh) noexcept;
    Properties(Properties &&other) noexcept;

    const bool ok;
    const std::vector<double> mobilities;
    const std::vector<double> basepKas;
    const std::vector<double> pKaSteps;

    const int chargeLow;
    const int chargeHigh;
  };

  explicit AmpholytePropertiesWidget(QWidget *parent = nullptr);
  ~AmpholytePropertiesWidget();

  auto properties() const -> Properties;
  auto setChargeSpan(const int lowest, const int highest) -> void;
  auto setNumAmpholytes(const int num) -> void;

private:
  using Pack = std::tuple<QLabel *,
                          FloatingValueLineEdit *,
                          FloatingValueLineEdit *,
                          FloatingValueLineEdit *,
                          QLineEdit *
                         >
                         ;
  using Map = std::map<int, Pack>;

  auto appendRow(const int charge) -> void;

  auto lockpKaLess() -> void;

  auto insertCharge(const int charge) -> void;

  template <int Idx>
  auto insertWidget(const int row, const Pack &widgets) -> void;

  auto pKaLessCharge() const -> int;
  auto prependRow(const int charge) -> void;
  auto removeCharge(const int charge) -> void;
  auto removeRow(const int charge) -> void;
  auto shiftDown() -> void;

  template <int Idx>
  auto shiftDownWidgets(const int row) -> void;

  template <int Idx, bool LockMobility>
  auto toggleWidgets(Pack &widgets, const bool lock) -> void;

  auto updateList(const int newLowest, const int newHighest) -> void;

  Ui::AmpholytePropertiesWidget *ui;

  int m_highest;
  int m_lowest;
  int m_numAmpholytes;
  Map m_map;

  bool m_clean;
};

#endif // AMPHOLYTEPROPERTIESWIDGET_H
