#ifndef DATABASECONSTITUENTSPHYSPROPSTABLEMODEL_H
#define DATABASECONSTITUENTSPHYSPROPSTABLEMODEL_H

#include <gearbox/databaseproxy.h>
#include <QAbstractTableModel>

class DatabaseConstituentsPhysPropsTableModel : public QAbstractTableModel {
  Q_OBJECT
public:
  explicit DatabaseConstituentsPhysPropsTableModel(QObject *parent = nullptr);

  // Header:
  auto headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const -> QVariant override;

  // Basic functionality:
  auto rowCount(const QModelIndex &parent = QModelIndex()) const -> int override;
  auto columnCount(const QModelIndex &parent = QModelIndex()) const -> int override;
  auto constituentAt(const int idx) const -> const gearbox::DatabaseConstituent &;
  auto data(const QModelIndex &index, int role = Qt::DisplayRole) const -> QVariant override;

  auto refreshData(std::vector<gearbox::DatabaseConstituent> &&constituents) noexcept -> void;

private:
  std::vector<gearbox::DatabaseConstituent> m_constituents;
  int m_maximumCharge;
  int m_minimumCharge;
  int m_span;

};

#endif // DATABASECONSTITUENTSPHYSPROPSTABLEMODEL_H
