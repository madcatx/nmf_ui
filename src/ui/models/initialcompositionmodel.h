#ifndef INITIALCOMPOSITIONMODEL_H
#define INITIALCOMPOSITIONMODEL_H

#include <QAbstractTableModel>
#include <QVector>

namespace gearbox {
  class ComplexationManager;
  class ConcentrationProfilesModel;
  class GDMProxy;
  class ProfilesColorizerModel;
} // namespace GDMProxy

class InitialCompositionModel : public QAbstractTableModel {
  Q_OBJECT
public:
  explicit InitialCompositionModel(QObject *parent = nullptr);

  auto clear() -> void;
  auto columnCount(const QModelIndex &parent = {}) const -> int override;
  auto data(const QModelIndex &index, int role = Qt::DisplayRole) const -> QVariant override;
  auto dropMimeData(const QMimeData *mdata, Qt::DropAction action, int row, int col, const QModelIndex &parent) -> bool override;
  auto flags(const QModelIndex &index) const -> Qt::ItemFlags override;
  auto headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const -> QVariant override;
  auto mimeData(const QModelIndexList &indexes) const -> QMimeData * override;
  auto mimeTypes() const -> QStringList override;
  auto refreshAll() -> void;
  auto remove(const int row) -> void;
  auto rowCount(const QModelIndex &parent = {}) const -> int override;
  auto setData(const QModelIndex &index, const QVariant &value, int role) -> bool override;
  auto updateName(const QString &oldName, const QString &newName) -> void;

private:
  auto complexationStatus(const std::string &name) const -> QList<QColor>;
  auto getConcentration(const QString &name, const bool injection) const -> double;

  gearbox::ConcentrationProfilesModel &h_concProfsModel;
  gearbox::ComplexationManager &h_cpxMgr;
  const gearbox::GDMProxy &h_proxy;
  const gearbox::ProfilesColorizerModel &h_profClrModel;

  QVector<QString> m_names;

public slots:
  void onCompositionChanged();

private slots:
  void onColorChanged(const QString &name);
  void onComplexationStatusUpdated();

};

#endif // INITIALCOMPOSITIONMODEL_H
