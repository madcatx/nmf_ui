#ifndef IONICCONCENTRATIONSMODEL_H
#define IONICCONCENTRATIONSMODEL_H

#include <QAbstractTableModel>

#include <utility>
#include <vector>

class IonicConcentrationsModel : public QAbstractTableModel {
  Q_OBJECT
public:
  using Data = std::vector<std::pair<QString, double>>;

  explicit IonicConcentrationsModel(QObject *parent = nullptr);

  auto columnCount(const QModelIndex &parent = QModelIndex()) const -> int override;
  auto data(const QModelIndex &index, int role = Qt::DisplayRole) const -> QVariant override;
  auto headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const -> QVariant override;
  auto rowCount(const QModelIndex &parent = QModelIndex()) const -> int override;
  auto update(Data data) -> void;

private:
  Data m_data;
};

#endif // IONICCONCENTRATIONSMODEL_H
