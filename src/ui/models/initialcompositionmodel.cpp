#include "initialcompositionmodel.h"

#include <gearbox/complexationmanager.h>
#include <gearbox/concentrationprofilesmodel.h>
#include <gearbox/gdmproxy.h>
#include <gearbox/profilescolorizermodel.h>
#include <gearbox/singletons.h>
#include <util/doubletostringconvertor.h>

#include <QCoreApplication>
#include <QDataStream>
#include <QMimeData>
#include <QPalette>

#include <algorithm>
#include <array>
#include <cassert>

static const QString MIME_FORMAT{"application/vnd.text.list"};

static const std::array<QVariant, 10> HEADER_CAPTIONS = {
  QObject::tr("Cplxs"),
  QObject::tr("T"),
  QObject::tr(" "),
  QObject::tr("Name"),
  QObject::tr("n"),
  QObject::tr("p"),
  QObject::tr("c(inj) (mM)"),
  QObject::tr("c (mM)"),
  "pKa",
  QString::fromUtf8("\xCE\xBC")
};

static const std::vector<int> EDITABLE_COLUMNS = { 6, 7 };

static
auto makeStringFromDoubleVec(const std::vector<double> &vec)
{
  QString s{};

  if (vec.empty())
    return s;

  for (size_t idx{0}; idx < vec.size() - 1; idx++)
    s += QString{"%1;"}.arg(util::DoubleToStringConvertor::convert(vec[idx]));
  s += util::DoubleToStringConvertor::convert(vec.back());

  return s;
}

InitialCompositionModel::InitialCompositionModel(QObject *parent) :
  QAbstractTableModel{parent},
  h_concProfsModel{gearbox::Singletons::concentrationProfilesModel()},
  h_cpxMgr{gearbox::Singletons::complexationManager()},
  h_proxy{gearbox::Singletons::gdmProxy()},
  h_profClrModel{gearbox::Singletons::profilesColorizerModel()}
{
  connect(
    &h_cpxMgr,
    &gearbox::ComplexationManager::complexationStatusChanged,
    this,
    &InitialCompositionModel::onComplexationStatusUpdated
  );
  connect(
    &h_profClrModel,
    &gearbox::ProfilesColorizerModel::colorChanged,
    this,
    &InitialCompositionModel::onColorChanged
  );
}

auto InitialCompositionModel::columnCount(const QModelIndex &parent) const -> int
{
  if (parent.isValid())
    return 0;

  return HEADER_CAPTIONS.size();
}

auto InitialCompositionModel::clear() -> void
{
  beginResetModel();
  m_names.clear();
  endResetModel();
}

auto InitialCompositionModel::complexationStatus(const std::string &name) const -> QList<QColor>
{
  const auto hues = h_cpxMgr.complexationStatus(name);

  if (hues.empty()) {
    QPalette p{};
    return { p.base().color() };
  }

  QList<QColor> _hues{};
  _hues.reserve(hues.size());

  for (const auto h : hues)
    _hues.append(QColor::fromHsv(h, 255, 255));

  return _hues;
}

auto InitialCompositionModel::data(const QModelIndex &index, int role) const -> QVariant
{
  if (!index.isValid())
    return {};

  const int row = index.row();
  if (row < 0 || row >= rowCount())
    return {};

  const auto &name = m_names[row];
  if (!h_proxy.contains(name.toStdString()))
    return {};

  const auto constituent = h_proxy.get(name.toStdString());

  if (role == Qt::UserRole + 2)
    return name;

  if (role == Qt::DisplayRole || role == Qt::EditRole) {
    switch (index.column()) {
    case 1:
      return [&constituent]() {
        if (constituent.type() == gdm::ConstituentType::Nucleus)
          return "N";
        return "L";
      }();
    case 3:
      return name;
    case 4:
      return constituent.physicalProperties().charges().low();
    case 5:
      return constituent.physicalProperties().charges().high();
    case 6:
      return getConcentration(name, true);
    case 7:
      return getConcentration(name, false);
    case 8:
      return makeStringFromDoubleVec(constituent.physicalProperties().pKas());
    case 9:
      return makeStringFromDoubleVec(constituent.physicalProperties().mobilities());
    default:
      return {};
    }
  } else if (role == Qt::UserRole + 1) {
    switch (index.column()) {
    case 0:
      return QVariant::fromValue(complexationStatus(name.toStdString()));
    case 2:
      return h_profClrModel.get(name.toStdString());
    default:
      return {};
    }
  }

  return {};
}

auto InitialCompositionModel::dropMimeData(const QMimeData *mdata, Qt::DropAction action, int row, int col, const QModelIndex &parent) -> bool
{
  Q_UNUSED(row); Q_UNUSED(col);

  if (action == Qt::IgnoreAction)
    return true;

  if (!mdata->hasFormat(MIME_FORMAT))
    return false;

  QByteArray encodedData = mdata->data(MIME_FORMAT);
  QDataStream stream(&encodedData, QIODevice::ReadOnly);

  QString targetName = data(parent, Qt::UserRole + 2).toString();
  QString sourceName{};
  qint64 pid;

  stream >> pid;
  if (stream.status() != QDataStream::Ok)
    return false;

  if (pid != QCoreApplication::applicationPid())
    return false;

  stream >> sourceName;
  if (stream.status() != QDataStream::Ok || !stream.atEnd())
    return false;

  h_cpxMgr.makeComplexation(sourceName.toStdString(), targetName.toStdString());

  return true;
}

auto InitialCompositionModel::flags(const QModelIndex &index) const -> Qt::ItemFlags
{
  static const Qt::ItemFlags defaultFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

  if (!index.isValid())
    return Qt::NoItemFlags;

  const auto row = index.row();
  if (row < 0 || row >= rowCount())
    return Qt::NoItemFlags;

  if (index.column() < 2)
    return defaultFlags | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;

  const auto col = index.column();
  if (std::any_of(EDITABLE_COLUMNS.cbegin(), EDITABLE_COLUMNS.cend(), [col](const auto v) { return col == v; })) {
    const auto name = m_names[row].toStdString();
    const auto info = h_concProfsModel.get(name);

    if (info.shape != gearbox::ConcentrationProfilesModel::ZONES)
      return defaultFlags | Qt::ItemIsEditable;
  }

  return defaultFlags;
}

auto InitialCompositionModel::getConcentration(const QString &name, const bool injection) const -> double
{
  const auto info = h_concProfsModel.get(name.toStdString());
  if (info.shape == gearbox::ConcentrationProfilesModel::ZONES)
    return -1;
  else {
    if (injection)
      return info.concentrations.at(1);
    return info.concentrations.at(0);
  }
}

auto InitialCompositionModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant
{
  if (role != Qt::DisplayRole)
    return {};

  if (orientation == Qt::Vertical)
    return {};

  if (section < HEADER_CAPTIONS.size())
    return HEADER_CAPTIONS[section];

  return {};
}

auto InitialCompositionModel::mimeData(const QModelIndexList &indexes) const -> QMimeData *
{
  auto mdata = new QMimeData{};
  QByteArray encodedData{};

  QDataStream stream{&encodedData, QIODevice::WriteOnly};

  for (int idx = 0; idx < 1; idx++) {
    const QModelIndex &midx = indexes.at(idx);

    if (midx.isValid()) {
      const QString name = data(midx, Qt::UserRole + 2).toString();
      stream << QCoreApplication::applicationPid();
      stream << name;
    }
  }

  mdata->setData(MIME_FORMAT, encodedData);
  return mdata;
}

auto InitialCompositionModel::mimeTypes() const -> QStringList
{
  return { MIME_FORMAT };
}

void InitialCompositionModel::onColorChanged(const QString &name)
{
  auto it = std::find(m_names.cbegin(), m_names.cend(), name);
  assert(it != m_names.cend());

  auto idx = std::distance(m_names.cbegin(), it);

  dataChanged(index(idx, 2), index(idx, 2), { Qt::DisplayRole });
}

void InitialCompositionModel::onComplexationStatusUpdated()
{
  if (m_names.size() < 1)
    return;

  dataChanged(index(0, 0), index(0, rowCount() - 1)); // All complexation relationship indicators
  dataChanged(index(0, 6), index(7, rowCount() - 1)); // All concentrations
}

void InitialCompositionModel::onCompositionChanged()
{
  refreshAll();
}

auto InitialCompositionModel::refreshAll() -> void
{
  beginResetModel();
  m_names.clear();

  for (auto it = h_proxy.cbegin(); it != h_proxy.cend(); it++)
    m_names.append(QString::fromStdString(it->name()));

  endResetModel();
}

auto InitialCompositionModel::remove(const int row) -> void
{
  if (row < 0 || row >= rowCount())
    return;

  beginRemoveRows({}, row, row);
  m_names.removeAt(row);
  endRemoveRows();
}

auto InitialCompositionModel::rowCount(const QModelIndex &parent) const -> int
{
  if (parent.isValid())
    return 0;

  return m_names.size();
}

auto InitialCompositionModel::setData(const QModelIndex &index, const QVariant &value, int role) -> bool
{
  if (role != Qt::EditRole)
    return false;

  if (!index.isValid())
    return false;

  const auto row = index.row();
  if (row < 0 || row >= rowCount())
    return false;

  const auto col = index.column();
  if (std::any_of(EDITABLE_COLUMNS.cbegin(), EDITABLE_COLUMNS.cend(), [col](const auto v) { return col == v; })) {
    if (!(flags(index) & Qt::ItemIsEditable))
      return false;

    const auto name = m_names[row].toStdString();
    auto info = h_concProfsModel.get(name);
    auto concs = info.concentrations;

    switch (col) {
    case 6: // Injection concentration
      assert(value.type() == QMetaType::Float || value.type() == QMetaType::Double);
      concs[1] = value.toReal();
      break;
    case 7: // BGE concentration
      assert(value.type() == QMetaType::Float || value.type() == QMetaType::Double);
      concs[0] = value.toReal();
      break;
    default:
      return false;
    }

    h_concProfsModel.set(name, info.shape, std::move(concs), std::move(info.edges));
    emit dataChanged(index, index);
    return true;
  }

  return false;
}

auto InitialCompositionModel::updateName(const QString &oldName, const QString &newName) -> void
{
  auto it =std::find_if(m_names.begin(), m_names.end(), [&oldName](const auto v) { return v == oldName;});
  *it = newName;
}
