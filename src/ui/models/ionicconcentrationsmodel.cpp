#include "ionicconcentrationsmodel.h"

IonicConcentrationsModel::IonicConcentrationsModel(QObject *parent)
  : QAbstractTableModel(parent)
{
}

auto IonicConcentrationsModel::columnCount(const QModelIndex &parent) const -> int
{
  if (parent.isValid())
    return 0;

  return 2;
}

auto IonicConcentrationsModel::data(const QModelIndex &index, int role) const -> QVariant
{
  if (!index.isValid())
    return {};

  if (role != Qt::DisplayRole)
    return {};

  const auto col = index.column();
  const auto row = index.row();

  if (row < 0 || row >= m_data.size())
    return {};

  auto &block = m_data.at(row);
  switch (col) {
  case 0:
    return std::get<0>(block);
  case 1:
    return std::get<1>(block);
  }

  return {};
}

auto IonicConcentrationsModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant
{
  if (orientation == Qt::Vertical)
    return {};
  if (role != Qt::DisplayRole)
    return {};

  switch (section) {
  case 0:
    return tr("Form");
  case 1:
    return tr("Concentration (mM)");
  }

  return {};
}

auto IonicConcentrationsModel::rowCount(const QModelIndex &parent) const -> int
{
  if (parent.isValid())
    return 0;

  return m_data.size();
}

auto IonicConcentrationsModel::update(Data data) -> void
{
  beginResetModel();
  m_data = std::move(data);
  endResetModel();
}
