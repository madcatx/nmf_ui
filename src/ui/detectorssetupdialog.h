#ifndef DETECTORSSETUPDIALOG_H
#define DETECTORSSETUPDIALOG_H

#include <QDialog>

class DetectorWidget;
class QVBoxLayout;

namespace Ui {
class DetectorsSetupDialog;
}

class DetectorsSetupDialog : public QDialog {
  Q_OBJECT

public:
  explicit DetectorsSetupDialog(const std::vector<std::pair<bool, double>> &detectors, const double capillaryLength, QWidget *parent = nullptr);
  ~DetectorsSetupDialog();
  auto detectors() const -> std::vector<std::pair<bool, double>>;

private:
  auto addDetector(const bool enabled, const double position) -> void;

  const double m_capillaryLength;
  QVBoxLayout *m_layout;
  std::vector<DetectorWidget *> m_detectors;

  Ui::DetectorsSetupDialog *ui;

private slots:
  void onAddDetectorClicked();
  void onRemoveDetector(DetectorWidget *w);
};

#endif // DETECTORSSETUPDIALOG_H
