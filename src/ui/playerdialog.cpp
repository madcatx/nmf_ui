#include "playerdialog.h"
#include "ui_playerdialog.h"

PlayerDialog::PlayerDialog(const std::size_t numFrames, QWidget *parent) :
  QDialog{parent},
  ui{new Ui::PlayerDialog},
  m_currentFrame{1},
  m_numFrames{numFrames}
{
  ui->setupUi(this);

  ui->qspb_frame->setRange(1, m_numFrames);
  ui->qscb_tape->setRange(1, m_numFrames);
  ui->qscb_tape->setTracking(false);
  updateDisplay();

  ui->ql_totalFrames->setText(QString{"%0"}.arg(m_numFrames));

  connect(ui->buttonBox, &QDialogButtonBox::close, this, &QDialog::accept);
  connect(ui->qpb_rewind, &QPushButton::clicked,
          [this]() {
    m_currentFrame = 1;
    process();
  });
  connect(ui->qpb_fastForward, &QPushButton::clicked,
          [this]() {
    m_currentFrame = m_numFrames;
    process();
  });
  connect(ui->qpb_prev, &QPushButton::clicked,
          [this]() {
    if (m_currentFrame > 1) {
      m_currentFrame--;
      process();
    }
  });
  connect(ui->qpb_next, &QPushButton::clicked,
          [this]() {
    if (m_currentFrame < m_numFrames) {
      m_currentFrame++;
      process();
    }
  });
  connect(ui->qscb_tape, &QScrollBar::valueChanged, [this](const int value) {
    m_currentFrame = value;
    process();
  });
  connect(ui->qspb_frame, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](const int value) {
    m_currentFrame = value;
    process();
  });
}

PlayerDialog::~PlayerDialog()
{
  delete ui;
}

auto PlayerDialog::currentFrame() -> int
{
  return m_currentFrame;
}

auto PlayerDialog::process() -> void
{
  updateDisplay();

  emit changeFrame(m_currentFrame - 1);
}

auto PlayerDialog::updateDisplay() -> void
{
  ui->qspb_frame->setValue(m_currentFrame);
  ui->qscb_tape->setValue(m_currentFrame);
}
