#include "initialprofilesdialog.h"
#include "ui_initialprofilesdialog.h"

#include <ui/initialprofileswidget.h>

#include <cassert>
#include <QBoxLayout>

class InitialProfilesWidget;

InitialProfilesDialog::InitialProfilesDialog(
  InitialProfilesWidget *w,
  QWidget *parent) :
  QDialog{parent},
  ui{new Ui::InitialProfilesDialog},
  m_w{w}
{
  ui->setupUi(this);

  assert(qobject_cast<QBoxLayout *>(layout()) != nullptr);
  static_cast<QBoxLayout *>(layout())->insertWidget(0, w);

  connect(
    ui->buttonBox,
    &QDialogButtonBox::accepted,
    [this,w]() {
      if (w->commit())
        accept();
    }
  );
  connect(
    ui->buttonBox,
    &QDialogButtonBox::rejected,
    [this, w]() {
      w->reject();
      reject();
    }
  );
}

InitialProfilesDialog::~InitialProfilesDialog()
{
  delete ui;
}

auto InitialProfilesDialog::exec() -> int
{
  m_w->pickConstituent(0);
  return QDialog::exec();
}
