#ifndef AMPHOLYTEGENERATORDIALOG_H
#define AMPHOLYTEGENERATORDIALOG_H

#include <QDialog>

namespace Ui {
class AmpholyteGeneratorDialog;
}

class AmpholytePropertiesWidget;

class AmpholyteGeneratorDialog : public QDialog {
  Q_OBJECT
public:
  explicit AmpholyteGeneratorDialog(QWidget *parent = nullptr);
  ~AmpholyteGeneratorDialog();

private:
  auto setupDialog() -> void;

  Ui::AmpholyteGeneratorDialog *ui;

  AmpholytePropertiesWidget *m_propsWidget;

private slots:
  void onAccepted();
  void onHighestChargeChanged(const int value);
  void onLowestChargeChanged(const int value);

};

#endif // AMPHOLYTEGENERATORDIALOG_H
