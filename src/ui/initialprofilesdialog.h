#ifndef INITIALPROFILESDIALOG_H
#define INITIALPROFILESDIALOG_H

#include <QDialog>

namespace Ui {
class InitialProfilesDialog;
}

class InitialProfilesWidget;

class InitialProfilesDialog : public QDialog {
  Q_OBJECT
public:
  explicit InitialProfilesDialog(InitialProfilesWidget *w, QWidget *parent = nullptr);
  ~InitialProfilesDialog() override;
  auto exec() -> int override;

private:
  Ui::InitialProfilesDialog *ui;

  InitialProfilesWidget *m_w;
};

#endif // INITIALPROFILESDIALOG_H
