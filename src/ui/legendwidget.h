#ifndef LEGENDWIDGET_H
#define LEGENDWIDGET_H

#include <QWidget>

#include <qwt_legend.h>

namespace Ui {
class LegendWidget;
}

class LegendWidget : public QWidget {
  Q_OBJECT
public:
  explicit LegendWidget(QWidget *parent = nullptr);
  ~LegendWidget();

  auto legend() const -> const QwtLegend *;
  auto updateVisibilityState(const QVector<QVariant> &infos) -> void;

private slots:
  void onLegendItemChecked(const QVariant &info, bool on, int index);

private:
  QwtLegend *m_legend;

  Ui::LegendWidget *ui;

signals:
  void toggleAllCurvesVisibility(bool visible);
  void toggleCurveVisibility(const QVariant &item, bool visible);
};

#endif // LEGENDWIDGET_H
