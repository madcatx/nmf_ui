#include "profileslicedialog.h"
#include "ui_profileslicedialog.h"

#include <ui/models/ionicconcentrationsmodel.h>
#include <util/doubletostringconvertor.h>

ProfileSliceDialog::ProfileSliceDialog(QWidget *parent) :
  QDialog{parent},
  ui{new Ui::ProfileSliceDialog}
{
  ui->setupUi(this);

  m_model = new IonicConcentrationsModel{this};
  ui->qtbv_concentrations->setModel(m_model);
}

ProfileSliceDialog::~ProfileSliceDialog()
{
  delete ui;
}

auto ProfileSliceDialog::setData(
  const QString &name,
  const int cell,
  const double x,
  const double value,
  const double effectiveMobility,
  const QString &caption,
  const QString &unit,
  const std::vector<std::pair<std::string, double>> &concentrations
) -> void
{
  ui->ql_profile->setText(name);
  ui->ql_cell->setText(QString::number(cell));

  ui->ql_valueCap->setText(caption);
  auto valueStr = util::DoubleToStringConvertor::convert(value, 6);
  if (!unit.isEmpty())
    valueStr += QString{" (%1)"}.arg(unit);
  ui->ql_value->setText(valueStr);

  ui->ql_position->setText(util::DoubleToStringConvertor::convert(x, 6));
  ui->ql_effectiveMobility->setText(util::DoubleToStringConvertor::convert(effectiveMobility, 6));

  const auto concsVisible = !concentrations.empty();
  ui->ql_effectiveMobilityCap->setVisible(concsVisible);
  ui->ql_effectiveMobility->setVisible(concsVisible);
  ui->ql_concentrations->setVisible(concsVisible);
  ui->qtbv_concentrations->setVisible(concsVisible);

  if (concsVisible) {
    IonicConcentrationsModel::Data data{};
    data.reserve(concentrations.size());

    for (const auto &item : concentrations)
      data.emplace_back(QString::fromStdString(item.first), item.second);

    m_model->update(std::move(data));
    ui->qtbv_concentrations->resizeColumnsToContents();
  }
}
