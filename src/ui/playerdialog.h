#ifndef PLAYERDIALOG_H
#define PLAYERDIALOG_H

#include <QDialog>

namespace Ui {
class PlayerDialog;
}

class PlayerDialog : public QDialog
{
  Q_OBJECT

public:
  explicit PlayerDialog(const std::size_t numFrames, QWidget *parent = nullptr);
  ~PlayerDialog();
  auto currentFrame() -> int;

private:
  auto process() -> void;
  auto updateDisplay() -> void;

  Ui::PlayerDialog *ui;

  size_t m_currentFrame;
  const size_t m_numFrames;

signals:
  void changeFrame(const std::size_t idx);
};

#endif // PLAYERDIALOG_H
