#ifndef SYSTEMINFODIALOG_H
#define SYSTEMINFODIALOG_H

#include <QDialog>

class QLabel;

namespace Ui {
class SystemInfoDialog;
}

class SystemInfoDialog : public QDialog {
  Q_OBJECT
public:
  explicit SystemInfoDialog(QWidget *parent = nullptr);
  ~SystemInfoDialog();

  auto setSimd(QLabel *l, const bool available) -> void;

private:
  Ui::SystemInfoDialog *ui;
};

#endif // SYSTEMINFODIALOG_H
