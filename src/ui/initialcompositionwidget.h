#ifndef INITIALCOMPOSITIONWIDGET_H
#define INITIALCOMPOSITIONWIDGET_H

#include <QWidget>

namespace Ui {
  class InitialCompositionWidget;
} // namespace Ui

namespace gearbox {
  class ComplexationManager;
  class ConcentrationProfilesModel;
  class DatabaseProxy;
  class GDMProxy;
  class ProfilesColorizerModel;
} // namespace gearbox

class ComplexationColorizerDelegate;
class FloatingValueDelegateConcentration;
class InitialCompositionModel;
class InitialProfilesDialog;
class MainParametersWidget;
class ProfilesColorizerDelegate;

class InitialCompositionWidget : public QWidget {
  Q_OBJECT
public:
  explicit InitialCompositionWidget(
    MainParametersWidget *mainParamsWidget,
    QWidget *parent = nullptr
  );
  ~InitialCompositionWidget();

  auto refreshComposition() -> void;

private:
  auto constituentsTableWidth() -> int;
  auto editColor(const std::string &name) -> void;
  auto editComplexation(const std::string &name) -> void;
  auto editConstituent(const std::string &name) -> void;

  Ui::InitialCompositionWidget *ui;

  ComplexationColorizerDelegate *m_ccDelegate;
  FloatingValueDelegateConcentration *m_fltDelegateConcs;
  InitialCompositionModel *m_model;
  InitialProfilesDialog *m_profilesDialog;
  ProfilesColorizerDelegate *m_profClrDelegate;

  gearbox::ConcentrationProfilesModel &h_concProfsModel;
  gearbox::ComplexationManager &h_cpxMgr;
  gearbox::DatabaseProxy &h_dbProxy;
  gearbox::GDMProxy &h_gdmProxy;
  gearbox::ProfilesColorizerModel &h_profClrModel;

  auto setupIcons() -> void;

private slots:
  void onAddConstituent();
  void onConstituentListDoubleClicked(const QModelIndex &idx);
  void onClearClicked();
  void onInitialProfilesClicked();
  void onRemoveConstituent();

signals:
  void arrangementChanged();
  void compositionChanged();
};

#endif // INITIALCOMPOSITIONWIDGET_H
