#ifndef RECORDINGDIALOG_H
#define RECORDINGDIALOG_H

#include <QDialog>

namespace Ui {
class RecordingDialog;
}

class QFileDialog;

class RecordingDialog : public QDialog {
  Q_OBJECT
public:
  class Parameters {
  public:
    Parameters();

    bool enabled;
    QString path;
    int rate;
  };

  explicit RecordingDialog(QWidget *parent = nullptr);
  ~RecordingDialog();
  auto parameters() const -> Parameters;

private:
  Ui::RecordingDialog *ui;

  QFileDialog *m_fileDlg;
  Parameters m_parameters;

private slots:
  void onBrowseClicked();
  void onEnableRecordingToggled(const bool checked);
  void onOkClicked();
  void onRejected();
};

#endif // RECORDINGDIALOG_H
