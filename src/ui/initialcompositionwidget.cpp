#include "initialcompositionwidget.h"
#include "ui_initialcompositionwidget.h"

#include <gearbox/complexationmanager.h>
#include <gearbox/concentrationprofilesmodel.h>
#include <gearbox/constituentmanipulator.h>
#include <gearbox/defaults.h>
#include <gearbox/gdmproxy.h>
#include <gearbox/profilescolorizermodel.h>
#include <gearbox/singletons.h>
#include <ui/editconstituentdialog.h>
#include <ui/initialprofilesdialog.h>
#include <ui/initialprofileswidget.h>
#include <ui/complexation/complexationcolorizerdelegate.h>
#include <ui/elementaries/floatingvaluedelegateconcentration.h>
#include <ui/elementaries/profilescolorizerdelegate.h>
#include <ui/models/initialcompositionmodel.h>
#include <util/lowlevel.h>
#include <util/premadevalidators.h>

#include <QColorDialog>
#include <QMessageBox>
#include <QScrollBar>

static const bool DISPLAY_VISCOSITY_CORRECTION{false};

static
auto enableDragDrop(QTableView *v)
{
  v->setDragDropMode(QAbstractItemView::DragDrop);
  v->setDragDropOverwriteMode(false);
  v->setDropIndicatorShown(true);
  v->setDragEnabled(true);
  v->setAcceptDrops(true);
}

static
auto makeEditDialog(
  const std::string &name,
  gearbox::GDMProxy &proxy,
  gearbox::DatabaseProxy &dbProxy,
  const bool viscosityCorrectionEnabled
) -> EditConstituentDialog *
{
  assert(proxy.contains(name));

  gdm::Constituent ctuent = proxy.get(name);
  auto type = [](gdm::ConstituentType _type) {
    switch (_type) {
    case gdm::ConstituentType::Nucleus:
      return IConstituentEditor::ConstituentType::NUCLEUS;
    case gdm::ConstituentType::Ligand:
      return IConstituentEditor::ConstituentType::LIGAND;
    }
    util::impossible_path();
  }(ctuent.type());

  const bool allowTypeChange = !proxy.complexes(name);

  return new EditConstituentDialog{dbProxy, QString::fromStdString(name),
                                   type, ctuent.physicalProperties(),
                                   allowTypeChange,
                                   viscosityCorrectionEnabled};
}

InitialCompositionWidget::InitialCompositionWidget(
  MainParametersWidget *mainParamsWidget,
  QWidget *parent) :
  QWidget{parent},
  ui{new Ui::InitialCompositionWidget},
  h_concProfsModel{gearbox::Singletons::concentrationProfilesModel()},
  h_cpxMgr{gearbox::Singletons::complexationManager()},
  h_dbProxy{gearbox::Singletons::databaseProxy()},
  h_gdmProxy{gearbox::Singletons::gdmProxy()},
  h_profClrModel{gearbox::Singletons::profilesColorizerModel()}
{
  ui->setupUi(this);

  setupIcons();

  auto validatorVec = QVariant::fromValue<util::AdditionalValidatorVec<double>>( {
    util::PremadeValidators::mustBeNonNegative<double>()
  } );
  m_fltDelegateConcs = new FloatingValueDelegateConcentration{this};
  m_fltDelegateConcs->setProperty(
    util::AdditionalValidator<double>::PROPERTY_NAME,
    validatorVec
  );

  m_model = new InitialCompositionModel{this};
  auto pw = new InitialProfilesWidget{h_concProfsModel, mainParamsWidget, this};
  m_profilesDialog = new InitialProfilesDialog{pw, this};

  m_ccDelegate = new ComplexationColorizerDelegate{m_model, this};
  m_profClrDelegate = new ProfilesColorizerDelegate{m_model, this};

  ui->qtbv_constituents->setModel(m_model);
  ui->qtbv_constituents->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

  ui->qtbv_constituents->setItemDelegateForColumn(0, m_ccDelegate);
  ui->qtbv_constituents->setItemDelegateForColumn(2, m_profClrDelegate);
  ui->qtbv_constituents->setItemDelegateForColumn(6, m_fltDelegateConcs);
  ui->qtbv_constituents->setItemDelegateForColumn(7, m_fltDelegateConcs);
  enableDragDrop(ui->qtbv_constituents);

  connect(
    &h_concProfsModel,
    &gearbox::ConcentrationProfilesModel::compositionChanged,
    pw,
    &InitialProfilesWidget::onCompositionChanged
  );
  connect(
    ui->qpb_addConstituent,
    &QPushButton::clicked,
    this,
    &InitialCompositionWidget::onAddConstituent
  );
  connect(
    ui->qpb_removeConstituent,
    &QPushButton::clicked,
    this,
    &InitialCompositionWidget::onRemoveConstituent
  );
  connect(
    ui->qpb_initialProfiles,
    &QPushButton::clicked,
    this,
    &InitialCompositionWidget::onInitialProfilesClicked
  );
  connect(
    ui->qtbv_constituents,
    &QTableView::doubleClicked,
    this,
    &InitialCompositionWidget::onConstituentListDoubleClicked
  );
  connect(
    ui->qpb_clear,
    &QPushButton::clicked,
    this,
    &InitialCompositionWidget::onClearClicked
  );
  connect(
    m_model,
    &QAbstractTableModel::dataChanged,
    [this]() { emit arrangementChanged(); }
  );
  connect(
    &h_concProfsModel,
    &gearbox::ConcentrationProfilesModel::compositionChanged,
    m_model,
    &InitialCompositionModel::onCompositionChanged
  );

  setMinimumWidth(constituentsTableWidth() + ui->qpb_addConstituent->width());
}

InitialCompositionWidget::~InitialCompositionWidget()
{
  delete ui;
}

auto InitialCompositionWidget::constituentsTableWidth() -> int
{
  const auto hdr = ui->qtbv_constituents->horizontalHeader();
  auto count = hdr->count();
  auto scrollBarWidth = ui->qtbv_constituents->verticalScrollBar()->width();
  int totalWidth = scrollBarWidth;

  for (int idx{0}; idx < count; idx++)
    totalWidth += hdr->sectionSize(idx);
  return totalWidth;
}

auto InitialCompositionWidget::editColor(const std::string &name) -> void
{
  QColorDialog dlg{h_profClrModel.get(name)};

  if (dlg.exec() == QDialog::Accepted)
    h_profClrModel.changeColor(name, dlg.selectedColor());
}

auto InitialCompositionWidget::editComplexation(const std::string &name) -> void
{
  if (!h_cpxMgr.complexes(name))
    return;

  h_cpxMgr.editComplexation(name);
}

auto InitialCompositionWidget::editConstituent(const std::string &name) -> void
{
  gearbox::ConstituentManipulator manipulator{h_gdmProxy};
  auto dlg = makeEditDialog(name, h_gdmProxy, h_dbProxy, DISPLAY_VISCOSITY_CORRECTION);

  connect(
    dlg,
    &EditConstituentDialog::validateInput,
    &manipulator,
    &gearbox::ConstituentManipulator::onValidateConstituentInputUpdate
  );

  if (dlg->exec() == QDialog::Accepted) {
    gdm::Constituent updatedCtuent = manipulator.makeConstituent(dlg);

    try {
      if (!h_gdmProxy.update(name, updatedCtuent)) {
        QMessageBox mbox{
          QMessageBox::Warning,
          tr("Operation failed"),
          tr("Failed to update the constituent properties")
        };
        mbox.exec();
      } else {
        h_concProfsModel.updateName(name, dlg->name().toStdString());
        h_concProfsModel.announceUpdate();
        h_profClrModel.updateName(name, dlg->name().toStdString());
        m_model->updateName(QString::fromStdString(name), dlg->name());
        h_cpxMgr.refreshAll();
        ui->qtbv_constituents->resizeColumnsToContents();
      }

      emit compositionChanged();
    } catch (GDMProxyException &ex) {
      QMessageBox mbox{QMessageBox::Warning, tr("Operation failed"), ex.what()};
      mbox.exec();
    }
  }

  delete dlg;
}

void InitialCompositionWidget::onAddConstituent()
{
  static QSize dlgSize{};

  EditConstituentDialog dlg{h_dbProxy, DISPLAY_VISCOSITY_CORRECTION, this};
  gearbox::ConstituentManipulator manipulator{h_gdmProxy};

  if (!dlgSize.isEmpty())
    dlg.resize(dlgSize);

  connect(
    &dlg,
    &EditConstituentDialog::validateInput,
    &manipulator,
    &gearbox::ConstituentManipulator::onValidateConstituentInput
  );

  if (dlg.exec() == QDialog::Accepted) {
    auto constituent = manipulator.makeConstituent(&dlg);

    h_gdmProxy.insert(constituent);
    h_concProfsModel.add(
      constituent.name(),
      gearbox::Defaults::CONCENTRATION_PROFILE_SHAPE,
      { 0.0, 0.0 },
      {{ gearbox::Defaults::INJECTION_POSITION, gearbox::Defaults::INJECTION_WIDTH }}
    );
    h_concProfsModel.announceUpdate();
    h_profClrModel.add(constituent.name());
    //m_model->add(QString::fromStdString(constituent.name()));

    dlgSize = dlg.size();
    ui->qtbv_constituents->resizeColumnsToContents();

    emit compositionChanged();
  }
}

void InitialCompositionWidget::onClearClicked()
{
  h_concProfsModel.clear();
  h_profClrModel.clear();
  h_profClrModel.reset();
  h_gdmProxy.clear();
  h_concProfsModel.announceUpdate();

  emit compositionChanged();
}

void InitialCompositionWidget::onConstituentListDoubleClicked(const QModelIndex &idx)
{
  if (!idx.isValid())
    return;

  const auto v = m_model->data(m_model->index(idx.row(), 3));
  const auto name = v.toString().toStdString();

  if (h_concProfsModel.find(name) == h_concProfsModel.cend())
    return;

  if (idx.column() < 2)
    editComplexation(name);
  else if (idx.column() == 2)
    editColor(name);
  else if (idx.column() == 3)
    editConstituent(name);
}

auto InitialCompositionWidget::onInitialProfilesClicked() -> void
{
  if (m_profilesDialog->exec() == QDialog::Accepted)
    emit arrangementChanged();
}

void InitialCompositionWidget::onRemoveConstituent()
{
  const auto indexes = ui->qtbv_constituents->selectionModel()->selectedIndexes();

  if (indexes.empty())
    return;

  QVector<QString> keys{};
  for (const auto &idx : indexes) {
    if (!idx.isValid())
      continue;
    auto name = m_model->data(m_model->index(idx.row(), 3)).toString();
    if (!keys.contains(name)) /* TODO: There must be a better way than this */
      keys.append(std::move(name));
  }

  for (const auto &k : keys) {
    const auto stdName = k.toStdString();
    h_gdmProxy.erase(stdName);
    h_concProfsModel.remove(stdName);
    h_concProfsModel.announceUpdate();
    h_profClrModel.remove(stdName);
    //m_model->remove(k);
  }

  emit compositionChanged();
}

auto InitialCompositionWidget::refreshComposition() -> void
{
  m_model->refreshAll();
}

auto InitialCompositionWidget::setupIcons() ->void
{
#ifdef Q_OS_LINUX
  ui->qpb_addConstituent->setIcon(QIcon::fromTheme("list-add"));
  ui->qpb_removeConstituent->setIcon(QIcon::fromTheme("list-remove"));
#else
  ui->qpb_addConstituent->setIcon(style()->standardIcon(QStyle::SP_DialogOkButton));
  ui->qpb_removeConstituent->setIcon(style()->standardIcon(QStyle::SP_DialogCancelButton));
#endif // Q_OS_LINUX
}
