#include "legendwidget.h"
#include "ui_legendwidget.h"

#include <qwt_plot_item.h>
#include <qwt_legend_label.h>

#include <cassert>

LegendWidget::LegendWidget(QWidget *parent) :
  QWidget{parent},
  ui{new Ui::LegendWidget}
{
  ui->setupUi(this);

  m_legend = new QwtLegend{};
  m_legend->setMaxColumns(1);
  m_legend->setDefaultItemMode(QwtLegendData::Checkable);
  ui->qlay_legend->addWidget(m_legend);
  m_legend->setSizePolicy({QSizePolicy::Preferred, QSizePolicy::Minimum});

  connect(m_legend, &QwtLegend::checked, this, &LegendWidget::onLegendItemChecked);
  connect(ui->qpb_showAll, &QPushButton::clicked, this, [this]() { emit toggleAllCurvesVisibility(true); });
  connect(ui->qpb_hideAll, &QPushButton::clicked, this, [this]() { emit toggleAllCurvesVisibility(false); });
}

LegendWidget::~LegendWidget()
{
  delete ui;
}

auto LegendWidget::legend() const -> const QwtLegend *
{
  return m_legend;
}

void LegendWidget::onLegendItemChecked(const QVariant &info, bool on, int index)
{
  emit toggleCurveVisibility(info, !on);
}

auto LegendWidget::updateVisibilityState(const QVector<QVariant> &infos) -> void
{
  for (const auto &info : infos) {
    assert(info.canConvert<QwtPlotItem *>());

    const auto item = info.value<QwtPlotItem *>();
    const bool checked = !item->isVisible();

    auto widgets = m_legend->legendWidgets(info);

    for (auto &w : widgets) {
      auto label = qobject_cast<QwtLegendLabel *>(w);
      if (label)
        label->setChecked(checked);
    }
  }
}
