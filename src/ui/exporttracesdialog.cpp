#include "exporttracesdialog.h"
#include "ui_exporttracesdialog.h"

#include <gearbox/csvexporter.h>
#include <ui/util.h>

#include <QCheckBox>
#include <QMessageBox>
#include <QStandardItemModel>

ExportTracesDialog::ExportTracesDialog(QWidget *parent) :
  QDialog{parent},
  ui{new Ui::ExportTracesDialog},
  m_conductivity{nullptr},
  m_pH{nullptr}
{
  ui->setupUi(this);

  m_saveDlg = new QFileDialog{this};
  m_saveDlg->setAcceptMode(QFileDialog::AcceptSave);
  m_saveDlg->setNameFilter("CSV file (*.csv)");

  auto model = new QStandardItemModel{this};
  auto item = new QStandardItem{"Period (.)"};
  item->setData(QVariant::fromValue(gearbox::CSVExporter::Separator::PERIOD));
  model->appendRow(item);

  item = new QStandardItem{"Comma (,)"};
  item->setData(QVariant::fromValue(gearbox::CSVExporter::Separator::COMMA));
  model->appendRow(item);

  ui->qcbox_decimalSeparator->setModel(model);

  m_selection = new QWidget{this};
  m_selection->setLayout(new QVBoxLayout{});
  ui->qscra_profiles->setWidget(m_selection);

  connect(
    ui->qpb_toClipboard,
    &QPushButton::clicked,
    this,
    &ExportTracesDialog::onToClipboardClicked
  );
  connect(
    ui->qpb_toFile,
    &QPushButton::clicked,
    this,
    &ExportTracesDialog::onToFileClicked
  );
  connect(
    ui->buttonBox,
    &QDialogButtonBox::rejected,
    this,
    &ExportTracesDialog::reject
  );
  connect(
   ui->qpb_selectAll,
   &QPushButton::clicked,
   this,
   &ExportTracesDialog::onSelectAllClicked
  );
  connect(
    ui->qpb_deselectAll,
    &QPushButton::clicked,
    this,
    &ExportTracesDialog::onDeselectAllClicked
  );
  connect(
    ui->qcbox_trace,
    static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
    this,
    &ExportTracesDialog::onTraceChanged
  );
}

ExportTracesDialog::~ExportTracesDialog()
{
  delete ui;
}

void ExportTracesDialog::onDeselectAllClicked()
{
  for (auto &[item, name] : m_profileSelectors)
    item->setCheckState(Qt::Unchecked);
}

void ExportTracesDialog::onSelectAllClicked()
{
  for (auto &[item, name] : m_profileSelectors) {
    if (item->isEnabled())
      item->setCheckState(Qt::Checked);
  }
}

void ExportTracesDialog::onToClipboardClicked()
{
  const auto varSep = ui->qcbox_decimalSeparator->currentData(Qt::UserRole + 1);
  const auto sep = varSep.value<gearbox::CSVExporter::Separator>();
  const auto trace = displayIndexToDetectorIndex(ui->qcbox_trace->currentData().toInt(), m_detectors);

  try {
    gearbox::CSVExporter::write(
      gearbox::CSVExporter::Target::CLIPBOARD,
      sep,
      ui->qle_fieldDelimiter->text(),
      "",
      m_data,
      selectedProfiles(),
      trace
    );
  } catch (const gearbox::CSVExporter::Error &ex) {
    QMessageBox mbox{
      QMessageBox::Warning,
      tr("Failed to export profiles"),
      ex.what()
    };
    mbox.exec();
  }
}

void ExportTracesDialog::onToFileClicked()
{
  if (m_saveDlg->exec() != QDialog::Accepted)
    return;

  const auto path = m_saveDlg->selectedFiles().constFirst();

  const auto varSep = ui->qcbox_decimalSeparator->currentData(Qt::UserRole + 1);
  const auto sep = varSep.value<gearbox::CSVExporter::Separator>();
  const auto trace = ui->qcbox_trace->currentData().toInt();

  try {
    gearbox::CSVExporter::write(
      gearbox::CSVExporter::Target::FILE,
      sep,
      ui->qle_fieldDelimiter->text(),
      path,
      m_data,
      selectedProfiles(),
      trace
    );
  } catch (const gearbox::CSVExporter::Error &ex) {
    QMessageBox mbox{
      QMessageBox::Warning,
      tr("Failed to export profiles"),
      ex.what()
    };
    mbox.exec();
  }
}

void ExportTracesDialog::onTraceChanged(const int)
{
  const auto data = ui->qcbox_trace->currentData();
  const auto idx = data.toInt();

  if (m_conductivity) {
    m_conductivity->setEnabled(idx == -1);
    m_conductivity->setCheckState(Qt::Unchecked);
  }
  if (m_pH) {
    m_pH->setEnabled(idx == -1);
    m_pH->setCheckState(Qt::Unchecked);
  }
}

auto ExportTracesDialog::selectedProfiles() -> std::vector<std::string>
{
  std::vector<std::string> selected{};

  for (auto &[cb, name] : m_profileSelectors) {
    if (cb->checkState() == Qt::Checked && cb->isEnabled())
      selected.push_back(name);
  }

  return selected;
}

auto ExportTracesDialog::setData(const std::vector<std::pair<bool, double> > &detectors, calculators::SimStateData data) -> void
{
  ui->qcbox_trace->clear();
  ui->qcbox_trace->addItem(tr("Concentrations"), -1);
  for (size_t idx{0}; idx < detectors.size(); idx++) {
    const auto &item = detectors[idx];
    if (item.first)
      ui->qcbox_trace->addItem(detectorTag(true, item.second, idx), QVariant{int(idx)});
  }

  m_detectors = detectors;
  m_data = std::move(data);

  auto layout = m_selection->layout();
  while (!layout->isEmpty()) {
    auto li = layout->takeAt(0);
    if (li->widget())
      delete li->widget();
    else if (li->spacerItem())
      delete li->spacerItem();
    delete li;
  }

  m_profileSelectors.clear();
  m_conductivity = nullptr;
  m_pH = nullptr;

  for (const auto &name : m_data.constituentNames) {
    auto qName = QString::fromStdString(name);
    auto cb = new QCheckBox{this};
    cb->setText(qName);
    layout->addWidget(cb);
    m_profileSelectors.emplace_back(cb, name);
  }

  m_conductivity = new QCheckBox{this};
  m_conductivity->setText(tr("Conductivity"));
  layout->addWidget(m_conductivity);
  m_profileSelectors.emplace_back(m_conductivity, "Conductivity");

  m_pH = new QCheckBox{this};
  m_pH->setText("pH");
  layout->addWidget(m_pH);
  m_profileSelectors.emplace_back(m_pH, "pH");
}
