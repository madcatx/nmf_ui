#include "recordingdialog.h"
#include "ui_recordingdialog.h"

#include <recording/recorder.h>

#include <QFileDialog>
#include <QMessageBox>

static int DEFAULT_RATE{1000};

RecordingDialog::Parameters::Parameters() :
  enabled{false},
  rate{DEFAULT_RATE}
{
}

RecordingDialog::RecordingDialog(QWidget *parent) :
  QDialog{parent},
  ui{new Ui::RecordingDialog}
{
  ui->setupUi(this);

  m_fileDlg = new QFileDialog{this};
  m_fileDlg->setAcceptMode(QFileDialog::AcceptSave);
  m_fileDlg->setNameFilter(QString{"NMF %1 file (*.%1)"}.arg(NMFUI_RECORDING_FILE_SUFFIX));

  ui->qspb_rate->setRange(100, 30000);
  ui->qspb_rate->setValue(DEFAULT_RATE);

  connect(
    ui->qcb_enableRecording,
    &QCheckBox::toggled,
    this,
    &RecordingDialog::onEnableRecordingToggled
  );
  connect(
    ui->qpb_browse,
    &QPushButton::clicked,
    this,
    &RecordingDialog::onBrowseClicked
  );
  connect(
    ui->buttonBox,
    &QDialogButtonBox::accepted,
    this,
    &RecordingDialog::onOkClicked
  );
  connect(
    ui->buttonBox,
    &QDialogButtonBox::rejected,
    this,
    &RecordingDialog::reject
  );
  connect(
    this,
    &RecordingDialog::rejected,
    this,
    &RecordingDialog::onRejected
  );
  connect(ui->qle_file, &QLineEdit::editingFinished, [this]() {
    const auto text = ui->qle_file->text();
    const auto suffix = QString{NMFUI_RECORDING_FILE_SUFFIX};
    if (!text.endsWith(suffix))
      ui->qle_file->setText(text + QString{".%1"}.arg(suffix));
  });

  m_parameters.enabled = false;
  m_parameters.rate = DEFAULT_RATE;

  ui->qcb_enableRecording->setCheckState(Qt::Unchecked);
  onEnableRecordingToggled(false);
}

RecordingDialog::~RecordingDialog()
{
  delete ui;
}

void RecordingDialog::onBrowseClicked()
{
  const auto path = ui->qle_file->text();

  if (!path.isEmpty())
    m_fileDlg->setDirectory(QFileInfo{path}.absolutePath());

  if (m_fileDlg->exec() == QDialog::Accepted)
    ui->qle_file->setText(m_fileDlg->selectedFiles().constFirst());
}

void RecordingDialog::onEnableRecordingToggled(const bool checked)
{
  ui->qle_file->setEnabled(checked);
  ui->qpb_browse->setEnabled(checked);
  ui->qspb_rate->setEnabled(checked);
}

void RecordingDialog::onOkClicked()
{
  m_parameters.enabled = ui->qcb_enableRecording->isChecked();
  if (m_parameters.enabled) {
    m_parameters.path = ui->qle_file->text();
    if (m_parameters.path.isEmpty()) {
      QMessageBox mbox{QMessageBox::Warning, tr("Invalid input"), tr("No output file was specified")};
      mbox.exec();
      return;
    }
    m_parameters.rate = ui->qspb_rate->value();
  }

  accept();
}

auto RecordingDialog::parameters() const -> Parameters
{
  return m_parameters;
}

void RecordingDialog::onRejected()
{
  ui->qcb_enableRecording->setChecked(m_parameters.enabled);
  ui->qle_file->setText(m_parameters.path);
  ui->qspb_rate->setValue(m_parameters.rate);

  onEnableRecordingToggled(m_parameters.enabled);
}

