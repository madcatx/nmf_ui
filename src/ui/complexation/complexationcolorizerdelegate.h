#ifndef COMPLEXATIONCOLORIZERDELEGATE_H
#define COMPLEXATIONCOLORIZERDELEGATE_H

#include <ui/models/initialcompositionmodel.h>

#include <QItemDelegate>

class ComplexationColorizerDelegate : public QItemDelegate {
public:
  explicit ComplexationColorizerDelegate(InitialCompositionModel *model, QObject *parent = nullptr);
  virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
  InitialCompositionModel *h_model;
};

#endif // COMPLEXATIONCOLORIZERDELEGATE_H
