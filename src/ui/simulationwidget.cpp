#include "simulationwidget.h"
#include "ui_simulationwidget.h"

#include <gearbox/profilescolorizermodel.h>
#include <gearbox/singletons.h>
#include <ui/elementaries/doubleclickableqwtplotzoomer.h>
#include <ui/legendwidget.h>
#include <ui/simulationwidgetcontrols.h>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_marker.h>
#include <qwt_scale_div.h>
#include <qwt_text.h>

#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

#include <algorithm>
#include <cassert>
#include <functional>

static const QString CONDUCTIVITY_TITLE{QObject::tr("Conductivity")};
static const QString PH_TITLE{QObject::tr("pH")};

static
auto findNearest(const QwtSeriesData<QPointF> *data, const qreal x)
{
  size_t left{0};
  size_t right{data->size() - 1};
  size_t pivot{right};

  while (right - left > 1) {
    pivot = (right - left) / 2 + left;
    const auto cx = data->sample(pivot).x();

    if (cx > x)
      right = pivot;
    else
      left = pivot;
  }

  return pivot;
}

SimulationWidget::SimulationWidget(QWidget *parent) :
  QWidget{parent},
  ui{new Ui::SimulationWidget},
  h_profClrModel{gearbox::Singletons::profilesColorizerModel()}
{
  ui->setupUi(this);

  setLayout(new QVBoxLayout{});

  auto plotRowLayout = new QHBoxLayout{};

  m_plot = new QwtPlot{this};
  m_legendWidget = new LegendWidget{this};

  connect(m_plot, &QwtPlot::legendDataChanged, m_legendWidget->legend(), &QwtLegend::updateLegend);
  connect(m_legendWidget, &LegendWidget::toggleCurveVisibility, this, &SimulationWidget::onToggleCurveVisibility);
  connect(m_legendWidget, &LegendWidget::toggleAllCurvesVisibility, this, &SimulationWidget::onToggleAllCurvesVisibility);

  m_ctrls = new SimulationWidgetControls{this};

  plotRowLayout->addWidget(m_plot, 11);
  plotRowLayout->addWidget(m_legendWidget, 1);

  connect(
    m_ctrls,
    &SimulationWidgetControls::traceChanged,
    this,
    &SimulationWidget::onTraceChanged
  );
  connect(
    m_ctrls,
    &SimulationWidgetControls::toggleLegend,
    [this]() {
      m_legendWidget->setVisible(!m_legendWidget->isVisible());
    }
  );
  connect(
    m_ctrls,
    &SimulationWidgetControls::zoomToFit,
    [this]() {
      this->setBaseZoom();
    }
  );

  setupPlot();
  m_currentPlotMode = PlotMode::NONE;

  layout()->addItem(plotRowLayout);
  layout()->addWidget(m_ctrls);
}

SimulationWidget::~SimulationWidget()
{
  m_conductivityCurve->detach();
  delete m_conductivityCurve;

  m_pHCurve->detach();
  delete m_pHCurve;

  delete ui;
}

auto SimulationWidget::attachNonConcentrationCurves(const bool attach)
{
  if (attach) {
    m_conductivityCurve->attach(m_plot);
    m_pHCurve->attach(m_plot);
  } else {
    m_conductivityCurve->detach();
    m_pHCurve->detach();
  }
}

auto SimulationWidget::clear() -> void
{
  for (auto &curve : m_constituentCurves) {
    curve->detach();
    delete curve;
  }

  m_constituentCurves.clear();

  m_conductivityCurve->setSamples({});
  m_pHCurve->setSamples({});

  attachNonConcentrationCurves(false);

  m_plot->replot();
}

auto SimulationWidget::displayedTrace() -> int
{
  return m_ctrls->trace();
}

auto SimulationWidget::drawDetectorPositions(const std::vector<std::pair<bool, double> > &detectors) -> void
{
  for (auto & dm : m_detectors) {
    dm->detach();
    delete dm;
  }
  m_detectors.clear();

  for (const auto &item : detectors) {
    auto dm = new QwtPlotMarker{};
    auto clr = item.first ? QColor{32, 32, 32} : QColor{212, 126, 126};
    dm->setXValue(item.second);
    dm->setLineStyle(QwtPlotMarker::VLine);
    dm->setLinePen(clr, 1.0, Qt::DotLine);
    dm->setVisible(true);
    dm->attach(m_plot);

    m_detectors.emplace_back(dm);
  }

  m_plot->replot();

  m_ctrls->setDetectors(detectors);
}

auto SimulationWidget::drawDetectorTrace(const std::vector<std::pair<double, std::vector<double>>> &trace) -> void
{
  setDetectorMode();

  QVector<QVector<QPointF>> allSamples{};
  allSamples.resize(m_constituentCurves.size());

  for (int idx{0}; idx < allSamples.size(); idx++) {
    for (size_t jdx{0}; jdx < trace.size(); jdx++) {
      const auto t = trace[jdx].first;
      allSamples[idx].push_back(QPointF{t, trace[jdx].second[idx]});
    }
  }

  for (size_t idx{0}; idx < m_constituentCurves.size(); idx++)
    m_constituentCurves[idx]->setSamples(allSamples[idx]);

  setBaseZoom();
}

auto SimulationWidget::drawProfiles(
  const std::vector<double> &conductivity,
  const std::vector<double> &pH,
  const std::vector<std::vector<double>> &constituents,
  const double dx
) -> void
{
  assert(constituents.size() == m_constituentCurves.size());

  const auto changed = setCapillaryMode(true);

  static QVector<QPointF> vec{};
  if (vec.size() != conductivity.size())
    vec.resize(conductivity.size());

  const double dxMM = dx * 1000.0;

  auto setCurve = [dxMM](const std::vector<double> &yData, QwtPlotCurve *curve,
                               auto xfrm) {
    double x{0.0};
    for (size_t idx{0}; idx < yData.size(); idx++) {
      vec[idx] = QPointF{x, yData[idx]};
      x += dxMM;
    }

    xfrm(vec);

    curve->setSamples(vec);
  };

  setCurve(conductivity, m_conductivityCurve, [](QVector<QPointF>& vec) {
    std::transform(vec.begin(), vec.end(), vec.begin(),
                   [](QPointF &p) { return QPointF{p.x(), p.y() * 1.0e2}; } );
    }
  );
  setCurve(pH, m_pHCurve, [](QVector<QPointF>&){});
  for (size_t idx{0}; idx < m_constituentCurves.size(); idx++)
    setCurve(constituents[idx], m_constituentCurves[idx], [](QVector<QPointF>&){});

  if (changed)
    setBaseZoom();
}

void SimulationWidget::onPointSelected(const QPointF &pt)
{
  if (m_constituentCurves.empty())
    return;

  if (m_currentPlotMode != PlotMode::CAPILLARY)
    return;

  const auto x = pt.x();
  const auto yLeft = pt.y();
  const auto yRP = m_plot->transform(QwtPlot::yLeft, pt.y());
  const auto yRight = m_plot->invTransform(QwtPlot::yRight, yRP);

  const QwtPlotCurve *selLeft{nullptr};
  const QwtPlotCurve *selRight{nullptr};

  const auto yLeftDiv = m_plot->axisScaleDiv(QwtPlot::yLeft);
  const auto yLeftRange = std::abs(yLeftDiv.upperBound() - yLeftDiv.lowerBound());
  const auto yRightDiv = m_plot->axisScaleDiv(QwtPlot::yRight);
  const auto yRightRange = std::abs(yRightDiv.upperBound() - yRightDiv.lowerBound());

  const auto size = m_constituentCurves.front()->data()->size();
  const auto nearestIdx = findNearest(m_constituentCurves.front()->data(), x);
  const auto leftOf = nearestIdx == 0 ? 0 : nearestIdx - 1;
  const auto rightOf = nearestIdx >= size - 1 ? size - 1 : nearestIdx + 1;

  auto testCurve = [leftOf, rightOf, x, nearestIdx, this](const QwtPlotCurve *curve, const qreal y, qreal &dist, const QwtPlotCurve *&sel) {
    if (!curve->isVisible())
      return;

    const auto data = curve->data();
    assert(curve->data()->size() == m_constituentCurves.front()->data()->size());

    const auto nearest = data->sample(nearestIdx);
    const auto left = data->sample(leftOf);
    const auto right = data->sample(rightOf);

    double d{};
    /* Interpolate to find the best match even on closely zoomed in plots */
    const auto kl = (nearest.y() - left.y()) / (nearest.x() - left.x());
    const auto ql = left.y();
    const auto yL = kl * (x - left.x()) + ql;

    const auto kr = (right.y() - nearest.y()) / (right.x() - nearest.x());
    const auto qr = nearest.y();
    const auto yR = kr * (x - nearest.x()) + qr;

    const double dL = std::abs(y - yL);
    const double dR = std::abs(y - yR);
    d = dL < dR ? dL : dR;
    if (d < dist) {
      dist = d;
      sel = curve;
    }
  };

  qreal distLeft = yLeftRange / 33.0;
  for (const auto curve : m_constituentCurves)
    testCurve(curve, yLeft, distLeft, selLeft);

  qreal distRight = yRightRange / 33.0;
  testCurve(m_conductivityCurve, yRight, distRight, selRight);
  testCurve(m_pHCurve, yRight, distRight, selRight);

  distRight *= yLeftRange / yRightRange;
  const auto sel = distLeft < distRight ? selLeft : selRight;

  if (sel) {
    auto type = [&sel]() {
      if (sel->title() == CONDUCTIVITY_TITLE)
        return CurveType::CONDUCTIVITY;
      else if (sel->title() == PH_TITLE)
        return CurveType::PH;
      return CurveType::CONSTITUENT;
    }();

    emit curvePointSelected(x, sel->title().text(), type);
  }
}

void SimulationWidget::onToggleAllCurvesVisibility(bool visible)
{
  auto toggler = [](const bool v) -> std::function<void (QwtPlotCurve *)> {
    if (v)
      return [](QwtPlotCurve *item) { item->show(); };
    else
      return [](QwtPlotCurve *item) { item->hide(); };
  }(visible);

  QVector<QVariant> infos{};
  for (auto &curve : m_constituentCurves) {
    toggler(curve);
    infos.append(m_plot->itemToInfo(curve));
  }

  m_legendWidget->updateVisibilityState(infos);

  m_plot->replot();
}

void SimulationWidget::onToggleCurveVisibility(const QVariant &item, bool visible)
{
  if (!item.canConvert<QwtPlotItem *>())
    return;

  auto curve = item.value<QwtPlotItem *>();

  if (visible)
    curve->show();
  else
    curve->hide();

  m_plot->replot();
}

void SimulationWidget::onTraceChanged(const int idx)
{
  emit traceChanged(idx);
}

auto SimulationWidget::replot() -> void
{
  m_plot->replot();
}

auto SimulationWidget::setBaseZoom() -> void
{
  QRectF brectLeft{};
  QRectF brectRight{};

  if (m_conductivityCurve->isVisible())
    brectRight = brectRight.united(m_conductivityCurve->boundingRect());
  if (m_pHCurve->isVisible())
    brectRight = brectRight.united(m_pHCurve->boundingRect());

  for (const auto &curve : m_constituentCurves) {
    if (curve->isVisible())
      brectLeft = brectLeft.united(curve->boundingRect());
  }

  m_zoomer->zoom(brectLeft);
  m_zoomer->setZoomBase(brectLeft);

  m_zoomerRight->zoom(brectRight);
  m_zoomerRight->setZoomBase(brectRight);
}

auto SimulationWidget::setCapillaryMode(const bool attach) -> bool
{
  if (m_currentPlotMode == PlotMode::CAPILLARY)
    return false;

  m_firstCell->setVisible(true);
  m_lastCell->setVisible(true);
  for (auto *m : m_detectors)
    m->setVisible(true);

  m_plot->setAxisTitle(QwtPlot::xBottom, "x (mm)");
  m_plot->setAxisTitle(QwtPlot::yRight, "pH / Cond (S/m 1e-2)");

  if (attach)
    attachNonConcentrationCurves(true);

  m_currentPlotMode = PlotMode::CAPILLARY;

  return true;
}

auto SimulationWidget::setDetectorMode() -> bool
{
  if (m_currentPlotMode == PlotMode::DETECTOR)
    return false;

  m_firstCell->setVisible(false);
  m_lastCell->setVisible(false);
  for (auto *m : m_detectors)
    m->setVisible(false);

  m_plot->setAxisTitle(QwtPlot::xBottom, tr("t (s)"));
  m_plot->setAxisTitle(QwtPlot::yRight, "");

  attachNonConcentrationCurves(false);

  m_currentPlotMode = PlotMode::DETECTOR;

  return true;
}

auto SimulationWidget::setFirstLastCellMarkers(const double firstCell, const double lastCell) -> void
{
  m_firstCell->setXValue(firstCell);
  m_lastCell->setXValue(lastCell);
}

auto SimulationWidget::setupPlot() -> void
{
  m_zoomer = new DoubleClickableQwtPlotZoomer{m_plot->canvas()};
  m_zoomer->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
  m_zoomerRight = new DoubleClickableQwtPlotZoomer{m_plot->canvas()};
  m_zoomerRight->setAxes(QwtPlot::xTop, QwtPlot::yRight);

  m_picker = new QwtPlotPicker(QwtPlot::Axis::xBottom, QwtPlot::Axis::yLeft,
                               QwtPicker::NoRubberBand, QwtPicker::AlwaysOff,
                               m_plot->canvas());

  m_zoomer->setTrackerMode(QwtPicker::AlwaysOn);
  m_zoomerRight->setTrackerMode(QwtPicker::AlwaysOff);
  m_zoomer->setMousePattern(QwtEventPattern::MouseSelect3, Qt::LeftButton, Qt::ShiftModifier);
  m_zoomer->setMousePattern(QwtEventPattern::MouseSelect3, Qt::LeftButton, Qt::ShiftModifier);
  m_picker->setStateMachine(new QwtPickerClickPointMachine{}); /* Come blow my ClickPoint machine! */
  m_picker->setMousePattern(QwtEventPattern::MouseSelect1, Qt::RightButton);
  connect(
    m_picker,
    static_cast<void (QwtPlotPicker::*)(const QPointF &)>(&QwtPlotPicker::selected),
    this,
    &SimulationWidget::onPointSelected
  );

  m_firstCell = new QwtPlotMarker{tr("First cell")};
  m_firstCell->setLineStyle(QwtPlotMarker::VLine);
  m_firstCell->setXValue(-1);
  m_firstCell->attach(m_plot);

  m_lastCell = new QwtPlotMarker{tr("Last cell")};
  m_lastCell->setLineStyle(QwtPlotMarker::VLine);
  m_lastCell->setXValue(-1);
  m_lastCell->attach(m_plot);

  m_plot->setCanvasBackground(Qt::white);
  m_plot->enableAxis(QwtPlot::yRight);
  m_plot->setAxisTitle(QwtPlot::yLeft, "c (mM)");

  /* Conductivity curve */
  m_conductivityCurve = new QwtPlotCurve{CONDUCTIVITY_TITLE};
  m_conductivityCurve->setYAxis(QwtPlot::yRight);
  m_conductivityCurve->setPen(Qt::black, 2.0, Qt::DashLine);
  m_conductivityCurve->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
  m_conductivityCurve->setLegendAttribute(QwtPlotCurve::LegendShowSymbol, false);
  m_conductivityCurve->setPaintAttribute(QwtPlotCurve::ClipPolygons, true);
  m_conductivityCurve->setRenderHint(QwtPlotItem::RenderAntialiased);

  /* pH curve */
  m_pHCurve = new QwtPlotCurve{PH_TITLE};
  m_pHCurve->setYAxis(QwtPlot::yRight);
  m_pHCurve->setPen(QColor{255, 128, 241}, 2.0);
  m_pHCurve->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
  m_pHCurve->setLegendAttribute(QwtPlotCurve::LegendShowSymbol, false);
  m_pHCurve->setPaintAttribute(QwtPlotCurve::ClipPolygons, true);
  m_pHCurve->setRenderHint(QwtPlotItem::RenderAntialiased);
}

auto SimulationWidget::setupConstituentCurves(const std::vector<std::string> &names) -> void
{
  clear();
  for (const auto &name : names) {
    const auto &clr = h_profClrModel.get(name);
    auto curve = new QwtPlotCurve{name.c_str()};
    curve->attach(m_plot);

    curve->setPen(clr, 2.0);
    curve->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
    curve->setLegendAttribute(QwtPlotCurve::LegendShowSymbol, false);
    curve->setPaintAttribute(QwtPlotCurve::ClipPolygons, true);
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);

    m_constituentCurves.emplace_back(curve);
  }

  attachNonConcentrationCurves(true);
}
