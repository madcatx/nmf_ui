#ifndef EDITCONSTITUENTDIALOG_H
#define EDITCONSTITUENTDIALOG_H

#include <QDialog>
#include <ui/models/constituentchargesmodel.h>
#include <gearbox/iconstituenteditor.h>

namespace Ui {
  class EditConstituentDialog;
}

namespace gdm {
  class PhysicalProperties;
}
namespace gearbox {
  class DatabaseProxy;
}

class EditChargesWidget;
class FloatingValueDelegate;

class EditConstituentDialog : public QDialog, public IConstituentEditor {
  Q_OBJECT
public:
  explicit EditConstituentDialog(gearbox::DatabaseProxy &dbProxy, const bool viscosityCorrectionEnabled, QWidget *parent = nullptr);
  explicit EditConstituentDialog(gearbox::DatabaseProxy &dbProxy, const QString &name, const EditConstituentDialog::ConstituentType type,
                                 const gdm::PhysicalProperties &props, const bool allowTypeChange, const bool viscosityCorrectionEnabled,
                                 QWidget *parent = nullptr);
  ~EditConstituentDialog() override;
  virtual auto chargeHigh() const -> int override;
  virtual auto chargeLow() const -> int override;
  virtual auto mobilities() const -> std::vector<double> override;
  virtual auto name() const -> QString override;
  virtual auto pKas() const -> std::vector<double> override;
  virtual auto type() const -> ConstituentType override;
  virtual auto viscosityCoefficient() const -> double override;

private:
  auto setConstituentType(const EditConstituentDialog::ConstituentType type) -> void;
  auto setViscosityElements(const double viscosityCoefficient) -> void;
  auto setupWidget() -> void;

  Ui::EditConstituentDialog *ui;

  EditChargesWidget *m_editChargesWidget;

  gearbox::DatabaseProxy &h_dbProxy;

private slots:
  void onAccepted();
  void onAddToDatabase();
  void onPickFromDatabase();
  void onRejected();
  void onViscosityCoefficientIndexChanged(const int idx);

signals:
  void addToDatabase(const EditConstituentDialog *me);
  void validateInput(const EditConstituentDialog *me, bool *ok);
};


#endif // EDITCONSTITUENTDIALOG_H
