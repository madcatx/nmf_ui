#ifndef PROFILESLICEDIALOG_H
#define PROFILESLICEDIALOG_H

#include <QDialog>

#include <utility>

namespace Ui {
class ProfileSliceDialog;
}

class IonicConcentrationsModel;

class ProfileSliceDialog : public QDialog {
  Q_OBJECT
public:
  explicit ProfileSliceDialog(QWidget *parent = nullptr);
  ~ProfileSliceDialog();

  auto setData(
    const QString &name,
    const int cell,
    const double x,
    const double value,
    const double effectiveMobility,
    const QString &caption,
    const QString &unit,
    const std::vector<std::pair<std::string, double>> &concentrations
  ) -> void;

private:
  Ui::ProfileSliceDialog *ui;

  IonicConcentrationsModel *m_model;
};

#endif // PROFILESLICEDIALOG_H
