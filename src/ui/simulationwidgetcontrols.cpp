#include "simulationwidgetcontrols.h"
#include "ui_simulationwidgetcontrols.h"

#include <ui/util.h>

SimulationWidgetControls::SimulationWidgetControls(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::SimulationWidgetControls)
{
  ui->setupUi(this);

  ui->qcbox_trace->addItem(tr("Concentrations"), QVariant::fromValue(int(-1)));

  connect(ui->qcbox_trace,
          static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
          [this](const int idx) {
    const auto m = ui->qcbox_trace->model();
    const auto didx = m->data(m->index(idx, 0), Qt::UserRole).toInt();
    emit traceChanged(didx);
  });
  connect(ui->qpb_toggleLegend,
          &QPushButton::clicked,
          [this]() { emit toggleLegend(); });
  connect(ui->qpb_zoomToFit,
          &QPushButton::clicked,
          [this]() { emit zoomToFit(); });
}

SimulationWidgetControls::~SimulationWidgetControls()
{
  delete ui;
}

auto SimulationWidgetControls::setDetectors(const std::vector<std::pair<bool, double>> &dets) -> void
{
  auto model = ui->qcbox_trace->model();

  if (model->rowCount() > 1)
    model->removeRows(1, model->rowCount() - 1);

  for (size_t idx{0}; idx < dets.size(); idx++) {
    const auto &d = dets[idx];
    ui->qcbox_trace->addItem(detectorTag(d.first, d.second, idx), QVariant::fromValue(int(idx)));
  }
}

auto SimulationWidgetControls::trace() -> int
{
  return ui->qcbox_trace->currentData().toInt();
}
