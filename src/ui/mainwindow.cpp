#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <calculators/nmfinterface.h>
#include <gdm/core/gdm.h>
#include <gearbox/singletons.h>
#include <persistence/persistence.h>
#include <persistence/types.h>
#include <recording/player.h>
#include <recording/recorder.h>
#include <recording/types.h>
#include <ui/aboutdialog.h>
#include <ui/ampholytegeneratordialog.h>
#include <ui/detectorssetupdialog.h>
#include <ui/exporttracesdialog.h>
#include <ui/initialcompositionwidget.h>
#include <ui/mainparameterswidget.h>
#include <ui/playerdialog.h>
#include <ui/profileslicedialog.h>
#include <ui/recordingdialog.h>
#include <ui/simulationwidget.h>
#include <ui/systeminfodialog.h>
#include <ui/util.h>
#include <util/util.h>
#include <globals.h>

#include <QCloseEvent>
#include <QFileDialog>
#include <QFileInfo>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QScreen>
#include <QSplitter>
#include <QTimer>
#include <QToolButton>
#include <QWindow>

#include <utility>

#ifdef CRASHHANDLING_WIN32
#include <windows.h>
#endif // CRASHHANDLING_WIN32

NMFUI_INLINE const QString SIM_NOT_INITED_TEXT{"Not initialized"};
NMFUI_INLINE const QString SIM_RUNNING_TEXT{"Running"};
NMFUI_INLINE const QString SIM_PAUSED_TEXT{"Paused"};

NMFUI_INLINE const QString NOT_RECORDING_TEXT{"Not recording"};
NMFUI_INLINE const QString RECORDING_TMPL{"Recording: %0"};

NMFUI_INLINE const QString TIMING_TMPL{"Avg time per iteration %0 msec | ETA %1"};

inline
auto hmsToText(const util::HMS &hms)
{
  return QString{"%0 h %1 m %2 s"}.arg(hms.h, 2, 10, QChar{'0'})
                                  .arg(hms.m, 2, 10, QChar{'0'})
                                  .arg(hms.s, 2, 'f', 2, QChar{'0'});
}

inline
auto makeEtaText(const double avgIter, const double tNow, const double tStop, const double dt)
{
  if (avgIter <= 0.0)
    return QString{"-"};

  auto tRem = (tStop - tNow) * 1000.0;
  auto remIters = tRem / (dt * 1000.0);
  auto etaMsec = remIters * avgIter;

  return hmsToText(util::msecToHms(etaMsec));
}

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow{parent},
  ui{new Ui::MainWindow},
  m_runState{RunState::UNAVAILABLE},
  m_player{nullptr},
  m_recorder{nullptr},
  m_playerDlg{nullptr},
  m_unsaved{false},
  m_needsInitialization{false}
{
  ui->setupUi(this);

  centralWidget()->setLayout(new QVBoxLayout{});

  auto splitter = new QSplitter{Qt::Vertical, this};
  auto top = new QWidget{this};
  auto topLayout = new QVBoxLayout{};
  top->setLayout(topLayout);

  auto topSplitter = new QSplitter{Qt::Horizontal, this};

  m_mainParamsWidget = new MainParametersWidget{this};
  m_initialCompositionWidget = new InitialCompositionWidget{m_mainParamsWidget, this};

  topSplitter->addWidget(m_mainParamsWidget);
  topSplitter->addWidget(m_initialCompositionWidget);
  topLayout->addWidget(topSplitter);
  topSplitter->setStretchFactor(0, 2);
  topSplitter->setStretchFactor(1, 3);

  auto bottom = new QWidget{this};
  auto bottomLayout = new QHBoxLayout{};
  bottom->setLayout(bottomLayout);

  m_simulationWidget = new SimulationWidget{this};
  bottomLayout->addWidget(m_simulationWidget);

  centralWidget()->layout()->addWidget(splitter);
  splitter->addWidget(top);
  splitter->addWidget(bottom);
  splitter->setStretchFactor(0, 1);
  splitter->setStretchFactor(1, 3);

  m_calculator = new calculators::NMFInterface(this);
  connectCalculatorSignals();

  m_loadSetupDlg = new QFileDialog{this, tr("Load setup"), "", "NMF JSON (*.json)"};
  m_loadSetupDlg->setAcceptMode(QFileDialog::AcceptOpen);

  m_saveSetupDlg = new QFileDialog{this, tr("Load setup"), "", "NMF JSON (*.json)"};
  m_saveSetupDlg->setAcceptMode(QFileDialog::AcceptSave);

  m_loadRecordedSim = new QFileDialog{this, tr("Load recorded simulation"), "", "NMF recorded simulation (*." NMFUI_RECORDING_FILE_SUFFIX ")" };
  m_loadRecordedSim->setAcceptMode(QFileDialog::AcceptOpen);

  m_profSliceDlg = new ProfileSliceDialog{this};
  m_exportDlg = new ExportTracesDialog{this};
  m_recDlg = new RecordingDialog{this};

  connect(
    ui->actionInitialize_sim,
    &QAction::triggered,
    this,
    &MainWindow::onInitializeClicked
  );
  connect(
    ui->actionRun_Pause,
    &QAction::triggered,
    this,
    &MainWindow::onRunPauseClicked
  );
  connect(
    ui->actionLoad_setup,
    &QAction::triggered,
    this,
    &MainWindow::onActionLoadSetup
  );
  connect(
    ui->actionSave_setup,
    &QAction::triggered,
    [this]() { saveSetup(false); }
  );
  connect(
    ui->actionSave_setup_as,
    &QAction::triggered,
    [this]() { saveSetup(true); }
  );
  connect(
    ui->actionExit,
    &QAction::triggered,
    [this] () {
      close();
    }
  );
  connect(
    ui->actionExportTraces,
    &QAction::triggered,
    this,
    &MainWindow::onActionExportProfiles
  );
  connect(
    m_simulationWidget,
    &SimulationWidget::curvePointSelected,
    this,
    &MainWindow::onCurvePointSelected
  );
  connect(
    ui->actionAmpholyte_generator,
    &QAction::triggered,
    [this]() {
      AmpholyteGeneratorDialog dlg{this};
      if (dlg.exec() == QDialog::Accepted)
        m_initialCompositionWidget->refreshComposition();
    }
  );
  connect(
    ui->actionSetupRecording,
    &QAction::triggered,
    [this]() {
      if (m_recDlg->exec() == QDialog::Accepted) {
        m_recParams = m_recDlg->parameters();
        wantReinitialization();
      }
    }
  );
  connect(
    ui->actionLoad_recorded_simulation,
    &QAction::triggered,
    this,
    &MainWindow::onLoadRecordedSimulation
  );
  connect(
    m_initialCompositionWidget,
    &InitialCompositionWidget::compositionChanged,
    this,
    &MainWindow::onCompositionChanged
  );
  connect(
    ui->actionAbout,
    &QAction::triggered,
    [this]() {
      AboutDialog dlg{this};
      dlg.exec();
    }
  );
  connect(
    m_initialCompositionWidget,
    &InitialCompositionWidget::arrangementChanged,
    [this]() {
      setUnsaved(true);
      wantReinitialization();
    }
  );
  connect(
    ui->actionSystem,
    &QAction::triggered,
    [this]() { SystemInfoDialog dlg{this}; dlg.exec(); }
  );
  connect(
    ui->actionDetectors,
    &QAction::triggered,
    [this]() { onSetupDetectors(); }
  );

#ifdef NMF_TEST_CRASHHANDLER
  auto crashAction = ui->menuExtras->addAction("Crash me!");
  connect(crashAction, &QAction::triggered, this, []() {
#ifdef CRASHHANDLING_WIN32
      RaiseException(0xc0000005, 0, 0, NULL);
#else
      std::abort();
#endif // CRASHHANDLING_WIN32
  });
#endif // NMF_TEST_CRASHHANDLER

  connect(m_simulationWidget,
          &SimulationWidget::traceChanged,
          this,
          &MainWindow::onTraceChanged);

  initStatusBar();
  setupIcons();
  setWindowTitle();

  QTimer::singleShot(0, this, [this]() { setWindowIcon(); });
}

MainWindow::~MainWindow()
{
  delete m_player;
  delete m_recorder;
  delete ui;
}

auto MainWindow::closeEvent(QCloseEvent *evt) -> void
{
  QMessageBox mbox{
    QMessageBox::Question,
    tr("Confirm action"),
    QString{tr("Exit %0?")}.arg(Globals::SOFTWARE_NAME),
    QMessageBox::Yes | QMessageBox::No
  };

  if (mbox.exec() == QMessageBox::Yes)
    evt->accept();
  else
    evt->ignore();
}

auto MainWindow::connectCalculatorSignals() -> void
{
  connect(
    m_calculator,
    &calculators::NMFInterface::simInitialized,
    this,
    &MainWindow::onSimInitialized,
    Qt::QueuedConnection
  );
  connect(
    m_calculator,
    &calculators::NMFInterface::simPaused,
    this,
    &MainWindow::onSimPaused,
    Qt::QueuedConnection
  );
  connect(
    m_calculator,
    &calculators::NMFInterface::simStarted,
    this,
    &MainWindow::onSimStarted,
    Qt::QueuedConnection
  );
  connect(
    m_calculator,
    &calculators::NMFInterface::simUpdated,
    this,
    &MainWindow::onSimUpdated,
    Qt::QueuedConnection
  );
  connect(
    m_calculator,
    &calculators::NMFInterface::simTerminated,
    this,
    &MainWindow::onSimTerminated,
    Qt::QueuedConnection
  );
}

auto MainWindow::displayLoadedSystemParameters(const persistence::System &sys) -> void
{
  MainParametersWidget::Params params{};
  params.dt = sys.dt;
  params.time = 0.0;
  params.cells = sys.cells;
  params.autoDt = sys.autoDt;
  params.stopTime = sys.stopTime;
  params.tolerance = sys.tolerance;
  params.constantForceValue = sys.constantForce == persistence::CF_VOLTAGE ? sys.voltage : sys.current;
  params.constantForce = sys.constantForce == persistence::CF_VOLTAGE
                           ? MainParametersWidget::CF_VOLTAGE : MainParametersWidget::CF_CURRENT;
  params.dependentForceValue = sys.constantForce == persistence::CF_VOLTAGE ? sys.current : sys.voltage;
  params.capillaryLength = sys.capillaryLength * 1000.0;
  params.firstCell = sys.firstCell;
  params.lastCell = sys.lastCell;
  params.autoCells = sys.autoCells;
  params.updateInterval = sys.updateInterval;
  m_mainParamsWidget->setParameters(params);

  m_initialCompositionWidget->refreshComposition();

  m_simulationWidget->drawDetectorPositions(m_detectors);
}

auto MainWindow::enterPlaybackMode() -> void
{
  assert(m_player != nullptr);
  assert(m_playerDlg == nullptr);

  m_mainParamsWidget->lockControls(true);
  lockControlsPlayerMode(true);
  m_initialCompositionWidget->setEnabled(false);

  m_playerDlg = new PlayerDialog{m_player->numFrames(), this};
  connect(m_playerDlg, &PlayerDialog::changeFrame, this, &MainWindow::onDisplayRecordedFrame);
  connect(m_playerDlg, &PlayerDialog::finished, this, &MainWindow::onLeavePlaybackMode);
  connect(m_playerDlg, &PlayerDialog::finished, [this]() {
    m_playerDlg->deleteLater();
    m_playerDlg = nullptr;
  });

  m_playerDlg->show();
}

auto MainWindow::fillInitParams(calculators::InitParams &params) -> void
{
  const auto mainParams = m_mainParamsWidget->parameters();

  params.autoDt = mainParams.autoDt;
  params.cells = mainParams.cells;
  params.constantForceValue = mainParams.constantForceValue;
  params.constantForce = mainParams.constantForce == MainParametersWidget::CF_CURRENT
                           ? calculators::ConstantForce::CURRENT
                           : calculators::ConstantForce::VOLTAGE;
  params.capillaryLength = mainParams.capillaryLength;
  params.firstCell = mainParams.firstCell;
  params.lastCell = mainParams.lastCell;
  params.adaptCalcSpace = mainParams.autoCells;
  params.detectors = [this]() {
    std::vector<double> dets{};
    for (const auto &item : m_detectors) {
      if (item.first)
        dets.emplace_back(item.second);
    }
    return dets;
  }();

  /* Hardcoded for now */
  params.debyeHuckel = false;
  params.onsagerFuoss = false;
  params.capillaryDiameter = 50.0;
}

auto MainWindow::fillStartParams(calculators::StartParams &params) -> void
{
  const auto mainParams = m_mainParamsWidget->parameters();

  params.tStop = mainParams.stopTime;
  params.dt = mainParams.dt;
  params.tolerance = mainParams.tolerance;
  params.constantForceValue = mainParams.constantForceValue;
  params.constantForce = mainParams.constantForce == MainParametersWidget::CF_CURRENT
                           ? calculators::ConstantForce::CURRENT
                           : calculators::ConstantForce::VOLTAGE;
  params.firstCell = mainParams.firstCell;
  params.lastCell = mainParams.lastCell;
  params.adaptCalcSpace = mainParams.autoCells;
  params.autoDt = mainParams.autoDt;

  /* Hardcoded for now */
  params.eofValue = 0.0;
  params.eofMode = calculators::EOFMode::MOBILITY;
  params.updateInterval = mainParams.updateInterval;
}

auto MainWindow::getSliceFromPlayer(const size_t idx, const double x, const QString &name, const SimulationWidget::CurveType type) -> calculators::ProfileSlice
{
  assert(m_player != nullptr);

  auto t = [type]() {
    switch (type) {
    case SimulationWidget::CurveType::CONDUCTIVITY:
      return calculators::ProfileType::CONDUCTIVITY;
    case SimulationWidget::CurveType::PH:
      return calculators::ProfileType::PH;
    case SimulationWidget::CurveType::CONSTITUENT:
      return calculators::ProfileType::CONCENTRATION;
    }
    assert(false);
#ifdef NDEBUG
    std::abort();
#endif // NDEBUG
  }();

  return m_player->readProfileSlice(idx, t, name.toStdString(), x);
}

auto MainWindow::getSliceFromSim(const double x, const QString &name, const SimulationWidget::CurveType type) -> calculators::ProfileSlice
{
  auto t = [type]() {
    switch (type) {
    case SimulationWidget::CurveType::CONDUCTIVITY:
      return calculators::ProfileType::CONDUCTIVITY;
    case SimulationWidget::CurveType::PH:
      return calculators::ProfileType::PH;
    case SimulationWidget::CurveType::CONSTITUENT:
      return calculators::ProfileType::CONCENTRATION;
    }
    assert(false);
#ifdef NDEBUG
    std::abort();
#endif // NDEBUG
  }();

  return m_calculator->profileSlice(t, name.toStdString(), x / 1000.0);
}

auto MainWindow::initDisplay(const calculators::SimStateData *state) -> void
{
  m_simulationWidget->setupConstituentCurves(state->constituentNames);

  updateDisplay(state, true);
}

auto MainWindow::initStatusBar() -> void
{
  m_sbSimState = new QLabel{this};
  m_sbRecording = new QLabel{this};
  m_sbAvgIterTime = new QLabel{this};

  ui->qsb_mainStatusBar->addWidget(m_sbSimState);
  ui->qsb_mainStatusBar->addWidget(m_sbRecording);
  ui->qsb_mainStatusBar->addWidget(m_sbAvgIterTime);

  m_sbSimState->setText(SIM_NOT_INITED_TEXT);
  m_sbRecording->setText(NOT_RECORDING_TEXT);
  m_sbAvgIterTime->setText(TIMING_TMPL.arg("-").arg("-"));
}

auto MainWindow::lockControlsPlayerMode(const bool lock) -> void
{
  ui->actionLoad_setup->setDisabled(lock);
  ui->actionSave_setup->setDisabled(lock);
  ui->actionInitialize_sim->setDisabled(lock);
  ui->actionRun_Pause->setDisabled(lock);
  ui->actionSetupRecording->setDisabled(lock);
  ui->actionAmpholyte_generator->setDisabled(lock);
}

auto MainWindow::makePersistentSystem() -> persistence::System
{
  const auto params = m_mainParamsWidget->parameters();

  persistence::System sys{};
  sys.autoDt = params.autoDt;
  sys.cells = params.cells;
  sys.dt = params.dt;
  sys.voltage = params.constantForce == MainParametersWidget::CF_VOLTAGE
                  ? params.constantForceValue : params.dependentForceValue * 1.0e-6;
  sys.current = params.constantForce == MainParametersWidget::CF_VOLTAGE
                  ? params.dependentForceValue : params.constantForceValue * 1.0e-6;
  sys.constantForce = params.constantForce == MainParametersWidget::CF_VOLTAGE
                        ? persistence::CF_VOLTAGE : persistence::CF_CURRENT;
  sys.eofValue = 0.0;
  sys.eofMode = persistence::EM_MOBILITY;
  sys.stopTime = params.stopTime;
  sys.diameterValues = { 50e-6 };
  sys.diameterSegments = {};
  sys.capillaryLength = params.capillaryLength / 1000.0;
  sys.tolerance = params.tolerance;
  sys.firstCell = params.firstCell;
  sys.lastCell = params.lastCell;
  sys.autoCells = params.autoCells;
  sys.updateInterval = params.updateInterval;
  sys.detectors = m_detectors;

  return sys;
}

void MainWindow::onActionExportProfiles()
{
  bool restart{false};

  if (m_player != nullptr) {
    /* Use recorded data */
    const auto frame = m_playerDlg->currentFrame();
    auto data = m_player->readFrame(frame).state;

    m_exportDlg->setData(m_detectors, std::move(data));
  } else {
    restart = m_calculator->isRunning();

    auto ret = m_calculator->pause();
    if (!ret) {
      QMessageBox mbox{
        QMessageBox::Warning,
        tr("Simulation error"),
        QString{tr("Failed to pause simulation: %1")}.arg(QString::fromStdString(m_calculator->errorToString()))
      };
      mbox.exec();
      return;
    }

    try {
      auto data = m_calculator->currentStateData();
      m_exportDlg->setData(m_detectors, std::move(data));
    } catch (const calculators::NMFInterfaceError &) {
      QMessageBox mbox{
        QMessageBox::Warning,
        tr("Simulation error"),
        QString{"Failed to get current simulation state: %1"}.arg(QString::fromStdString(m_calculator->errorToString()))
      };
      mbox.exec();
    }
  }

  m_exportDlg->exec();

  if (restart) {
    calculators::StartParams params{};
    fillStartParams(params);
    auto ret = m_calculator->start(params);
    if (!ret) {
      QMessageBox mbox{
        QMessageBox::Warning,
        tr("Simulation error"),
        QString{"Failed to restart simulation: %1"}.arg(QString::fromStdString(m_calculator->startErrorToString()))
      };
      mbox.exec();
    }
  }
}

void MainWindow::onActionLoadSetup()
{
  if (!m_lastLoadedSetupPath.isEmpty())
    m_loadSetupDlg->setDirectory(QFileInfo{m_lastLoadedSetupPath}.absolutePath());

  if (m_loadSetupDlg->exec() == QDialog::Accepted) {
    if (m_loadSetupDlg->selectedFiles().empty())
      return;

    const auto path = m_loadSetupDlg->selectedFiles().constFirst();
    try {
      persistence::System sys{};
      persistence::loadSetup(path, sys);
      m_detectors = std::move(sys.detectors);
      displayLoadedSystemParameters(sys);

      m_lastLoadedSetupPath = path;
      m_activeFilePath = path;

      setUnsaved(false);
      wantReinitialization();
    } catch (const persistence::Error &error) {
      QMessageBox mbox{QMessageBox::Warning, tr("Input error"), error.what()};
      mbox.exec();
    }
  }
}

void MainWindow::onCompositionChanged()
{
  m_simulationWidget->clear();
  m_runState = RunState::UNAVAILABLE;
  updateStatusBar();
  setUnsaved(true);
  wantReinitialization();
}

void MainWindow::onCurvePointSelected(const double x, const QString &name, const SimulationWidget::CurveType type)
{
  const auto restart = m_calculator->isRunning();

  if (restart) {
    auto ret = m_calculator->pause();
    if (!ret) {
      QMessageBox mbox{
        QMessageBox::Warning,
        tr("Simulation error"),
        QString{tr("Failed to pause simulation: %1")}.arg(QString::fromStdString(m_calculator->errorToString()))
      };
      mbox.exec();
      return;
    }
  }

  try {
    auto slice = [&, this] {
      if (m_player != nullptr) {
        assert(m_playerDlg != nullptr);
        return getSliceFromPlayer(m_playerDlg->currentFrame() - 1, x, name, type);
      }
      return getSliceFromSim(x, name, type);
    }();

    auto nu = [type]() -> std::pair<QString, QString> {
      switch (type) {
      case SimulationWidget::CurveType::CONDUCTIVITY:
        return { QString{"Conductivity"}, QString{"S/m"}};
      case SimulationWidget::CurveType::PH:
        return { QString{"pH"}, QString{""}};
      case SimulationWidget::CurveType::CONSTITUENT:
        return { QString{"Concentration"}, QString{"mM"}};
      }
      assert(false);
#ifdef NDEBUG
      std::abort();
#endif // NDEBUG
    }();

    m_profSliceDlg->setData(
      QString::fromStdString(slice.name),
      slice.cell,
      slice.x * 1000.0,
      slice.value,
      slice.effectiveMobility,
      nu.first,
     nu.second,
      slice.concentrations
    );
    m_profSliceDlg->exec();
  } catch (const recording::Error &ex) {
    QMessageBox::warning(
      this,
      tr("Playback error"),
      QString{"Failed to get profile slice: %1"}.arg(ex.what())
    );
  } catch (const calculators::NMFInterfaceError &) {
    QMessageBox::warning(
      this,
      tr("Simulation error"),
      QString{"Failed to get profile slice: %1"}.arg(QString::fromStdString(m_calculator->errorToString()))
    );
  }

  if (restart) {
    calculators::StartParams params{};
    fillStartParams(params);
    auto ret = m_calculator->start(params);
    if (!ret) {
      QMessageBox mbox{
        QMessageBox::Warning,
        tr("Simulation error"),
        QString{"Failed to restart simulation: %1"}.arg(QString::fromStdString(m_calculator->startErrorToString()))
      };
      mbox.exec();
    }
  }
}

auto MainWindow::onDisplayRecordedFrame(const std::size_t idx) -> void
{
  assert(m_player != nullptr);

  try {
    auto frame = m_player->readFrame(idx);
    updateDisplay(&frame.state);
    m_mainParamsWidget->setAutoDt(frame.autoDt);
    m_mainParamsWidget->setTStop(frame.tStop);
    m_mainParamsWidget->setTolerance(frame.tolerance);
  } catch (const recording::Error &err) {
    QMessageBox::warning(this, tr("Playback error"), err.what());
  }
}

void MainWindow::onInitializeClicked()
{
  calculators::InitParams params{};

  fillInitParams(params);

  if (!prepareRecorder())
    return;

  auto ret = m_calculator->initialize(params, gearbox::Singletons::gdm(), gearbox::Singletons::concentrationProfilesModel());
  if (!ret) {
    QMessageBox mbox{
      QMessageBox::Warning,
      tr("Initialization failure"),
      QString::fromStdString(m_calculator->initErrorToString())
    };
    mbox.exec();
    return;
  }
}

void MainWindow::onLeavePlaybackMode()
{
  assert(m_player != nullptr);

  delete m_player;
  m_player = nullptr;

  m_mainParamsWidget->lockControls(false);
  lockControlsPlayerMode(false);
  m_initialCompositionWidget->setEnabled(true);
}

void MainWindow::onLoadRecordedSimulation()
{
  if (m_recorder != nullptr) {
    delete m_recorder;
    m_recorder = nullptr;
  }

  if (!m_lastLoadedRecordingPath.isEmpty())
    m_loadRecordedSim->setDirectory(QFileInfo{m_lastLoadedRecordingPath}.absolutePath());

  const auto ret = m_loadRecordedSim->exec();
  if (ret != QDialog::Accepted)
    return;

  if (m_player != nullptr) {
    delete m_player;
    m_player = nullptr;
  }

  try {
    const auto path = m_loadRecordedSim->selectedFiles().constFirst();
    m_player = new recording::Player{path.toStdString()};
    const auto initial = m_player->readInitial();

    persistence::System sys{};
    persistence::loadSetupFromRaw(initial.description, sys);
    m_detectors = std::move(sys.detectors);

    m_player->applyDescription(
      sys.cells,
      util::dx<double>(sys.capillaryLength, sys.cells),
      gearbox::Singletons::gdm().size(),
      std::accumulate(m_detectors.cbegin(), m_detectors.cend(), size_t{0}, [](auto active, const auto &det) { return det.first ? active + 1 : active; })
    );
    displayLoadedSystemParameters(sys);

    auto firstFrame = m_player->readFrame(0);
    initDisplay(&firstFrame.state);
    enterPlaybackMode();
    m_lastLoadedRecordingPath = path;

    wantReinitialization();
  } catch (const recording::Error &err) {
    QMessageBox::warning(this, tr("Cannot replay simulation"), err.what());
    delete m_player;
    m_player = nullptr;
  } catch (const persistence::Error &err) {
    QMessageBox::warning(this, tr("Invalid system description"), err.what());
    delete m_player;
    m_player = nullptr;
  }
}

void MainWindow::onRunPauseClicked()
{
  switch (m_runState) {
  case RunState::RUNNING:
    if (!m_calculator->pause()) {
      QMessageBox mbox{
        QMessageBox::Warning,
        tr("Cannot pause simulation"),
        QString::fromStdString(m_calculator->errorToString())
      };
      mbox.exec();
    }
    break;
  case RunState::PAUSED:
    {
    if (m_needsInitialization) {
      QMessageBox::warning(
        this,
        tr("Initialize simulation"),
        tr("Simulation parameters have changed. Please reinitialize simulation before you start it.")
      );
      return;
    }

    calculators::StartParams params{};
    fillStartParams(params);

    if (m_recorder != nullptr) {
      const auto rate = m_recDlg->parameters().rate;
      const bool sensible = rate >= params.updateInterval && (rate % params.updateInterval) == 0;
      if (!sensible) {
        auto ret = QMessageBox::question(this,
                                         tr("Mismatching update intervals"),
                                         QString{tr("Recording rate (%0) is not a multiple of update rate (%1)\n"
                                                    "Recording will not be consistent. Continue anyway?")}.arg(rate).arg(params.updateInterval));
        if (ret != QMessageBox::Yes)
          return;
      }
    }
    if (!m_calculator->start(params)) {
      QMessageBox mbox{
        QMessageBox::Warning,
        tr("Cannot start simulation"),
        QString::fromStdString(m_calculator->startErrorToString())
      };
      mbox.exec();
    }
    break;
    }
  case RunState::UNAVAILABLE:
    QMessageBox::information(this, tr("Unable to proceed"), tr("Simulation is not initialized"));
    break;
  }
}

void MainWindow::onSetupDetectors()
{
  DetectorsSetupDialog dlg{m_detectors, m_mainParamsWidget->parameters().capillaryLength, this};

  if (dlg.exec() != QDialog::Accepted)
    return;

  m_detectors = dlg.detectors();
  m_simulationWidget->drawDetectorPositions(m_detectors);
  wantReinitialization();
}

void MainWindow::onSimInitialized(const calculators::SimState *state)
{
  state->lock.lock();
  initDisplay(state);

  m_runState = RunState::PAUSED;

  if (m_recorder != nullptr) {
    const auto params = m_mainParamsWidget->parameters();
    m_recorder->writeFullFrame(state, params.stopTime, params.tolerance, params.autoDt, true);
  }

  state->lock.unlock();
  updateStatusBar();

  m_needsInitialization = false;
}

void MainWindow::onSimPaused(const calculators::SimState *state)
{
  state->lock.lock();
  updateDisplay(state);

  if (m_recorder != nullptr) {
    const auto params = m_mainParamsWidget->parameters();
    m_recorder->writeFrame(state, params.stopTime, params.tolerance, params.autoDt, true);
  }

  m_runState = RunState::PAUSED;

  state->lock.unlock();

  toggleUiRunState();
  updateStatusBar();
}

void MainWindow::onSimStarted()
{
  m_runState = RunState::RUNNING;
  toggleUiRunState();
  updateStatusBar();
}

void MainWindow::onSimUpdated(const calculators::SimState *state)
{
  state->lock.lock();
  updateDisplay(state);

  if (m_recorder != nullptr) {
    const auto params = m_mainParamsWidget->parameters();
    m_recorder->writeFrame(state, params.stopTime, params.tolerance, params.autoDt, false);
  }

  const auto avgIter = state->avgTimePerIter;
  const auto tNow = state->t;
  const auto dt = state->dt;

  state->lock.unlock();
  updateStatusBar(avgIter, tNow, m_mainParamsWidget->tStop(), dt);
}

void MainWindow::onSimTerminated(QString reason)
{
  m_runState = RunState::UNAVAILABLE;

  QMessageBox::warning(this, tr("Simulation terminated prematurely"), reason);

  if (m_recorder)
    m_recorder->finalize();

  toggleUiRunState();
  updateStatusBar();
}

void MainWindow::onTraceChanged(const int disp)
{
  if (disp != -1) {
    assert(disp < m_detectors.size());
    if (!m_detectors[disp].first) {
      QMessageBox::information(this, tr("Data unavailable"), tr("Selected detector is disabled"));
      return;
    }
  }

  try {
    const auto state = [this]() {
      if (m_player == nullptr)
        return m_calculator->currentStateData();
      else
        return m_player->lastReadFrame().state;
    }();
    updateTrace(&state, disp);
  } catch (const calculators::NMFInterfaceError &) {
    // NOOP
  } catch (const recording::Error &) {
    // NOOP
  }
}

auto MainWindow::prepareRecorder() -> bool
{
  if (m_recorder) {
    delete m_recorder;
    m_recorder = nullptr;
  }

  if (m_recParams.enabled) {
    auto path = m_recParams.path;
    const auto suffix = QString{NMFUI_RECORDING_FILE_SUFFIX};
    if (!path.endsWith(suffix))
      path = path + QString{".%1"}.arg(suffix);

    QFileInfo finfo{m_recParams.path};
    if (finfo.exists()) {
      auto ret = QMessageBox::question(
                   this,
                   tr("File exists"),
                   QString{tr("Simulation record file %1 already exists. Initialization will overwrite this file. Is that okay?")}.arg(finfo.fileName()));
      if (ret != QMessageBox::Yes)
        return false;
    }

    try {
      m_recorder = new recording::Recorder{
        m_recParams.path.toStdString(),
        m_recParams.rate,
        makePersistentSystem(),
        std::accumulate(m_detectors.cbegin(), m_detectors.cend(), size_t{0}, [](auto active, const auto &det) { return det.first ? active + 1 : active; })
      };
    } catch (const recording::Error &ex) {
      QMessageBox::warning(this, tr("Recorder error"), ex.what());
      m_recorder = nullptr;
      return false;
    }
  }

  return true;
}

auto MainWindow::saveSetup(const bool saveAs) -> void
{
  if (!saveAs && !m_activeFilePath.isEmpty()) {
    persistence::saveSetup(m_activeFilePath, makePersistentSystem());
    setUnsaved(false);
    return;
  }

  m_saveSetupDlg->setDirectory(QFileInfo{m_lastSavedSetupPath}.absolutePath());
  m_saveSetupDlg->setWindowTitle(saveAs ? "Save setup as" : "Save setup");
  if (m_saveSetupDlg->exec() != QDialog::Accepted)
    return;

  if (m_saveSetupDlg->selectedFiles().empty())
    return;

  const auto path = m_saveSetupDlg->selectedFiles().constFirst();

  persistence::saveSetup(path, makePersistentSystem());

  m_lastSavedSetupPath = path;
  m_activeFilePath = path;

  setUnsaved(false);
}

auto MainWindow::setActionToolButtonStyle(QAction *a) -> void
{
  auto btn = qobject_cast<QToolButton *>(ui->mainToolBar->widgetForAction(a));
  if (btn != nullptr)
    btn->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
}

auto MainWindow::setUnsaved(const bool unsaved) -> void
{
  m_unsaved = unsaved;
  setWindowTitle();
}

auto MainWindow::setWindowIcon() -> void
{
  const auto screen = window()->windowHandle()->screen();

  if (screen == nullptr) {
    QMainWindow::setWindowIcon(Globals::icon());
  } else {
    const auto dpi = screen->logicalDotsPerInch();
    if (dpi > 140)
      QMainWindow::setWindowIcon(Globals::iconHiDpi());
    else
      QMainWindow::setWindowIcon(Globals::icon());
  }
}

auto MainWindow::setWindowTitle() -> void
{
  auto title = QString{"%0 - Chicken Inside!"}.arg(Globals::SOFTWARE_NAME);

  if (!m_activeFilePath.isEmpty()) {
    auto path = QString{" (%0)"}.arg(m_activeFilePath);
    title += path;
  }

  if (m_unsaved)
    title += " [unsaved]";

  QMainWindow::setWindowTitle(title);
}

auto MainWindow::setupIcons() -> void
{
#ifdef Q_OS_LINUX
  ui->actionLoad_setup->setIcon(QIcon::fromTheme("document-open"));
  ui->actionSave_setup->setIcon(QIcon::fromTheme("document-save"));
  ui->actionSave_setup_as->setIcon(QIcon::fromTheme("document-save-as"));
  ui->actionExit->setIcon(QIcon::fromTheme("application-exit"));

  ui->actionInitialize_sim->setIcon(QIcon::fromTheme("view-refresh"));
  ui->actionRun_Pause->setIcon(QIcon::fromTheme("media-playback-start"));

#else
  ui->actionLoad_setup->setIcon(style()->standardIcon(QStyle::SP_DialogOpenButton));
  ui->actionSave_setup->setIcon(style()->standardIcon(QStyle::SP_DialogSaveButton));
  ui->actionSave_setup_as->setIcon(style()->standardIcon(QStyle::SP_DialogSaveButton));
  ui->actionExit->setIcon(style()->standardIcon(QStyle::SP_DialogCloseButton));

  ui->actionInitialize_sim->setIcon(style()->standardIcon(QStyle::SP_BrowserReload));
  ui->actionRun_Pause->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
#endif // Q_OS_LINUX

  setActionToolButtonStyle(ui->actionInitialize_sim);
  setActionToolButtonStyle(ui->actionRun_Pause);
}

auto MainWindow::toggleRunPauseIcon(const bool running) -> void
{
#ifdef Q_OS_LINUX
  auto icon = running ? QIcon::fromTheme("media-playback-pause")
                      : QIcon::fromTheme("media-playback-start");
#else
  auto icon = running ? style()->standardIcon(QStyle::SP_MediaPause)
                      : style()->standardIcon(QStyle::SP_MediaPlay);
#endif // Q_OS_LINUX

  ui->actionRun_Pause->setIcon(icon);
}

auto MainWindow::toggleUiRunState() -> void
{
  const bool running = m_runState == RunState::RUNNING;

  m_mainParamsWidget->lockControls(running);
  ui->actionInitialize_sim->setEnabled(!running);
  toggleRunPauseIcon(running);
  m_initialCompositionWidget->setEnabled(!running);
}

auto MainWindow::updateDisplay(const calculators::SimStateData *state, const bool setBaseZoom) -> void
{
  const double constantForceValue = state->constantForce == calculators::ConstantForce::VOLTAGE
                                      ? state->voltage
                                      : state->current * 1.0e6;
  const double dependentForceValue = state->constantForce == calculators::ConstantForce::VOLTAGE
                                       ? state->current * 1.0e6
                                       : state->voltage;

  m_mainParamsWidget->updateLiveParameters(
    state->dt,
    state->t,
    constantForceValue,
    dependentForceValue,
    state->firstCell,
    state->lastCell
  );

  auto disp = m_simulationWidget->displayedTrace();
  updateTrace(state, disp);

  const auto cellToX = [&state](const int cell) {
    return state->dx * cell * 1000.0;
  };

  m_simulationWidget->setFirstLastCellMarkers(cellToX(state->firstCell), cellToX(state->lastCell));

  if (setBaseZoom)
      m_simulationWidget->setBaseZoom();

  m_simulationWidget->replot();
}

auto MainWindow::updateStatusBar(const double avgIter, const double tNow, const double tStop, const double dt) -> void
{
  switch (m_runState) {
  case RunState::UNAVAILABLE:
    m_sbSimState->setText(SIM_NOT_INITED_TEXT);
    m_sbAvgIterTime->setText(TIMING_TMPL.arg("-").arg("-"));
    break;
  case RunState::RUNNING:
  {
    m_sbSimState->setText(SIM_RUNNING_TEXT);
    auto eta = makeEtaText(avgIter, tNow, tStop, dt);
    m_sbAvgIterTime->setText(TIMING_TMPL.arg(avgIter, 0, 'f', 2).arg(eta));
    break;
  }
  case RunState::PAUSED:
    m_sbSimState->setText(SIM_PAUSED_TEXT);
    m_sbAvgIterTime->setText(TIMING_TMPL.arg("-").arg("-"));
    break;
  }

  if (m_recorder != nullptr) {
    const auto path = QString::fromStdString(m_recorder->currentPath());
    m_sbRecording->setText(RECORDING_TMPL.arg(path));
  } else
    m_sbRecording->setText(NOT_RECORDING_TEXT);
}

auto MainWindow::updateTrace(const calculators::SimStateData *state, const int disp) -> void
{
  if (disp == -1)
    m_simulationWidget->drawProfiles(state->conductivity, state->pH, state->concentrations, state->dx);
  else {
    const auto idx = displayIndexToDetectorIndex(disp, m_detectors);
    if (idx >= state->detectorTraces->size()) {
      QMessageBox::information(this, tr("No data"), tr("No data is available for this detector. Did you forget to initialize the simulation?"));
      return;
    }
    const auto &trace = state->detectorTraces->operator[](idx);
    m_simulationWidget->drawDetectorTrace(trace);
  }
}

auto MainWindow::wantReinitialization() -> void
{
  m_needsInitialization = true;
}
