#include "detectorssetupdialog.h"
#include "ui_detectorssetupdialog.h"

#include <ui/detectorwidget.h>

#include <algorithm>
#include <cassert>

DetectorsSetupDialog::DetectorsSetupDialog(const std::vector<std::pair<bool, double>> &detectors, const double capillaryLength, QWidget *parent) :
  QDialog{parent},
  m_capillaryLength{capillaryLength},
  ui{new Ui::DetectorsSetupDialog}
{
  ui->setupUi(this);

  m_layout = new QVBoxLayout{this};
  m_layout->addStretch();

  auto w = new QWidget{this};
  w->setLayout(m_layout);
  ui->qscra_detectors->setWidget(w);

  connect(ui->qpb_addDetector, &QPushButton::clicked, this, &DetectorsSetupDialog::onAddDetectorClicked);

  for (const auto & d : detectors)
    addDetector(d.first, d.second);
}

DetectorsSetupDialog::~DetectorsSetupDialog()
{
  delete ui;
}

auto DetectorsSetupDialog::addDetector(const bool enabled, const double position) -> void
{
  auto det = new DetectorWidget{m_detectors.size(), enabled, position, m_capillaryLength, this};

  m_layout->insertWidget(m_detectors.size(), det);
  m_detectors.emplace_back(det);

  connect(det, &DetectorWidget::removeMe, this, &DetectorsSetupDialog::onRemoveDetector);
}

auto DetectorsSetupDialog::detectors() const -> std::vector<std::pair<bool, double>>
{
  std::vector<std::pair<bool, double>> d{};

  std::transform(
    m_detectors.cbegin(),
    m_detectors.cend(),
    std::back_inserter(d),
    [](auto v) { return std::make_pair(v->enabled(), v->position()); }
  );

  return d;
}

void DetectorsSetupDialog::onAddDetectorClicked()
{
  addDetector(true, 0.0);
}

void DetectorsSetupDialog::onRemoveDetector(DetectorWidget *w)
{
  size_t idx{0};
  for (;idx < m_detectors.size(); idx++) {
    if (m_detectors[idx] == w)
      break;
  }
  assert(idx < m_detectors.size());

  m_layout->removeWidget(w);
  delete w;

  m_detectors.erase(m_detectors.begin() + idx);

  for(;idx < m_detectors.size(); idx++)
    m_detectors[idx]->setNumber(idx);
}
