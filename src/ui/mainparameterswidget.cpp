#include "mainparameterswidget.h"
#include "ui_mainparameterswidget.h"

#include <gearbox/defaults.h>
#include <util/doubletostringconvertor.h>
#include <util/premadevalidators.h>
#include <ui/elementaries/floatingvaluedelegatefix.h>

#include <QDataWidgetMapper>

static const QString CURRENT_CAPTION{"Current"};
static const QString CURRENT_UNIT = QString::fromUtf8("\xCE\xBC""A");
static const QString VOLTAGE_CAPTION{"Voltage"};
static const QString VOLTAGE_UNIT{"V"};

MainParametersWidget::MainParametersWidget(QWidget *parent) :
  QWidget{parent},
  ui{new Ui::MainParametersWidget},
  m_paramsMapperModel{this}
{
  ui->setupUi(this);

  ui->qcbox_constantForceType->addItem(VOLTAGE_UNIT, CF_VOLTAGE);
  ui->qcbox_constantForceType->addItem(CURRENT_UNIT, CF_CURRENT);

  auto validatorVec = QVariant::fromValue<util::AdditionalValidatorVec<double>>( {
    util::PremadeValidators::mustBePositive<double>()
  } );
  /* Stop time */
  ui->qle_stopTime->setProperty(
    util::AdditionalValidator<double>::PROPERTY_NAME,
    validatorVec
  );

  /* Capillary length */
  ui->ql_capillaryLength->setProperty(
    util::AdditionalValidator<double>::PROPERTY_NAME,
    validatorVec
  );

  initParamsMapper();
  setInputDefaults();

  connect(
    ui->qcbox_constantForceType,
    static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
    this,
    &MainParametersWidget::onConstantForceTypeChanged
  );
  connect(ui->qcb_autoDt, &QCheckBox::toggled, this, &MainParametersWidget::onAutoDtToggled);
  connect(ui->qdspb_dt, &SciNotDoubleSpinBox::valueChanged, this, &MainParametersWidget::onParamsChanged);
  connect(
    ui->qspb_cells,
    static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
    this,
    &MainParametersWidget::onParamsChanged
  );
  connect(
    ui->qspb_tolerance,
    static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
    this,
    &MainParametersWidget::onParamsChanged
  );
  connect(
    ui->qspb_firstCell,
    static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
    this,
    &MainParametersWidget::setFirstLastCellRanges
  );
   connect(
    ui->qspb_lastCell,
    static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
    this,
    &MainParametersWidget::setFirstLastCellRanges
  );
}

MainParametersWidget::~MainParametersWidget()
{
  delete ui;
}

auto MainParametersWidget::commit() -> void
{
  ui->qdspb_dt->clearFocus();
  ui->qspb_tolerance->clearFocus();
  ui->qspb_cells->clearFocus();
  ui->qle_stopTime->clearFocus();
  ui->qle_constantForce->clearFocus();
  ui->qle_capillaryLength->clearFocus();
  ui->qspb_firstCell->clearFocus();
  ui->qspb_lastCell->clearFocus();
  ui->qspb_updateInterval->clearFocus();
}

auto MainParametersWidget::initParamsMapper() -> void
{
  auto mapper = new QDataWidgetMapper{this};
  auto delegate = new FloatingValueDelegateFix{this};

  m_paramsMappedData.resize(m_paramsMapperModel.indexFromItem(ParamList::LAST_INDEX));

  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::TIME)] = 0.0;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::STOP_TIME)] = gearbox::Defaults::STOP_TIME;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::CONSTANT_FORCE)] = gearbox::Defaults::VOLTAGE;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::DEPENDENT_FORCE)] = 0.0;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::CAPILLARY_LENGTH)] = gearbox::Defaults::CAPILLARY_LENGTH;

  m_paramsMapperModel.setUnderlyingData(&m_paramsMappedData);
  mapper->setModel(&m_paramsMapperModel);
  mapper->setItemDelegate(delegate);
  mapper->addMapping(ui->qle_time, m_paramsMapperModel.indexFromItem(ParamList::TIME));
  mapper->addMapping(ui->qle_stopTime, m_paramsMapperModel.indexFromItem(ParamList::STOP_TIME));
  mapper->addMapping(ui->qle_constantForce, m_paramsMapperModel.indexFromItem(ParamList::CONSTANT_FORCE));
  mapper->addMapping(ui->qle_dependentForce, m_paramsMapperModel.indexFromItem(ParamList::DEPENDENT_FORCE));
  mapper->addMapping(ui->qle_capillaryLength, m_paramsMapperModel.indexFromItem(ParamList::CAPILLARY_LENGTH));
  mapper->toFirst();

  connect(
    &m_paramsMapperModel,
    &decltype(m_paramsMapperModel)::dataChanged,
    this,
    &MainParametersWidget::onParamsChanged
  );
  connect(
    ui->qcb_autoCells,
    &QCheckBox::stateChanged,
    this,
    &MainParametersWidget::onAutoCellsToggled
  );
}

auto MainParametersWidget::lockControls(const bool lock) -> void
{
  ui->ql_stopTime->setEnabled(!lock);
  ui->qcb_autoDt->setEnabled(!lock);
  ui->qle_constantForce->setEnabled(!lock);
  ui->qcbox_constantForceType->setEnabled(!lock);
  ui->qspb_cells->setEnabled(!lock);
  ui->qle_capillaryLength->setEnabled(!lock);
  ui->qcb_autoCells->setEnabled(!lock);
  ui->qspb_updateInterval->setEnabled(!lock);

  if (lock) {
    ui->qspb_firstCell->setEnabled(false);
    ui->qspb_lastCell->setEnabled(false);
    ui->qspb_tolerance->setEnabled(false);
    ui->qdspb_dt->setEnabled(false);
  } else {
    auto cellsLocked = !ui->qcb_autoCells->isChecked();
    ui->qspb_firstCell->setEnabled(cellsLocked);
    ui->qspb_lastCell->setEnabled(cellsLocked);
    ui->qspb_tolerance->setEnabled(ui->qcb_autoDt->isChecked());
    ui->qdspb_dt->setEnabled(!ui->qcb_autoDt->isChecked());
  }
}

void MainParametersWidget::onAutoCellsToggled(const int state)
{
  bool enabled = state != Qt::Checked;
  ui->qspb_firstCell->setEnabled(enabled);
  ui->qspb_lastCell->setEnabled(enabled);
}

void MainParametersWidget::onAutoDtToggled(const bool checked)
{
  if (ui->qcb_autoDt->isEnabled()) {
    ui->qdspb_dt->setEnabled(!checked);
    ui->qspb_tolerance->setEnabled(checked);

    onParamsChanged();
  }
}

void MainParametersWidget::onConstantForceTypeChanged(const int)
{
  const auto v = ui->qcbox_constantForceType->currentData();

  switch (v.value<ConstantForce>()) {
  case CF_VOLTAGE:
    ui->ql_constantForce->setText(VOLTAGE_CAPTION);
    ui->ql_dependentForce->setText(QString{"%1 (%2)"}.arg(CURRENT_CAPTION, CURRENT_UNIT));
    break;
  case CF_CURRENT:
    ui->ql_constantForce->setText(CURRENT_CAPTION);
    ui->ql_dependentForce->setText(QString{"%1 (%2)"}.arg(VOLTAGE_CAPTION, VOLTAGE_UNIT));
    break;
  }

  onParamsChanged();
}

void MainParametersWidget::onParamsChanged()
{
  emit parametersChanged(parameters());
}

auto MainParametersWidget::parameters() -> Params
{
  commit();

  Params p{};

  p.time = ui->qle_time->numericValue();
  p.dt = ui->qdspb_dt->numericValue();
  p.autoDt = ui->qcb_autoDt->checkState() == Qt::Checked;
  p.tolerance =  ui->qspb_tolerance->value();
  p.cells = ui->qspb_cells->value();
  p.stopTime = m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::STOP_TIME)];
  p.constantForceValue = m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::CONSTANT_FORCE)];
  p.constantForce = ui->qcbox_constantForceType->currentData().value<ConstantForce>();
  p.dependentForceValue =  0.0;
  p.capillaryLength = m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::CAPILLARY_LENGTH)];
  p.firstCell = ui->qspb_firstCell->value();
  p.lastCell = ui->qspb_lastCell->value();
  p.autoCells = ui->qcb_autoCells->checkState() == Qt::Checked;
  p.updateInterval = ui->qspb_updateInterval->value();

  return p;
}

auto MainParametersWidget::setAutoDt(const bool enabled) -> void
{
  ui->qcb_autoDt->setChecked(enabled);
}

auto MainParametersWidget::setFirstLastCellRanges() -> void
{
  const auto cells = ui->qspb_cells->value();

  ui->qspb_firstCell->setMinimum(0);
  ui->qspb_lastCell->setMaximum(cells - 1);
  ui->qspb_firstCell->setMaximum(ui->qspb_lastCell->value() - 1);
  ui->qspb_lastCell->setMinimum(ui->qspb_firstCell->value() + 1);
}

auto MainParametersWidget::setInputDefaults() -> void
{
  ui->qle_time->setText(util::DoubleToStringConvertor::convert(0.0));

  /* Tolerance limits */
  ui->qspb_tolerance->setRange(1, 15);
  ui->qspb_tolerance->setValue(gearbox::Defaults::TOLERANCE);

  /* dt */
  ui->qdspb_dt->setRange(1.0e-15, 10.0);
  ui->qdspb_dt->setValue(gearbox::Defaults::DT);

  /* Cells */
  ui->qspb_cells->setRange(500, 10000000);
  ui->qspb_cells->setValue(gearbox::Defaults::CELLS);

  /* Update interval */
  ui->qspb_updateInterval->setRange(100, 10000);
  ui->qspb_updateInterval->setSingleStep(10);
  ui->qspb_updateInterval->setValue(2000);

  ui->qcbox_constantForceType->setCurrentIndex(0);
  onConstantForceTypeChanged(0);

  ui->qcb_autoDt->setCheckState(Qt::Checked);
  onAutoDtToggled(ui->qcb_autoDt->isChecked());

  /* Cells */
  ui->qspb_firstCell->setMinimum(0);
  ui->qspb_lastCell->setMaximum(ui->qspb_cells->value() - 1);
  ui->qspb_firstCell->setValue(0);
  ui->qspb_lastCell->setValue(ui->qspb_cells->value() - 1);
  setFirstLastCellRanges();

  ui->qcb_autoCells->setChecked(true);
}

auto MainParametersWidget::setParameters(const Params &params) -> void
{
  ui->qdspb_dt->setValue(params.dt);
  ui->qcb_autoDt->setChecked(params.autoDt);
  ui->qspb_tolerance->setValue(params.tolerance);

  ui->qspb_cells->setValue(params.cells);
  ui->qspb_firstCell->setMinimum(0);
  ui->qspb_lastCell->setMaximum(ui->qspb_cells->value() - 1);
  ui->qspb_firstCell->setValue(0);
  ui->qspb_lastCell->setValue(ui->qspb_cells->value() - 1);
  setFirstLastCellRanges();
  ui->qspb_firstCell->setValue(params.firstCell);
  ui->qspb_lastCell->setValue(params.lastCell);
  ui->qcb_autoCells->setChecked(params.autoCells);
  ui->qspb_updateInterval->setValue(params.updateInterval);

  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::STOP_TIME)] = params.stopTime;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::TIME)] = params.time;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::CONSTANT_FORCE)] = params.constantForceValue;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::DEPENDENT_FORCE)] = params.dependentForceValue;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::CAPILLARY_LENGTH)] = params.capillaryLength;

  m_paramsMapperModel.notifyAllDataChanged();
}

auto MainParametersWidget::setTolerance(const int tol) -> void
{
  ui->qspb_tolerance->setValue(tol);
}

auto MainParametersWidget::setTStop(const double t) -> void
{
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::STOP_TIME)] = t;
  m_paramsMapperModel.notifyDataChanged(ParamList::STOP_TIME, ParamList::STOP_TIME);
}

auto MainParametersWidget::updateLiveParameters(
  const double dt,
  const double time,
  const double constantForceValue,
  const double dependentForceValue,
  const int firstCell,
  const int lastCell
) -> void
{
  if (dt >= ui->qdspb_dt->minimum())
    ui->qdspb_dt->setValue(dt);

  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::TIME)] = time;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::CONSTANT_FORCE)] = constantForceValue;
  m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::DEPENDENT_FORCE)] = dependentForceValue;

  ui->qspb_firstCell->setValue(firstCell);
  ui->qspb_lastCell->setValue(lastCell);

  m_paramsMapperModel.notifyDataChanged(ParamList::TIME, ParamList::DEPENDENT_FORCE);
}

auto MainParametersWidget::tStop() const -> double
{
  return m_paramsMappedData[m_paramsMapperModel.indexFromItem(ParamList::STOP_TIME)];
}
