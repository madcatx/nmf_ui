#ifndef INITIALPROFILESWIDGET_H
#define INITIALPROFILESWIDGET_H

#include <gearbox/concentrationprofilesmodel.h>
#include <mappers/floatmappermodel.h>
#include <ui/mainparameterswidget.h>
#include <util/premadevalidators.h>

#include <QVector>
#include <QWidget>

#include <map>
#include <tuple>

namespace Ui {
class InitialProfilesWidget;
}

class DoubleClickableQwtPlotZoomer;
class FloatingValueDelegateFix;
class QComboBox;
class QGraphicsLineItem;
class QGraphicsScene;
class QGraphicsSimpleTextItem;
class QStandardItemModel;
class QwtPlot;
class QwtPlotCurve;
class QwtPlotMarker;

class InitialProfilesWidget : public QWidget {
  Q_OBJECT
public:
  enum InjectionShape {
    BOX,
    GAUSS
  };

  enum ProfileType {
    INJECTION,
    ZONES
  };

  explicit InitialProfilesWidget(
    gearbox::ConcentrationProfilesModel &concProfsModel,
    MainParametersWidget *mainParamsWidget,
    QWidget *parent = nullptr
  );
  ~InitialProfilesWidget();

  auto commit() -> bool;
  auto pickConstituent(const int idx) -> void;
  auto reject() -> void;

private:
  enum class InjectionItems {
    POSITION_ABS,
    POSITION_REL,
    WIDTH_ABS,
    WIDTH_REL,
    STEEPNESS_ABS,
    STEEPNESS_REL,
    LAST_INDEX
  };

  enum class ZonesItems {
    POSITION_ABS,
    POSITION_REL,
    WIDTH_ABS,
    WIDTH_REL,
    CONCENTRATION,
    LAST_INDEX
  };

  class ProfileParams {
  public:
    explicit ProfileParams(
      ProfileType type,
      InjectionShape shape,
      std::vector<double> concentrations,
      std::vector<gearbox::ConcentrationProfilesModel::Edge> edges
    );

    ProfileType type;
    InjectionShape shape;

    std::vector<double> concentrations;
    std::vector<gearbox::ConcentrationProfilesModel::Edge> edges;
  };

  using Map = std::map<std::string, ProfileParams>;

  auto checkEdgesSanity(const std::vector<gearbox::ConcentrationProfilesModel::Edge> &edges) -> bool;
  auto convertConcentrations(const gearbox::ConcentrationProfilesModel::Info &info) -> std::vector<double>;
  auto currentData() -> Map::iterator;
  auto dimToAbs(const double v) const -> double;
  auto dimToRel(const double v) const -> double;
  auto initInjectionMapper(FloatingValueDelegateFix *dlgt) -> void;
  auto initZonesMapper(FloatingValueDelegateFix *dlgt) -> void;
  auto makeData() -> void;
  auto makeEdgesMarkers() -> void;
  auto makeInjLine() -> void;
  auto makeZonesLines() -> void;
  auto redrawInjProfile() -> void;
  auto resetAll() -> void;
  auto selectConstituent(const QModelIndex &index) -> void;
  auto setInjBaseZoom(const double cMin, const double cMax) -> void;
  auto setZoneBaseZoom(const double cMin, const double cMax) -> void;
  template <typename T>
  auto switchComboBox(const T value, QComboBox *cbox) -> void;
  auto switchWidgetStack(const ProfileType type) -> void;
  auto updateEdgeInjection() -> void;
  auto updateZoneControls(const int idx, const double cMin , const double cMax) -> void;
  auto updateZoneData() -> void;

  Ui::InitialProfilesWidget *ui;

  gearbox::ConcentrationProfilesModel &h_concProfsModel;
  QStandardItemModel *m_constituentsModel;
  double m_capillaryLength;

  FloatMapperModel<InjectionItems> m_injectionMapper;
  QVector<double> m_injectionData;

  FloatMapperModel<ZonesItems> m_zonesMapper;
  QVector<double> m_zonesData;

  QwtPlot *m_zonePlot;
  QwtPlotCurve *m_zoneProfile;
  std::vector<QwtPlotMarker *> m_edgeMarkers;
  DoubleClickableQwtPlotZoomer *m_zoneZoomer;

  QwtPlot *m_injPlot;
  QwtPlotCurve *m_injProfile;
  DoubleClickableQwtPlotZoomer *m_injZoomer;

  Map m_data;

  std::shared_ptr<util::IsWithinValidator<double>> m_isWithinAbsValidator;
  std::shared_ptr<util::IsWithinValidator<double>> m_isWithinRelValidator;

public slots:
  void onAddZoneClicked();
  void onApplyToAllClicked();
  void onCompositionChanged();
  void onConstituentListClicked(const QModelIndex &index);
  void onInjectionDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);
  void onRemoveZoneClicked();
  void onShapeChanged();
  void onZoneChanged(int idx);
  void onZoneDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);

private slots:
  void onTypeChanged(int);
  void onMainParametersChanged(const MainParametersWidget::Params &params);
};

Q_DECLARE_METATYPE(InitialProfilesWidget::InjectionShape)
Q_DECLARE_METATYPE(InitialProfilesWidget::ProfileType)

#endif // INITIALPROFILESWIDGET_H
