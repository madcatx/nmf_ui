#include "aboutdialog.h"
#include "ui_aboutdialog.h"

#include <globals.h>

#include <QPixmap>

inline
auto scaleFactor(AboutDialog *dlg)
{
  const auto fm = dlg->fontMetrics();

  const qreal sf = qRound(fm.height() / 16.0); /* "...and keep the SF-score low." */

  return sf;
}

AboutDialog::AboutDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::AboutDialog)
{
  ui->setupUi(this);

  auto nmfPix = QPixmap{":/images/res/nmf.png"}.scaledToWidth(qRound(256 * scaleFactor(this)), Qt::SmoothTransformation);
  ui->ql_logo->setPixmap(nmfPix);

  ui->ql_version->setText(Globals::VERSION_STRING());

  setWindowTitle(QString{"About %0"}.arg(Globals::SOFTWARE_NAME));
}

AboutDialog::~AboutDialog()
{
  delete ui;
}
