#include "ampholytepropertieswidget.h"
#include "ui_ampholytepropertieswidget.h"

#include <ui/elementaries/floatingvaluelineedit.h>
#include <util/doubletostringconvertor.h>
#include <util/util.h>

#include <cassert>

static
auto dispLastpKa(const FloatingValueLineEdit *basepKa, const FloatingValueLineEdit *pKaStep, QLineEdit *lastpKa, const int numAmpholytes)
{
  if (basepKa->isInputValid() && pKaStep->isInputValid()) {
    auto b = basepKa->numericValue();
    auto s = pKaStep->numericValue();
    auto last = b + numAmpholytes * s;

    auto txt = util::DoubleToStringConvertor::convert(last);
    lastpKa->setText(txt);
  } else
    lastpKa->setText("");
}

AmpholytePropertiesWidget::Properties::Properties() :
  ok{false},
  mobilities{{}},
  basepKas{{}},
  pKaSteps{{}},
  chargeLow{0},
  chargeHigh{0}
{
}

AmpholytePropertiesWidget::Properties::Properties(
  std::vector<double> _mobilities,
  std::vector<double> _basepKas,
  std::vector<double> _pKaSteps,
  int _chargeLow,
  int _chargeHigh) noexcept :
  ok{true},
  mobilities{std::move(_mobilities)},
  basepKas{std::move(_basepKas)},
  pKaSteps{std::move(_pKaSteps)},
  chargeLow{_chargeLow},
  chargeHigh{_chargeHigh}
{
}

AmpholytePropertiesWidget::Properties::Properties(Properties &&other) noexcept :
  ok{other.ok},
  mobilities{std::move(other.mobilities)},
  basepKas{std::move(other.basepKas)},
  pKaSteps{std::move(other.pKaSteps)},
  chargeLow{other.chargeLow},
  chargeHigh{other.chargeHigh}
{
}

AmpholytePropertiesWidget::AmpholytePropertiesWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::AmpholytePropertiesWidget),
  m_highest{0},
  m_lowest{0},
  m_clean{true}
{
  ui->setupUi(this);
}

AmpholytePropertiesWidget::~AmpholytePropertiesWidget()
{
  delete ui;
}

auto AmpholytePropertiesWidget::appendRow(const int charge) -> void
{
  auto it = m_map.find(charge);
  assert(it != m_map.end());

  const auto row = ui->g_lay->rowCount();

  auto widgets = it->second;
  insertWidget<0>(row, widgets);
  insertWidget<1>(row, widgets);
  insertWidget<2>(row, widgets);
  insertWidget<3>(row, widgets);
  insertWidget<4>(row, widgets);
}

auto AmpholytePropertiesWidget::insertCharge(const int charge) -> void
{
  auto chargeLabel = new QLabel{QString::number(charge), this};
  auto mobility = new FloatingValueLineEdit{this};
  auto basepKa = new FloatingValueLineEdit{this};
  auto pKaStep = new FloatingValueLineEdit{this};
  auto lastpKa = new QLineEdit{this};
  lastpKa->setDisabled(true);

  /* Recalculate last pKa automatically */
  connect(
    basepKa,
    &FloatingValueLineEdit::valueChanged,
    [this, basepKa, pKaStep, lastpKa]() {
      dispLastpKa(basepKa, pKaStep, lastpKa, this->m_numAmpholytes);
    }
  );
  connect(
    pKaStep,
    &FloatingValueLineEdit::valueChanged,
    [this, basepKa, pKaStep, lastpKa]() {
      dispLastpKa(basepKa, pKaStep, lastpKa, this->m_numAmpholytes);
    }
  );

  assert(m_map.find(charge) == m_map.end());

  m_map.emplace(std::make_pair(charge, std::make_tuple(chargeLabel, mobility, basepKa, pKaStep, lastpKa)));
}

template <int Idx>
auto AmpholytePropertiesWidget::insertWidget(const int row, const Pack &widgets) -> void
{
  ui->g_lay->addWidget(std::get<Idx>(widgets), row, Idx);
}

auto AmpholytePropertiesWidget::lockpKaLess() -> void
{
  const auto valueless = pKaLessCharge();

  for (int charge = m_lowest; charge <= m_highest; charge++) {
    auto it = m_map.find(charge);
    assert(it != m_map.end());

    if (valueless == 0)
      toggleWidgets<3, true>(it->second, valueless == charge);
    else
      toggleWidgets<3, false>(it->second, valueless == charge);
  }
}

auto AmpholytePropertiesWidget::pKaLessCharge() const -> int
{
  const auto sgnLow = util::sgn(m_lowest);
  const auto sgnHigh = util::sgn(m_highest);

  if (sgnLow == sgnHigh) {
    if (sgnHigh < 0)
      return m_highest;
    else
      return m_lowest;
  } else
    return 0;
}

auto AmpholytePropertiesWidget::prependRow(const int charge) -> void
{
  auto it = m_map.find(charge);
  assert(it != m_map.end());

  shiftDown();

  auto widgets = it->second;
  insertWidget<0>(1, widgets);
  insertWidget<1>(1, widgets);
  insertWidget<2>(1, widgets);
  insertWidget<3>(1, widgets);
  insertWidget<4>(1, widgets);
}

auto AmpholytePropertiesWidget::properties() const -> Properties
{
  std::vector<double> mobilities{};
  std::vector<double> basepKas{};
  std::vector<double> pKaSteps{};

  const auto sz = m_map.size();
  mobilities.reserve(sz);
  basepKas.reserve(sz > 1 ? sz - 1 : 0);
  pKaSteps.reserve(sz > 1 ? sz - 1 : 0);

  const auto pKaLess = pKaLessCharge();
  for (int charge = m_lowest; charge <= m_highest; charge++) {
    auto it = m_map.find(charge);
    assert(it != m_map.end());

    const auto pack = it->second;
    const auto wMob = std::get<1>(pack);
    const auto wBase = std::get<2>(pack);
    const auto wStep = std::get<3>(pack);

    if (!wMob->isInputValid())
      return {};

    mobilities.emplace_back(wMob->numericValue());

    if (charge != pKaLess) {
      if (!wBase->isInputValid() || !wStep->isInputValid())
        return {};
      basepKas.emplace_back(wBase->numericValue());
      pKaSteps.emplace_back(wStep->numericValue());
    }
  }

  return {
    std::move(mobilities),
    std::move(basepKas),
    std::move(pKaSteps),
    m_lowest,
    m_highest
  };
}

auto AmpholytePropertiesWidget::removeCharge(const int charge) -> void
{
  auto it = m_map.find(charge);
  assert (it != m_map.end());

  m_map.erase(it);
}

auto AmpholytePropertiesWidget::removeRow(const int charge) -> void
{
  auto it = m_map.find(charge);
  assert(it != m_map.end());

  auto widgets = it->second;

  ui->g_lay->removeWidget(std::get<0>(widgets));
  ui->g_lay->removeWidget(std::get<1>(widgets));
  ui->g_lay->removeWidget(std::get<2>(widgets));
  ui->g_lay->removeWidget(std::get<3>(widgets));
  ui->g_lay->removeWidget(std::get<4>(widgets));

  delete std::get<0>(widgets);
  delete std::get<1>(widgets);
  delete std::get<2>(widgets);
  delete std::get<3>(widgets);
  delete std::get<4>(widgets);
}

auto AmpholytePropertiesWidget::setChargeSpan(const int lowest, const int highest) -> void
{
  for (int charge = lowest; charge <= highest; charge++) {
    auto it = m_map.find(charge);
    if (it == m_map.end())
      insertCharge(charge);
  }

  updateList(lowest, highest);
}

auto AmpholytePropertiesWidget::setNumAmpholytes(const int num) -> void
{
  m_numAmpholytes = num;

  for (auto it = m_map.begin(); it != m_map.end(); it++) {
    if (pKaLessCharge() == it->first)
      continue;

    auto basepKa = std::get<2>(it->second);
    auto pKaStep = std::get<3>(it->second);
    auto lastpKa = std::get<4>(it->second);

    dispLastpKa(basepKa, pKaStep, lastpKa, m_numAmpholytes);
  }
}

auto AmpholytePropertiesWidget::shiftDown() -> void
{
  auto rc = ui->g_lay->rowCount() - 1;
  for (; rc > 0; rc--)
    shiftDownWidgets<4>(rc);
}

template <int Idx>
auto AmpholytePropertiesWidget::shiftDownWidgets(const int row) -> void
{
  auto item = ui->g_lay->itemAtPosition(row, Idx);
  if (item != nullptr) {
    ui->g_lay->removeItem(item);
    ui->g_lay->addItem(item, row + 1, Idx);
  }

  shiftDownWidgets<Idx - 1>(row);
}

template <>
auto AmpholytePropertiesWidget::shiftDownWidgets<0>(const int row) -> void
{
  auto item = ui->g_lay->itemAtPosition(row, 0);
  if (item != nullptr) {
    ui->g_lay->removeItem(item);
    ui->g_lay->addItem(item, row + 1, 0);
  }
}

template <int Idx, bool LockMobility>
auto AmpholytePropertiesWidget::toggleWidgets(Pack &widgets, const bool lock) -> void
{
  auto w = std::get<Idx>(widgets);
  w->setDisabled(lock);
  if (lock)
    w->setText("");
  toggleWidgets<Idx - 1, LockMobility>(widgets, lock);
}

template <>
auto AmpholytePropertiesWidget::toggleWidgets<1, true>(Pack &widgets, const bool lock) -> void
{
  auto w =std::get<1>(widgets);
  w->setDisabled(lock);
  if (lock)
    w->setText("0");
}

template <>
auto AmpholytePropertiesWidget::toggleWidgets<1, false>(Pack &widgets, const bool lock) -> void
{
  if (!lock)
    std::get<1>(widgets)->setDisabled(false);
}

auto AmpholytePropertiesWidget::updateList(const int newLowest, const int newHighest) -> void
{
  if (!m_clean) {
    for (int charge = m_lowest; charge < newLowest; charge++) {
      removeRow(charge);
      removeCharge(charge);
    }
    for (int charge = m_highest; charge > newHighest; charge--) {
      removeRow(charge);
      removeCharge(charge);
    }

    for (int charge = m_lowest - 1; charge >= newLowest; charge--)
      prependRow(charge);
    for (int charge = m_highest + 1; charge <= newHighest; charge++)
      appendRow(charge);
  } else {
    for (int charge = newLowest; charge <= newHighest; charge++)
      appendRow(charge);
  }

  m_lowest = newLowest;
  m_highest = newHighest;
  m_clean = false;

  lockpKaLess();
}
