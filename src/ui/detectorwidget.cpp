#include "detectorwidget.h"
#include "ui_detectorwidget.h"

#include <ui/util.h>

DetectorWidget::DetectorWidget(
  const size_t num,
  const bool enabled,
  const double position,
  const double capillaryLength, QWidget *parent
) :
  QWidget{parent},
  ui{new Ui::DetectorWidget}
{
  ui->setupUi(this);

  setNumber(num);
  ui->qdspb_position->setRange(0.0, capillaryLength);
  ui->qdspb_position->setValue(position);
  ui->qcb_enabled->setCheckState(enabled ? Qt::Checked : Qt::Unchecked);

  connect(ui->qpb_remove, &QPushButton::clicked, [this]() { emit removeMe(this); });
}

DetectorWidget::~DetectorWidget()
{
  delete ui;
}

auto DetectorWidget::enabled() const -> bool
{
  return ui->qcb_enabled->checkState() == Qt::Checked;
}

auto DetectorWidget::position() const -> double
{
  return ui->qdspb_position->value();
}

auto DetectorWidget::setNumber(const size_t num) -> void
{
  ui->ql_name->setText(detectorName(num));
}
