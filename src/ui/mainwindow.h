#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <ui/recordingdialog.h>
#include <ui/simulationwidget.h>

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

namespace calculators {
  class ProfileSlice;
  class NMFInterface;
  class InitParams;
  class SimState;
  class SimStateData;
  class StartParams;
}

namespace persistence {
  class System;
}

namespace recording {
  class Player;
  class Recorder;
}

class ExportTracesDialog;
class InitialCompositionWidget;
class MainParametersWidget;
class PlayerDialog;
class ProfileSliceDialog;
class QFileDialog;
class QLabel;
class SimulationWidget;

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  void closeEvent(QCloseEvent *evt) override;

private:
  enum class RunState {
    RUNNING,
    PAUSED,
    UNAVAILABLE
  };

  auto connectCalculatorSignals() -> void;
  auto displayLoadedSystemParameters(const persistence::System &sys) -> void;
  auto enterPlaybackMode() -> void;
  auto fillInitParams(calculators::InitParams &params) -> void;
  auto fillStartParams(calculators::StartParams &params) -> void;
  auto getSliceFromPlayer(const size_t idx, const double x, const QString &name, const SimulationWidget::CurveType type) -> calculators::ProfileSlice;
  auto getSliceFromSim(const double x, const QString &name, const SimulationWidget::CurveType type) -> calculators::ProfileSlice;
  auto initDisplay(const calculators::SimStateData *state) -> void;
  auto initStatusBar() -> void;
  auto lockControlsPlayerMode(const bool lock) -> void;
  auto makePersistentSystem() -> persistence::System;
  auto prepareRecorder() -> bool;
  auto saveSetup(const bool saveAs) -> void;
  auto setActionToolButtonStyle(QAction *a) -> void;
  auto setUnsaved(const bool unsaved) -> void;
  auto setWindowIcon() -> void;
  auto setWindowTitle() -> void;
  auto setupIcons() -> void;
  auto toggleRunPauseIcon(const bool running) -> void;
  auto toggleUiRunState() -> void;
  auto updateDisplay(const calculators::SimStateData *state, const bool setBaseZoom = false) -> void;
  auto updateStatusBar(const double avgIter = 0.0, const double tNow = 0.0, const double tStop = 0.0, const double dt = 0.0) -> void;
  auto updateTrace(const calculators::SimStateData *state, const int disp) -> void;
  auto wantReinitialization() -> void;

  Ui::MainWindow *ui;

  InitialCompositionWidget *m_initialCompositionWidget;
  MainParametersWidget *m_mainParamsWidget;
  SimulationWidget *m_simulationWidget;

  RunState m_runState;
  RecordingDialog::Parameters m_recParams;

  calculators::NMFInterface *m_calculator;
  recording::Player *m_player;
  recording::Recorder *m_recorder;
  std::vector<std::pair<bool, double>> m_detectors;

  QFileDialog *m_loadSetupDlg;
  QFileDialog *m_saveSetupDlg;
  QFileDialog *m_loadRecordedSim;
  ProfileSliceDialog *m_profSliceDlg;
  ExportTracesDialog *m_exportDlg;
  RecordingDialog *m_recDlg;
  PlayerDialog *m_playerDlg;

  QString m_lastLoadedSetupPath;
  QString m_lastSavedSetupPath;
  QString m_activeFilePath;
  QString m_lastLoadedRecordingPath;
  bool m_unsaved;
  bool m_needsInitialization;

  /* Statusbar widgets */
  QLabel *m_sbSimState;
  QLabel *m_sbRecording;
  QLabel *m_sbAvgIterTime;

private slots:
  void onActionExportProfiles();
  void onActionLoadSetup();
  void onCompositionChanged();
  void onDisplayRecordedFrame(const std::size_t idx);
  void onCurvePointSelected(const double x, const QString &name, const SimulationWidget::CurveType type);
  void onInitializeClicked();
  void onLeavePlaybackMode();
  void onLoadRecordedSimulation();
  void onRunPauseClicked();
  void onSetupDetectors();
  void onSimInitialized(const calculators::SimState *state);
  void onSimPaused(const calculators::SimState *state);
  void onSimStarted();
  void onSimUpdated(const calculators::SimState *state);
  void onSimTerminated(QString reason);
  void onTraceChanged(const int disp);
};
#endif // MAINWINDOW_H
