#ifndef UI_UTIL_H
#define UI_UTIL_H

#include <QString>

#include <utility>
#include <vector>

auto detectorName(const size_t num) -> QString;
auto detectorTag(const bool enabled, const double pos, const size_t num) -> QString;
auto displayIndexToDetectorIndex(const int disp, const std::vector<std::pair<bool, double>> &detectors) -> size_t;

#endif // UI_UTIL_H
