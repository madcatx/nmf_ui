#include "abstractfloatingvaluedelegate.h"
#include <util/doubletostringconvertor.h>
#include <util/additionalvalidator.h>
#include <QLineEdit>
#include <QEvent>

AbstractFloatingValueDelegate::AbstractFloatingValueDelegate(QObject *parent) :
  QStyledItemDelegate{parent}
{
}

auto AbstractFloatingValueDelegate::createEditor(
  QWidget *parent,
  const QStyleOptionViewItem &option,
  const QModelIndex &index
) const -> QWidget *
{
  Q_UNUSED(option)

  auto lineEdit = new QLineEdit(parent);

  const auto &current = index.model()->data(index);

  lineEdit->setText(util::DoubleToStringConvertor::convert(current.toDouble()));
  lineEdit->selectAll();

  connect(lineEdit, &QLineEdit::textChanged, this, &AbstractFloatingValueDelegate::onTextChanged);
  connect(this, &AbstractFloatingValueDelegate::editorCommit, lineEdit, &QLineEdit::clearFocus);

  return lineEdit;
}

auto AbstractFloatingValueDelegate::displayText(
  const QVariant &value,
  const QLocale &locale
) const -> QString
{
  /* We have to set the actual displayText from initStyleOption() */
  Q_UNUSED(value) Q_UNUSED(locale)

  return {};
}

auto AbstractFloatingValueDelegate::forceCommit() -> void
{
  emit editorCommit();
}

auto AbstractFloatingValueDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const -> void
{
  QStyledItemDelegate::initStyleOption(option, index);

  auto value = index.data(Qt::DisplayRole);
  if (value.isValid()) {
    option->features |= QStyleOptionViewItem::HasDisplay;
    option->text = formatText(value.toDouble(), index);
  }
}

auto AbstractFloatingValueDelegate::precision(const QModelIndex &index) const-> int
{
  const auto pv = index.model()->data(index, Qt::UserRole + 1);
  if (!pv.canConvert<int>())
    return 9;
  return pv.toInt();
}

auto AbstractFloatingValueDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const -> void
{
  if (!index.isValid())
    return;

  const QVariant v = index.model()->data(index, Qt::EditRole);
  if (!v.isValid())
    return;

  bool ok;
  double value = v.toDouble(&ok);
  if (!ok)
    return;

  auto lineEdit = qobject_cast<QLineEdit *>(editor);
  if (lineEdit == nullptr)
    return;

  lineEdit->setText(formatText(value, index));
}

auto AbstractFloatingValueDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const -> void
{
  double value;
  bool ok;
  auto lineEdit = qobject_cast<QLineEdit *>(editor);
  if (lineEdit == nullptr)
    return;

  QString s(lineEdit->text());
  s = s.replace(QChar::Nbsp, QString(""));
  value = util::DoubleToStringConvertor::back(s, &ok);
  if (!ok)
    return;
  if (!util::AdditionalValidator<double>::additionalValidatorsOk(this, value))
    return;

  const int prec  = util::DoubleToStringConvertor::decimalDigits(s);
  /* Calling order matters. See FloatingMapperModel::setData() for details */
  model->setData(index, prec, Qt::UserRole + 1);
  model->setData(index, value, Qt::EditRole);
}

auto AbstractFloatingValueDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const -> void
{
  Q_UNUSED(index)

  editor->setGeometry(option.rect);
}

void AbstractFloatingValueDelegate::onTextChanged(const QString &)
{
  bool ok;
  auto lineEdit = qobject_cast<QLineEdit *>(QObject::sender());
  if (lineEdit == nullptr)
    return;

  QString s(lineEdit->text());
  s = s.replace(QChar::Nbsp, QString(""));
  const double dv = util::DoubleToStringConvertor::back(s, &ok);
  ok = ok || lineEdit->text().length() == 0;

  if (ok)
    ok = util::AdditionalValidator<double>::additionalValidatorsOk(lineEdit, dv);
  if (ok)
    ok = util::AdditionalValidator<double>::additionalValidatorsOk(this, dv);

  if (ok)
    lineEdit->setPalette(QPalette());
  else {
    auto palette = lineEdit->palette();

    palette.setColor(QPalette::Base, QColor(Qt::red));
    lineEdit->setPalette(palette);
  }
}
