#include "modifyconstituentchargepushbutton.h"
#include <stdexcept>
#include <cassert>
#include <util/lowlevel.h>

ModifyConstituentChargePushButton::ModifyConstituentChargePushButton(const ChargeOperation op, QWidget *parent) :
  QPushButton{parent},
  m_chargeOp{op}
{
}

auto ModifyConstituentChargePushButton::setText(const int charge) -> void
{
  QString text = [](const ChargeOperation op) {
    switch (op) {
    case ChargeOperation::ADD:
      return tr("Add");
    case ChargeOperation::REMOVE:
      return tr("Remove");
    }

    util::impossible_path();
  }(m_chargeOp);

  text += QString{" (%1)"}.arg(charge);
  QPushButton::setText(text);
}
