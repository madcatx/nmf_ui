#ifndef FLOATINGVALUEDELEGATESCI_H
#define FLOATINGVALUEDELEGATESCI_H

#include <ui/elementaries/abstractfloatingvaluedelegate.h>

class FloatingValueDelegateSci : public AbstractFloatingValueDelegate {
  Q_OBJECT
public:
  explicit FloatingValueDelegateSci(QObject *parent = nullptr);

protected:
  auto formatText(const double value, const QModelIndex &index) const -> QString override;
};

#endif // FLOATINGVALUEDELEGATESCI_H
