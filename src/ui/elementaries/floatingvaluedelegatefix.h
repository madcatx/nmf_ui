#ifndef FLOATINGVALUEDELEGATEFIX_H
#define FLOATINGVALUEDELEGATEFIX_H

#include <ui/elementaries/abstractfloatingvaluedelegate.h>

class FloatingValueDelegateFix : public AbstractFloatingValueDelegate {
  Q_OBJECT
public:
  explicit FloatingValueDelegateFix(QObject *parent = nullptr);

protected:
  auto formatText(const double value, const QModelIndex &index) const -> QString override;
};

#endif // FLOATINGVALUEDELEGATEFIX_H
