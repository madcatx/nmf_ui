#ifndef PROFILESCOLORIZERDELEGATE_H
#define PROFILESCOLORIZERDELEGATE_H

#include <QItemDelegate>

class InitialCompositionModel;

class ProfilesColorizerDelegate : public QItemDelegate {
public:
  explicit ProfilesColorizerDelegate(const InitialCompositionModel *model, QObject *parent = nullptr);
  virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
  const InitialCompositionModel *h_model;
};

#endif // PROFILESCOLORIZERDELEGATE_H
