#include "scinotdoublespinbox.h"

#include <util/doubletostringconvertor.h>
#include <util/util.h>

#include <QLineEdit>

#include <algorithm>
#include <cmath>

static
auto stepFromExp(const double exp)
{
  return 1.25 * std::pow(10.0, exp);
}

static
auto expCoeff(double val)
{
  return std::floor(std::log10(std::abs(val)));
}

SciNotDoubleSpinBox::SciNotDoubleSpinBox(QWidget *parent) :
  QAbstractSpinBox{parent},
  m_maximum{1.0},
  m_minimum{0.0},
  m_numericValue{0.0}
{
  setText(m_numericValue);

  connect(lineEdit(),
          &QLineEdit::editingFinished,
          this,
          &SciNotDoubleSpinBox::onValueEntered);
}

auto SciNotDoubleSpinBox::maximum() const -> double
{
  return m_maximum;
}

auto SciNotDoubleSpinBox::minimum() const -> double
{
  return m_minimum;
}

auto SciNotDoubleSpinBox::numericValue() const -> double
{
  return m_numericValue;
}

void SciNotDoubleSpinBox::onValueEntered()
{
  int pos{0};
  auto text = lineEdit()->text();

  if (validate(text, pos) == QValidator::Acceptable) {
    bool ok;
    setValue(util::DoubleToStringConvertor::back(text, &ok));
  }
}

void SciNotDoubleSpinBox::setRange(const double minimum, const double maximum)
{
  m_maximum = maximum;
  m_minimum = minimum;

  m_numericValue = std::clamp(m_numericValue, m_minimum, m_maximum);
  setText(m_numericValue);
}

void SciNotDoubleSpinBox::setValue(const double value)
{
  m_numericValue = std::clamp(value, m_minimum, m_maximum);
  setText(m_numericValue);

  emit valueChanged(m_numericValue);
}

void SciNotDoubleSpinBox::stepBy(int steps)
{
  auto expDrop = [&](const double newVal, const double oldVal) {
    auto oldE = expCoeff(oldVal);
    auto newE = expCoeff(newVal);

    if (newVal < 0.0 && oldVal < 0.0)
      return newE > oldE;
    return newE < oldE;
  };

  const auto oldVal = m_numericValue;
  const auto exp = expCoeff(oldVal);
  auto ch = [oldVal, exp, steps]() {
    const auto ONE = std::pow(10.0, exp);
    if (qFuzzyCompare(oldVal, ONE) && steps == 1)
      return 0.25 * ONE;
    return stepFromExp(exp);
  }();

  if (std::abs(oldVal + ch * steps) <= std::pow(10.0, exp-12)) {
    m_numericValue = std::pow(10.0, exp);
  } else {
    double newVal = oldVal + ch * steps;
    if (expDrop(newVal, oldVal)) {
      ch = stepFromExp(exp - 1);
      newVal = oldVal + ch * steps;
    }

    m_numericValue = std::clamp(newVal, m_minimum, m_maximum);
  }

  setValue(m_numericValue);
}

void SciNotDoubleSpinBox::setText(const double value)
{
  auto text = util::DoubleToStringConvertor::locale().toString(value, 'e', 2);
  lineEdit()->setText(text);
}

QAbstractSpinBox::StepEnabled SciNotDoubleSpinBox::stepEnabled() const
{
  QAbstractSpinBox::StepEnabled state{QAbstractSpinBox::StepNone};

  if (m_numericValue < m_maximum)
    state |= QAbstractSpinBox::StepUpEnabled;
  if (m_numericValue > m_minimum)
    state |= QAbstractSpinBox::StepDownEnabled;

  return state;
}

QValidator::State SciNotDoubleSpinBox::validate(QString &text, int &) const
{
  bool ok;

  util::DoubleToStringConvertor::back(text, &ok);

  if (!ok) {
    auto str = text.toLower();
    if (str.endsWith("e") ||
        str.endsWith("e+") ||
        str.endsWith("e-"))
      return QValidator::Intermediate;
  }

  return ok ? QValidator::Acceptable : QValidator::Invalid;
}
