#ifndef SCINOTDOUBLESPINBOX_H
#define SCINOTDOUBLESPINBOX_H

#include <QAbstractSpinBox>

class SciNotDoubleSpinBox : public QAbstractSpinBox {
  Q_OBJECT
public:
  SciNotDoubleSpinBox(QWidget *parent = nullptr);

  auto maximum() const -> double;
  auto minimum() const -> double;
  auto numericValue() const -> double;
  void setRange(const double minimum, const double maximum);
  void setValue(const double value);
  void stepBy(int steps) override;
  QAbstractSpinBox::StepEnabled stepEnabled() const override;
  QValidator::State validate(QString &text, int &pos) const override;

private:
  void setText(const double value);

  double m_maximum;
  double m_minimum;
  double m_numericValue;

private slots:
  void onValueEntered();

signals:
  void valueChanged(double value);
};

#endif // SCINOTDOUBLESPINBOX_H
