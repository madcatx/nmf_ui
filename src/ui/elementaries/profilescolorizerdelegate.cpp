#include "profilescolorizerdelegate.h"

#include <ui/models/initialcompositionmodel.h>

#include <QPainter>

#include <cassert>
#include <cmath>

ProfilesColorizerDelegate::ProfilesColorizerDelegate(const InitialCompositionModel *model, QObject *parent) :
  QItemDelegate{parent},
  h_model{model}
{
}

void ProfilesColorizerDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  const auto v = h_model->data(h_model->index(index.row(), 2), Qt::UserRole + 1);
  const auto clr = [&v]() {
    if (v.canConvert<QColor>())
      return v.value<QColor>();
    return QColor::fromHsv(128, 128, 128);
  }();

  painter->save();
  painter->fillRect(option.rect, QBrush{clr});
  painter->restore();
}
