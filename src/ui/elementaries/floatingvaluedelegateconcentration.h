#ifndef FLOATINGVALUEDELEGATECONCENTRATION_H
#define FLOATINGVALUEDELEGATECONCENTRATION_H

#include <ui/elementaries/abstractfloatingvaluedelegate.h>

class FloatingValueDelegateConcentration : public AbstractFloatingValueDelegate {
  Q_OBJECT
public:
  explicit FloatingValueDelegateConcentration(QObject *parent = nullptr);

protected:
  auto formatText(const double value, const QModelIndex &index) const -> QString override;
};

#endif // FLOATINGVALUEDELEGATECONCENTRATION_H
