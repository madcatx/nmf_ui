#ifndef ABSTRACTFLOATINGVALUEDELEGATE_H
#define ABSTRACTFLOATINGVALUEDELEGATE_H

#include <QLocale>
#include <QStyledItemDelegate>

class AbstractFloatingValueDelegate : public QStyledItemDelegate {
  Q_OBJECT
public:
  explicit AbstractFloatingValueDelegate(QObject *parent = nullptr);

  auto createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const -> QWidget * override;
  auto displayText(const QVariant &value, const QLocale &locale) const -> QString override;
  auto forceCommit() -> void;
  auto initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const -> void override;
  auto setEditorData(QWidget *editor, const QModelIndex &index) const -> void override;
  auto setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const -> void override;
  auto updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const -> void override;

protected:
  virtual auto formatText(const double value, const QModelIndex &index) const -> QString = 0;
  auto precision(const QModelIndex &index) const -> int;

private slots:
  void onTextChanged(const QString &);

signals:
  void editorCommit();
};

#endif // ABSTRACTFLOATINGVALUEDELEGATE_H
