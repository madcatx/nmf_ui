#ifndef FLOATINGVALUELINEEDIT_H
#define FLOATINGVALUELINEEDIT_H

#include <QLineEdit>
#include <QLocale>
#include <util/inumberformatchangeable.h>

class FloatingValueLineEdit : public QLineEdit, public util::INumberFormatChangeable
{
  Q_OBJECT
  Q_INTERFACES(util::INumberFormatChangeable)
public:
  FloatingValueLineEdit(QWidget *parent = nullptr);
  bool isEmptyAllowed() const;
  virtual bool isInputValid() const;
  double numericValue() const;
  void onNumberFormatChanged(const QLocale *oldLocale) override;
  void setAllowEmpty(const bool allow);

public slots:
  void revalidate();

protected:
  virtual bool isInputValid(QString text) const;

private:
  void setNumberText(const double dv);

  bool m_allowEmpty;

private slots:
  void ensureSanity(const QString &text);
  void onEditingFinished();

signals:
  void valueChanged(double);
};

#endif // FLOATINGVALUELINEEDIT_H
