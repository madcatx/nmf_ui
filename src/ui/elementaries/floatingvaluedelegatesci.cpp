#include "floatingvaluedelegatesci.h"

#include <util/doubletostringconvertor.h>

FloatingValueDelegateSci::FloatingValueDelegateSci(QObject *parent) : AbstractFloatingValueDelegate{parent}
{
}

auto FloatingValueDelegateSci::formatText(const double value, const QModelIndex &index) const -> QString
{
  return util::DoubleToStringConvertor::convert(value, 'g', precision(index));
}
