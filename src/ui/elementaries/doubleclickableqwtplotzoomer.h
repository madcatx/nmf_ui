#ifndef DOUBLECLICKABLEQWTPLOTZOOMER_H
#define DOUBLECLICKABLEQWTPLOTZOOMER_H

#include <qwt_plot_zoomer.h>

class DoubleClickableQwtPlotZoomer : public QwtPlotZoomer
{
public:
  explicit DoubleClickableQwtPlotZoomer(QWidget *parent = nullptr);

  auto kill() -> void;
  virtual auto widgetMouseDoubleClickEvent(QMouseEvent *evt) -> void override;
};

#endif // DOUBLECLICKABLEQWTPLOTZOOMER_H
