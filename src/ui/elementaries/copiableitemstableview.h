#ifndef COPIABLEITEMSTABLEVIEW_H
#define COPIABLEITEMSTABLEVIEW_H

#include <QTableView>

class CopiableItemsTableView : public QTableView
{
public:
  explicit CopiableItemsTableView(QWidget *parent = nullptr);

protected:
  virtual auto keyPressEvent(QKeyEvent *evt) -> void override;
  virtual auto mousePressEvent(QMouseEvent *evt) -> void override;

private:
  auto editItem(const QModelIndex &idx) -> void;
  auto selectionToClipboard(const QModelIndexList &indexes) -> void;

  QMenu *m_rightClickMenu;

private slots:
  void onCopyActionTriggered();
};

#endif // COPIABLEITEMSTABLEVIEW_H
