#include "floatingvaluedelegateconcentration.h"

#include <util/doubletostringconvertor.h>

FloatingValueDelegateConcentration::FloatingValueDelegateConcentration(QObject *parent) : AbstractFloatingValueDelegate{parent}
{
}

auto FloatingValueDelegateConcentration::formatText(const double value, const QModelIndex &index) const -> QString
{
  if (value < 0.0)
    return tr("Zoned");

  return util::DoubleToStringConvertor::convert(value, 'g', precision(index));
}
