#include "floatingvaluedelegatefix.h"

#include <util/doubletostringconvertor.h>

FloatingValueDelegateFix::FloatingValueDelegateFix(QObject *parent) : AbstractFloatingValueDelegate{parent}
{
}

auto FloatingValueDelegateFix::formatText(const double value, const QModelIndex &index) const -> QString
{
  const int prec = index.model()->data(index, Qt::UserRole + 1).toInt();

  return util::DoubleToStringConvertor::convert(value, 'f', prec);
}
