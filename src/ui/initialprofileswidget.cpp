/* vim: set sw=2 ts=2 sts=2 expandtab : */

#include "initialprofileswidget.h"
#include "ui_initialprofileswidget.h"

#include <gdm/core/gdm.h>
#include <gearbox/defaults.h>
#include <gearbox/singletons.h>
#include <ui/elementaries/floatingvaluedelegatefix.h>
#include <ui/elementaries/doubleclickableqwtplotzoomer.h>
#include <util/lowlevel.h>
#include <util/util.h>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_marker.h>

#include <QDataWidgetMapper>
#include <QGraphicsLineItem>
#include <QGraphicsScene>
#include <QGraphicsSimpleTextItem>
#include <QMessageBox>
#include <QStandardItemModel>

#include <cassert>
#include <cmath>

static const double MIN_ABS_VAL{1e-9};

template <typename T>
static
auto addValidators(QWidget *w, const QVariant &vec)
{
  w->setProperty(
    util::AdditionalValidator<T>::PROPERTY_NAME,
    vec
  );
}

static
auto clampConcentration(const double c)
{
  if (gearbox::Singletons::gdm().isComplexing())
    return c < gearbox::Singletons::minimumComplexingConcentration() ? gearbox::Singletons::minimumComplexingConcentration() : c;
  return c;
}

static
auto injConcentrations(const std::string &name)
{
  const auto &comp = gearbox::Singletons::gdm().composition();
  const auto ctIt = comp.find(name);
  assert(ctIt != comp.cend());

  return gearbox::Singletons::gdm().concentrations(ctIt);
}

static
auto profileFromTypeShape(const InitialProfilesWidget::ProfileType type, const InitialProfilesWidget::InjectionShape shape)
{
  if (type == InitialProfilesWidget::ZONES)
    return gearbox::ConcentrationProfilesModel::ZONES;
  else {
    switch (shape) {
    case InitialProfilesWidget::BOX:
      return gearbox::ConcentrationProfilesModel::INJECTION_BOX;
    case InitialProfilesWidget::GAUSS:
      return gearbox::ConcentrationProfilesModel::INJECTION_GAUSS;
    }
  }

  util::impossible_path();
}

static
auto profileToType(const gearbox::ConcentrationProfilesModel::ProfileShape shape)
{
  switch (shape) {
  case gearbox::ConcentrationProfilesModel::ZONES:
    return InitialProfilesWidget::ZONES;
  default:
    return InitialProfilesWidget::INJECTION;
  }
}

static
auto profileToShape(const gearbox::ConcentrationProfilesModel::ProfileShape shape)
{
  switch (shape) {
  case gearbox::ConcentrationProfilesModel::INJECTION_BOX:
    return InitialProfilesWidget::BOX;
  case gearbox::ConcentrationProfilesModel::INJECTION_GAUSS:
    return InitialProfilesWidget::GAUSS;
  default:
    return InitialProfilesWidget::BOX; // We have to do something
  }
}

static
auto setProfileSamples(const std::vector<std::pair<double, double>> &profile, QwtPlotCurve *curve)
{
  QVector<QPointF> samples{};
  samples.resize(profile.size());

  for (size_t idx{0}; idx < profile.size(); idx++) {
    const auto &xy = profile[idx];
    samples[idx].setX(xy.first);
    samples[idx].setY(xy.second);
  }

  curve->setSamples(samples);
}

InitialProfilesWidget::ProfileParams::ProfileParams(
  ProfileType _type,
  InjectionShape _shape,
  std::vector<double> _concentrations,
  std::vector<gearbox::ConcentrationProfilesModel::Edge> _edges
) :
  type{_type},
  shape{_shape},
  concentrations{std::move(_concentrations)},
  edges{std::move(_edges)}
{
}

InitialProfilesWidget::InitialProfilesWidget(
  gearbox::ConcentrationProfilesModel &concProfsModel,
  MainParametersWidget *mainParamsWidget,
  QWidget *parent) :
  QWidget{parent},
  ui{new Ui::InitialProfilesWidget},
  h_concProfsModel{concProfsModel},
  m_capillaryLength{mainParamsWidget->parameters().capillaryLength},
  m_injectionMapper{this},
  m_zonesMapper{this}
{
  ui->setupUi(this);

  m_zonePlot = new QwtPlot{this};
  m_zonePlot->setAxisTitle(QwtPlot::xBottom, "x (mm)");
  m_zonePlot->setAxisTitle(QwtPlot::yLeft, "c (mM)");
  m_zoneProfile = new QwtPlotCurve{};
  m_zoneProfile->setPen(Qt::red, 2.0);
  m_zoneProfile->attach(m_zonePlot);
  ui->qvlay_plot->addWidget(m_zonePlot);
  m_zonePlot->setCanvasBackground(Qt::white);
  m_zonePlot->setAxisScale(QwtPlot::xBottom, 0, m_capillaryLength);
  m_zoneZoomer = new DoubleClickableQwtPlotZoomer{m_zonePlot->canvas()};

  m_injPlot = new QwtPlot{this};
  m_injPlot->setAxisTitle(QwtPlot::xBottom, "x (mm)");
  m_injPlot->setAxisTitle(QwtPlot::yLeft, "c (mM)");
  m_injProfile = new QwtPlotCurve{};
  m_injProfile->setPen(Qt::red, 2.0);
  m_injProfile->attach(m_injPlot);
  ui->qvlay_inj->addWidget(m_injPlot);
  m_injPlot->setCanvasBackground(Qt::white);
  m_injPlot->setAxisScale(QwtPlot::xBottom, 0, m_capillaryLength);
  m_injZoomer = new DoubleClickableQwtPlotZoomer{m_injPlot->canvas()};

  m_isWithinAbsValidator = std::make_shared<util::IsWithinValidator<double>>(MIN_ABS_VAL, m_capillaryLength);
  m_isWithinRelValidator = std::make_shared<util::IsWithinValidator<double>>(MIN_ABS_VAL * 100.0, 100.0);

  ui->qspb_zone->setRange(0, 0);

  auto dlgt = new FloatingValueDelegateFix{this};
  initInjectionMapper(dlgt);
  initZonesMapper(dlgt);

  ui->qcbox_profileType->addItem(tr("Injection"), INJECTION);
  ui->qcbox_profileType->addItem(tr("Zones"), ZONES);

  ui->qcbox_injectionShape->addItem(tr("Box"), BOX);
  ui->qcbox_injectionShape->addItem(tr("Gauss"), GAUSS);

  m_constituentsModel = new QStandardItemModel{this};
  ui->qlv_constituents->setModel(m_constituentsModel);
  ui->qlv_constituents->setSelectionMode(QAbstractItemView::SingleSelection);

  auto absValVec = QVariant::fromValue<util::AdditionalValidatorVec<double>>({ m_isWithinAbsValidator });
  auto relValVec = QVariant::fromValue<util::AdditionalValidatorVec<double>>({ m_isWithinRelValidator });
  addValidators<double>(ui->qle_positionAbsolute, absValVec);
  addValidators<double>(ui->qle_positionRelative, relValVec);
  addValidators<double>(ui->qle_widthAbsolute, absValVec);
  addValidators<double>(ui->qle_widthRelative, relValVec);
  addValidators<double>(ui->qle_positionAbsoluteZones, absValVec);
  addValidators<double>(ui->qle_positionRelativeZones, relValVec);
  addValidators<double>(ui->qle_widthAbsoluteZones, absValVec);
  addValidators<double>(ui->qle_widthRelativeZones, relValVec);
  addValidators<double>(
    ui->qle_concentration,
    QVariant::fromValue<util::AdditionalValidatorVec<double>>( { util::PremadeValidators::mustBeNonNegative<double>() } )
  );

  onCompositionChanged();

  connect(
    ui->qcbox_profileType,
    static_cast<auto (QComboBox::*)(int) -> void>(&QComboBox::activated),
    this,
    &InitialProfilesWidget::onTypeChanged
  );
  connect(
    ui->qcbox_injectionShape,
    static_cast<auto (QComboBox::*)(int) -> void>(&QComboBox::activated),
    this,
    &InitialProfilesWidget::onShapeChanged
  );
  connect(
    mainParamsWidget,
    &MainParametersWidget::parametersChanged,
    this,
    &InitialProfilesWidget::onMainParametersChanged
  );
  connect(
    ui->qlv_constituents,
    &QListView::clicked,
    this,
    &InitialProfilesWidget::onConstituentListClicked
  );
  connect(
    &m_injectionMapper,
    &decltype(m_injectionMapper)::dataChanged,
    this,
    &InitialProfilesWidget::onInjectionDataChanged
  );
  connect(
    &m_zonesMapper,
    &decltype(m_zonesMapper)::dataChanged,
    this,
    &InitialProfilesWidget::onZoneDataChanged
  );
  connect(
    ui->qpb_applyToAll,
    &QPushButton::clicked,
    this,
    &InitialProfilesWidget::onApplyToAllClicked
  );
  connect(ui->qspb_zone,
    static_cast<auto (QSpinBox::*)(int) -> void>(&QSpinBox::valueChanged),
    this,
    &InitialProfilesWidget::onZoneChanged
  );
  connect(
    ui->qpb_addZone,
    &QPushButton::clicked,
    this,
    &InitialProfilesWidget::onAddZoneClicked
  );
  connect(
    ui->qpb_removeZone,
    &QPushButton::clicked,
    this,
    &InitialProfilesWidget::onRemoveZoneClicked
  );
}

InitialProfilesWidget::~InitialProfilesWidget()
{
  delete ui;
}

auto InitialProfilesWidget::checkEdgesSanity(const std::vector<gearbox::ConcentrationProfilesModel::Edge> &edges) -> bool
{
   double prevPos{0.0};
   for (auto &eitem : edges) {
     if (eitem.position <= prevPos)
       return false;
     prevPos = eitem.position;
   }

   return true;
}

auto InitialProfilesWidget::commit() -> bool
{
  /* Do sanity check first */
  for (const auto & item : m_data) {
    const auto &profile = item.second;

    if (profile.type == ZONES) {
      if (!checkEdgesSanity(profile.edges)) {
        QMessageBox mbox{
          QMessageBox::Warning,
          tr("Invalid edge position"),
          tr("Positions of edges must be ascending")
        };
        mbox.exec();
        return false;
      }
    }
  }

  /* Commit changes */
  for (const auto & item : m_data) {
    const auto &name = item.first;
    const auto &profile = item.second;
    auto ptype = profileFromTypeShape(profile.type, profile.shape);

    auto edges = profile.edges;
    const auto info = h_concProfsModel.get(name);
    const auto concs = [&info](const auto profile) {
      if ((info.shape == gearbox::ConcentrationProfilesModel::INJECTION_BOX || info.shape == gearbox::ConcentrationProfilesModel::INJECTION_GAUSS) &&
          (profile.shape == BOX || profile.shape == GAUSS) && (profile.type == INJECTION))
        return info.concentrations;
      return profile.concentrations;
    }(profile);

    h_concProfsModel.set(
      name,
      ptype,
      concs,
      std::move(edges)
    );
  }

  return true;
}

auto InitialProfilesWidget::convertConcentrations(const gearbox::ConcentrationProfilesModel::Info &info) -> std::vector<double>
{
  if (info.shape != gearbox::ConcentrationProfilesModel::ZONES)
    return info.concentrations;

  auto concentrations = info.concentrations;
  concentrations.resize(info.edges.size() + 1);

  return concentrations;
}

auto InitialProfilesWidget::currentData() -> Map::iterator
{
  const auto index = ui->qlv_constituents->currentIndex();
  if (!index.isValid())
    return m_data.end();

  const auto key = m_constituentsModel->data(index).toString().toStdString();
  return m_data.find(key);
}

auto InitialProfilesWidget::dimToAbs(const double v) const -> double
{
  return (v * m_capillaryLength) / 100.0;
}

auto InitialProfilesWidget::dimToRel(const double v) const -> double
{
  return 100.0 * v / m_capillaryLength;
}

auto InitialProfilesWidget::initInjectionMapper(FloatingValueDelegateFix *dlgt) -> void
{
  auto mapper = new QDataWidgetMapper{this};

  m_injectionData.resize(m_injectionMapper.indexFromItem(InjectionItems::LAST_INDEX));

  m_injectionMapper.setUnderlyingData(&m_injectionData);
  mapper->setModel(&m_injectionMapper);
  mapper->setItemDelegate(dlgt);
  mapper->addMapping(ui->qle_positionAbsolute, m_injectionMapper.indexFromItem(InjectionItems::POSITION_ABS));
  mapper->addMapping(ui->qle_positionRelative, m_injectionMapper.indexFromItem(InjectionItems::POSITION_REL));
  mapper->addMapping(ui->qle_widthAbsolute, m_injectionMapper.indexFromItem(InjectionItems::WIDTH_ABS));
  mapper->addMapping(ui->qle_widthRelative, m_injectionMapper.indexFromItem(InjectionItems::WIDTH_REL));
  mapper->addMapping(ui->qle_steepnessAbsolute, m_injectionMapper.indexFromItem(InjectionItems::STEEPNESS_ABS));
  mapper->addMapping(ui->qle_steepnessRelative, m_injectionMapper.indexFromItem(InjectionItems::STEEPNESS_REL));
  mapper->toFirst();
}

auto InitialProfilesWidget::initZonesMapper(FloatingValueDelegateFix *dlgt) -> void
{
  auto mapper = new QDataWidgetMapper{this};

  m_zonesData.resize(m_zonesMapper.indexFromItem(ZonesItems::LAST_INDEX));

  m_zonesMapper.setUnderlyingData(&m_zonesData);
  mapper->setModel(&m_zonesMapper);
  mapper->setItemDelegate(dlgt);
  mapper->addMapping(ui->qle_positionAbsoluteZones, m_zonesMapper.indexFromItem(ZonesItems::POSITION_ABS));
  mapper->addMapping(ui->qle_positionRelativeZones, m_zonesMapper.indexFromItem(ZonesItems::POSITION_REL));
  mapper->addMapping(ui->qle_widthAbsoluteZones, m_zonesMapper.indexFromItem(ZonesItems::WIDTH_ABS));
  mapper->addMapping(ui->qle_widthRelativeZones, m_zonesMapper.indexFromItem(ZonesItems::WIDTH_REL));
  mapper->addMapping(ui->qle_concentration, m_zonesMapper.indexFromItem(ZonesItems::CONCENTRATION));
  mapper->toFirst();
}

auto InitialProfilesWidget::makeData() -> void
{
  m_data.clear();

  for (auto it = h_concProfsModel.cbegin(); it != h_concProfsModel.cend(); it++) {
    const auto name = it->first;
    const auto &info = h_concProfsModel.get(name);

    const auto type = profileToType(info.shape);
    const auto shape = profileToShape(info.shape);

    auto concentrations = convertConcentrations(info);

    m_data.try_emplace(
      name,
      type,
      shape,
      std::move(concentrations),
      info.edges
    );
  }
}

auto InitialProfilesWidget::makeEdgesMarkers() -> void
{
  for (auto item : m_edgeMarkers) {
    item->detach();
    delete item;
  }
  m_edgeMarkers.clear();

  const auto it = currentData();
  assert (it != m_data.end());

  const auto &edges = it->second.edges;

  for (const auto &e : edges) {
    auto marker = new QwtPlotMarker{};
    marker->setLineStyle(QwtPlotMarker::VLine);
    marker->attach(m_zonePlot);
    marker->setXValue(e.position);
    m_edgeMarkers.emplace_back(marker);
  }
  m_zonePlot->replot();
}

auto InitialProfilesWidget::makeInjLine() -> void
{
  static const size_t LINE_PTS{250};
  const double DX{m_capillaryLength / LINE_PTS};

  auto it = currentData();
  assert(it != m_data.cend());
  auto concs = injConcentrations(it->first);

  const auto profile = [&]() {
    switch (it->second.shape) {
    case BOX:
      return gearbox::ConcentrationProfilesModel::boxProfile(
            concs,
            it->second.edges,
            LINE_PTS,
            DX);
    case GAUSS:
      return gearbox::ConcentrationProfilesModel::gaussianProfile(
            concs,
            it->second.edges,
            LINE_PTS,
            DX);
    default:
      assert(false);
    }

#ifdef NDEBUG
    std::abort();
#endif // NDEBUG
  }();

  setProfileSamples(profile, m_injProfile);
  m_injPlot->replot();
}

auto InitialProfilesWidget::makeZonesLines() -> void
{
  static const size_t LINE_PTS{250};
  const double DX{m_capillaryLength / LINE_PTS};

  auto it = currentData();
  assert(it != m_data.cend());

  if (it->second.edges.size() == 0)
    return;

  const auto profile = gearbox::ConcentrationProfilesModel::zonedProfile(
    it->second.concentrations,
    it->second.edges,
    LINE_PTS,
    DX
  );

  setProfileSamples(profile, m_zoneProfile);
  m_zonePlot->replot();
}

void InitialProfilesWidget::onAddZoneClicked()
{
  auto it = currentData();
  assert(it != m_data.cend());

  assert(it->second.type == ZONES);

  if (it->second.concentrations.size() == 100) // TODO: Do not have this hardcoded
    return;

  it->second.edges.emplace_back(0.0, 0.00);
  it->second.concentrations.emplace_back(0.0);

  assert(it->second.edges.size() + 1 == it->second.concentrations.size());

  const int zoneIdx = it->second.edges.size();
  ui->qspb_zone->setMaximum(zoneIdx);

  auto marker = new QwtPlotMarker{};
  marker->setLineStyle(QwtPlotMarker::VLine);
  m_edgeMarkers.emplace_back(marker);
  marker->attach(m_zonePlot);
  marker->setXValue(0);

  ui->qspb_zone->setValue(zoneIdx);
}

void InitialProfilesWidget::onApplyToAllClicked()
{
  const auto it = currentData();
  assert(it != m_data.cend());

  for (auto & it2 : m_data)
    it2.second = it->second;
}

void InitialProfilesWidget::onCompositionChanged()
{
  resetAll();
}

void InitialProfilesWidget::onConstituentListClicked(const QModelIndex &index)
{
  const auto row = index.row();
  if (row < 0 || row >= m_constituentsModel->rowCount())
    return;

  selectConstituent(index);
}

void InitialProfilesWidget::onInjectionDataChanged(
  const QModelIndex &topLeft,
  const QModelIndex &bottomRight,
  const QVector<int> &roles
)
{
  Q_UNUSED(roles)

  /* This might get a little tricky.
   * We shall treat absolute value changes with priority,
   * meaning that if both absolute and relative values change,
   * the relative change shall be ignored and adjusted to the corresponding
   * absolute change */
  const auto posAbsIdx = m_injectionMapper.indexFromItem(InjectionItems::POSITION_ABS);
  const auto posRelIdx = m_injectionMapper.indexFromItem(InjectionItems::POSITION_REL);
  const auto widthAbsIdx = m_injectionMapper.indexFromItem(InjectionItems::WIDTH_ABS);
  const auto widthRelIdx = m_injectionMapper.indexFromItem(InjectionItems::WIDTH_REL);
  const auto steepAbsIdx = m_injectionMapper.indexFromItem(InjectionItems::STEEPNESS_ABS);
  const auto steepRelIdx = m_injectionMapper.indexFromItem(InjectionItems::STEEPNESS_REL);
  const auto tl = topLeft.column();
  const auto br = bottomRight.column();
  QVector<InjectionItems> itemsToUpdate{};

  auto handle = [this, &itemsToUpdate](const auto srcIdx, const auto dstIdx, const bool toAbs) {
    const auto nv = toAbs ? dimToAbs(m_injectionData[srcIdx]) : dimToRel(m_injectionData[srcIdx]);
    if (!qFuzzyCompare(nv, m_injectionData[dstIdx])) {
      m_injectionData[dstIdx] = nv;
      itemsToUpdate.append(m_injectionMapper.itemFromIndex(dstIdx));
    }
  };

  if (util::within(tl, br, posAbsIdx))
    handle(posAbsIdx, posRelIdx, false);
  else if (util::within(tl, br, posRelIdx))
    handle(posRelIdx, posAbsIdx, true);

  if (util::within(tl, br, widthAbsIdx))
    handle(widthAbsIdx, widthRelIdx, false);
  else if (util::within(tl, br, widthRelIdx))
    handle(widthRelIdx, widthAbsIdx, true);

  if (util::within(tl, br, steepAbsIdx))
    handle(steepAbsIdx, steepRelIdx, false);
  else if (util::within(tl, br, steepRelIdx))
    handle(steepRelIdx, steepAbsIdx, true);

  updateEdgeInjection();

  for (const auto &item : itemsToUpdate)
    m_injectionMapper.notifyDataChanged(item, item);
}

void InitialProfilesWidget::onMainParametersChanged(const MainParametersWidget::Params &params)
{
  m_capillaryLength = params.capillaryLength;
  m_isWithinAbsValidator->changeRange(MIN_ABS_VAL, m_capillaryLength);

  m_zonePlot->setAxisScale(QwtPlot::xBottom, 0, m_capillaryLength);
}

void InitialProfilesWidget::onRemoveZoneClicked()
{
  auto it = currentData();
  assert(it != m_data.end());

  assert(it->second.type == ZONES);

  const int zoneIdx = ui->qspb_zone->value();
  if (zoneIdx == 0)
    return;

  it->second.edges.erase(it->second.edges.begin() + zoneIdx - 1);
  it->second.concentrations.erase(it->second.concentrations.begin() + zoneIdx);

  auto eit = m_edgeMarkers.begin();
  std::advance(eit, zoneIdx - 1);
  auto marker = *eit;
  marker->detach();
  m_edgeMarkers.erase(eit);
  delete marker;

  assert(it->second.edges.size() + 1 == it->second.concentrations.size());

  const auto numZones = it->second.edges.size();
  ui->qspb_zone->setMaximum(numZones);
  ui->qspb_zone->setValue(zoneIdx > 0 ? zoneIdx - 1 : 0);
}

void InitialProfilesWidget::onShapeChanged()
{
  const auto shape = ui->qcbox_injectionShape->currentData().value<InjectionShape>();
  auto it = currentData();
  assert(it != m_data.end());

  assert(it->second.type == INJECTION);

  const auto steepnessVisible = shape == BOX;
  ui->ql_steepness->setVisible(steepnessVisible);
  ui->qle_steepnessAbsolute->setVisible(steepnessVisible);
  ui->qle_steepnessRelative->setVisible(steepnessVisible);

  it->second.shape = shape;
}

void InitialProfilesWidget::onTypeChanged(int)
{
  const auto type = ui->qcbox_profileType->currentData().value<ProfileType>();
  auto it = currentData();
  assert(it != m_data.end());

  switch (type) {
  case INJECTION:
    it->second.type = INJECTION;
    it->second.shape = BOX;
    it->second.edges = { { gearbox::Defaults::INJECTION_POSITION >= m_capillaryLength ? m_capillaryLength / 2 : gearbox::Defaults::INJECTION_POSITION,
                           gearbox::Defaults::INJECTION_WIDTH,
                           gearbox::Defaults::INJECTION_STEEPNESS } };
    switchComboBox(BOX, ui->qcbox_injectionShape);
    break;
  case ZONES:
    it->second.type = ZONES;
    it->second.edges = {};
    it->second.concentrations = { 0.0 };
    ui->qspb_zone->setMaximum(0);
    updateZoneControls(ui->qspb_zone->value(), 0.0, 0.0);
    makeEdgesMarkers();
    m_zoneProfile->setSamples({});
    m_zonePlot->replot();
    break;
  }

  switchWidgetStack(type);
}

void InitialProfilesWidget::onZoneChanged(int idx)
{
  auto it = currentData();
  assert(it != m_data.end());

  const auto &edges = it->second.edges;
  const auto &concs = it->second.concentrations;

  if (edges.size() == 0)
    return;

  assert(edges.size() + 1 > idx); assert(concs.size() > idx);

  /* NOTE:
   * This code originally used relative dimensions as principial and expected
   * them to be converted back to absolute dimensions. This turned out to cause
   * problems and the benefits of keeping edge positions in relative dimensions
   * are unclear. Therefore the code was changed back to use absolute dimensions
   */

  auto get = [&edges, idx](double gearbox::ConcentrationProfilesModel::Edge:: *m) {
    if (idx == 0)
      return 0.0;
    else
      return edges[idx - 1].*m;
  };

  m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::POSITION_REL)] = dimToRel(get(&gearbox::ConcentrationProfilesModel::Edge::position));
  m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::POSITION_ABS)] = get(&gearbox::ConcentrationProfilesModel::Edge::position);
  m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::WIDTH_REL)] = dimToRel(get(&gearbox::ConcentrationProfilesModel::Edge::width));
  m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::WIDTH_ABS)] = get(&gearbox::ConcentrationProfilesModel::Edge::width);
  m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::CONCENTRATION)] = clampConcentration(concs[idx]);

  const double cMax = std::accumulate(concs.cbegin(), concs.cend(), 0.0, [](const auto v, const auto c) { return c > v ? c : v;});

  updateZoneControls(idx, 0, cMax);
  m_zonesMapper.notifyAllDataChanged();
}

void InitialProfilesWidget::onZoneDataChanged(
  const QModelIndex &topLeft,
  const QModelIndex &bottomRight,
  const QVector<int> &roles
)
{
  Q_UNUSED(roles)

  /* Logic here is the exact opposite to
   * onInjectionDataChanged() logic
   */
  const auto posAbsIdx = m_zonesMapper.indexFromItem(ZonesItems::POSITION_ABS);
  const auto posRelIdx = m_zonesMapper.indexFromItem(ZonesItems::POSITION_REL);
  const auto widthAbsIdx = m_zonesMapper.indexFromItem(ZonesItems::WIDTH_ABS);
  const auto widthRelIdx = m_zonesMapper.indexFromItem(ZonesItems::WIDTH_REL);
  const auto tl = topLeft.column();
  const auto br = bottomRight.column();
  QVector<ZonesItems> itemsToUpdate{};

  auto handle = [this, &itemsToUpdate](const auto srcIdx, const auto dstIdx, const bool toAbs) {
    const auto nv = toAbs ? dimToAbs(m_zonesData[srcIdx]) : dimToRel(m_zonesData[srcIdx]);
    if (!qFuzzyCompare(nv, m_zonesData[dstIdx])) {
      m_zonesData[dstIdx] = nv;
      itemsToUpdate.append(m_zonesMapper.itemFromIndex(dstIdx));
    }
  };

  if (util::within(tl, br, posRelIdx))
    handle(posRelIdx, posAbsIdx, true);
  else if (util::within(tl, br, posAbsIdx))
    handle(posAbsIdx, posRelIdx, false);

  if (util::within(tl, br, widthRelIdx))
    handle(widthRelIdx, widthAbsIdx, true);
  else if (util::within(tl, br, widthAbsIdx))
    handle(widthAbsIdx, widthRelIdx, false);

  updateZoneData();

  for (const auto &item : itemsToUpdate)
    m_zonesMapper.notifyDataChanged(item, item);

  auto it = currentData();
  assert(it != m_data.end());

  const auto &concs = it->second.concentrations;

  const double cMax = std::accumulate(concs.cbegin(), concs.cend(), 0.0, [](const auto v, const auto c) { return c > v ? c : v;});

  setZoneBaseZoom(0.0, cMax);
}

auto InitialProfilesWidget::pickConstituent(const int idx) -> void
{
  if (util::within(0, m_constituentsModel->rowCount() - 1, idx)) {
    const auto index = m_constituentsModel->index(idx, 0);
    /* Calling order matters here! */
    ui->qlv_constituents->setCurrentIndex(index);
    selectConstituent(index);
  }
}

auto InitialProfilesWidget::redrawInjProfile() -> void
{
  assert(currentData() != m_data.end());

  makeInjLine();
}

auto InitialProfilesWidget::reject() -> void
{
  resetAll();
}

auto InitialProfilesWidget::resetAll() -> void
{
  m_constituentsModel->clear();

  for (auto it = h_concProfsModel.cbegin(); it != h_concProfsModel.cend(); it++) {
    const auto s = QString::fromStdString(it->first);
    auto item = new QStandardItem{s};
    item->setData(s);

    m_constituentsModel->appendRow(item);
  }

  const bool visible = m_constituentsModel->rowCount() > 0;
  ui->ql_profileType->setVisible(visible);
  ui->qcbox_profileType->setVisible(visible);
  ui->stackedWidget->setVisible(visible);
  ui->qpb_applyToAll->setVisible(visible);

  makeData();

  if (visible) {
    const auto index = m_constituentsModel->index(0, 0);
    ui->qlv_constituents->setCurrentIndex(index);
    selectConstituent(index);
  }

}

auto InitialProfilesWidget::selectConstituent(const QModelIndex &index) -> void
{
  const auto it = m_data.find(m_constituentsModel->data(index, Qt::UserRole + 1).toString().toStdString());

  assert(it != m_data.end());
  const auto &params = it->second;

  switchComboBox(params.type, ui->qcbox_profileType);
  if (params.type == INJECTION) {
    const auto &dim = params.edges.at(0);
    m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::POSITION_ABS)] = dim.position;
    m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::WIDTH_ABS)] = dim.width;
    m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::STEEPNESS_ABS)] = dim.steepness;
    m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::POSITION_REL)] = dimToRel(dim.position);
    m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::WIDTH_REL)] = dimToRel(dim.width);
    m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::STEEPNESS_REL)] = dimToRel(dim.steepness);

    m_injectionMapper.notifyAllDataChanged();

    const auto &concs = injConcentrations(it->first);
    const double cMax = concs[0] > concs[1] ? concs[0] : concs[1];
    setInjBaseZoom(0.0, cMax);

    switchComboBox(params.shape, ui->qcbox_injectionShape);
    redrawInjProfile();
  } else if (params.type == ZONES) {
    ui->qspb_zone->setMaximum(params.edges.size());
    ui->qspb_zone->setValue(0);

    m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::POSITION_ABS)] = 0.0;
    m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::WIDTH_ABS)] = 0.0;
    m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::POSITION_REL)] = 0.0;
    m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::WIDTH_REL)] = 0.0;
    m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::CONCENTRATION)] = clampConcentration(params.concentrations.at(0));

    m_zonesMapper.notifyAllDataChanged();

    makeEdgesMarkers();
    updateZoneData();
  } else
    assert(false);

  switchWidgetStack(params.type);
}

auto InitialProfilesWidget::setInjBaseZoom(const double cMin, const double cMax) -> void
{
  QRectF brect{};

  brect.setRect(0, cMin, m_capillaryLength * 1.05, (cMax - cMin) * 1.05);

  m_injZoomer->zoom(brect);
  m_injZoomer->setZoomBase(brect);
}


auto InitialProfilesWidget::setZoneBaseZoom(const double cMin, const double cMax) -> void
{
  QRectF brect{};

  brect.setRect(0, cMin, m_capillaryLength * 1.05, (cMax - cMin) * 1.05);

  m_zoneZoomer->zoom(brect);
  m_zoneZoomer->setZoomBase(brect);
}

template <typename T>
auto InitialProfilesWidget::switchComboBox(const T value, QComboBox *cbox) -> void
{
  for (int idx{0}; idx < cbox->count(); idx++) {
    const auto data = ui->qcbox_profileType->itemData(idx).value<T>();
    if (value == data) {
      cbox->setCurrentIndex(idx);
      return;
    }
  }
}

auto InitialProfilesWidget::switchWidgetStack(const ProfileType type) -> void
{
  switch (type) {
  case INJECTION:
    ui->stackedWidget->setCurrentWidget(ui->qpage_injection);
    break;
  case ZONES:
    ui->stackedWidget->setCurrentWidget(ui->qpage_zones);
    break;
  }
}

auto InitialProfilesWidget::updateEdgeInjection() -> void
{
  auto it = currentData();
  assert(it != m_data.end());
  assert(it->second.type == INJECTION);

  auto &edges = it->second.edges;
  assert(edges.size() == 1);

  edges[0].position = m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::POSITION_ABS)];
  edges[0].width = m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::WIDTH_ABS)];
  edges[0].steepness = m_injectionData[m_injectionMapper.indexFromItem(InjectionItems::STEEPNESS_ABS)];

  redrawInjProfile();
}

auto InitialProfilesWidget::updateZoneControls(const int idx, const double cMin, const double cMax) -> void
{
  const bool enabled = idx > 0;
  ui->qle_widthAbsoluteZones->setEnabled(enabled);
  ui->qle_widthRelativeZones->setEnabled(enabled);
  ui->qle_positionAbsoluteZones->setEnabled(enabled);
  ui->qle_positionRelativeZones->setEnabled(enabled);

  ui->qle_widthAbsoluteZones->setZoneNumber(idx);
  ui->qle_widthRelativeZones->setZoneNumber(idx);
  ui->qle_positionAbsoluteZones->setZoneNumber(idx);
  ui->qle_positionRelativeZones->setZoneNumber(idx);

  setZoneBaseZoom(cMin, cMax);
}

auto InitialProfilesWidget::updateZoneData() -> void
{
  auto it = currentData();
  assert(it != m_data.end());
  assert(it->second.type == ZONES);

  m_zoneProfile->setSamples({});

  const auto idx = ui->qspb_zone->value();

  auto &edges = it->second.edges;
  auto &concs = it->second.concentrations;

  assert(idx < edges.size() + 1); assert(idx < concs.size());
  if (idx > 0) {
    edges[idx-1].position = m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::POSITION_ABS)];
    edges[idx-1].width = m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::WIDTH_ABS)];

    m_edgeMarkers[idx-1]->setXValue(edges[idx-1].position);
  }
  concs[idx] = clampConcentration(m_zonesData[m_zonesMapper.indexFromItem(ZonesItems::CONCENTRATION)]);
  m_zonePlot->replot();

  if (checkEdgesSanity(edges))
    makeZonesLines();
}
