#ifndef EDITCHARGESWIDGET_H
#define EDITCHARGESWIDGET_H

#include <QWidget>
#include <ui/models/constituentchargesmodel.h>

namespace Ui {
  class EditChargesWidget;
}

namespace gdm {
  class PhysicalProperties;
}

class FloatingValueDelegateFix;
class ModifyConstituentChargePushButton;

class EditChargesWidget : public QWidget
{
  Q_OBJECT

public:
  explicit EditChargesWidget(QWidget *parent = nullptr);
  explicit EditChargesWidget(const  gdm::PhysicalProperties &props, QWidget *parent = nullptr);
  explicit EditChargesWidget(const std::vector<double> &pKas, const std::vector<double> &mobilities, const int chargeLow, const int chargeHigh, QWidget *parent = nullptr);
  explicit EditChargesWidget(std::map<int, double> pKas, const std::map<int, double> &mobilities, const int chargeLow, const int chargeHigh, QWidget *parent = nullptr);
  ~EditChargesWidget();
  auto chargeHigh() const -> int;
  auto chargeLow() const -> int;
  auto mobilities() const -> std::vector<double>;
  auto pKas() const -> std::vector<double>;
  auto setCharges(const std::map<int, double> &pKas, const std::map<int, double> &mobilities, const int chargeLow, const int chargeHigh) -> void;

private:
  auto setupWidget() -> void;
  auto updateChargeHigh() -> void;
  auto updateChargeLow() -> void;
  auto updateChargeModifiers() -> void;

  Ui::EditChargesWidget *ui;

  ModifyConstituentChargePushButton *m_qpb_addLow;
  ModifyConstituentChargePushButton *m_qpb_removeLow;
  ModifyConstituentChargePushButton *m_qpb_addHigh;
  ModifyConstituentChargePushButton *m_qpb_removeHigh;

  FloatingValueDelegateFix *m_fltDelegate;
  ConstituentChargesModel m_chargesModel;

private slots:
  void onAddChargeLow();
  void onAddChargeHigh();
  void onRemoveChargeLow();
  void onRemoveChargeHigh();
  void onReturnPressed();

signals:
  void acceptRequested();
};

#endif // EDITCHARGESWIDGET_H
