#include "systeminfodialog.h"
#include "ui_systeminfodialog.h"

#include <echmetelems.h>

#include <QSysInfo>

#ifdef Q_OS_LINUX
#include <sys/sysinfo.h>
#elif defined Q_OS_WIN
#include <Windows.h>
#endif // Q_OS_

#define _STR(x) #x
#define ADD_AVX512_SET(set) \
	if (simd.AVX512.set) avx512Sets.append(_STR(set));

static
auto getArchitecture()
{
  const auto prod = QSysInfo::prettyProductName();
  const auto arch = QSysInfo::buildCpuArchitecture();

  return QString{"%0 - %1"}.arg(arch).arg(prod);
}

static
auto getSystemMemory()
{
#ifdef Q_OS_LINUX
  struct sysinfo si{};

  if (sysinfo(&si) < 0)
    return 0UL;
  return si.totalram / 1024 / 1024;
#elif defined Q_OS_WIN
  MEMORYSTATUSEX ms{};
  ms.dwLength = sizeof(ms);
  if (GlobalMemoryStatusEx(&ms) == TRUE)
    return ms.ullTotalPhys / 1024 / 1024;
  return 0ULL;
#endif // Q_OS_
}

SystemInfoDialog::SystemInfoDialog(QWidget *parent) :
  QDialog{parent},
  ui{new Ui::SystemInfoDialog}
{
  ui->setupUi(this);

  const auto cpu = ECHMET::cpuIdentifier();
  const auto simd = ECHMET::cpuSupportedSIMD();
  const auto mem = getSystemMemory();

  ui->ql_cpu->setText(QString{cpu->c_str()}.trimmed());
  ui->ql_arch->setText(getArchitecture());
  ui->ql_memory->setText(mem > 0 ? QString{"%0 MiB"}.arg(mem) : "Unknown");
  setSimd(ui->ql_sse2, simd.SSE2);
  setSimd(ui->ql_sse3, simd.SSE3);
  setSimd(ui->ql_ssse3, simd.SSSE3);
  setSimd(ui->ql_sse41, simd.SSE41);
  setSimd(ui->ql_sse42, simd.SSE42);
  setSimd(ui->ql_avx, simd.AVX);
  setSimd(ui->ql_avx2, simd.AVX2);
  setSimd(ui->ql_fma3, simd.FMA3);

  QStringList avx512Sets{};
  ADD_AVX512_SET(F);
  ADD_AVX512_SET(CD);
  ADD_AVX512_SET(PF);
  ADD_AVX512_SET(ER);
  ADD_AVX512_SET(VL);
  ADD_AVX512_SET(BW);
  ADD_AVX512_SET(DQ);
  ADD_AVX512_SET(IFMA);
  ADD_AVX512_SET(VBM);
  ADD_AVX512_SET(VBM2);
  ADD_AVX512_SET(VNNI);
  ADD_AVX512_SET(BITALG);
  ADD_AVX512_SET(VPOPCNTDQ);
  if (simd.AVX512._4VNNIW) avx512Sets.append("4VNNIW");
  if (simd.AVX512._4FMAPS) avx512Sets.append("4FMAPS");
  ADD_AVX512_SET(VP2INTERSECT);
  ADD_AVX512_SET(FP16);
  ADD_AVX512_SET(BF16);
  if (avx512Sets.empty()) {
    setSimd(ui->ql_avx512, false);
  } else {
    ui->ql_avx512->setStyleSheet("QLabel { color : green; }");
    ui->ql_avx512->setText(avx512Sets.join(" "));
  }

  cpu->destroy();
}

SystemInfoDialog::~SystemInfoDialog()
{
  delete ui;
}

auto SystemInfoDialog::setSimd(QLabel *l, const bool available) -> void
{
  if (available) {
    l->setStyleSheet("QLabel { color : green; }");
    l->setText(tr("Available"));
  } else {
    l->setStyleSheet("QLabel { color : red; }");
    l->setText(tr("Not available"));
  }
}
