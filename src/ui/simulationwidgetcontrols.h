#ifndef SIMULATIONWIDGETCONTROLS_H
#define SIMULATIONWIDGETCONTROLS_H

#include <QWidget>

namespace Ui {
class SimulationWidgetControls;
}

class SimulationWidgetControls : public QWidget {
  Q_OBJECT

public:
  explicit SimulationWidgetControls(QWidget *parent = nullptr);
  ~SimulationWidgetControls();
  auto setDetectors(const std::vector<std::pair<bool, double> > &dets) -> void;
  auto trace() -> int;

private:
  Ui::SimulationWidgetControls *ui;

signals:
  void toggleLegend();
  void traceChanged(const int idx);
  void zoomToFit();
};

#endif // SIMULATIONWIDGETCONTROLS_H
