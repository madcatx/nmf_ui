#ifndef EXPORTTRACESDIALOG_H
#define EXPORTTRACESDIALOG_H

#include <calculators/nmfinterfacetypes.h>

#include <QDialog>
#include <QFileDialog>

#include <string>
#include <vector>

namespace Ui {
class ExportTracesDialog;
}

class QCheckBox;

class ExportTracesDialog : public QDialog {
  Q_OBJECT
public:
  explicit ExportTracesDialog(QWidget *parent = nullptr);
  ~ExportTracesDialog();

  auto setData(const std::vector<std::pair<bool, double>> &detectors, calculators::SimStateData data) -> void;

private:
  auto selectedProfiles() -> std::vector<std::string>;

  Ui::ExportTracesDialog *ui;

  QFileDialog *m_saveDlg;
  QWidget *m_selection;

  QCheckBox *m_conductivity;
  QCheckBox *m_pH;

  calculators::SimStateData m_data;
  std::vector<std::pair<bool, double>> m_detectors;
  std::vector<std::pair<QCheckBox *, std::string>> m_profileSelectors;

  void onDeselectAllClicked();
  void onSelectAllClicked();
  void onToClipboardClicked();
  void onToFileClicked();
  void onTraceChanged(const int idx);
};

#endif // EXPORTTRACESDIALOG_H
