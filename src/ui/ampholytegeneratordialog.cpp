#include "ampholytegeneratordialog.h"
#include "ui_ampholytegeneratordialog.h"

#include <gearbox/ampholytegenerator.h>
#include <ui/ampholytepropertieswidget.h>

#include <QMessageBox>

#include <cassert>

static const int MAX_ABS_CHARGE{99};

AmpholyteGeneratorDialog::AmpholyteGeneratorDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::AmpholyteGeneratorDialog)
{
  ui->setupUi(this);
  m_propsWidget = new AmpholytePropertiesWidget{this};

  setupDialog();

  ui->qscra_properties->setWidget(m_propsWidget);
  m_propsWidget->setChargeSpan(-1, 1);
  m_propsWidget->setNumAmpholytes(ui->qspb_numberOfAmpholytes->value());

  connect(
   ui->qspb_lowestCharge,
   static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
   this,
   &AmpholyteGeneratorDialog::onLowestChargeChanged
  );
  connect(
   ui->qspb_highestCharge,
   static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
   this,
   &AmpholyteGeneratorDialog::onHighestChargeChanged
  );
  connect(
    ui->buttonBox,
    &QDialogButtonBox::accepted,
    this,
    &AmpholyteGeneratorDialog::onAccepted
  );
  connect(
    ui->buttonBox,
    &QDialogButtonBox::rejected,
    this,
    &AmpholyteGeneratorDialog::reject
  );
  connect(
    ui->qspb_numberOfAmpholytes,
    static_cast<void (QSpinBox:: *)(int)>(&QSpinBox::valueChanged),
    [this]() {
      m_propsWidget->setNumAmpholytes(ui->qspb_numberOfAmpholytes->value());
    }
  );
}

AmpholyteGeneratorDialog::~AmpholyteGeneratorDialog()
{
  delete ui;
}

auto AmpholyteGeneratorDialog::setupDialog() -> void
{
  ui->qspb_numberOfAmpholytes->setRange(1, 9999);
  ui->qspb_numberOfAmpholytes->setValue(1);

  ui->qspb_lowestCharge->setRange(-MAX_ABS_CHARGE, 0);
  ui->qspb_lowestCharge->setValue(-1);

  ui->qspb_highestCharge->setRange(0, MAX_ABS_CHARGE);
  ui->qspb_highestCharge->setValue(1);
}

void AmpholyteGeneratorDialog::onAccepted()
{
  auto props = m_propsWidget->properties();
  if (!props.ok) {
    QMessageBox mbox{
      QMessageBox::Warning,
      tr("Invalid input"),
      tr("Ampholyte generation parameters are not valid")
    };
    mbox.exec();
    return;
  }

  const auto ret = gearbox::AmpholyteGenerator::generate(
    ui->qspb_numberOfAmpholytes->value(),
    ui->qle_namePrefix->text().toStdString(),
    props.mobilities,
    props.basepKas,
    props.pKaSteps,
    props.chargeLow,
    props.chargeHigh
  );

  if (ret == gearbox::AmpholyteGenerator::RetCode::OK)
    accept();
  else {
    const auto msg = [ret]() -> QString {
      switch (ret) {
      case gearbox::AmpholyteGenerator::RetCode::E_PREFIX:
        return tr("Invalid name prefix");
      case gearbox::AmpholyteGenerator::RetCode::E_CONSTITUENT:
        return tr("Generated constituent cannot be inserted");
      case gearbox::AmpholyteGenerator::RetCode::E_CROSSING_PKAS:
        return tr("pKas of higher ampholytes would cross");
      case gearbox::AmpholyteGenerator::RetCode::E_INVERTED_PKAS:
        return tr("pKas must be descending");
      case gearbox::AmpholyteGenerator::RetCode::E_NUM_AMPHOLYTES:
        return tr("Invalid number of ampholytes");
      default:
        assert(false);
#ifdef NDEBUG
        std::abort();
#endif // NDEBUG
      }
    }();

    QMessageBox mbox{
      QMessageBox::Warning,
      tr("Cannot generate ampholytes"),
      msg
    };
    mbox.exec();
  }
}

void AmpholyteGeneratorDialog::onHighestChargeChanged(const int value)
{
  ui->qspb_lowestCharge->setRange(-MAX_ABS_CHARGE, value - 1);

  m_propsWidget->setChargeSpan(ui->qspb_lowestCharge->value(), value);
}

void AmpholyteGeneratorDialog::onLowestChargeChanged(const int value)
{
  ui->qspb_highestCharge->setRange(value + 1, MAX_ABS_CHARGE);

  m_propsWidget->setChargeSpan(value, ui->qspb_highestCharge->value());
}
