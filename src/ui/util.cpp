#include "util.h"

auto detectorName(const size_t num) -> QString
{
  return QString{"Detector %0"}.arg(num + 1);
}

auto detectorTag(const bool enabled, const double pos, const size_t num) -> QString
{
  return QString{"Detector %0 (%1 mm)%2"}
    .arg(num + 1)
    .arg(pos)
    .arg(enabled ? "" : " (Disabled)");
}

auto displayIndexToDetectorIndex(const int disp, const std::vector<std::pair<bool, double>> &detectors) -> size_t
{
  auto newDisp{disp};
  for (size_t idx{0}; idx <= disp; idx++)
     newDisp -= size_t(!detectors[idx].first);
  return newDisp;
};
