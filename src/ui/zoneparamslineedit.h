#ifndef ZONEPARAMSLINEEDIT_H
#define ZONEPARAMSLINEEDIT_H

#include <ui/elementaries/floatingvaluelineedit.h>

class ZoneParamsLineEdit : public FloatingValueLineEdit {
public:
  ZoneParamsLineEdit(QWidget *parent = nullptr);

  auto isInputValid() const -> bool override;
  auto setZoneNumber(const int num) -> void;

private:
  auto isInputValid(QString text) const -> bool override;

private:
  int m_zoneNumber;
};

#endif // ZONEPARAMSLINEEDIT_H
