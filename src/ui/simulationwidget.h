#ifndef SIMULATIONWIDGET_H
#define SIMULATIONWIDGET_H

#include <QWidget>

#include <string>
#include <vector>

namespace Ui {
class SimulationWidget;
}

namespace gearbox {
  class ProfilesColorizerModel;
}

class DoubleClickableQwtPlotZoomer;
class LegendWidget;
class QwtPlot;
class QwtPlotCurve;
class QwtPlotMarker;
class QwtPlotPicker;
class QwtPlotZoomer;
class SimulationWidgetControls;

class SimulationWidget : public QWidget {
  Q_OBJECT
public:
  enum class CurveType {
    CONDUCTIVITY,
    PH,
    CONSTITUENT
  };

  explicit SimulationWidget(QWidget *parent = nullptr);
  ~SimulationWidget();
  auto attachNonConcentrationCurves(const bool attach);
  auto clear() -> void;
  auto displayedTrace() -> int;
  auto drawDetectorPositions(const std::vector<std::pair<bool, double>> &detectors) -> void;
  auto drawDetectorTrace(const std::vector<std::pair<double, std::vector<double>>> &trace) -> void;
  auto drawProfiles(
    const std::vector<double> &conductivity,
    const std::vector<double> &pH,
    const std::vector<std::vector<double>> &constituents,
    const double dx
  ) -> void;
  auto replot() -> void;
  auto setBaseZoom() -> void;
  auto setFirstLastCellMarkers(const double firstCell, const double lastCell) -> void;
  auto setupConstituentCurves(const std::vector<std::string> &names) -> void;

private:
  enum class PlotMode {
    NONE,
    CAPILLARY,
    DETECTOR
  };

  auto setCapillaryMode(const bool attach) -> bool;
  auto setDetectorMode() -> bool;
  auto setupPlot() -> void;

  Ui::SimulationWidget *ui;
  SimulationWidgetControls *m_ctrls;
  PlotMode m_currentPlotMode;

  LegendWidget *m_legendWidget;
  DoubleClickableQwtPlotZoomer *m_zoomer;
  DoubleClickableQwtPlotZoomer *m_zoomerRight;
  const gearbox::ProfilesColorizerModel &h_profClrModel;

  QwtPlot *m_plot;
  QwtPlotMarker *m_firstCell;
  QwtPlotMarker *m_lastCell;
  QwtPlotPicker *m_picker;

  QwtPlotCurve *m_conductivityCurve;
  QwtPlotCurve *m_pHCurve;

  std::vector<QwtPlotCurve *> m_constituentCurves;
  std::vector<QwtPlotMarker *> m_detectors;

private slots:
  void onPointSelected(const QPointF &pt);
  void onToggleAllCurvesVisibility(bool visible);
  void onToggleCurveVisibility(const QVariant &item, bool visible);
  void onTraceChanged(const int idx);

signals:
  void curvePointSelected(const double x, const QString &name, const CurveType type);
  void traceChanged(const int idx);
};

Q_DECLARE_METATYPE(SimulationWidget::CurveType)

#endif // SIMULATIONWIDGET_H
