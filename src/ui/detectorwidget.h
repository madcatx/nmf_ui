#ifndef DETECTORWIDGET_H
#define DETECTORWIDGET_H

#include <QWidget>

namespace Ui {
class DetectorWidget;
}

class DetectorWidget : public QWidget {
  Q_OBJECT

public:
  explicit DetectorWidget(const size_t num, const bool enabled, const double position, const double capillaryLength, QWidget *parent = nullptr);
  ~DetectorWidget();
  auto enabled() const -> bool;
  auto position() const -> double;
  auto setNumber(const size_t num) -> void;

private:
  Ui::DetectorWidget *ui;

signals:
  void removeMe(DetectorWidget *me);
};

#endif // DETECTORWIDGET_H
