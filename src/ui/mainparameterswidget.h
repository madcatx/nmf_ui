#ifndef MAINPARAMETERSWIDGET_H
#define MAINPARAMETERSWIDGET_H

#include <mappers/floatmappermodel.h>

#include <QVector>
#include <QWidget>

namespace Ui {
class MainParametersWidget;
}

class MainParametersWidget : public QWidget
{
  Q_OBJECT
public:
  enum ConstantForce {
    CF_VOLTAGE,
    CF_CURRENT
  };

  enum class ParamList {
    STOP_TIME,
    TIME,
    CONSTANT_FORCE,
    DEPENDENT_FORCE,
    CAPILLARY_LENGTH,
    LAST_INDEX
  };

  class Params {
  public:
    double time;
    double dt;
    bool autoDt;
    int tolerance;
    int cells;
    double stopTime;
    double constantForceValue;
    ConstantForce constantForce;
    double dependentForceValue;
    double capillaryLength;
    int firstCell;
    int lastCell;
    bool autoCells;
    int updateInterval;
  };

  explicit MainParametersWidget(QWidget *parent = nullptr);
  ~MainParametersWidget();
  auto lockControls(const bool lock) -> void;
  auto parameters()-> Params;
  auto setAutoDt(const bool enabled) -> void;
  auto setParameters(const Params &params) -> void;
  auto setTolerance(const int tol) -> void;
  auto setTStop(const double t) -> void;
  auto tStop() const -> double;
  auto updateLiveParameters(
    const double dt,
    const double time,
    const double constantForceValue,
    const double dependentForceValue,
    const int leftWall,
    const int rightWall
  ) -> void;

private:
  auto commit() -> void;
  auto initParamsMapper() -> void;
  auto setFirstLastCellRanges() -> void;
  auto setInputDefaults() -> void;

  Ui::MainParametersWidget *ui;

  FloatMapperModel<ParamList> m_paramsMapperModel;
  QVector<double> m_paramsMappedData;

private slots:
  void onAutoCellsToggled(int state);
  void onAutoDtToggled(const bool checked);
  void onConstantForceTypeChanged(const int);
  void onParamsChanged();

signals:
  void parametersChanged(const Params &params);
};

Q_DECLARE_METATYPE(MainParametersWidget::ConstantForce)
Q_DECLARE_METATYPE(MainParametersWidget::Params)

#endif // MAINPARAMETERSWIDGET_H
