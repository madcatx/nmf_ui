#include "nmfinterface.h"

#include <nmf.h>
#include <calculators/nmfcontext.h>
#include <gdm/conversion/conversion.h>
#include <gearbox/concentrationprofilesmodel.h>
#include <util/util.h>

#include <cstring>

#include <cassert>

namespace calculators {

class Callbacker {
public:
  static auto ECHMET_CC handleEvent(NMF::Worker &worker, const NMF::Event &event, void *context) -> void
  {
    static_cast<NMFInterface *>(context)->nmfEvent(worker, event);
  }
};

static auto copyConcentrationProfiles(std::vector<std::vector<double>> &concentrations, const NMF::Profiles *profs)
{
  const auto NCells = profs->numCells();
  const auto NRows = profs->numRows();

  for (int32_t cell{0}; cell < NCells; cell++) {
    const auto block = profs->data(cell); /* Concentrations of all constituents in cell "cell" */

    for (int32_t row{0}; row < NRows; row++)
      concentrations[row][cell] = block[row];
  }
}

static auto copySingleProfile(std::vector<double> &target, const NMF::Profiles *prof)
{
  assert(target.size() == size_t(prof->numCells()));

  std::memcpy(target.data(), prof->data(0), prof->numCells() * sizeof(double));
}

static auto profileInfoToArray(const gearbox::ConcentrationProfilesModel::Info &info, const size_t points, const double dx)
{
  auto profileXY = [&info, points, dx]() {
    switch (info.shape) {
    case gearbox::ConcentrationProfilesModel::INJECTION_BOX:
      return gearbox::ConcentrationProfilesModel::boxProfile(info.concentrations, info.edges, points, dx);
    case gearbox::ConcentrationProfilesModel::INJECTION_GAUSS:
      return gearbox::ConcentrationProfilesModel::gaussianProfile(info.concentrations, info.edges, points, dx);
    case gearbox::ConcentrationProfilesModel::ZONES:
      return gearbox::ConcentrationProfilesModel::zonedProfile(info.concentrations, info.edges, points, dx);
    }
    assert(false);
#ifdef NDEBUG
    std::abort();
#endif // NDEBUG
  }();

  std::vector<double> cArray{};
  cArray.resize(points);

  std::transform(profileXY.cbegin(), profileXY.cend(), cArray.begin(), [](auto pt){ return pt.second; });

  return cArray;
}

static
std::vector<std::vector<double>> makeProfiles(const gearbox::ConcentrationProfilesModel &concs, const size_t points, const double dx,
                                              const ECHMET::SysComp::InConstituentVec *constituents)
{
  std::vector<std::vector<double>> space{};

  ECHMET::SysComp::ChemicalSystem chemSystem{};
  ECHMET::SysComp::CalculatedProperties calcProps{};

  auto tRet = ECHMET::SysComp::makeComposition(chemSystem, calcProps, constituents);
  if (tRet != ECHMET::RetCode::OK)
    throw NMFInterfaceError{"Failed to create input concentration profiles"};

  for (size_t idx{0}; idx < chemSystem.constituents->size(); idx++) {
    const auto &ctuent = chemSystem.constituents->at(idx);
    auto it = concs.find(ctuent->name->c_str());
    if (it == concs.cend()) {
      ECHMET::SysComp::releaseChemicalSystem(chemSystem);
      ECHMET::SysComp::releaseCalculatedProperties(calcProps);
      throw NMFInterfaceError("ChemSystem constituent not found in concentrations profiles");
    }

    const auto &info = concs.get(it->first);
    space.emplace_back(profileInfoToArray(info, points, dx));
  }

  ECHMET::SysComp::releaseChemicalSystem(chemSystem);
  ECHMET::SysComp::releaseCalculatedProperties(calcProps);

  return space;
}

NMFInterface::NMFInterface(QObject *parent) :
  QObject{parent},
  m_ctx{std::make_unique<NMFContext>()}
{
}

NMFInterface::~NMFInterface()
{
}

auto NMFInterface::currentStateData() -> SimStateData
{
  if (m_ctx->worker == nullptr) {
    m_ctx->lastError = NMF::RetCode::E_WORKER_NOT_INITIALIZED;
    throw NMFInterfaceError{"Worker not initialized"};
  }

  NMF::Packet *packet{nullptr};

  auto tRet = m_ctx->worker->currentState(packet);
  if (tRet != NMF::RetCode::OK) {
    m_ctx->lastError = tRet;
    throw NMFInterfaceError{"Failed to get current simulation state"};
  }

  SimStateData data{};
  initializeStateArrays(*packet, &data);
  fillSimState(*packet, &data);

  data.detectorTraces = &m_ctx->detectorTraces;

  return data;
}

auto NMFInterface::errorToString() const -> std::string
{
  return NMF::NMFerrorToString(m_ctx->lastError);
}

auto NMFInterface::fillSimState(const NMF::Packet &packet, SimStateData *state) -> void
{
  state->t = packet.get<double>(NMF_K_U_TIME);
  state->dt = packet.get<double>(NMF_K_U_DT);
  state->voltage = packet.get<double>(NMF_K_IU_VOLTAGE);
  state->current = packet.get<double>(NMF_K_IU_CURRENT);
  state->firstCell = packet.get<int32_t>(NMF_K_IU_FIRST_CELL);
  state->lastCell = packet.get<int32_t>(NMF_K_IU_LAST_CELL);
  state->avgTimePerIter = packet.get<double>(NMF_K_U_ITER_AVG_TIME);
  state->dx = packet.get<double>(NMF_K_IU_DX);
  state->constantForce = packet.get<NMF::ConstantForce>(NMF_K_IU_CFTYPE) == NMF::ConstantForce::VOLTAGE
                           ? ConstantForce::VOLTAGE : ConstantForce::CURRENT;

  const auto &cond = packet.get<NMF::Profiles *>(NMF_K_IU_COND_PROFILE);
  copySingleProfile(state->conductivity, cond);

  const auto &pH = packet.get<NMF::Profiles *>(NMF_K_IU_PH_PROFILE);
  copySingleProfile(state->pH, pH);

  const auto &profiles = packet.get<NMF::Profiles *>(NMF_K_IU_PROFILES);
  copyConcentrationProfiles(state->concentrations, profiles);

  if (m_ctx->detectors.size() > 0)
    recordDetectors(state);
}

auto NMFInterface::handleEventDataUpdated(const NMF::Packet &packet) -> void
{
  if (!m_ctx->state->lock.try_lock())
    return;

  updateSimState(packet);

  m_ctx->state->lock.unlock();

  emit simUpdated(m_ctx->state.get());
}

auto NMFInterface::handleEventInitialized(const NMF::Packet &packet) -> void
{
  auto &state = m_ctx->state;

  state->lock.lock();

  initializeStateArrays(packet, state.get());

  const auto &cond = packet.get<NMF::Profiles *>(NMF_K_IU_COND_PROFILE);
  const auto &pH = packet.get<NMF::Profiles *>(NMF_K_IU_PH_PROFILE);
  const auto &profs = packet.get<NMF::Profiles *>(NMF_K_IU_PROFILES);
  const auto dx = packet.get<double>(NMF_K_IU_DX);
  const auto cfType = packet.get<NMF::ConstantForce>(NMF_K_IU_CFTYPE) == NMF::ConstantForce::VOLTAGE
                        ? ConstantForce::VOLTAGE : ConstantForce::CURRENT;
  const auto firstCell = packet.get<int32_t>(NMF_K_IU_FIRST_CELL);
  const auto lastCell = packet.get<int32_t>(NMF_K_IU_LAST_CELL);
  const auto voltage = packet.get<double>(NMF_K_IU_VOLTAGE);
  const auto current = packet.get<double>(NMF_K_IU_CURRENT);

  setInitialSimState(dx, cfType, cond, pH, profs, firstCell, lastCell, voltage, current);

  m_ctx->state->lock.unlock();

  emit simInitialized(state.get());
}

auto NMFInterface::handleEventPaused(const NMF::Packet &packet) -> void
{
  m_ctx->state->lock.lock();

  updateSimState(packet);

  m_ctx->state->lock.unlock();

  emit simPaused(m_ctx->state.get());
}

auto NMFInterface::handleEventStarted(const NMF::Packet &) -> void
{
  emit simStarted();
}

auto NMFInterface::handleEventTerminated(const NMF::Packet &packet) -> void
{
  const auto reason = packet.get<NMF::SimulationFailure>(NMF_K_T_REASON);

  emit simTerminated(NMF::simFailToString(reason));
}

auto NMFInterface::initErrorToString() const -> std::string
{
  const auto genErr = errorToString();

  if (m_ctx->lastError == NMF::RetCode::E_INVALID_ARGUMENT) {
    const std::string initErrStr{NMF::initFailToString(m_ctx->initError)};

    return genErr + std::string{": "} + initErrStr;
  }

  return genErr;
}

auto NMFInterface::initialize(const InitParams &params, const gdm::GDM &gdm, const gearbox::ConcentrationProfilesModel &concs) -> bool
{
  if (std::any_of(params.detectors.cbegin(), params.detectors.cend(), [&params](auto d) { return d > params.capillaryLength; })) {
    m_ctx->lastError = NMF::RetCode::E_INVALID_ARGUMENT;
    return false;
  }

  if (m_ctx->worker)
    m_ctx->worker->destroy();
  m_ctx->worker = nullptr;

  const auto stepper = params.autoDt ? NMF::Stepper::RKCK54 : NMF::Stepper::ABM4;

  m_ctx->lastError = NMF::makeWorker(
    m_ctx->worker,
    stepper,
    &Callbacker::handleEvent,
    this
  );

  if (m_ctx->lastError != NMF::RetCode::OK) {
    m_ctx->worker = nullptr;
    return false;
  }

  ECHMET::NonidealityCorrections corrs{};
  if (params.debyeHuckel)
      ECHMET::nonidealityCorrectionSet(corrs, ECHMET::NonidealityCorrectionsItems::CORR_DEBYE_HUCKEL);
  if (params.onsagerFuoss)
      ECHMET::nonidealityCorrectionSet(corrs, ECHMET::NonidealityCorrectionsItems::CORR_ONSAGER_FUOSS);

  auto constituents = conversion::makeECHMETInConstituentVec(gdm).release();
  std::vector<std::vector<double>> profiles{};

  const auto dx =  util::dx<double>(params.capillaryLength, params.cells);
  try {
    profiles = makeProfiles(concs, params.cells, dx, constituents);
  } catch (const NMFInterfaceError &) {
    m_ctx->lastError = NMF::RetCode::E_RUNTIME_ERROR;
    return false;
  }

  NMF::Initialization init{};
  init.cells = params.cells;

  init.corrections = corrs;
  init.constantForceValue = params.constantForceValue;
  if (params.constantForce == ConstantForce::CURRENT)
    init.constantForceValue *= 1.0e-6;
  init.constantForce = params.constantForce == ConstantForce::CURRENT ? NMF::ConstantForce::CURRENT : NMF::ConstantForce::VOLTAGE;
  init.capillaryLength = params.capillaryLength * 1.0e-3;
  init.capillaryDiameter = params.capillaryDiameter * 1.0e-6;
  init.composition = constituents;
  init.firstCell = params.firstCell;
  init.lastCell = params.lastCell;
  init.adaptCalcSpace = params.adaptCalcSpace;

  m_ctx->lastError = NMF::makeProfiles(profiles, init.profiles, m_ctx->initError);
  if (m_ctx->lastError != NMF::RetCode::OK) {
    NMF::releaseInitialization(init);
    return false;
  }

  m_ctx->lastError = m_ctx->worker->initialize(init, m_ctx->initError);
  NMF::releaseInitialization(init);

  m_ctx->prepareDetectors(params.detectors, dx, params.cells);

  return m_ctx->lastError == NMF::RetCode::OK;
}

auto NMFInterface::initializeStateArrays(const NMF::Packet &packet, SimStateData *state) -> void
{
  const size_t numCells = packet.get<int32_t>(NMF_K_IU_NUM_CELLS);

  state->conductivity.resize(numCells);
  state->pH.resize(numCells);

  state->concentrations.resize(numCells);
  for (size_t idx{0}; idx < numCells; idx++) {
    auto &cVec = state->concentrations[idx];
    cVec.resize(numCells);
  }

  const auto &names = packet.get<NMF::Strings *>(NMF_K_IU_CONSTITUENT_NAMES);
  const auto namesSize = names->size();
  state->constituentNames.resize(names->size());
  for (size_t idx{0}; idx < namesSize; idx++) {
    const auto str = names->elem(idx);
    state->constituentNames[idx] = std::string(str->c_str());
  }

  const size_t numConstituents = packet.get<int32_t>(NMF_K_IU_NUM_CONSTITUENTS);
  state->concentrations.resize(numConstituents);
  for (size_t idx{0}; idx < numConstituents; idx++)
    state->concentrations[idx].resize(numCells);
}

auto NMFInterface::isRunning() -> bool
{
  if (m_ctx->worker == nullptr)
    return false;

  return m_ctx->worker->isRunning();
}

auto NMFInterface::nmfEvent(NMF::Worker &worker, const NMF::Event &event) -> void
{
  (void)worker;

  switch (event.type()) {
  case NMF::EventType::INITIALIZED:
    handleEventInitialized(event.packet());
    break;
  case NMF::EventType::STARTED:
    handleEventStarted(event.packet());
    break;
  case NMF::EventType::PAUSED:
    handleEventPaused(event.packet());
    // TODO unlocking ?
    break;
  case NMF::EventType::DATA_UPDATED:
    handleEventDataUpdated(event.packet());
    break;
  case NMF::EventType::TERMINATED:
    handleEventTerminated(event.packet());
    break;
  }
}

auto NMFInterface::pause() -> bool
{
  if (m_ctx->worker)
    m_ctx->lastError = m_ctx->worker->pause();

  return (m_ctx->lastError == NMF::RetCode::OK) || (m_ctx->lastError == NMF::RetCode::NOT_RUNNING);
}

auto NMFInterface::profileSlice(const ProfileType type, const std::string &name, const double x) -> ProfileSlice
{
  if (m_ctx->worker == nullptr) {
    m_ctx->lastError = NMF::RetCode::E_WORKER_NOT_INITIALIZED;
    throw NMFInterfaceError{"Not initialized"};
  }

  auto tRet = m_ctx->worker->pause();
  if (!(tRet == NMF::RetCode::OK || tRet == NMF::RetCode::NOT_RUNNING)) {
    m_ctx->lastError = tRet;
    throw NMFInterfaceError{"Failed to pause simulation"};
  }

  auto t = [type]() {
    switch (type) {
    case ProfileType::CONDUCTIVITY:
      return NMF::ProfileType::CONDUCTIVITY;
    case ProfileType::PH:
      return NMF::ProfileType::PH;
    case ProfileType::CONCENTRATION:
      return NMF::ProfileType::CONCENTRATION;
    }
    assert(false);
#ifdef NDEBUG
    std::abort();
#endif // NDEBUG
  }();

  NMF::ProfileSlice nmfSlice{};
  const auto eName = ECHMET::createFixedString(name.c_str());
  tRet = m_ctx->worker->profileSlice(t, eName, x, nmfSlice);
  eName->destroy();

  if (tRet != NMF::RetCode::OK) {
    m_ctx->lastError = tRet;
    throw NMFInterfaceError{"Failed to get profile slice"};
  }

  ProfileSlice slice{};
  slice.name = std::string{nmfSlice.name->c_str()};
  slice.x = nmfSlice.x;
  slice.cell = nmfSlice.cell;
  slice.value = nmfSlice.value;
  slice.type = [nmfSlice]() {
    switch (nmfSlice.type) {
    case NMF::ProfileType::CONDUCTIVITY:
      return ProfileType::CONDUCTIVITY;
    case NMF::ProfileType::PH:
      return ProfileType::PH;
    case NMF::ProfileType::CONCENTRATION:
      return ProfileType::CONCENTRATION;
    }
    assert(false);
#ifdef NDEBUG
    std::abort();
#endif // NDEBUG
  }();
  slice.effectiveMobility = nmfSlice.effectiveMobility;

  if (nmfSlice.concentrations != nullptr) {
    for (size_t idx{0}; idx < nmfSlice.concentrations->size(); idx++) {
      const auto c = nmfSlice.concentrations->at(idx);

      slice.concentrations.emplace_back(std::string{c.name->c_str()}, c.concentration);
    }
  }

  NMF::releaseProfileSlice(nmfSlice);

  return slice;
}

auto NMFInterface::recordDetectors(SimStateData *data) -> void
{
  const auto N = data->constituentNames.size();
  std::vector<double> detConcs{};
  detConcs.resize(N);

  for (size_t idx{0}; idx < m_ctx->detectors.size(); idx++) {
    const auto pos = m_ctx->detectors[idx];

    auto &trace = m_ctx->detectorTraces[idx];
    for (size_t jdx{0}; jdx < N; jdx++)
      detConcs[jdx] = data->concentrations[jdx][pos];

    trace.emplace_back(data->t, detConcs);
  }
}

auto NMFInterface::start(const StartParams &params) -> bool
{
  assert(m_ctx->worker);

  auto stepper = m_ctx->worker->stepper();

  if (params.autoDt && stepper != NMF::Stepper::RKCK54) {
    m_ctx->lastError = NMF::changeStepper(NMF::Stepper::RKCK54, m_ctx->worker);
    if (m_ctx->lastError != NMF::RetCode::OK)
      return false;
  } else if (!params.autoDt && stepper != NMF::Stepper::ABM4) {
    m_ctx->lastError = NMF::changeStepper(NMF::Stepper::ABM4, m_ctx->worker);
    if (m_ctx->lastError != NMF::RetCode::OK)
      return false;
  } // TODO: Set startError appropriately

  NMF::Start start{};
  start.dt = params.dt;
  start.tStop = params.tStop;
  start.eofMode = params.eofMode == EOFMode::MOBILITY ? NMF::ElectroosmoticFlowMode::MOBILITY : NMF::ElectroosmoticFlowMode::VELOCITY;
  start.eofValue = params.eofValue * 1.0e-9;
  start.tolerance = params.tolerance;
  start.updateInterval = double(params.updateInterval) *1.0e-3;
  start.constantForceValue = params.constantForceValue;
  if (params.constantForce == ConstantForce::CURRENT)
    start.constantForceValue *= 1.0e-6;
  start.constantForce = params.constantForce == ConstantForce::CURRENT ? NMF::ConstantForce::CURRENT : NMF::ConstantForce::VOLTAGE;
  start.firstCell = params.firstCell;
  start.lastCell = params.lastCell;
  start.adaptCalcSpace = params.adaptCalcSpace;

  NMF::StartFailure fail{};

  m_ctx->lastError = m_ctx->worker->start(start, fail);
  if (m_ctx->lastError != NMF::RetCode::OK) {
      m_ctx->startError = fail;
      return false;
  }

  return true;
}

auto NMFInterface::startErrorToString() const -> std::string
{
  const auto genErr = errorToString();

  if (m_ctx->lastError == NMF::RetCode::E_INVALID_ARGUMENT) {
    const std::string initErrStr{NMF::startFailToString(m_ctx->startError)};

    return genErr + std::string{": "} + initErrStr;
  }

  return genErr;
}

/*
 * This function is expected to be called with State::lock held if there
 * is a possibility of concurrent access
 */
auto NMFInterface::updateSimState(const NMF::Packet &packet) -> void
{
  auto state = m_ctx->state.get();

  fillSimState(packet, state);
}

/*
 * This function is expected to be called with State::lock held if there
 * is a possibility of concurrent access
 */
auto NMFInterface::setInitialSimState(
  const double dx,
  const ConstantForce cf,
  const NMF::Profiles *cond,
  const NMF::Profiles *pH,
  const NMF::Profiles *profs,
  const int32_t firstCell,
  const int32_t lastCell,
  const double voltage,
  const double current
) -> void
{
  auto state = m_ctx->state.get();

  state->dx = dx;
  state->t = 0.0;
  state->dt = 0.0;
  state->voltage = 0.0;
  state->current = 0.0;
  state->firstCell = firstCell;
  state->lastCell = lastCell;
  state->avgTimePerIter = 0.0;
  state->constantForce = cf;
  state->voltage = voltage;
  state->current = current;

  state->detectorTraces = &m_ctx->detectorTraces;

  copySingleProfile(state->conductivity, cond);
  copySingleProfile(state->pH, pH);
  copyConcentrationProfiles(state->concentrations, profs);
}

} // namespace calculators
