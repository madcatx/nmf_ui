#include "nmfcontext.h"

#include <util/util.h>

namespace calculators {

NMFContext::NMFContext() :
  worker{nullptr},
  lastError{NMF::RetCode::OK},
  state{new SimState{}}
{
}

NMFContext::~NMFContext()
{
  if (worker)
    worker->destroy();
}

auto NMFContext::prepareDetectors(const std::vector<double> &dets, const double dx, const size_t numCells) -> void
{
  detectors.clear();
  for (const auto &d : dets)
    detectors.emplace_back(util::index(d, dx, numCells));

  detectorTraces.clear();
  detectorTraces.resize(detectors.size());
}

} // namespace calculators
