#ifndef NMFINTERFACETYPES_H
#define NMFINTERFACETYPES_H

#include <mutex>
#include <string>
#include <utility>
#include <vector>

namespace calculators {

enum class ConstantForce : uint8_t {
  VOLTAGE,
  CURRENT
};

enum class EOFMode : uint8_t {
  VELOCITY,
  MOBILITY
};

enum class ProfileType : uint8_t {
  CONDUCTIVITY,
  PH,
  CONCENTRATION
};

class InitParams {
public:
  bool autoDt;                      /*!< Adjust dt automatically */
  int cells;                        /*!< Number of cells to divide the calculation space to */
  double capillaryLength;           /*!< Length of the simulated capillary in millimetres */
  bool debyeHuckel;                 /*!< Use Debye-Hückel correction */
  bool onsagerFuoss;                /*!< Use Onsager-Fuoss correction */
  double capillaryDiameter;         /*!< Capillary diameter in micrometres */
  double constantForceValue;        /*!< Value of either voltage or current that shall remain constant.
                                         Voltage - in volts, current - in microamperes */
  ConstantForce constantForce;      /*!< Type of force (either voltage or current) that shall remain constant */
  int firstCell;                    /*!< First cell of the calculation space */
  int lastCell;                     /*!< Last cell of the calculation space */
  bool adaptCalcSpace;              /*!< Adjust first and last cell of the calculation space automatically */
  std::vector<double> detectors;    /*!< Detector positions in millimeters */
};

using IonicFormInfo = std::pair<std::string, double>;

class ProfileSlice {
public:
  std::string name;                 /*!< Name of the profile (pH, conductivity, constituent, ...) */
  ProfileType type;                 /*!< Type of the profile */
  int cell;                         /*!< Sliced cell */
  double x;                         /*!< Position of the slice in meters */
  double value;                     /*!< Data associated with the slice. Actual calue of conductivity, pH or concentration */
  double effectiveMobility;         /*!< Effective mobility of constituent. Valid only for concentration profiles */
  std::vector<IonicFormInfo> concentrations; /*!< Concentrations of indivudual ionic forms. Valid only for
                                                  concentration profiles */
};

class StartParams {
public:
  bool autoDt;                      /*!< Adjust dt automatically */
  double dt;                        /*!< dt in seconds. If autoDt is set, this value is used only for the first step */
  int tolerance;                    /*!< -log10 of error tolerance */
  int updateInterval;               /*!< How often to send current simulation data in milliseconds */
  double constantForceValue;        /*!< Value of either voltage or current that shall remain constant.
                                         Voltage - in volts, current - in microamperes */
  ConstantForce constantForce;      /*!< Type of force (either voltage or current) that shall remain constant */
  double eofValue;                  /*!< Value of either EOF mobility or velocity */
  EOFMode eofMode;                  /*!< Type of eofValue, either mobility or velocity */
  double tStop;                     /*!< Time when to stop the simulation */
  int firstCell;                    /*!< First cell of the calculation space */
  int lastCell;                     /*!< Last cell of the calculation space */
  bool adaptCalcSpace;              /*!< Adjust first and last cell of the calculation space automatically */
};

class SimStateData {
public:
  double dx;                        /*!< Width of a calculation space cell in meters */
  double dt;                        /*!< Last reported dt in seconds */
  double t;                         /*!< Simulated time in seconds */
  double voltage;                   /*!< Voltage in volts */
  double current;                   /*!< Current in amperes */
  int firstCell;                    /*!< First calculation cell */
  int lastCell;                     /*!< Last calculation cell */
  double avgTimePerIter;            /*!< Average wall time per iteration */
  ConstantForce constantForce;      /*!< Type of force (either voltage or current) that remains constant */

  std::vector<double> conductivity;                     /*!< Conductivity in S/m */
  std::vector<double> pH;                               /*!< pH in pH units */
  /* Order of profileNames and concentrations is the same */
  std::vector<std::string> constituentNames;            /*!< Names of concentration profiles */
  std::vector<std::vector<double>> concentrations;      /*!< Concentration profiles */

  const std::vector<std::vector<std::pair<double, std::vector<double>>>> *detectorTraces;
};

class SimState : public SimStateData {
public:
  mutable std::mutex lock;
};

} // namespace calculators

#endif // NMFINTERFACETYPES_H
