#ifndef NMFINTERFACE_P_H
#define NMFINTERFACE_P_H

#include <nmf.h>
#include <calculators/nmfinterfacetypes.h>

#include <memory>
#include <utility>
#include <vector>

namespace calculators {

class NMFContext {
public:
  NMFContext();
  NMFContext(const NMFContext &other) = delete;
  auto operator=(const NMFContext &other) -> NMFContext & = delete;
  ~NMFContext();
  auto prepareDetectors(const std::vector<double> &dets, const double dx, const size_t numCells) -> void;

  NMF::Worker *worker;
  NMF::RetCode lastError;
  NMF::InitializationFailure initError;
  NMF::StartFailure startError;
  std::unique_ptr<SimState> state;

  std::vector<size_t> detectors;
  std::vector<std::vector<std::pair<double, std::vector<double>>>> detectorTraces;
};

} // namespace calculators

#endif // NMFINTERFACE_P_H
