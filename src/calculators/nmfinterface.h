#ifndef NMFINTERFACE_H
#define NMFINTERFACE_H

#include <calculators/nmfinterfacetypes.h>

#include <QObject>

#include <memory>
#include <stdexcept>

namespace gearbox {
  class ConcentrationProfilesModel;
}

namespace gdm {
  class GDM;
}

namespace NMF {
  class Event;
  class Packet;
  class Profiles;
  class Worker;
}

namespace calculators {

class NMFContext;
class Callbacker;

class NMFInterfaceError : public std::runtime_error {
public:
  using std::runtime_error::runtime_error;
};

class NMFInterface : public QObject {
  Q_OBJECT

  friend class Callbacker;
public:
  explicit NMFInterface(QObject *parent = nullptr);
  NMFInterface(NMFInterface &&other) noexcept = delete;
  auto operator=(const NMFInterface &) -> NMFInterface & = delete;
  auto operator=(NMFInterface &&other) noexcept -> NMFInterface & = delete;
  ~NMFInterface();

  auto currentStateData() -> SimStateData;
  auto errorToString() const -> std::string;
  auto initErrorToString() const -> std::string;
  auto initialize(const InitParams &params, const gdm::GDM &gdm, const gearbox::ConcentrationProfilesModel &concs) -> bool;
  auto isRunning() -> bool;
  auto pause() -> bool;
  auto profileSlice(const ProfileType type, const std::string &name, const double x) -> ProfileSlice;
  auto start(const StartParams &params) -> bool;
  auto startErrorToString() const -> std::string;

private:
  auto fillSimState(const NMF::Packet &packet, SimStateData *data) -> void;
  auto handleEventDataUpdated(const NMF::Packet &packet) -> void;
  auto handleEventInitialized(const NMF::Packet &packet) -> void;
  auto handleEventPaused(const NMF::Packet &packet) -> void;
  auto handleEventStarted(const NMF::Packet &packet) -> void;
  auto handleEventTerminated(const NMF::Packet &packet) -> void;
  auto initializeStateArrays(const NMF::Packet &packet, SimStateData *state) -> void;
  auto nmfEvent(NMF::Worker &worker, const NMF::Event &event) -> void;
  auto recordDetectors(SimStateData *state) -> void;
  auto setInitialSimState(
    const double dx,
    const ConstantForce cf,
    const NMF::Profiles *cond,
    const NMF::Profiles *pH,
    const NMF::Profiles *profs,
    const int32_t firstCell,
    const int32_t lastCell,
    const double voltage,
    const double current
  ) -> void;
  auto updateSimState(const NMF::Packet &packet) -> void;

  std::unique_ptr<NMFContext> m_ctx;

signals:
  void simInitialized(const SimState *state);
  void simPaused(const SimState *state);
  void simStarted();
  void simUpdated(const SimState *state);
  void simTerminated(QString reason);
};

} // namespace calculators

#endif // NMFINTERFACE_H
